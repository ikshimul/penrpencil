<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/boishak-1425', 'HomeController@Boishak')->name('Boishak');
//eid collection
Route::get('/eid-collection-2018', 'HomeController@EidCollection2018')->name('EidCollection2018');
Route::get('/eid-collection-2018/pride-girls/{id}', 'HomeController@EidCollection2018Product')->name('EidCollection2018.pridegirls');
Route::get('/eid-collection-201/pride-signature/{id}', 'HomeController@EidCollection2018Product')->name('EidCollection2018.signature');
Route::get('/eid-collection-201/ethnic-menswear/{id}', 'HomeController@EidCollection2018Product')->name('EidCollection2018.ethnic.menswear');
Route::get('/eid-collection-201/pride-kids/{id}', 'HomeController@EidCollection2018Product')->name('EidCollection2018.pride.kids');
Route::get('/eid-collection-201/pride-homes/{id}', 'HomeController@EidCollection2018Product')->name('EidCollection2018.pride.kids');

//boishak
Route::get('/boishakh-1425', 'HomeController@Boishak')->name('Boishak');
Route::get('/boishakh-1425/pride-girls/{id}', 'HomeController@BoishakProduct')->name('BoishakProductList');
Route::get('/boishakh-1425/pride-signature/{id}', 'HomeController@BoishakProduct')->name('BoishakProductList');
Route::get('/boishakh-1425/ethnic-menswear/{id}', 'HomeController@BoishakProduct')->name('BoishakProductList');
Route::get('/boishakh-1425/pride-kids/{id}', 'HomeController@BoishakProduct')->name('BoishakProductList');
Route::get('/woman/limited-edition-sari', 'HomeController@LimitedSari')->name('BoishakProductList');
Route::get('/coming-soon', 'HomeController@ComingSoonPage')->name('comingsoon');
Route::get('/new-in/{id}', 'product\ProductController@NewIn')->name('product.list');
Route::get('/new-arrival', 'product\ProductController@NewArrival')->name('new.arrival');
Route::get('/new-arrival/{id}/{id2}', 'product\ProductController@NewInByCategory')->name('new.arrival.category');

//puja collection 
Route::get('/puja-collection-2018/{id1?}/{id2?}', 'HomeController@PujaCollection2018')->name('PujaCollection2018');

//falgun Collection 2019
Route::get('/falgun-collection-2019/{id1?}/{id2?}', 'HomeController@FlagunCollection2019')->name('FlagunCollection2019');

//indepandance day
Route::get('/independence-day/{id1?}/{id2?}', 'CampaignController@IndependenceDay')->name('IndependenceDay');

//Boishak day
Route::get('/pohela-boishak-1426/{id1?}/{id2?}', 'CampaignController@BoishakProduct')->name('BoishakProduct');

//Eid collection 
Route::get('/eid-collection-19/{id1?}/{id2?}', 'CampaignController@EidCollection2019')->name('eid.collection.2019');

//summer collection 
Route::get('/summer-collection-19/{id1?}/{id2?}', 'CampaignController@SummerCollection')->name('summer.collection.2019');

//capsule collection
Route::get('/capsule-collection-19/{id1?}/{id2?}', 'CampaignController@CapsuleCollection')->name('capsule.collection.2019');

//offer page
Route::get('/offers', 'product\ProductController@OfferProduct')->name('offer.product.list');

//Amar Ekushay Collection
Route::get('/amar-ekushay-collection-2019/{id1?}/{id2?}', 'HomeController@AmarEkushayCollection2019')->name('AmarEkushayCollection2019');

//Promotion Code
Route::get('/promotion-code/{id}', 'ServiceController@PromotionCode')->name('PromotionCode');
Route::get('/promotion-amount-update/{id}/{id2}', 'ServiceController@UpdatePromotionAmount')->name('PromotionCode');

//search 
Route::get('/search/{id}', 'HomeController@search')->name('search');
Route::get('/search-new/{id}', 'HomeController@SearchNew')->name('search.new');
Route::get('/search-view/{id}', 'HomeController@SearchResultView')->name('search.view');

//product list

//category wise
//Route::get('/{name}/{id}', 'product\ProductController@ProductListCategoryWise')->name('product.list.category');

Route::get('/list/woman/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/list/kids/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
// Signature Sari
Route::get('/signature/signature-sari/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/signature/cotton-sari/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/signature/taat-silk-sari/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/signature/half-silk-sari/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/signature/andi-silk-sari/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/signature/muslin-sari/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/signature/blouse/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/signature/kameez/digital_print/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/signature/single-tunic/digital_print/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/signature/unstitched_three_piece/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/signature-sari/dupatta/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/signature/digital-print/{id1}/{id2}', 'product\ProductController@ProductList')->name('digital.print');


Route::get('/product-colors', 'product\ProductController@getProductColors');
//Classic 
Route::get('/classic/classic-sari/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/classic/classic-dupatta/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/classic/unstitched-three-piece/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');

//Pride Girls 
Route::get('/pride-girls/all/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/pride-girls/formal/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/pride-girls/semi-formal/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/pride-girls/casual/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/pride-girls/bottoms/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/pride-girls/dupatta/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/pride-girls/jewelry/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');

//Ethnic men
Route::get('/athenic-men/panjabi/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/athenic-men/long-panjabi/regular-fit/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/athenic-men/long-panjabi/slim-fit/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/athenic-men/short-panjabi/regular-fit/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/athenic-men/short-panjabi/slim-fit/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');

//kids 
Route::get('/kids/girls/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/kids/girls/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/kids/boys/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');

//pride homes 
Route::get('/pride-homes/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');

//Product Filtering by price
Route::get('/product-by-price/{category}/{subcategory}/{lower_price?}/{upper_price?}', 'product\ProductController@ProductByPrice')->name('product.price');
Route::get('/product-by-size/{category}/{subcategory}/{size?}', 'product\ProductController@ProductBySize')->name('product.size');
Route::get('/product-by-color/{category}/{subcategory}/{color?}', 'product\ProductController@ProductByColor')->name('product.color');
Route::get('/product-by-fabric/{category}/{subcategory}/{fabric?}', 'product\ProductController@ProductByFabric')->name('product.fabric');

Route::get('/search-by-price/{text}/{lower_price?}/{upper_price?}', 'product\ProductController@SearchByPrice')->name('search.price');
Route::get('/search-by-size/{texxt}/{size?}', 'product\ProductController@SearchBySize')->name('search.size');
Route::get('/search-by-color/{text}/{color?}', 'product\ProductController@SearchByColor')->name('search.color');

//product details
Route::get('/shop/{id1}/color-{id2}/{id3}', 'product\ProductController@ProductDetails')->name('product.details');
Route::get('/GetTotalForAlert/{id1}', 'product\ProductController@ProductTotalQty')->name('ProductTotalQty');

//get fin in store city list
Route::get('/find-get-city/{id1}', 'product\ProductController@FindInStoreCity')->name('get.city');

//cart
Route::get('/shop-cart', 'cart\CartController@index');
Route::post('/cart/add-to-cart', 'cart\CartController@addProduct');
Route::get('/cart/cart-destroy', 'cart\CartController@cartDestroy');
Route::get('/cart/delete-product/{id1}', 'cart\CartController@cartProductDelete');
Route::post('/cart/update-product', 'cart\CartController@cartProductUpdate');
Route::post('/cart/update-cart', 'cart\CartController@orderpreviewUpdate');

//User Sign up
Route::post('/create/user', 'user\UserController@create');
Route::get('/user-details', 'user\UserController@UserDetails')->name('user-details');
Route::post('/user-details-save', 'user\UserController@SaveUserDetails')->name('UserDetailsSave');
Route::post('/update-shipping-info', 'user\UserController@UpdateShippingInfo')->name('ShippingInfo');
Route::post('/reset-password', 'user\UserController@ResetPassword')->name('ResetPassword');
Route::post('/update-password', 'user\UserController@UpdatePassword')->name('UpdatePassword');
Route::get('/forgot-password/{id}', 'user\UserController@ForgotPassword')->name('ForgetPassword');
Route::post('/create/form', 'user\UserController@showRegisterForm')->name('user.create.form');
Route::get('/create/form-fields', 'user\UserController@showRegisterForm');
Route::get('/create/form', 'user\UserController@showRegisterFormBadRequest');

//mobile login
Route::post('/mobile-login', 'user\UserController@MobileLogin');
Route::get('/verify-mobile', 'user\UserController@MobileVerify');
Route::post('/verify-mobile-check', 'user\UserController@MobileVarifyCheck');

//Social Login
Route::get('auth/{service}', 'Auth\SocialLoginController@redirectToProvider');
Route::get('auth/{service}/callback', 'Auth\SocialLoginController@handleProviderCallback');

//checkout
Route::get('/checkout', 'OrderPreviewController@index')->name('checkout');
Route::get('/orderpreview/delete-product/{id1}', 'cart\CartController@orderpreviewproductDelete');
Route::post('/saveshippinginfo', 'OrderPreviewController@SaveShippingProductInfo')->name('saveshippinginfo');
Route::get('/checkout-v4', 'OrderPreviewv4Controller@index');
Route::post('/saveshippinginfov4', 'OrderPreviewv4Controller@SaveShippingProductInfo');
Route::get('/bkash-transactionId-verification/{id}', 'OrderPreviewController@bKashVerification')->name('bKashVerification');

//guest checkout
Route::get('/guest-checkout', 'GuestCheckoutController@index')->name('guest.checkout');
Route::post('/guest-order', 'GuestCheckoutController@SaveData')->name('guest.order.save');
Route::get('/guest-order-confirmed', 'GuestCheckoutController@OrderConfirmed')->name('guest.order.confirm');
Route::get('/bkash/order-confirm/{id}', 'GuestCheckoutController@BkashOrderConfirmed')->name('bkash.order.confirm');
//promocode
Route::get('/promo-code-check/{code?}', 'PromoCodeController@promoCodeCheck')->name('promo.code.check');

//payment gateway
Route::get('/{ipay}/success', 'OrderPreviewController@success');
Route::get('/{ipay}/failure', 'OrderPreviewController@failure');
Route::get('/{ipay}/cancel', 'OrderPreviewController@cancel');
Route::post('/{ssl}/success', 'OrderPreviewController@success');
Route::post('/{ssl}/failure', 'OrderPreviewController@failure');
Route::post('/{ssl}/cancel', 'OrderPreviewController@cancel');
Route::post('/ssl/ipn', 'OrderPreviewController@ipn');

//bkash
Route::post('/bKash-token', 'OrderPreviewv4Controller@updateToken')->name('bkash.token');
Route::get('/bKash-execute', 'bKashController@ExecutePayment')->name('bkash.execute');
Route::get('/bkash-success', 'OrderPreviewv4Controller@success');
Route::get('/guest-bkash-success', 'GuestCheckoutController@success');



//test ipay
Route::get('/order-confirmed', 'OrderPreviewController@OrderConfirmed');

//order confirm
Route::get('/order', 'OrderPreviewController@iPay')->name('ipay');

//User Account
Route::get('/track-order', 'user\TrackOrderController@UserTrackOrder');
Route::get('/user-orders', 'user\TrackOrderController@UserOrders');
Route::get('/order-preview/{id1}/{id2}', 'user\TrackOrderController@orderPreview')->name('Preview');
Route::get('/my-account', 'user\TrackOrderController@MyAccount')->name('my.account');
Route::get('/my-account-info', 'user\TrackOrderController@MyAccountInfo')->name('account.info');
Route::post('/account-edit', 'user\TrackOrderController@EditAccount')->name('account.edit');
Route::get('/account/change-email', 'user\TrackOrderController@AccountEmailChange');
Route::post('/account/update-email', 'user\TrackOrderController@UpdateAccountEmail');
Route::get('/account/change-password', 'user\TrackOrderController@AccountChangePassword');
Route::post('/account/update-password', 'user\TrackOrderController@UpdatePassword');
Route::get('/set-default-address/{id}', 'user\TrackOrderController@SetDefaultAddress')->name('address.create');
Route::get('/address-delete/{id}', 'user\TrackOrderController@AddressDelete')->name('address.delete');
Route::get('/eidt-additional-address/{id}', 'user\TrackOrderController@EditAdditionalAddress')->name('address.additional.edit');
Route::post('/additional-address-update', 'user\TrackOrderController@UpdateAdditionalAddress')->name('update.additional.address');

//ecourier track order 
Route::get('/track-order-byecr', 'user\TrackOrderController@OrderTrackByECR')->name('OrderTrackByECR');
Route::get('/track-order-status', 'user\TrackOrderController@TrackOrderStatus')->name('TrackOrderStatus');


//Rewards points
Route::get('/reward-points', 'RewardPoints@PointBalance')->name('rewards.points');
Route::get('/get-users-points', 'RewardPoints@GetPointBalance')->name('gets.points');
Route::get('/points-history', 'RewardPoints@PointsHistory')->name('points.history');

//address book
Route::get('/address-book', 'user\TrackOrderController@AddressBook');
Route::get('/address-create', 'user\TrackOrderController@AddressCreate')->name('address.create');
Route::post('/address-save', 'user\TrackOrderController@SaveAddress')->name('SaveAddress');
Route::get('/address-edit/{id}', 'user\TrackOrderController@EditAddress')->name('address.create');
Route::post('/address-update', 'user\TrackOrderController@UpdateAddress')->name('update.address');

//ajax call
Route::get('/getSizeByColor/{id1}/{id2}', 'AjaxCallController@getSizeByColor')->name('GetSize');
Route::get('/ajaxcall-getQuantityByColor/{id1}/{id2}/{id3}', 'AjaxCallController@getQuantityByColor')->name('GetQty');
Route::get('/getImageByColor/{id1}/{id2}', 'AjaxCallController@getImageByColor')->name('GetQty');
Route::get('citylist/{id}', 'OrderPreviewController@GetCityList');
Route::get('/ProductFiltering/{id1}/{id2}', 'AjaxCallController@ProductFiltering')->name('GetSize');
Route::get('/add-number/{id}/{id1}/{id2?}', 'AjaxCallController@AddNumber')->name('AddNumber');
Route::get('/ajaxcall-getBarcode/{id1}/{id2}/{id3}', 'AjaxCallController@GetProductBarcodeBySize')->name('GetBarcode');

//footer page
Route::get('/work-with-us', 'HomeController@WorkWithUs')->name('WorkWithUs');
Route::get('/contact-us', 'HomeController@ContactUs')->name('ContactUs');
Route::get('/exchange-policy', 'HomeController@ExchangePolicy')->name('ExchangePolicy');
Route::get('/faq', 'HomeController@Faq')->name('Faq');
Route::get('/store-locator', 'HomeController@StoreLocator')->name('StoreLocator');
Route::get('/privacy-cookies', 'HomeController@PrivacyCookies')->name('PrivacyCookies');
Route::get('/about-us', 'HomeController@AboutUs')->name('AboutUs');
Route::get('/how-to-order', 'HomeController@HowToOrder')->name('AboutUs');

//loyalty program
Route::get('/loyalty-terms-&-conditions', 'HomeController@LoyaltyTerms')->name('loyalty.terms');
Route::get('/insider-rewards', 'HomeController@InsideRewards')->name('loyalty.insider');
Route::get('/insider-rewards-faq', 'HomeController@InsideRewardsFaq')->name('loyalty.insider.faq');

//Admin Login
Route::get('/admin-login', 'admin\Auth\LoginController@Login')->name('Login');
Route::post('/pride-login-check', 'admin\Auth\LoginController@LoginCheck')->name('LoginCheck');
Route::get('/admin-logout', 'admin\Auth\LoginController@AdminLogout')->name('LoginCheck');

//Admin Order
Route::get('/pride-admin/manage-incomplete-order', 'admin\ManageOrderController@manageIncompleteOrder')->name('manageincompleteorder');
Route::get('/pride-admin/manage-all-order', 'admin\ManageOrderController@OrderFilteringView')->name('OrderFilteringView');
Route::get('/pride-admin/all-order', 'admin\ManageOrderController@manageAllOrder')->name('manageallorder');
Route::get('/pride-admin/order-details/{id}', 'admin\ManageOrderController@OrderDetails')->name('order-details');
Route::get('/order-details/{id}', 'admin\ManageOrderController@OrderDetails')->name('order-details');
Route::post('/order-details/update-address', 'admin\ManageOrderController@updateAddress')->name('update-address');
Route::post('/update-city-in-product-details', 'admin\ManageOrderController@updateCityInProductDetails');

//eCourier Status update
Route::get('/ecourier-cancel-order', 'admin\ManageOrderController@eCourierCancelOrder');

//cashbook
Route::get('/cash-book-histroy', 'admin\CashBookController@CashbookHistrory')->name('cash.book');
Route::get('/cash-out', 'admin\CashBookController@cashOut')->name('cash.out');
Route::post('/cash-out', 'admin\CashBookController@storeCashout')->name('store.cashout');
Route::post('/extra-charge', 'admin\CashBookController@extraCharge')->name('extra.charge');

//Admin reports
Route::get('/pride-admin/sale-reports', 'admin\ReportController@saleReport')->name('pride_admin.sale_reports');

//order status
Route::post('/update_order_status', 'admin\ManageOrderController@UpdateOrderStatus')->name('UpdateOrderStatus');
//Route::get('/update_order_status/{id}/{id2}', 'admin\ManageOrderController@UpdateOrderStatus')->name('UpdateOrderStatus');
Route::get('/update_threepl_status/{id1}/{id2}', 'admin\ManageOrderController@UpdateTPLStatus')->name('UpdateTPLStatus');
Route::get('/cancel_indivisual_product_qty/{id1}/{id2}/{id3}/{id4}/{id5}', 'admin\ManageOrderController@CanceIndivisuallQty')->name('CancelQty');
Route::post('/exchange', 'admin\ManageOrderController@ExchangeUpdateSave')->name('ExchangeUpdateSave');

Route::get('/pride-admin/gebrowserdata', 'AjaxCallController@geBrowserData')->name('GetBrowser');
Route::get('/pride-admin/getmonthlysalesdata/{id1}', 'AjaxCallController@getmonthlysalesdata')->name('getmonthlysalesdata');


Route::get('/admin', 'admin\DashboardController@index')->name('hadmin');
//Invoice Print
Route::get('/invoice-print/{id1}', 'admin\ManageOrderController@InvoicePrint')->name('UpdateTPLStatus');
// admin category
Route::get('/manage-category', 'admin\category\CategoryController@ManageCategory');
Route::post('/save-procat', 'admin\category\CategoryController@AddProCat');
Route::get('/edit-procate/{id1}', 'admin\category\CategoryController@EditProcat');
Route::post('update-procat', 'admin\category\CategoryController@UpdateProcat');
Route::get('/delete-procat/{id}', 'admin\category\CategoryController@deleteProcat');
Route::get('/add-subcat', 'admin\category\CategoryController@AddSubCat');
Route::post('/save-subpro', 'admin\category\CategoryController@SaveSubPro');
Route::get('/manage-subpro', 'admin\category\CategoryController@ManageSubPro');
Route::get('/delete-subcat/{id}', 'admin\category\CategoryController@deleteSubPro');

//admin review
//admin product
Route::get('/add-product', 'admin\product\ProductController@ProductAddForm');
Route::get('/subpro-list/{id}', 'admin\category\CategoryController@GetProWiseSubpro');
Route::get('/size-list/{id}', 'admin\category\CategoryController@GetProWiseSizes');
Route::post('/product-save', 'admin\product\ProductController@ProductSave')->name('ProductForm');
Route::get('/product-manage/{id?}', 'admin\product\ProductController@ViewAllProduct')->name('ViewAllProduct');
Route::get('/edit-product/{id}', 'admin\product\ProductController@EditProduct')->name('ViewAllProduct');
Route::post('/product-edit', 'admin\product\ProductController@UpdateProduct')->name('edit.product');
Route::get('/product-deactive-active/{id1}/{id2}/{id3}/{id4}', 'admin\product\ProductController@ProductActiveDeactive');
//Route::get('/product-delete/{id1}/{id2}', 'admin\product\ProductController@ProductDelete')->name('ViewAllProduct');
Route::get('/product-in-trash/{id1}/{id2}', 'admin\product\ProductController@ProductDeleteTrash')->name('ProductDeleteTrash');
Route::get('/product-delete/{id1}/{id2}', 'admin\product\ProductController@ProductDelete')->name('ProductDelete');
Route::get('/show-deactive-product', 'admin\product\ProductController@ProductInDeactive')->name('ProductInDeactive');
Route::get('/show-trash-product', 'admin\product\ProductController@ProductInTrash')->name('ProductInTrash');
Route::get('/product-restore/{id}/{id2}', 'admin\product\ProductController@ProductRestore')->name('ProductRestore');

//product discount
Route::POST('/set-product-discount', 'admin\DiscountController@UpdateProductDiscount');

//admin product album
Route::get('/product-album-manage/{id}/{id2}', 'admin\product\ProductController@ManageProductAlbum')->name('ManageProductAlbum');
Route::get('/manage-product-gallery/{id}/{id2}', 'admin\product\ProductController@ManageProductGallery')->name('ManageProductAlbum');
Route::get('/edit-album-image/{id}/{id2}/{id3}/{id4}', 'admin\product\ProductController@EditAlbumImage')->name('ManageProductAlbum');
Route::post('/update-album-image', 'admin\product\ProductController@UpdateAlbumImage')->name('ManageProductAlbum');
Route::get('/delete-product-album/{id}', 'admin\product\ProductController@DeleteAlbum')->name('DeleteAlbum');

Route::get('/delete_album_image/{id}/{id2}/{id3}', 'admin\product\ProductController@DeleteAlbumImage')->name('DeleteProductAlbum');
Route::get('/add-album/{id}/{id2}', 'admin\product\AlbumController@AddAlbumForm')->name('AddAlbum');
Route::post('/save-album', 'admin\product\AlbumController@SaveAlbum')->name('SaveAlbum');
Route::get('/add-product-style/{id}', 'admin\product\AlbumController@AddProductStyle')->name('AddProductStyle');
Route::post('/save-product-style', 'admin\product\AlbumController@SaveProductStyle')->name('AddProductStyle');

//add album image
Route::get('/add-album-image/{id2}/{id3}', 'admin\product\ProductController@AddSingleProductImage')->name('AddSingleProductImage');
Route::post('/add-new-album-image', 'admin\product\ProductController@SaveSingleProductImage')->name('SaveSingleProductImage');

//admin related product
Route::get('/add-related-product/{id}', 'admin\product\ProductController@AddRelatedProduct')->name('add.related.product');
Route::get('/get-product-by-subpro/{id1}', 'AjaxCallController@GetProductListByCategory')->name('get.product.by.category');
Route::post('/related-product-save', 'admin\product\ProductController@AddRelatedProductSave')->name('add.related.product.store');

//New swatch edit
Route::get('/edit-swatch/{id}/{id2}/{id3}', 'admin\product\AlbumController@EditSwatch')->name('edit.swatch');
Route::post('/save-edit-swatch', 'admin\product\AlbumController@UpdateSwatch')->name('update.swatch');

//product quantity 
Route::get('/update-quantity-search', 'admin\product\QuantityController@UpdateQuantitySearchForm');
Route::post('/update-quantity', 'admin\product\QuantityController@UpdateQuantitySearch');
Route::post('/update-quantity-save', 'admin\product\QuantityController@UpdateQuantitySave');
Route::get('/update-quantity-save-by-ajax/{id}/{id2}/{id3?}', 'admin\product\QuantityController@UpdateQuantitySaveByAjax');
Route::post('/save-new-size-qty', 'admin\product\QuantityController@SaveNewSizeQty');
Route::get('/delete-product-size/{id}', 'admin\product\QuantityController@DeleteProductSize');

//product filtering
Route::get('/product-filtering', 'admin\DiscountController@ProductFiltering');

//site user manage
Route::get('/site-user', 'admin\DashboardController@SiteUserList')->name('site.user');
Route::get('/site-user/delete-user/{id}', 'admin\DashboardController@DeleteUser')->name('site.user.delete');
Route::get('/edit-site-user/{id}', 'admin\DashboardController@EditSiteUser')->name('site.user.delete');
Route::post('/update-site-user-info', 'admin\DashboardController@UpdateSiteUser')->name('site.user.update');
Route::get('/customer-send-message-form', 'admin\siteuser\SiteuserController@CustomerSendMessageForm')->name('site.user.message');
Route::get('/search-customer', 'admin\siteuser\SiteuserController@CustomerSearch');
Route::post('/customer-send-message', 'admin\siteuser\SiteuserController@CustomerSendMessage')->name('site.user.message');

//role management
Route::get('/add-user', 'admin\user\usercontroller@index');
Route::post('/create-admin-user', 'admin\user\usercontroller@create');
Route::get('/view-user', 'admin\user\usercontroller@view');
Route::get('/user-edit/{id}', 'admin\user\usercontroller@edit');
Route::post('/update-user', 'admin\user\usercontroller@update');
Route::get('/delete-user/{id}/{id2}', 'admin\user\usercontroller@delete');
Route::get('/add-role', 'admin\user\RoleController@index');
Route::post('/create-role', 'admin\user\RoleController@create');
Route::get('/view-role', 'admin\user\RoleController@view');
Route::get('/edit-role/{id}', 'admin\user\RoleController@edit');
Route::post('/role-update/{id}', 'admin\user\RoleController@update');


//Manage home page
//slider
Route::get('/add-slider', 'admin\ContentHandleController@AddSlider')->name('add.slider');
Route::post('/save-slider', 'admin\ContentHandleController@SaveSlider')->name('save.slider');
Route::get('/manage-slider', 'admin\ContentHandleController@ManageSlider')->name('manage.slider');
Route::get('/delete-slider/{id}', 'admin\ContentHandleController@DeleteSlider')->name('delete.slider');
Route::get('/edit-slider/{id}', 'admin\ContentHandleController@EditSlider')->name('edit.slider');
Route::post('/update-slider', 'admin\ContentHandleController@UpdateSlider')->name('update.slider');
Route::get('/active-slider/{id}', 'admin\ContentHandleController@ActiveSlider')->name('active.slider');
Route::get('/deactive-slider/{id}', 'admin\ContentHandleController@DeactiveSlider')->name('deactive.slider');

//banner
Route::get('/add-banner', 'admin\ContentHandleController@AddBanner')->name('add.banner');
Route::post('/save-banner', 'admin\ContentHandleController@SaveBanner')->name('save.banner');
Route::get('/manage-banner', 'admin\ContentHandleController@ManageBanner')->name('manage.banner');
Route::get('/active-banner/{id}', 'admin\ContentHandleController@ActiveBanner')->name('active.banner');
Route::get('/deactive-banner/{id}', 'admin\ContentHandleController@DeactiveBanner')->name('deactive.banner');
Route::get('/edit-banner/{id}', 'admin\ContentHandleController@EditBanner')->name('edit.banner');
Route::post('/update-banner', 'admin\ContentHandleController@UpdateBanner')->name('update.banner');
Route::get('/delete-banner', 'admin\ContentHandleController@DeleteBanner')->name('delete.banner');

//popup upload
Route::get('/add-popup', 'admin\ContentHandleController@AddPopup')->name('add.popup');
Route::post('/save-popup', 'admin\ContentHandleController@SavePopup')->name('save.popup');
Route::get('/manage-popup', 'admin\ContentHandleController@ManagePopup')->name('manage.popup');
Route::get('/edit-popup/{id}', 'admin\ContentHandleController@EditPopup')->name('edit.popup');
Route::post('/update-popup', 'admin\ContentHandleController@UpdatePopup')->name('update.popup');
Route::get('/active-popup/{id}', 'admin\ContentHandleController@ActivePopup')->name('active.popup');
Route::get('/deactive-popup/{id}', 'admin\ContentHandleController@DeactivePopup')->name('deactive.popup');
Route::get('/delete-popup/{id}', 'admin\ContentHandleController@DeletePopup')->name('delete.popup');


//report
Route::post('/search-order', 'admin\report\ReportController@SearchOrder')->name('Search.Order');
Route::post('/pdf-report', 'admin\report\ReportController@PdfReport')->name('PdfReport');
Route::post('/pdf-sales-report', 'admin\report\ReportController@SalesPdfReport')->name('SalesPdfReport');
Route::get('/order-report', 'admin\report\ReportController@TodayReport')->name('TodayReport');

//phone request
Route::get('/phone-request', 'admin\siteuser\SiteuserController@PhoneRequest')->name('PhoneRequest');

//pos online
Route::get('/get-item-defination', 'admin\ItemDefination@ShowPgItem')->name('ShowPgItem');

//item defination 
Route::get('/get-item-list', 'admin\ItemDefineController@ShowPosItemList')->name('itemlist');
Route::get('/save-item-list', 'admin\ItemDefineController@AddPosItemList')->name('AddPosItemList');
Route::get('/add-product-pos', 'admin\ItemDefineController@AddProductByPos')->name('AddProductByPos');
Route::post('/product-save-by-pos', 'admin\ItemDefineController@ProductSave')->name('ProductSave');

//create QR Code
Route::get('/invoice/{id}', 'admin\ManageOrderController@Invoice');

//Find in store
Route::get('/showroom-city-list', 'FindStoreController@ShowroomCityList')->name('ShowroomStock');
Route::get('/store-list/{district}/{barcode}', 'FindStoreController@ShowroomStock')->name('store.list');
Route::get('/showroom-wise-stock-by-designref/{id}/{id1}', 'FindStoreController@ShowroomStockByDesignRef')->name('ShowroomStockByDesignRef');

//coupon generate
Route::get('/coupon-generate', 'CuponController@GenerateCoupon')->name('GenerateCoupon');
Route::post('/save-promcodes', 'CuponController@StorePromo')->name('StorePromo');
Route::GET('/manage-promcodes', 'CuponController@ManageCoupon')->name('ManageCoupon');
