<?php

use App\Http\Controllers\HomeController;
?>
<!DOCTYPE html>
<html lang="zxx">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Pen R Pencil</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="<?php echo nl2br(e(csrf_token())); ?>">
        <link rel="apple-touch-icon" href="<?php echo nl2br(e(asset('assets/images/favicon.png'))); ?>">
        <link rel="shortcut icon" href="<?php echo nl2br(e(asset('assets/images/favicon.png'))); ?>">

        <!-- Imported Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700,700i&display=swap" rel="stylesheet">

        <!-- CSS FILES HERE -->
        <!-- inject:css -->
        <link rel="stylesheet" href="<?php echo nl2br(e(asset('assets/css/vendors/plugins.min.css'))); ?>">
        <link rel="stylesheet" href="<?php echo nl2br(e(asset('assets/css/style.css'))); ?>">
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
        <script src="<?php echo nl2br(e(asset('assets/js/jquery.js'))); ?>"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        <!-- endinject -->
    </head>

    <body>

        <!-- Site Wrapper -->
        <div id="wrapper" class="wrapper">

            <!-- Site Navigation -->
            <header class="mi-header">
                <div class="mi-container">
                    <div class="mi-header-inner">
                        <button class="mi-header-catmenu">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a href="<?php echo nl2br(e(url('/'))); ?>" class="mi-header-logo">
                            <img src="<?php echo nl2br(e(asset('assets/images/site-logo.png'))); ?>" alt="logo">
                        </a>
                        <form action="#" class="mi-header-searchform">
                            <input type="text" placeholder="What are you looking for?">
                            <button type=submit><i class="fa fa-search"></i></button>
                        </form>
                        <div class="mi-header-options">
                            <a href="help.html" class="mi-button mi-button-transparent mi-button-small"><i
                                    class="fa fa-question-circle"></i> Help</a>
						 <?php if(auth()->guard()->guest()): ?>
                            <a href="#login-register-popup" data-toggle="modal"
                               class="mi-button mi-button-transparent mi-button-small">
							   <i class="fa fa-user"></i>
                                Login
							</a>
						 <?php else: ?>
						 <a href="#login-register-popup" data-toggle="modal"
						   class="mi-button mi-button-transparent mi-button-small">
						    <?php echo nl2br(e(Auth::user()->name)); ?>

						</a>
						<?php endif; ?>
                        </div>
                    </div>
                </div>
            </header>
            <!--// Site Navigation -->

            <!-- Page Content -->
            <main class="mi-pagecontent">

                <!-- Category Menu -->
                <div class="mi-categorymenu">
                    <div class="mi-categorymenu-inner">
                        <ul class="mi-categorymenu-pages">
                            <li><a href="offers.html"><b>Offers</b></a></li>
                            <li><a href="#"><b>Discounts</b></a></li>
                            <li><a href="#product-request-trigger" data-toggle="modal">Product Request</a></li>
                        </ul>
                        <ul class="mi-categorymenu-inside">            
                            <?php
                            $menu = HomeController::Menu();
                            foreach ($menu as $menuItem) {
                                $lowname = strtolower($menuItem->procat_name);
                                $cate_url = str_replace(' ', '-', $lowname);
                                if (strpos($lowname, '/')) {
                                    $cate_url = str_replace('/', '-', $lowname);
                                } else {
                                    $cate_url = str_replace(' ', '-', $lowname);
                                }
                                ?>
                                <li><a href="<?php echo nl2br(e(url("category/{$cate_url}/{$menuItem->procat_id}"))); ?>"><i class="fa fa-book"></i> <?php echo nl2br(e($menuItem->procat_name)); ?></a>
                                    <ul>
                                        <?php
                                        $submenu = HomeController::SubMenu($menuItem->procat_id);
                                        foreach ($submenu as $sub) {
                                            ?>
                                            <li><a href="#"><?php echo nl2br(e($sub->subprocat_name)); ?></a></li>
                                        <?php } ?>
                                    </ul>
                                </li>
                            <?php } ?>
                        </ul>

                        <div class="mi-sidebar">
                            <h5 class="mi-sidebar-title">Filter Options</h5>

                            <!-- Single Widget -->
                            <div class="single-widget widget-brandfilter">
                                <h6 class="widget-title">By Brand</h6>
                                <div class="widget-brandfilter-inner">
                                    <ul>
                                        <li data-brand="apple">
                                            <button>Apple</button>
                                        </li>
                                        <li data-brand="asus">
                                            <button>Asus</button>
                                        </li>
                                        <li data-brand="dell">
                                            <button>Dell</button>
                                        </li>
                                        <li data-brand="hp">
                                            <button>Hp</button>
                                        </li>
                                        <li data-brand="lenevo">
                                            <button>Lenevo</button>
                                        </li>
                                        <li data-brand="sony">
                                            <button>Sony</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!--// Single Widget -->

                        </div>
                    </div>
                </div>
                <!-- Shopping Cart -->
                <button class="mi-shopcart-trigger">
                    <i class="fa fa-shopping-bag"></i>
                    <span class="mi-shopcart-items">
                        <?php
                        $i = 0;
                        foreach (Cart::instance('products')->content() as $row) : $i++;
                            ?>
                        <?php endforeach;
                        ?><?php echo nl2br(e($i)); ?>

                        Items</span>
                    <span class="mi-shopcart-amount">৳ <?php echo Cart::subtotal(); ?></span>
                </button>
                <div class="mi-shopcart">
                    <div class="mi-shopcart-inner">
                        <div class="mi-shopcart-header">
                            <h6><i class="fa fa-shopping-bag"></i> Cartbox</h6>
                            <button class="mi-shopcart-close">
                                &times;
                            </button>
                        </div>
                        <div class="mi-shopcart-products">
                            <?php
                            $i = 0;
                            foreach (Cart::instance('products')->content() as $row) :
                                $i++;
                                $name = $row->name;
                                $pro_name = str_replace(' ', '-', $name);
                                $id = $row->id;
                                $color = $row->options->color;
                                ?>
                                <div class="mi-shopcart-product">
                                    <div class="mi-shopcart-quantity">
                                        <button class="inc">+</button>
                                        <span class="count"><?php echo $row->qty; ?></span>
                                        <button class="dec">-</button>
                                    </div>
                                    <div class="mi-shopcart-product-image">
                                        <img src="<?php echo nl2br(e(URL::to(''))); ?>/storage/app/public/pgallery/<?php echo ($row->options->has('product_image') ? $row->options->product_image : ''); ?>" alt="product image">
                                    </div>
                                    <div class="mi-shopcart-product-content">
                                        <h5 class="mi-shopcart-product-title"><a href="#"><?php echo $row->name; ?></a></h5>
                                        <span class="mi-shopcart-product-price">৳ <?php echo $row->price; ?> * <?php echo $row->qty; ?> = ৳ <?php echo $row->price * $row->qty; ?></span>
                                    </div>
                                    <button class="mi-shopcart-product-close">
                                        &times;
                                    </button>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="mi-shopcart-footer">
                            <div class="mi-shopcart-coupon">
                                <button class="mi-shopcart-coupontrigger">Have a special coupon? Click Here</button>
                                <form action="#">
                                    <input type="text" placeholder="Special code...">
                                    <button type="submit">Go</button>
                                </form>
                            </div>
                            <button class="mi-shopcart-orderbutton">
                                Place Order :
                                <span>৳ <?php echo Cart::subtotal(); ?></span>
                            </button>
                        </div>
                    </div>
                </div>
                <!--// Shopping Cart -->

                <!-- Category Slider -->

                <!-- main content --->
                <?php echo $__env->yieldContent('content'); ?>
                <!-- end main content ---->

            </main>
            <!--// Page Content -->

            <!-- Footer -->
            <footer class="mi-footer">
                <div class="mi-footer-toparea">
                    <div class="mi-container">
                        <div class="row mi-footer-widgets">

                            <!-- Footer Widget -->
                            <div class="col-lg-3 mi-footer-widget mi-widget-quicklinks">
                                <h6 class="mi-widget-title">Know Us</h6>
                                <ul>
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Insider Blog</a></li>
                                    <li><a href="#">Corporate Responsibility</a></li>
                                    <li><a href="#">Terms & Conditions</a></li>
                                    <li><a href="contact.html">Contact Us</a></li>
                                </ul>
                            </div>
                            <!--// Footer Widget -->

                            <!-- Footer Widget -->
                            <div class="col-lg-3 mi-footer-widget mi-widget-quicklinks">
                                <h6 class="mi-widget-title">Help & Support</h6>
                                <ul>
                                    <li><a href="#">Your Account</a></li>
                                    <li><a href="#">Track Order</a></li>
                                    <li><a href="#">Order & Payment</a></li>
                                    <li><a href="#">Shipping & Returns</a></li>
                                    <li><a href="faq.html">FAQ</a></li>
                                </ul>
                            </div>
                            <!--// Footer Widget -->

                            <!-- Footer Widget -->
                            <div class="col-lg-3 mi-footer-widget mi-widget-quicklinks">
                                <h6 class="mi-widget-title">Policy</h6>
                                <ul>
                                    <li><a href="#">Return & Cancellation</a></li>
                                    <li><a href="#">Shipping Policy</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Sitemap & Security</a></li>
                                    <li><a href="#">Copyright</a></li>
                                </ul>
                            </div>
                            <!--// Footer Widget -->

                            <!-- Footer Widget -->
                            <div class="col-lg-3 mi-footer-widget mi-widget-info">
                                <a href="#" class="mi-widget-info-logo">
                                    <img src="assets/images/site-logo.png" alt="penrpencil">
                                </a>
                                <ul class="mi-widget-info-socialicons">
                                    <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li class="instagram"><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li class="youtube"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                                </ul>
                                <form action="#">
                                    <input type="email" name="subscribe-email" id="subscribe-email"
                                           placeholder="Subscribe Now" required>
                                    <button type="submit"><i class="fa fa-paper-plane-o"></i></button>
                                </form>
                            </div>
                            <!--// Footer Widget -->

                        </div>
                    </div>
                </div>
                <div class="mi-footer-bottomarea">
                    <div class="mi-container">
                        <p class="mi-footer-copyright">©2019 <img src="assets/images/text-logo.png" alt="image"> All rights
                            reserved | Powered By <b><span style="color: #159E00">Store</span> <span
                                    style="color: #1363C6">Republic</span></b></p>
                    </div>
                </div>
            </footer>
            <!--// Footer -->

            <!-- Login & Register Popup -->
            <div class="modal fade mi-loginregister-popup" id="login-register-popup" tabindex="-1" role="dialog"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Login/Register</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="mi-loginregister">
                                <button class="mi-loginregister-facebook">
                                    <i class="fa fa-facebook"></i>
                                    Signup or Login with <b>Facebook</b>
                                </button>
                                <ul class="mi-loginregister-button">
                                    <li data-field="field-phone">
                                        <i class="fa fa-phone"></i>
                                        Login with <b>Phone Number</b>
                                    </li>
                                    <li data-field="field-email">
                                        <i class="fa fa-envelope"></i>
                                        Login with <b>Email</b>
                                    </li>
                                </ul>
                                <div class="mi-loginregister-border"><span>Or</span></div>
                                <div class="mi-loginregister-content">
                                    <div class="mi-loginregister-phone" data-visible="field-phone">
                                        <form action="<?php echo nl2br(e(url('/mobile-login'))); ?>" method="post" class="mi-form">
										 <?php echo nl2br(e(csrf_field())); ?>

                                            <div class="form-field">
                                                <label for="login-phone-number">Enter your mobile number:</label>
                                            </div>
                                            <div class="form-field">
                                                <input id="login-phone-number" name="mobile_number" type="text" value="+880">
                                                <span class="animated-border"></span>
                                            </div>
                                            <div class="form-field">
                                                <button class="mi-button" type="submit">Signup / Login</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="mi-loginregister-email" data-visible="field-email">
                                        <form action="#" class="mi-form">
                                            <div class="form-field">
                                                <input type="email" placeholder="Email address...">
                                                <span class="animated-border"></span>
                                            </div>
                                            <div class="form-field">
                                                <input type="password" placeholder="Password...">
                                                <span class="animated-border"></span>
                                            </div>
                                            <div class="form-field">
                                                <button class="mi-button" type="submit">Login</button>
                                            </div>
                                        </form>
                                        <a class="forgot-pass" href="#">Forgot Password?</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--// Login & Register Popup -->

            <!-- Login & Register Popup -->
            <div class="modal fade mi-productrequest-popup" id="product-request-trigger" tabindex="-1" role="dialog"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Need other product?</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="mi-productrequest">
                                <p>Can not find a product you want in our inventory? Let us know what you are looking for
                                    and we will try our best to bring it in.</p>
                                <form action="#" class="mi-form">
                                    <div class="form-field">
                                        <label for="product-request-description">Description</label>
                                        <textarea id="product-request-description" cols="30" rows="5"></textarea>
                                        <span class="animated-border"></span>
                                    </div>
                                    <div class="form-field">
                                        <label for="product-request-number">Enter your phone number</label>
                                        <input type="text" id="product-request-number">
                                        <span class="animated-border"></span>
                                    </div>
                                    <div class="form-field">
                                        <button type="submit" class="mi-button">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--// Login & Register Popup -->

            <!-- Product Quickview -->
            <div class="modal fade mi-productview-popup" id="product-quickview-popup" tabindex="-1" role="dialog"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div class="modal-body">
                            <!-- Product Details -->
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="mi-prodetails-viewbox">
                                        <div id="img-1" class="mi-prodetails-largeimage">
                                            <img id="zoom1" src="assets/images/products/product-image-1.png" alt="big-1">
                                        </div>
                                        <div class="mi-prodetails-thumbs">
                                            <ul class="mi-prodetails-thumbslist" id="gallery_01">
                                                <li>
                                                    <a href="#" class="elevatezoom-gallery"
                                                       data-image="assets/images/products/product-image-1.png"
                                                       data-zoom-image="assets/images/products/product-image-lg-1.png">
                                                        <img src="assets/images/products/product-image-1.png"
                                                             alt="zo-th-1" />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="elevatezoom-gallery"
                                                       data-image="assets/images/products/product-image-2.png"
                                                       data-zoom-image="assets/images/products/product-image-lg-2.png">
                                                        <img src="assets/images/products/product-image-2.png"
                                                             alt="zo-th-1" />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="elevatezoom-gallery"
                                                       data-image="assets/images/products/product-image-3.png"
                                                       data-zoom-image="assets/images/products/product-image-lg-3.png">
                                                        <img src="assets/images/products/product-image-3.png"
                                                             alt="zo-th-1" />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="elevatezoom-gallery"
                                                       data-image="assets/images/products/product-image-1.png"
                                                       data-zoom-image="assets/images/products/product-image-lg-1.png">
                                                        <img src="assets/images/products/product-image-1.png"
                                                             alt="zo-th-1" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="mi-prodetails-content">
                                        <h4 class="product-title">Double A A4 1 rim</h4>
                                        <div class="product-price">
                                            <h5>Price:</h5> <ins>৳245</ins><del>৳ 275</del>
                                        </div>
                                        <div class="product-discount">
                                            <h5>Offer:</h5><span>25% Off</span>
                                        </div>
                                        <div class="product-quantity">
                                            <h5>Quantity: </h5>
                                            <div class="quantitybox">
                                                <button class="dec">-</button>
                                                <input type="number" value="1">
                                                <button class="inc">+</button>
                                            </div>
                                            <a href="#" class="mi-button">Add to Cart</a>
                                        </div>
                                        <div class="product-types">
                                            <h5>Types:</h5>
                                            <ul>
                                                <li><a href="#">Pen</a></li>
                                                <li><a href="#">Pencil</a></li>
                                                <li><a href="#">Paper</a></li>
                                            </ul>
                                        </div>
                                        <div class="product-colors">
                                            <h5>Colors:</h5>
                                            <ul>
                                                <li class="orange active"><a href="#"><span></span></a></li>
                                                <li class="green"><a href="#"><span></span></a></li>
                                                <li class="violet"><a href="#"><span></span></a></li>
                                                <li class="grey"><a href="#"><span></span></a></li>
                                            </ul>
                                        </div>
                                        <div class="product-sizes">
                                            <h5>Sizes:</h5>
                                            <ul>
                                                <li class="active"><a href="#"><span></span>S</a></li>
                                                <li><a href="#"><span></span>M</a></li>
                                                <li><a href="#"><span></span>L</a></li>
                                                <li><a href="#"><span></span>XL</a></li>
                                                <li><a href="#"><span></span>XXL</a></li>
                                            </ul>
                                        </div>
                                        <div class="product-share">
                                            <h5>Share On: </h5>
                                            <ul>
                                                <li>
                                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--// Product Details -->
                        </div>
                    </div>
                </div>
            </div>
            <!--// Product Quickview -->

            <button id="back-top-top"><i class="fa fa-chevron-up"></i></button>

        </div>
        <!--// Site Wrapper -->

        <!-- JS FILES HERE -->
        <!-- inject:js -->

        <script src="<?php echo nl2br(e(asset('assets/js/vendors/plugins.min.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets/js/main.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('public/js/app.js'))); ?>"></script>
		<script>
            $('.mi-categorymenu-inside li a').on('click', function (e) {
                e.preventDefault();
                var url = $(this).attr('href');
                $.ajax({
                    type: "GET",
                    url: url
                }).done(function (response) {
                    $('.mi-products-area').empty();
                    $(response).find('.mi-category-block').appendTo('.mi-products-area');
                });
        
                $(this).parent('li').siblings('li').find('ul').stop().slideUp();
                $(this).siblings('ul').stop().slideDown();
            });
        </script>
		<?php echo $__env->yieldContent('appended.script'); ?>
		<?php echo app('toastr')->render(); ?>
        <script>
            import VueProgressBar from 'vue-progressbar'
                Vue.use(VueProgressBar, {
                  color: 'rgb(143, 255, 199)',
                  failedColor: 'red',
                  height: '2px'
                })
        </script>
        <!-- endinject -->
    </body>

</html>