<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="<?php echo nl2br(e(csrf_token())); ?>">
        <title><?php echo $__env->yieldContent('title'); ?></title>
        <link rel="icon" type="image/png" href="<?php echo nl2br(e(asset('assets_admin/images/logo.png'))); ?>">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?php echo nl2br(e(asset('assets_admin/bootstrap/css/bootstrap.min.css'))); ?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

        <link rel="stylesheet" href="<?php echo nl2br(e(asset('assets_admin/plugins/iCheck/all.css'))); ?>">
        <!-- Bootstrap Color Picker -->
        <link rel="stylesheet" href="<?php echo nl2br(e(asset('assets_admin/plugins/colorpicker/bootstrap-colorpicker.min.css'))); ?>">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="<?php echo nl2br(e(asset('assets_admin/plugins/timepicker/bootstrap-timepicker.min.css'))); ?>">
        <!-- Select2 -->
        <link rel="stylesheet" href="<?php echo nl2br(e(asset('assets_admin/plugins/select2/select2.min.css'))); ?>">
        <link rel="stylesheet" href="<?php echo nl2br(e(asset('assets_admin/plugins/datatables/dataTables.bootstrap.css'))); ?>">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo nl2br(e(asset('assets_admin/dist/css/fileinput.min.css'))); ?>">
        <link rel="stylesheet" href="<?php echo nl2br(e(asset('assets_admin/dist/css/AdminLTE.min.css'))); ?>">
        <link rel="stylesheet" href="<?php echo nl2br(e(asset('assets_admin/dist/css/fileinput.min.css'))); ?>">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo nl2br(e(asset('assets_admin/dist/css/skins/_all-skins.min.css'))); ?>">
        <link rel="stylesheet" href="<?php echo nl2br(e(asset('assets_admin/plugins/morris/morris.css'))); ?>">
        <link rel="stylesheet" href="<?php echo nl2br(e(asset('assets_admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css'))); ?>">
        <!-- daterange picker -->
        <link rel="stylesheet" href="<?php echo nl2br(e(asset('assets_admin/plugins/daterangepicker/daterangepicker.css'))); ?>">
        <!-- bootstrap datepicker -->
        <link rel="stylesheet" href="<?php echo nl2br(e(asset('assets_admin/plugins/datepicker/datepicker3.css'))); ?>">
        <link rel="stylesheet" href="<?php echo nl2br(e(asset('assets_admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'))); ?>">

        <script src="<?php echo nl2br(e(asset('assets_admin/highcharts.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/highcharts-more.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/exporting.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/plugins/jQuery/jquery-3.1.1.min.js'))); ?>"></script>
        
       <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script> -->

        <!-- iCheck for checkboxes and radio inputs -->
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <script>
var base_url = "<?php echo nl2br(e(URL::to(''))); ?>";
var csrf_token = "<?php echo nl2br(e(csrf_token())); ?>";
        </script>
        <style>
            .bv-form .help-block {
                margin-bottom: 0;
            }
            .bv-form .tooltip-inner {
                text-align: left;
            }
            .nav-tabs li.bv-tab-success > a {
                color: #3c763d;
            }
            .nav-tabs li.bv-tab-error > a {
                color: #a94442;
            }

            .bv-form .bv-icon-no-label {
                top: 0;
            }

            .bv-form .bv-icon-input-group {
                top: 0;
                z-index: 100;
            }
            .error{
                color: red;
                padding: 5px;
            }
            .loading-modal {
            display:    none;
            position:   fixed;
            z-index:    1000;
            top:        0;
            left:       0;
            height:     100%;
            width:      100%;
            background: rgba( 255, 255, 255, .8 ) 
                        url("<?php echo nl2br(e(url('/'))); ?>/storage/app/public/loading.gif") 
                        50% 50% 
                        no-repeat;
        }
        </style>
        <script>
            (function(w,d,s,g,js,fs){
              g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
              js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
              js.src='https://apis.google.com/js/platform.js';
              fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
            }(window,document,'script'));
       </script>
    </head>
    <?php
      use App\Http\Controllers\admin\ManageOrderController;
      $total_incomplete_order = ManageOrderController::getTotlaIncomplte_order();
      $total_phone_request = ManageOrderController::getNewPhoneRequest();
      $total_phone_request_list = ManageOrderController::getNewPhoneRequestList();
    ?>
    <body ng-app="crudApp" ng-controller="crudController" class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="<?php echo nl2br(e(url('/admin'))); ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>PEN</b>PENCIL</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>PEN R</b> PENCIL</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Messages: style can be found in dropdown.less-->
                            <!--<li>
                                <a href="#" id="datasync"> Data Sync <i class="fa fa-refresh" aria-hidden="true"></i></a>
                            </li> -->
                            <li class="dropdown messages-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-phone"></i>
                                    <span class="label label-success"><?php echo $total_phone_request;?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have <?php echo $total_phone_request;?> phone request(s)</li>
                                    <li>
                                        <ul class="menu">
                                            <?php foreach($total_phone_request_list as $p_list) { ?>
                                            <li>
                                                <a href="#">
                                                <div class="pull-left">
                                                    <?php echo $p_list->phnumber_name;?>
                                                </div>
                                                <p><?php echo $p_list->phnumber_number;?></p>
                                                <p><?php echo $p_list->comment_box;?></p>
                                              </a>
                                            </li>
                                          <?php } ?>
                                        </ul>
                                    </li>
                                    <li class="footer">
                                        <a href="<?php echo nl2br(e(url('/phone-request'))); ?>">See All Phone request(s)</a>
                                    </li>
                                </ul>
                            </li>
                            <!-- Notifications: style can be found in dropdown.less -->
                            <li class="dropdown notifications-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-cart-plus"></i>
                                    <span class="label label-warning"><?php echo nl2br(e($total_incomplete_order)); ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have <?php echo nl2br(e($total_incomplete_order)); ?> new order</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <li>
                                                <a href="#">
                                                  
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="<?php echo nl2br(e(url('/pride-admin/manage-incomplete-order'))); ?>"> Go to incomplete order page</a></li>
                                </ul>
                            </li>
                            <!-- Tasks: style can be found in dropdown.less -->
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo nl2br(e(url('/'))); ?>/storage/app/public/img/site-logo.png" class="user-image" alt="User Image">
                                    <span class="hidden-xs">Admin</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?php echo nl2br(e(url('/'))); ?>/storage/app/public/img/site-logo.png" class="img-circle" alt="User Image">
                                        <p>
                                            Admin
                                            <small>since Nov. 2018</small>
                                        </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?php echo nl2br(e(url('/admin-logout'))); ?>" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button 
                            <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>-->
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo nl2br(e(url('/'))); ?>/storage/app/public/img/site-logo.png" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p>Admin</p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header"></li>
                        <li class="<?php echo nl2br(e(Request::is('admin') ? 'active' : '')); ?> treeview">
						  <a href="#">
							<i class="fa fa-dashboard"></i> <span>Dashboard</span>
							<span class="pull-right-container">
							  <i class="fa fa-angle-left pull-right"></i>
							</span>
						  </a>
						  <ul class="treeview-menu">
							<li class="<?php echo nl2br(e(Request::is('admin') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/admin'))); ?>"><i class="fa fa-circle-o"></i> Dashboard</a></li>
						  </ul>
						</li>
                        <li class="<?php echo nl2br(e(Request::is('manage-category') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('add-subcat') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('manage-subpro') ? 'active' : '')); ?> treeview">
                            <a href="#">
                                <i class="fa fa-list" aria-hidden="true"></i> <span>Category</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php echo nl2br(e(Request::is('manage-category') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/manage-category'))); ?>"><i class="fa fa-circle-o"></i> Manage Category</a></li>
                                <li class="<?php echo nl2br(e(Request::is('add-subcat') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/add-subcat'))); ?>"><i class="fa fa-circle-o"></i> Add New Sub Category</a></li>
                                <li class="<?php echo nl2br(e(Request::is('manage-subpro') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/manage-subpro'))); ?>"><i class="fa fa-circle-o"></i> Browse All Sub Category</a></li>
                            </ul>
                        </li>
                        <!--<li class="treeview">
                            <a href="#">
                                <i class="fa fa-arrows-alt" aria-hidden="true"></i> <span>Manage Size</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="#"><i class="fa fa-circle-o"></i> Add New Product Size</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Browse Product Sizes</a></li>
                            </ul>
                        </li> --->
                        <li class="<?php echo nl2br(e(Request::is('add-product') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('product-manage') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('show-deactive-product') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('update-quantity-search') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('show-trash-product') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('add-product-pos') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('get-item-list') ? 'active' : '')); ?> treeview">
                            <a href="#">
                                <i class="fa fa-product-hunt" aria-hidden="true"></i>
                                <span>Product</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php echo nl2br(e(Request::is('add-product') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('add-product'))); ?>"><i class="fa fa-circle-o"></i> Add Product</a></li>
                                <li class="<?php echo nl2br(e(Request::is('product-manage') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/product-manage'))); ?>"><i class="fa fa-circle-o"></i> View All Product</a></li>
                                <!--<li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i> Manage Check This Out
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li><a href="#"><i class="fa fa-circle-o"></i> Add Product To List</a></li>
                                        <li><a href="#"><i class="fa fa-circle-o"></i> Switch To Auto/Manual Mode</a></li>
                                    </ul>
                                </li> -->
                                <li class="<?php echo nl2br(e(Request::is('update-quantity-search') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/update-quantity-search'))); ?>"><i class="fa fa-circle-o"></i> Update Quntity</a></li>
                                <li class="<?php echo nl2br(e(Request::is('show-deactive-product') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/show-deactive-product'))); ?>"><i class="fa fa-circle-o"></i> Deactive Products</a></li>
                                <li class="<?php echo nl2br(e(Request::is('show-trash-product') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('show-trash-product'))); ?>"><i class="fa fa-circle-o"></i>Recycle bin</a></li>
                            </ul>
                        </li>
                        <li class="<?php echo nl2br(e(Request::is('pride-admin/manage-incomplete-order') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('pride-admin/manage-all-order') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('order-report') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('search-order') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('pride-admin/all-order') ? 'active' : '')); ?> treeview">
                            <a href="#">
                                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                <span>Manage Order</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                    <small class="label pull-right bg-red">
                                       <?php echo nl2br(e($total_incomplete_order)); ?>

                                    </small>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php echo nl2br(e(Request::is('pride-admin/manage-incomplete-order') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/pride-admin/manage-incomplete-order'))); ?>"><i class="fa fa-circle-o"></i> Incomplete Order 	
                                        <span class="pull-right-container">
                                            <small class="label pull-right bg-blue"><?php echo nl2br(e($total_incomplete_order)); ?></small>
                                        </span></a></li>
                                <li class="<?php echo nl2br(e(Request::is('pride-admin/all-order') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/pride-admin/all-order'))); ?>"><i class="fa fa-circle-o"></i> All Order</a></li>
                                <li class="<?php echo nl2br(e(Request::is('pride-admin/manage-all-order') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/pride-admin/manage-all-order'))); ?>"><i class="fa fa-circle-o"></i>Order Filtering</a></li>
                                <li class="<?php echo nl2br(e(Request::is('order-report') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/order-report'))); ?>"><i class="fa fa-circle-o"></i> Today Report</a></li>
                            </ul>
                        </li>
                        <!--<li class="<?php echo nl2br(e(Request::is('cash-book-histroy') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('cash-out') ? 'active' : '')); ?> treeview">
                            <a href="#">
                                <i class="fa fa-money" aria-hidden="true"></i>
                                <span>CashBook</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                   
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php echo nl2br(e(Request::is('cash-book-histroy') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/cash-book-histroy'))); ?>"><i class="fa fa-circle-o"></i>Cashbook 	
                                        <span class="pull-right-container">
                                            <small class="label pull-right bg-blue"></small>
                                        </span></a></li>
                                    <li class="<?php echo nl2br(e(Request::is('cash-out') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(route('cash.out'))); ?>"><i class="fa fa-circle-o"></i>Cash Transfer</a></li>
                            </ul>
                        </li> --->
                        <!--    <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-database" aria-hidden="true"></i> <span> Store/Outlet</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li><a href=""><i class="fa fa-circle-o"></i> Add New Store/Outlet</a></li>
                                    <li><a href=""><i class="fa fa-circle-o"></i> Browse All Store/Outlet</a></li>
                                </ul>
                            </li> --->
                            <li class="<?php echo nl2br(e(Request::is('add-slider') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('manage-slider') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('add-banner') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('manage-banner') ? 'active' : '')); ?> treeview">
                                <a href="#">
                                    <i class="fa fa-home" aria-hidden="true"></i> <span>Manage Home Page</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="<?php echo nl2br(e(Request::is('add-slider') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('manage-slider') ? 'active' : '')); ?> treeview">
                                        <a href="#"><i class="fa fa-circle-o"></i> Slider
                                            <span class="pull-right-container">
                                                <i class="fa fa-angle-left pull-right"></i>
                                            </span>
                                        </a>
                                        <ul class="treeview-menu">
                                            <li class="<?php echo nl2br(e(Request::is('add-slider') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/add-slider'))); ?>"><i class="fa fa-circle-o"></i> Add Slider</a></li>
                                            <li class="<?php echo nl2br(e(Request::is('manage-slider') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/manage-slider'))); ?>"><i class="fa fa-circle-o"></i> Manage Slider</a></li>
                                        </ul>
                                    </li>
                                    <li class="<?php echo nl2br(e(Request::is('add-banner') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('manage-banner') ? 'active' : '')); ?> treeview">
                                        <a href="#"><i class="fa fa-circle-o"></i> Banner
                                            <span class="pull-right-container">
                                                <i class="fa fa-angle-left pull-right"></i>
                                            </span>
                                        </a>
                                        <ul class="treeview-menu">
                                            <li class="<?php echo nl2br(e(Request::is('add-banner') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/add-banner'))); ?>"><i class="fa fa-circle-o"></i> Add Banner</a></li>
                                            <li class="<?php echo nl2br(e(Request::is('manage-banner') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/manage-banner'))); ?>"><i class="fa fa-circle-o"></i> Manage Banner</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <!--- <li>
                                <a href="">
                                    <i class="fa fa-link" aria-hidden="true"></i> <span>Manage All Inner Page</span>
                                </a>
                            </li>
                            <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-cog" aria-hidden="true"></i> <span>Site Setting</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li><a href="#"><i class="fa fa-circle-o"></i> Site Configuation</a></li>
                                    <li><a href="#"><i class="fa fa-circle-o"></i> Site Appearance</a></li>
                                </ul>
                            </li>
                            <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-user" aria-hidden="true"></i> <span>Admin Manager</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li><a href=""><i class="fa fa-circle-o"></i> Add New User</a></li>
                                    <li><a href=""><i class="fa fa-circle-o"></i> Manage All User</a></li>
                                </ul>
                            </li>-->
                            <li class="<?php echo nl2br(e(Request::is('add-user') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('view-user') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('add-role') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('view-role') ? 'active' : '')); ?> treeview">
                            <a href="#">
                                <i class="fa fa-user"></i> <span>Admin User</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php echo nl2br(e(Request::is('add-user') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('view-user') ? 'active' : '')); ?> treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i> Manage User
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li class="<?php echo nl2br(e(Request::is('add-user') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/add-user'))); ?>"><i class="fa fa-circle-o"></i> Add User</a></li>
                                        <li class="<?php echo nl2br(e(Request::is('view-user') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/view-user'))); ?>"><i class="fa fa-circle-o"></i> View User</a></li>
                                    </ul>
                                </li>
                                <li class="<?php echo nl2br(e(Request::is('add-role') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('view-role') ? 'active' : '')); ?> treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i> Manage Permission Role
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li class="<?php echo nl2br(e(Request::is('add-role') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/add-role'))); ?>"><i class="fa fa-circle-o"></i> Add Role</a></li>
                                        <li class="<?php echo nl2br(e(Request::is('view-role') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/view-role'))); ?>"><i class="fa fa-circle-o"></i> View Role</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <!--<li class="<?php echo nl2br(e(Request::is('site-user') ? 'active' : '')); ?>">
                            <a href="<?php echo nl2br(e(url('/site-user'))); ?>">
                                <i class="fa fa-users" aria-hidden="true"></i> <span>Site User</span>
                            </a>
                        </li> --->
                        <li class="<?php echo nl2br(e(Request::is('site-user') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('customer-send-message-form') ? 'active' : '')); ?> treeview">
                            <a href="#">
                                <i class="fa fa-users" aria-hidden="true"></i>
                                <span>Manage Customer</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                    <!--<small class="label pull-right bg-red">
                                        </small> --->
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php echo nl2br(e(Request::is('site-user') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/site-user'))); ?>"><i class="fa fa-circle-o"></i>Customer List	
                                    <span class="pull-right-container">
                                        <small class="label pull-right bg-blue"></small>
                                    </span>
                                </a></li>
                                <li class="<?php echo nl2br(e(Request::is('customer-send-message-form') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/customer-send-message-form'))); ?>"><i class="fa fa-circle-o"></i>Send Message</a></li>
                            </ul>
                        </li>
                        <!--<li class="<?php echo nl2br(e(Request::is('phone-request') ? 'active' : '')); ?>">
                            <a href="<?php echo nl2br(e(url('/phone-request'))); ?>">
                                <i class="fa fa-phone" aria-hidden="true"></i> <span>Phone Request</span>
                            </a>
                        </li>  -->
                        <li class="<?php echo nl2br(e(Request::is('pride-admin/sale-reports') ? 'active' : '')); ?> treeview">
                            <a href="#">
                                <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                <span>Reports</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                    <!--<small class="label pull-right bg-red">
                                        </small> --->
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php echo nl2br(e(Request::is('pride-admin/sale-reports') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(route('pride_admin.sale_reports'))); ?>"><i class="fa fa-circle-o"></i>Sales Report	
                                    <span class="pull-right-container">
                                        <small class="label pull-right bg-blue"></small>
                                    </span>
                                </a></li>
                            </ul>
                        </li>
						
                        <li class="<?php echo nl2br(e(Request::is('coupon-generate') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('manage-promcodes') ? 'active' : '')); ?> treeview">
                            <a href="#">
                                <i class="fa fa-money" aria-hidden="true"></i>
                                <span>Admin Review</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php echo nl2br(e(Request::is('manage-review') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/manage-promcodes'))); ?>"><i class="fa fa-circle-o"></i>Review Manage</a></li>
                            </ul>
                        </li>
                       
                        <!--
                        <li class="<?php echo nl2br(e(Request::is('coupon-generate') ? 'active' : '')); ?><?php echo nl2br(e(Request::is('manage-promcodes') ? 'active' : '')); ?> treeview">
                            <a href="#">
                                <i class="fa fa-money" aria-hidden="true"></i>
                                <span>Promo Codes</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php echo nl2br(e(Request::is('coupon-generate') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/coupon-generate'))); ?>"><i class="fa fa-circle-o"></i>Generate</li>
                                <li class="<?php echo nl2br(e(Request::is('manage-promcodes') ? 'active' : '')); ?>"><a href="<?php echo nl2br(e(url('/manage-promcodes'))); ?>"><i class="fa fa-circle-o"></i>Manage</a></li>
                            </ul>
                        </li>
                        ---->
                        <li class="header"></li>
                       <!-- <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Database Backup</span></a></li>-->
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Cont                                                                                                                                                                                                        ent Wrapper. Contai                                                                                                                                                                                                        ns page content -->
            <div class="content-wrapper">
                <div class="loading-modal"></div>
                <!-- Main content -->
                <?php echo $__env->yieldContent('content'); ?>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Laravel Version</b> 5.5
                </div>
                <strong>Copyright &copy; 2019 <a href="#">penrpencil</a>.</strong> All rights
                reserved.
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Create the tabs -->
                <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                    <li><a href="#control-sidebar-hom                                                                                                                                                                                                        e-tab" data-toggle="ta                                                                                                                                                                                                        b"><i class="fa fa-home"></i></a></                                                                                                                                                                                                        li>
                    <li><a href="#control-sidebar-settings                                                                                                                                                                                                        -tab" data-toggle="tab"><i class="fa fa-gears"></i></a></l                                                                                                                                                                                                        i>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- Home tab content -->
                    <div class="tab-pane" id="control-sidebar-home-tab">
                        <h3 class="control-sidebar-heading">Recent Activity</h3>
                        <ul class="control-sidebar-menu">
                            
                        </ul>
                        <!-- /.control-sidebar-menu -->
                        <h3 class="control-sidebar-heading">Tasks Progress</h3>
                        <ul class="control-sidebar-menu">
                        </ul>
                    </div>
                    <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                    <div class="tab-pane" id="control-sidebar-settings-tab">
                    </div>
                </div>
            </aside>
            <div class="control-sidebar-bg"></div>
        </div>

        <script src="<?php echo nl2br(e(asset('assets_admin/jquery-ui.min.js'))); ?>"></script>
        <script>
$.widget.bridge('uibutton', $.ui.button);</script>
        <script src="<?php echo nl2br(e(asset('assets_admin/bootstrap/js/bootstrap.min.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/plugins/datatables/jquery.dataTables.min.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/plugins/datatables/dataTables.bootstrap.min.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/plugins/select2/select2.full.min.js'))); ?>"></script>
        <!-- InputMask -->
        <script src="<?php echo nl2br(e(asset('assets_admin/plugins/input-mask/jquery.inputmask.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/plugins/input-mask/jquery.inputmask.date.extensions.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/plugins/input-mask/jquery.inputmask.extensions.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/raphael-min.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/plugins/morris/morris.min.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/plugins/sparkline/jquery.sparkline.min.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/plugins/knob/jquery.knob.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/moment.min.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/plugins/daterangepicker/daterangepicker.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/plugins/datepicker/bootstrap-datepicker.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/plugins/slimScroll/jquery.slimscroll.min.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/plugins/fastclick/fastclick.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/dist/js/adminlte.min.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/dist/js/fileinput.min.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/dist/js/pages/dashboard.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/plugins/chartjs/Chart.min.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/dist/js/pages/dashboard2.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/dist/js/demo.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/dist/js/form_validation.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/ajax_modal.js'))); ?>"></script>
        <script src="<?php echo nl2br(e(asset('assets_admin/custom-script.js'))); ?>"></script>
        <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false
            });
            $("#datasync").click(function(){
             //   alert('ok');
             var url = base_url + "/save-item-list/";
             $(".loading-modal").show();
             $.ajax({
                 url:url,
                 type: 'GET',
                 success: function (data) {
                    $(".loading-modal").hide();
                    alert(data); 
                    location.reload();
                 }
               });
            });
        });
        </script>
        <script>
            $(function () {
                //Initialize Select2 Elements
                $(".select2").select2();
                //Datemask dd/mm/yyyy
                $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                //Datemask2 mm/dd/yyyy
                $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
                //Money Euro
                $("[data-mask]").inputmask();

                //Date range picker
                $('#reservation').daterangepicker();
                //Date range picker with time picker
                $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
                //Date range as a button
                $('#daterange-btn').daterangepicker(
                        {
                            ranges: {
                                'Today': [moment(), moment()],
                                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                            },
                            startDate: moment().subtract(29, 'days'),
                            endDate: moment()
                        },
                        function (start, end) {
                            $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                        }
                );

                //Date picker
                $('#datepicker').datepicker({
                    autoclose: true
                });

                //iCheck for checkbox and radio inputs
                $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue',
                    radioClass: 'iradio_minimal-blue'
                });
                //Red color scheme for iCheck
                $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                    checkboxClass: 'icheckbox_minimal-red',
                    radioClass: 'iradio_minimal-red'
                });
                //Flat red color scheme for iCheck
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });

                //Colorpicker
                $(".my-colorpicker1").colorpicker();
                //color picker with addon
                $(".my-colorpicker2").colorpicker();

                //Timepicker
                $(".timepicker").timepicker({
                    showInputs: false
                });
            });
        </script>
    </body>
</html>
