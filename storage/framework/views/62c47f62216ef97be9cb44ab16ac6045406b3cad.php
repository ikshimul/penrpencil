<?php $__env->startSection('title', 'Track Order'); ?>
<?php $__env->startSection('content'); ?>
<style>
    .error-parent {
        height:60vh;
        position:relative;
    }
    .error-parent h1 {
        position:absolute;
        margin:0;
        left:50%;
        top:50%;
        transform:translate(-50%, -50%);
    }
    
</style>
<div class="container">
    <div class="error-parent">
        <h1>Error 404! Page not found. <a href="<?php echo nl2br(e(url('/'))); ?>" style="text-decoration:underline">Go to Home</a></h1>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>