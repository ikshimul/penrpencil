<?php

use App\Http\Controllers\cart\LastseenController;
use App\Http\Controllers\product\ProductController;
?>

<?php $__env->startSection('title',$title); ?>
<?php $__env->startSection('content'); ?>
<!-- Breadcrumb Area -->
<div class="mi-breadcrumb-area mi-bggrey">
    <div class="mi-breadcrumb-path">
        <div class="mi-container">
            <ul>
                <li><a href="<?php echo nl2br(e(url('/'))); ?>">Home</a></li>
                <li><a href="#"><?php echo $category_name; ?></a></li>
                <li><?php echo nl2br(e($singleproduct->product_name)); ?></li>
            </ul>
        </div>
    </div>
</div>
<!--// Breadcrumb Area -->
<!-- Product Detaails Area -->
<div class="mi-prodetails-area pt-0 pb-50">
    <div class="mi-container">
        <div class="row">
            <div class="col-lg-8">
                <div class="row">

                    <div class="col-lg-6">
                        <div class="mi-prodetails-viewbox">
                            <div id="img-1" class="mi-prodetails-largeimage">
                                <?php
                                $i = 0;
                                foreach ($singleproductmultiplepic as $smplist) {
                                    $i++;
                                    if ($i == 1) {
                                        ?>
                                        <img id="zoom1" src="<?php echo nl2br(e(url('/'))); ?>/storage/app/public/pgallery/<?php echo nl2br(e($smplist->productimg_img)); ?>"
                                             data-zoom-image="<?php echo nl2br(e(url('/'))); ?>/storage/app/public/pgallery/<?php echo nl2br(e($smplist->productimg_img)); ?>"
                                             alt="big-1">
                                             <?php
                                         }
                                     }
                                     ?>
                            </div>
                            <div class="mi-prodetails-thumbs">
                                <ul class="mi-prodetails-thumbslist" id="gallery_01">
                                    <?php $__currentLoopData = $singleproductmultiplepic; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $smplist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>	
                                    <li>
                                        <a href="#" class="elevatezoom-gallery"
                                           data-image="<?php echo nl2br(e(URL::to(''))); ?>/storage/app/public/pgallery/<?php echo nl2br(e($smplist->productimg_img_tiny)); ?>"
                                           data-zoom-image="<?php echo nl2br(e(URL::to(''))); ?>/storage/app/public/pgallery/<?php echo nl2br(e($smplist->productimg_img)); ?>">
                                            <img src="<?php echo nl2br(e(URL::to(''))); ?>/storage/app/public/pgallery/<?php echo nl2br(e($smplist->productimg_img_tiny)); ?>"
                                                 alt="zo-th-1" />
                                        </a>
                                    </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="mi-prodetails-content">
                            <h4 class="product-title"><?php echo nl2br(e($singleproduct->product_name)); ?></h4>
                            <div class="product-price">
                                <h5>Price:</h5> 
                                <?php if ($singleproduct->product_pricediscounted > 0) { ?>
                                    <ins>৳ <?php echo $singleproduct->discount_product_price; ?></ins>
                                    <del>৳ <?php echo nl2br(e($singleproduct->product_price)); ?></del>
                                <?php } else { ?>
                                    ৳ <?php echo nl2br(e($singleproduct->product_price)); ?>

                                <?php } ?>
                            </div>
                            <div class="product-discount">
                                <?php if ($singleproduct->product_pricediscounted > 0) { ?>
                                    <h5>Offer:</h5><span><?php echo nl2br(e($singleproduct->product_pricediscounted)); ?>% Off</span>
                                <?php } ?>
                            </div>
                            <form action="<?php echo nl2br(e(url('/cart/add-to-cart'))); ?>" method="post" id="product_addtocart_form">
                                <?php echo nl2br(e(csrf_field())); ?>

                                <div class="product-quantity">
                                    <h5>Quantity: </h5>
                                    <div class="quantitybox">
                                        <button class="dec">-</button>
                                        <input type="number" value="1" name="productqty" max="<?php echo nl2br(e($product_qty)); ?>">
                                        <button class="inc">+</button>
                                    </div>

                                    <input type="hidden" name="productid" value="<?php echo nl2br(e($singleproduct->product_id)); ?>" />
                                    <input type="hidden" name="productcolor" id="selectcolor" value="<?php echo nl2br(e($product_color)); ?>">
                                    <input type="hidden" name="productimage" id="productImage" value="<?php echo nl2br(e($cart_image->productimg_img_thm)); ?>">
                                    <button  type="submit" class="mi-button">Add to Cart</button>

                                </div>
                                <div class="product-types">
                                    <h5>Types:</h5>
                                    <ul>
                                        <li><a href="#">Pen</a></li>
                                        <li><a href="#">Pencil</a></li>
                                        <li><a href="#">Paper</a></li>
                                    </ul>
                                </div>
                                <div class="product-colors">
                                    <h5>Colors:</h5>
                                    <ul>
                                        <li class="orange active"><a href="#"><span></span></a></li>
                                        <li class="green"><a href="#"><span></span></a></li>
                                        <li class="violet"><a href="#"><span></span></a></li>
                                        <li class="grey"><a href="#"><span></span></a></li>
                                    </ul>
                                </div>
                                <div class="product-sizes">
                                    <h5>Sizes:</h5>
                                    <ul>
                                        <?php ($i = 0); ?>
                                        <?php $__currentLoopData = $product_sizes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $size): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li class="<?php
                                        if ($size->SizeWiseQty < 1)
                                            echo 'disabled-size';
                                        elseif ($i == 0)
                                            echo 'active';
                                        ?>"><a href="#"><span></span><?php echo nl2br(e($size->productsize_size)); ?></a></li>
                                        <?php ($i++); ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                    <input id="product-size-input" class="swatch-input super-attribute-select" name="productsize" type="text" value="<?php echo nl2br(e($product_sizes[0]->productsize_size)); ?>" data-selector="productsize" data-validate="{required: true}" aria-required="true" aria-invalid="false" data-attr-name="size">
                                </div>
                            </form>
                            <div class="product-share">
                                <h5>Share On: </h5>
                                <ul>
                                    <li>
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-instagram"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-4">
                <div class="mi-prodetails-info">
                    <div class="delivery-details">
                        <div class="mi-title">
                            <h5>Delivery Options</h5>
                        </div>
                        <div class="single-block">
                            <h6>Types of Delivery:</h6>
                            <ul>
                                <li>Express Delivery</li>
                                <li>Regular Delivery</li>
                            </ul>
                        </div>
                        <div class="single-block">
                            <h6>Delivery Address:</h6>
                            <address>House 10, Road 12, Block F, Banani, Dhaka, Bangladesh</address>
                        </div>
                        <div class="single-block">
                            <h6>Delivery Charge:</h6>
                            <span>৳20 (Regular), ৳40 (Express)</span>
                        </div>
                        <div class="single-block">
                            <h6>Delivery Time:</h6>
                            <span>10AM, 20 Sep 2019</span>
                        </div>
                    </div>
                    <div class="payment-details">
                        <div class="mi-title mt-30">
                            <h5>Payment Methods</h5>
                        </div>
                        <ul>
                            <li>Cash On Delivery</li>
                            <li>Visa Card / Master Card / Amex Card / Nexus Card</li>
                            <li>Mobile Banking:
                                <div class="mt-2">
                                    <img src="<?php echo nl2br(e(asset('assets/images/payment/bkash.png'))); ?>" alt="bkash">
                                    <img src="<?php echo nl2br(e(asset('assets/images/payment/rocket.png'))); ?>" alt="rocket">
                                    <img src="<?php echo nl2br(e(asset('assets/images/payment/nogod.png'))); ?>" alt="nogod">
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="mi-buttongroup">
                        <a href="#" class="mi-button">Return Policy</a>
                        <a href="#" class="mi-button">Replacement Policy</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="mi-prodetails-bottom mt-50">

            <ul class="nav mi-tab" id="bstab1" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="bstab1-area1-tab" data-toggle="tab" href="#bstab1-area1"
                       role="tab" aria-controls="bstab1-area1" aria-selected="true">Description</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="bstab1-area2-tab" data-toggle="tab" href="#bstab1-area2"
                       role="tab" aria-controls="bstab1-area2" aria-selected="false">Reviews</a>
                </li>
            </ul>
            <div class="tab-content" id="bstab1-ontent">
                <div class="tab-pane fade show active" id="bstab1-area1" role="tabpanel"
                     aria-labelledby="bstab1-area1-tab">
                    <div class="mi-prodetails-description">
                        <ul>
                            <li>OS: Android 9.0</li>
                            <li>Processor: Hisilicon Kirin 710F</li>
                            <li>CPU: Octa-core</li>
                            <li>Display: 6.59 inches</li>
                            <li>Resolution: 1080 x 2340</li>
                            <li>RAM: 4GB</li>
                            <li>ROM: 128GB</li>
                            <li>Rear Camera: 16.0 MP + 8.0 MP + 2.0 MP</li>
                            <li>Front Camera: 16.0 MP</li>
                            <li>Battery: 4000mAh</li>
                        </ul>
                    </div>
                </div>
                <div class="tab-pane fade" id="bstab1-area2" role="tabpanel"
                     aria-labelledby="bstab1-area2-tab">
                    <div class="mi-prodetails-reviews">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="review-form">
                                    <h5>Give a rating on this product</h5>
                                    <form action="#" class="mi-form">
                                        <div class="form-field">
                                            <div class="mi-rattingbox mi-rattingbox-input">
                                                <span class="active"><i class="fa fa-star"></i></span>
                                                <span class="active"><i class="fa fa-star"></i></span>
                                                <span class="active"><i class="fa fa-star"></i></span>
                                                <span class="active"><i class="fa fa-star"></i></span>
                                                <span class="active"><i class="fa fa-star"></i></span>
                                            </div>
                                        </div>
                                        <div class="form-field-half">
                                            <input type="text" placeholder="Enter Your Name">
                                        </div>
                                        <div class="form-field-half">
                                            <input type="email" placeholder="Enter Your Email">
                                        </div>
                                        <div class="form-field">
                                            <textarea cols="30" rows="7"
                                                      placeholder="Enter your text"></textarea>
                                        </div>
                                        <div class="form-field">
                                            <button type="submit" class="mi-button">Submit Review</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="product-reviews">
                                    <h5>Product Reviews: </h5>
                                    <div class="mi-rattingbox">
                                        <span class="active"><i class="fa fa-star"></i></span>
                                        <span class="active"><i class="fa fa-star"></i></span>
                                        <span class="active"><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                    </div>
                                </div>
                                <p>(4.21 average based on 3 ratings.)</p>
                                <div class="mi-reviewbox">
                                    <div class="mi-reviewbox-image">
                                        <img src="assets/images/user-thumb.png" alt="user">
                                    </div>
                                    <div class="mi-reviewbox-content">
                                        <h5>Jordan Gaylord</h5>
                                        <div class="mi-rattingbox">
                                            <span class="active"><i class="fa fa-star"></i></span>
                                            <span class="active"><i class="fa fa-star"></i></span>
                                            <span class="active"><i class="fa fa-star"></i></span>
                                            <span><i class="fa fa-star"></i></span>
                                            <span><i class="fa fa-star"></i></span>
                                        </div>
                                        <p>It's a great product. You can buy this product without any
                                            hesitation.</p>
                                    </div>
                                </div>
                                <div class="mi-reviewbox">
                                    <div class="mi-reviewbox-image">
                                        <img src="assets/images/user-thumb.png" alt="user">
                                    </div>
                                    <div class="mi-reviewbox-content">
                                        <h5>Stella Sawayn</h5>
                                        <div class="mi-rattingbox">
                                            <span class="active"><i class="fa fa-star"></i></span>
                                            <span class="active"><i class="fa fa-star"></i></span>
                                            <span class="active"><i class="fa fa-star"></i></span>
                                            <span><i class="fa fa-star"></i></span>
                                            <span><i class="fa fa-star"></i></span>
                                        </div>
                                        <p>It's a great product. You can buy this product without any
                                            hesitation. It has great color combination that photos.</p>
                                    </div>
                                </div>
                                <div class="mi-reviewbox">
                                    <div class="mi-reviewbox-image">
                                        <img src="assets/images/user-thumb.png" alt="user">
                                    </div>
                                    <div class="mi-reviewbox-content">
                                        <h5>Gerson Wunsch</h5>
                                        <div class="mi-rattingbox">
                                            <span class="active"><i class="fa fa-star"></i></span>
                                            <span class="active"><i class="fa fa-star"></i></span>
                                            <span class="active"><i class="fa fa-star"></i></span>
                                            <span><i class="fa fa-star"></i></span>
                                            <span><i class="fa fa-star"></i></span>
                                        </div>
                                        <p>It's a great product. You can buy this product without any
                                            hesitation.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!--// Product Detaails Area -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>