<?php

use App\Http\Controllers\product\ProductController;
use App\Http\Controllers\HomeController;
?>

<?php $__env->startSection('title','Pride Limited | A Pride Group venture'); ?>
<?php $__env->startSection('content'); ?>
<!--// Category Menu -->
<div class="mi-category-area mt-30" id="app">
<progress-bar></progress-bar>
    <div class="mi-container">
        <div class="mi-category-wrapper">
            <?php
            $category = HomeController::GetMainCategory();
            foreach ($category as $cat) {
                ?>
                <div class="mi-category">
                    <a href='<?php echo nl2br(e(url("/$cat->procat_name/$cat->procat_id"))); ?>'>
                        <span class="mi-category-icon">
                            <?php echo html_entity_decode($cat->procat_icon, ENT_QUOTES, "UTF-8"); ?>
                        </span>
                        <h6><?php echo nl2br(e($cat->procat_name)); ?></h6>
                    </a>
                </div>
            <?php } ?>
            <!--<div class="mi-category">
                <a href="category.html">
                    <span class="mi-category-icon">
                        <i class="flaticon-pencil-case"></i>
                    </span>
                    <h6>Writing Supplies</h6>
                </a>
            </div>
            <div class="mi-category">
                <a href="category.html">
                    <span class="mi-category-icon">
                        <i class="flaticon-folder"></i>
                    </span>
                    <h6>Office Supplies</h6>
                </a>
            </div>
            <div class="mi-category">
                <a href="category.html">
                    <span class="mi-category-icon">
                        <i class="flaticon-notebook"></i>
                    </span>
                    <h6>Notebook/ Khata</h6>
                </a>
            </div>
            <div class="mi-category">
                <a href="category.html">
                    <span class="mi-category-icon">
                        <i class="flaticon-document"></i>
                    </span>
                    <h6>Paper</h6>
                </a>
            </div>
            <div class="mi-category">
                <a href="category.html">
                    <span class="mi-category-icon">
                        <i class="flaticon-product"></i>
                    </span>
                    <h6>Cleaning Item</h6>
                </a>
            </div>
            <div class="mi-category">
                <a href="products.html">
                    <span class="mi-category-icon">
                        <i class="flaticon-giftbox"></i>
                    </span>
                    <h6>Gift Items</h6>
                </a>
            </div>
            <div class="mi-category">
                <a href="products.html">
                    <span class="mi-category-icon">
                        <i class="flaticon-circuit"></i>
                    </span>
                    <h6>Electronics Item</h6>
                </a>
            </div>
            <div class="mi-category">
                <a href="products.html">
                    <span class="mi-category-icon">
                        <i class="flaticon-creativity"></i>
                    </span>
                    <h6>Art & Crafts</h6>
                </a>
            </div>
            <div class="mi-category">
                <a href="products.html">
                    <span class="mi-category-icon">
                        <i class="flaticon-snowflake"></i>
                    </span>
                    <h6>Corporate</h6>
                </a>
            </div> --->
        </div>
    </div>
</div>
<!--// Category Slider -->
<!-- Heroarea -->
<div class="mi-heroslider-area mt-30">
    <div class="mi-container">
        <div class="mi-heroslider-slider">
            <?php ($i=0); ?>
            <?php $__currentLoopData = $slides; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($slide->device == 'desktop'): ?>

            <div class="mi-heroslider">
                <a href="<?php echo nl2br(e($slide->url_link)); ?>"><img src="<?php echo nl2br(e(url('/'))); ?>/storage/app/public/slider/<?php echo nl2br(e($slide->slider_image)); ?>" alt="heroslider image"></a>
            </div>
            <?php ($i++); ?>
            <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</div>
<!--// Heroarea -->
<!-- Special Offer Area -->
<div class="mi-specialoffer-area">
    <div class="mi-container">
        <div class="row">
            <?php foreach ($banner_list as $banner) { ?>
                <div class="col-lg-4 col-12 mt-30">
                    <a href="<?php echo $banner->banner_link; ?>" class="mi-banner">
                        <img src="<?php echo nl2br(e(URL::to(''))); ?>/storage/app/public/banner/<?php echo $banner->banner_image; ?>" alt="banner image">
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<!--// Special Offer Area -->
<!-- Most Popular Products -->
<div class="mi-mostpopular-area mt-50">
    <div class="mi-container">
        <div class="mi-title">
            <span class="mi-title-icon"><i class="fa fa-thumbs-o-up"></i></span>
            <h5>Most Popular Products</h5>
            <a href="products.html" class="mi-button mi-button-small mi-button-style2">View All</a>
        </div>
        <div class="mi-mostpopular-slider mi-product-slider">
            <?php $__currentLoopData = $product_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php
            $product_name = str_replace(' ', '-', $product->product_name);
            $product_url = strtolower($product_name);
            $medium_image = ProductController::GetProductImage($product->product_id);
            $data = ProductController::GetProductColorAlbum($product->product_id);
            $color_album = str_replace('/', '-', $product->productalbum_name);
            ?>
            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="<?php echo nl2br(e(url("shop/{$product_url}/color-{$color_album}/{$product->product_id}"))); ?>" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="<?php echo nl2br(e(URL::to(''))); ?>/storage/app/public/pgallery/<?php echo $medium_image->productimg_img_medium; ?>" alt="product image">
                            <?php if ($product->product_pricediscounted > 1) { ?>
                                <span class="mi-product-discount"><?php echo $product->product_pricediscounted; ?>% <br> Off</span>
                            <?php } ?>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title"><?php echo nl2br(e($product->product_name)); ?></h6>
                            <span class="mi-product-price">
                                <?php if ($product->product_pricediscounted > 1) { ?>
                                    <del>TK. <?php echo nl2br(e($product->product_price)); ?></del> 
                                    TK. <?php echo nl2br(e($product->discount_product_price)); ?>

                                <?php } else { ?>
                                    TK. <?php echo nl2br(e($product->product_price)); ?>

                                <?php } ?>
                            </span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <!--<ul>
                            <li>
                                <a href="#"><i class="fa fa-shopping-bag"></i></a>
                            </li>
                            <li>
                                <a href="#product-quickview-popup"  data-toggle="modal" data-target="#product_<?php echo $product->product_id; ?>" data-id="<?php echo $product->product_id; ?>"><i class="fa fa-eye"></i></a>
                            </li>
                        </ul> --->
                        <!-- Product Quickview -->
                        <div class="modal fade mi-productview-popup" id="product-quickview-popup"  tabindex="-1" role="dialog" >
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="modal-body">
                                        <!-- Product Details -->
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="mi-prodetails-viewbox">
                                                    <div id="img-1" class="mi-prodetails-largeimage">
                                                        <img id="zoom1" src="assets/images/products/product-image-1.png" alt="big-1">
                                                    </div>
                                                    <div class="mi-prodetails-thumbs">
                                                        <ul class="mi-prodetails-thumbslist" id="gallery_01">
                                                            <li>
                                                                <a href="#" class="elevatezoom-gallery"
                                                                   data-image="assets/images/products/product-image-1.png"
                                                                   data-zoom-image="assets/images/products/product-image-lg-1.png">
                                                                    <img src="assets/images/products/product-image-1.png"
                                                                         alt="zo-th-1" />
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="elevatezoom-gallery"
                                                                   data-image="assets/images/products/product-image-2.png"
                                                                   data-zoom-image="assets/images/products/product-image-lg-2.png">
                                                                    <img src="assets/images/products/product-image-2.png"
                                                                         alt="zo-th-1" />
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="elevatezoom-gallery"
                                                                   data-image="assets/images/products/product-image-3.png"
                                                                   data-zoom-image="assets/images/products/product-image-lg-3.png">
                                                                    <img src="assets/images/products/product-image-3.png"
                                                                         alt="zo-th-1" />
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="elevatezoom-gallery"
                                                                   data-image="assets/images/products/product-image-1.png"
                                                                   data-zoom-image="assets/images/products/product-image-lg-1.png">
                                                                    <img src="assets/images/products/product-image-1.png"
                                                                         alt="zo-th-1" />
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="mi-prodetails-content">
                                                    <h4 class="product-title">Double A A4 1 rim</h4>
                                                    <div class="product-price">
                                                        <h5>Price:</h5> <ins>৳245</ins><del>৳ 275</del>
                                                    </div>
                                                    <div class="product-discount">
                                                        <h5>Offer:</h5><span>25% Off</span>
                                                    </div>
                                                    <div class="product-quantity">
                                                        <h5>Quantity: </h5>
                                                        <div class="quantitybox">
                                                            <button class="dec">-</button>
                                                            <input type="number" value="1">
                                                            <button class="inc">+</button>
                                                        </div>
                                                        <a href="#" class="mi-button">Add to Cart</a>
                                                    </div>
                                                    <div class="product-types">
                                                        <h5>Types:</h5>
                                                        <ul>
                                                            <li><a href="#">Pen</a></li>
                                                            <li><a href="#">Pencil</a></li>
                                                            <li><a href="#">Paper</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="product-colors">
                                                        <h5>Colors:</h5>
                                                        <ul>
                                                            <li class="orange active"><a href="#"><span></span></a></li>
                                                            <li class="green"><a href="#"><span></span></a></li>
                                                            <li class="violet"><a href="#"><span></span></a></li>
                                                            <li class="grey"><a href="#"><span></span></a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="product-sizes">
                                                        <h5>Sizes:</h5>
                                                        <ul>
                                                            <li class="active"><a href="#"><span></span>S</a></li>
                                                            <li><a href="#"><span></span>M</a></li>
                                                            <li><a href="#"><span></span>L</a></li>
                                                            <li><a href="#"><span></span>XL</a></li>
                                                            <li><a href="#"><span></span>XXL</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="product-share">
                                                        <h5>Share On: </h5>
                                                        <ul>
                                                            <li>
                                                                <a href="#"><i class="fa fa-facebook"></i></a>
                                                            </li>
                                                            <li>
                                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                            </li>
                                                            <li>
                                                                <a href="#"><i class="fa fa-instagram"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--// Product Details -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--// Product Quickview -->
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</div>
<!--// Most Popular Products -->

<!-- Most Popular Products -->
<div class="mi-mostpopular-area mt-50">
    <div class="mi-container">
        <div class="mi-title">
            <span class="mi-title-icon"><i class="fa fa-thumbs-o-up"></i></span>
            <h5>Most Popular Products</h5>
            <a href="products.html" class="mi-button mi-button-small mi-button-style2">View All</a>
        </div>
        <div class="mi-mostpopular-slider mi-product-slider">

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-2.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-3.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-4.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-5.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-6.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!--// Most Popular Products -->

<!-- Most Popular Products -->
<div class="mi-mostpopular-area mt-50">
    <div class="mi-container">
        <div class="mi-title">
            <span class="mi-title-icon"><i class="fa fa-thumbs-o-up"></i></span>
            <h5>Most Popular Products</h5>
            <a href="products.html" class="mi-button mi-button-small mi-button-style2">View All</a>
        </div>
        <div class="mi-mostpopular-slider mi-product-slider">

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-2.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-3.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-4.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-5.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-6.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!--// Most Popular Products -->


<!-- Testimonial Area -->
<div class="mi-testimonial-area mt-50">
    <div class="mi-container">
        <div class="row">
            <div class="col-lg-5">
                <div class="mi-title">
                    <span class="mi-title-icon"><i class="fa fa-thumbs-o-up"></i></span>
                    <h5>Our VIP customers reviews</h5>
                </div>
                <div class="mi-testimonial-videowrapper">
                    <div class="mi-testimonial-video">
                        <img src="assets/images/testimonial-star-image-1.jpg" alt="image">
                        <button class="video-popup-button" data-channel="youtube"
                                data-video-id='v0QTqHXiVNw'><i class="fa fa-play"></i></button>
                        <div class="text">
                            <h5>Mariano Sanford</h5>
                            <h6>Customer</h6>
                        </div>
                    </div>
                    <div class="mi-testimonial-video">
                        <img src="assets/images/testimonial-star-image-2.jpg" alt="image">
                        <button class="video-popup-button" data-channel="youtube"
                                data-video-id='v0QTqHXiVNw'><i class="fa fa-play"></i></button>
                        <div class="text">
                            <h5>Mariano Sanford</h5>
                            <h6>Customer</h6>
                        </div>
                    </div>
                    <div class="mi-testimonial-video">
                        <img src="assets/images/testimonial-star-image-1.jpg" alt="image">
                        <button class="video-popup-button" data-channel="youtube"
                                data-video-id='v0QTqHXiVNw'><i class="fa fa-play"></i></button>
                        <div class="text">
                            <h5>Mariano Sanford</h5>
                            <h6>Customer</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="mi-title ml-30">
                    <span class="mi-title-icon"><i class="fa fa-thumbs-o-up"></i></span>
                    <h5>Our customers reviews</h5>
                </div>
                <div class="mi-testimonial-textwrapper ml-30">
                    <div class="mi-testimonial-text">
                        <div class="mi-testimonial-content">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium error a
                                quibusdam recusandae similique, sint itaque et, modi sunt totam provident
                                ipsam
                                sit voluptate minima, molestias deserunt eum dolorem odio.</p>
                        </div>
                        <div class="mi-testimonial-author">
                            <div class="image">
                                <img src="assets/images/user-thumb.png" alt="image">
                            </div>
                            <div class="text">
                                <h5>Lamar Olson</h5>
                                <h6>Customer</h6>
                            </div>
                        </div>
                    </div>
                    <div class="mi-testimonial-text">
                        <div class="mi-testimonial-content">
                            <p>Et voluptas sit. Ab cum quis natus omnis amet doloremque vel. Distinctio qui
                                sed tempora quis modi iste ipsa quisquam sint.</p>
                        </div>
                        <div class="mi-testimonial-author">
                            <div class="image">
                                <img src="assets/images/user-thumb.png" alt="image">
                            </div>
                            <div class="text">
                                <h5>Waino Hand</h5>
                                <h6>Customer</h6>
                            </div>
                        </div>
                    </div>
                    <div class="mi-testimonial-text">
                        <div class="mi-testimonial-content">
                            <p>Labore et voluptates soluta unde officiis. Aut quidem fugit quaerat
                                temporibus commodi. Harum impedit laborum eum. Nihil ullam asperiores sed
                                est voluptatibus ut doloribus maxime sed. Non nam accusamus ab ipsam odio.
                            </p>
                        </div>
                        <div class="mi-testimonial-author">
                            <div class="image">
                                <img src="assets/images/user-thumb.png" alt="image">
                            </div>
                            <div class="text">
                                <h5>Mariano Sanford</h5>
                                <h6>Customer</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// Testimonial Area -->
<script>
    $(document).ready(function () {
        $('a[data-toggle=modal], button[data-toggle=modal]').click(function () {
            var data_id = '';
            if (typeof $(this).data('id') !== 'undefined') {

                data_id = $(this).data('id');
            }
            //alert(data_id);

            var total_loop = $("#total_loop").val();
            // alert(total_loop);           
        });
    });
</script>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>