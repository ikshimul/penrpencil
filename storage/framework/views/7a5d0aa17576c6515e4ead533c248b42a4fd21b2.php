<?php $__env->startSection('title', 'Pride Limited | Coupon List'); ?>
<?php $__env->startSection('content'); ?>

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" media="all"/>
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet" media="all"/>
<section class="content-header">
    <h1>
        Analytics Reports 
        <small>Most View</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Coupon</a></li>
        <li class="active">List</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- /.box -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Browse All Coupon List </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <center>
                        <?php if(session('update')): ?>
                        <div class="alert alert-success">
                            <?php echo nl2br(e(session('update'))); ?>

                        </div>
                        <?php endif; ?>
                    </center>
                    <center>
                        <?php if(session('delete')): ?>
                        <div class="alert alert-danger">
                            <?php echo nl2br(e(session('delete'))); ?>

                        </div>
                        <?php endif; ?>
                    </center>
                    <table id="example" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="2%">SL</th>
                                <th>Coupon Code</th>
                                <th>Showrrom</th>                        	
                                <th>Address</th>                                                        
                                <th>Phone</th>                                                        
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            ?>
                            <?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $promo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>                        	
                                <td width="2%"><?php echo nl2br(e($i++)); ?></td>
                                <td><?php echo $promo->promo_code;?></td>
                                <td>
                                   <?php echo $promo->SalesPointName;?> 
                                </td>
                                <td>
                                   <?php echo $promo->SPAddress;?>  
                                </td>																		
                                <td>
                                    <?php echo $promo->Phone;?>  
                                </td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
                            <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
                                <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script>
    $(document).ready(function() {
    $('#example').DataTable( {
        "ordering": false,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>