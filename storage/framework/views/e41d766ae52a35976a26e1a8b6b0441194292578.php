<?php $__env->startSection('title','Pen R Pencil'); ?>
<?php $__env->startSection('content'); ?>
<style>
    .mi-breadcrumb-area::before {
        content: "";
        position: absolute;
        left: 0;
        top: 0;
        height: 100%;
        width: 100%;
        background-image: url(../../storage/app/productbanner/);
        background-size: contain;
        background-repeat: repeat;
        z-index: -1;
        opacity: .5;
    }
</style>
<!-- Breadcrumb Area -->
<div class="mi-breadcrumb-area mi-bggrey">
    <div class="mi-breadcrumb-path">
        <div class="mi-container">
            <ul>
                <li><a href="<?php echo nl2br(e(url('/'))); ?>">Home</a></li>
                <li><a href="#">Mobile Verify</a></li>
            </ul>
        </div>
    </div>
</div>
<!--// Breadcrumb Area -->

<!-- Products Area -->
<div class="mi-products-area pt-0 pb-50">
    <div class="mi-container">
        <div class="mi-products-wrapper">
            <div class="row no-gutters">
                <div class="mi">
					<div class="mi-loginregister-phone" data-visible="field-phone">
						<form action="<?php echo nl2br(e(url('/verify-mobile-check'))); ?>" method="post" class="mi-form">
						 <?php echo nl2br(e(csrf_field())); ?>

							
							<div class="form-field">
								<label for="my-account-city">Enter your verify code:</label>
								<input type="text" name="verify_code" id="verify_code" required />
							</div>
							<div class="form-field">
								<button class="mi-button" type="submit">Verify</button>
							</div>
							
								<a href="#">Resend Code</a>
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
<!--// Products Area -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>