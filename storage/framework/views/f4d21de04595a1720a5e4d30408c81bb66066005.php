<?php $__env->startSection('title','Add sub Category'); ?>
<?php $__env->startSection('content'); ?>
<section class="content-header">
    <h1>
        Sub Category
        <small>Add </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Sub Category</li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">SUB CATEGORY FORM</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <form name="add_subpro" action="<?php echo nl2br(e(url('/save-subpro'))); ?>" method="post" enctype="multipart/form-data">
            <?php echo nl2br(e(csrf_field())); ?>

            <div class="box-body">
                <div class="row">
                    <div class="col-md-offset-1 col-md-6">
                        <center>
                            <?php if(session('save')): ?>
                            <div class="alert alert-success">
                                <?php echo nl2br(e(session('save'))); ?>

                            </div>
                            <?php endif; ?>
                        </center>
                        <center>
                            <?php if(session('error')): ?>
                            <div class="alert alert-success">
                                <?php echo nl2br(e(session('error'))); ?>

                            </div>
                            <?php endif; ?>
                        </center>
                        <div class="form-group">
                            <label>Main Category</label>
                            <select name="procat_id" class="form-control select2" style="width: 100%;">
                                <option value=""> ---- Select Main Category ---- </option>
                                <?php $__currentLoopData = $main_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $main): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo nl2br(e($main->procat_id)); ?>"> <?php echo nl2br(e($main->procat_name)); ?> </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Sub Category Name</label>
                            <input type="text" class="form-control" name="txtsubcategoryname"/>
                        </div>
                        <div class="form-group">
                            <label>Sub Category Order</label>
                            <input type="number" class="form-control" name="txtorder" min="1" max="100"/>
                            <span class="help-block" style="color:#f39c12;">Only Numbers</span>
                        </div>
                        <div class="form-group">
                            <label>Banner Image</label>
                            <input id="input-upload-img1" type="file" class="file" name="filename" data-preview-file-type="text">
                            <span class="help-block" style="color:#f39c12;">only .jpg image is allowed Size (Width: 118px X Height: 143px)</span>  
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="col-md-offset-1">
                    <input type="submit" name="btnsubmit" class="btn bg-navy btn-flat margin" value="Add Sub-Category"/>
                </div>
            </div>
        </form>    
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>