<?php $__env->startSection('title', 'Product Add Form'); ?>
<?php $__env->startSection('content'); ?>
<style>
    .file_new {
        visibility: hidden;
        position: absolute;
    }
    .error {
        color: #cb0e0e;
        padding: 5px;
    }
    input {
        background-repeat: no-repeat;
        background-position: right 1rem center;
        background-size: 0.79rem;
    }

    .help-text {
        display: block;
        margin-top: 0.5rem;
    }

    input:placeholder-shown + label {
        opacity: 0;
        transform: translateY(1rem);
    }

    .has-dynamic-label {
        position: relative;
        padding-top: 1.5rem;
    }

    .has-dynamic-label label {
        position: absolute;
        top: 0;
        opacity: 1;
        transform: translateY(0);
        transition: all 0.2s ease-out;
    }

    input:required + .help-text::before {
        content: '*Required';
        font-size: 13px;
    }

    input:optional + .help-text::before {
        content: '*Optional';
        font-size: 13px;
    }

    input:read-only {
        cursor: not-allowed;
    }

    input:valid {
        border-color:#00c0ef;
        background-image: url("data:image/svg+xml,%3Csvg width='45px' height='34px' viewBox='0 0 45 34' version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'%3E%3Cg stroke='none' stroke-width='1' fill='none' fill-rule='evenodd'%3E%3Cg transform='translate%28-56.000000, -59.000000%29' fill='%232EEC96'%3E%3Cpolygon points='70.1468531 85.8671329 97.013986 59 100.58042 62.5664336 70.1468531 93 56 78.8531469 59.5664336 75.2867133'%3E%3C/polygon%3E%3C/g%3E%3C/g%3E%3C/svg%3E%0A");
    }

    input:invalid {
        border-color: #00c0ef;
        background-image: url("data:image/svg+xml,%3Csvg width='30px' height='30px' viewBox='0 0 30 30' version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'%3E%3Cg stroke='none' stroke-width='1' fill='none' fill-rule='evenodd'%3E%3Cg transform='translate%28-128.000000, -59.000000%29' fill='%23F44336'%3E%3Cpolygon points='157.848404 61.9920213 145.980053 73.8603723 157.848404 85.7287234 154.856383 88.7207447 142.988032 76.8523936 131.119681 88.7207447 128.12766 85.7287234 139.996011 73.8603723 128.12766 61.9920213 131.119681 59 142.988032 70.8683511 154.856383 59'%3E%3C/polygon%3E%3C/g%3E%3C/g%3E%3C/svg%3E%0A");
    }

    input:invalid:focus {
        border-color: black;
    }

    input:invalid + .help-text {
        color: #cb0e0e;
    }

    input[type='email']:invalid + .help-text::before {
        content: 'You must enter a valid email.'
    }

    input:out-of-range + .help-text::before {
        content: 'Out of range';
    }
    td, th {
        padding: 5px;
    }
    button, input, select, textarea {
        font-family: inherit;
        font-size: inherit;
        line-height: inherit;
        width: 236px;
    }

    /* table styling */
    table {
        width: 100%
    }
    table caption {
        margin-bottom: 1rem;
        text-align: left;
        font-weight: 700;
    }
    th,
    td {
        padding: 0.5em 0.75em;
        border: 0 solid white;
        border-bottom-width: 1px;
        text-align: left;
        vertical-align: top
    }
    th:first-child, td:first-child {
        padding-left: 0;
    }
    th {
        font-weight: 600;
    }
    thead {
    }
    thead td,
    thead th {
        border-bottom-width: 2px;
    }
    tfoot {
    }
    tfoot td,
    tfoot th {
        border-bottom-width: 2px;
    }
    tbody {
    }
    tbody tr:last-child {
    }
    tbody tr:last-child td,
    tbody tr:last-child th {
        border-bottom-width: 0;
    }

    button,
    [role='button'],
    input[type='submit'],
    input[type='button'],
    input[type='reset'] {
        display: -webkit-inline-box;
        display: -ms-inline-flexbox;
        display: inline-flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        width: auto;
        height: 2.5rem;
        padding: 0 0.75rem;
        border-radius: 4px;
        outline: 0;
        text-decoration: none;
        white-space: nowrap;
        color: hsl(0, 0%, 25%);
        background-color: hsl(0, 0%, 90%);
        transition: background-color 0.1s ease-out;
        cursor: pointer;
    }
    button:hover:not(:disabled), [role='button']:hover:not(:disabled), input[type='submit']:hover:not(:disabled), input[type='button']:hover:not(:disabled), input[type='reset']:hover:not(:disabled) {
        color: hsl(0, 0%, 25%);
        background-color: rgb(162, 162, 162);
    }
    /* general form styling */
    fieldset {
        margin-bottom: 1rem;
        padding: 0;
        border: 0
    }
    fieldset:not(:last-child) {
        margin-bottom: 1rem;
    }
    label {
        display: block;
        margin-bottom: 0.2rem;
        font-weight: 600;
        font-size:16px;
    }
    input,
    select,
    textarea {
        outline: 0;
    }
    /* text input styling */
    input[type='text'],
    input[type='password'],
    input[type='url'],
    input[type='email'],
    input[type='tel'],
    input[type='search'],
    input[type='number'],
    input[type='date'],
    input[type='month'],
    input[type='week'],
    input[type='datetime-local'],
    input[type='color'] {
        display: block;
        border: 1px solid hsl(0, 0%, 75%);
        color: hsl(0, 0%, 25%);
    }
    input[type='text']::-webkit-input-placeholder, input[type='password']::-webkit-input-placeholder, input[type='url']::-webkit-input-placeholder, input[type='email']::-webkit-input-placeholder, input[type='tel']::-webkit-input-placeholder, input[type='search']::-webkit-input-placeholder, input[type='number']::-webkit-input-placeholder, input[type='date']::-webkit-input-placeholder, input[type='month']::-webkit-input-placeholder, input[type='week']::-webkit-input-placeholder, input[type='datetime-local']::-webkit-input-placeholder, input[type='color']::-webkit-input-placeholder {
        color: hsl(0, 0%, 50%);
    }
    input[type='text']:-ms-input-placeholder, input[type='password']:-ms-input-placeholder, input[type='url']:-ms-input-placeholder, input[type='email']:-ms-input-placeholder, input[type='tel']:-ms-input-placeholder, input[type='search']:-ms-input-placeholder, input[type='number']:-ms-input-placeholder, input[type='date']:-ms-input-placeholder, input[type='month']:-ms-input-placeholder, input[type='week']:-ms-input-placeholder, input[type='datetime-local']:-ms-input-placeholder, input[type='color']:-ms-input-placeholder {
        color: hsl(0, 0%, 50%);
    }
    input[type='text']::placeholder, input[type='password']::placeholder, input[type='url']::placeholder, input[type='email']::placeholder, input[type='tel']::placeholder, input[type='search']::placeholder, input[type='number']::placeholder, input[type='date']::placeholder, input[type='month']::placeholder, input[type='week']::placeholder, input[type='datetime-local']::placeholder, input[type='color']::placeholder {
        color: hsl(0, 0%, 50%);
    }
    textarea {
        display: block;
        height: auto;
        padding: 0.5rem 0.75rem;
        border: 1px solid hsl(0, 0%, 75%);
        color: hsl(0, 0%, 25%);
    }
    textarea::-webkit-input-placeholder {
        color: hsl(0, 0%, 50%);
    }
    textarea:-ms-input-placeholder {
        color: hsl(0, 0%, 50%);
    }
    textarea::placeholder {
        color: hsl(0, 0%, 50%);
    }
    /* select element styling */
    select {
        display: block;
        position: relative;
        /* width: 100%; */
        border: 1px solid hsl(0, 0%, 75%);
        color: hsl(0, 0%, 25%);
        background-repeat: no-repeat;
        background-position: right 1rem center;
        background-size: 0.75rem;
    }
    select:focus {
        border: 1px solid #0032a0;
    }
    /* checkbox styling */
    .checkbox {
        position: relative
    }
    .checkbox label {
        position: relative;
        padding: 0 2rem;
        font-weight: 400;
        cursor: pointer;
    }
    .checkbox input[type='checkbox'] {
        position: absolute;
        top: 0;
        left: 0;
        width: 0;
        height: 0;
        opacity: 0;
        pointer-events: none
    }
    .checkbox input[type='checkbox']:focus + label::before {
        background-color: hsl(0, 0%, 75%);
    }
    /* radio button styling */
    .radio {
        position: relative
    }
    .radio label {
        position: relative;
        padding: 0 2rem;
        font-weight: 400;
        cursor: pointer;
    }
    .radio input[type='radio'] {
        position: absolute;
        top: 0;
        left: 0;
        width: 0;
        height: 0;
        opacity: 0
    }
    .radio input[type='radio']:focus + label::before {
        background-color: hsl(0, 0%, 75%);
    }
    /* meter element styling â€“ currently only works in Webkit browsers and Firefox */
    meter {
        -webkit-appearance: meter;
        width: 100%;
        background: hsl(0, 0%, 90%);
    }
    meter::-webkit-meter-bar {
        background: hsl(0, 0%, 90%);
    }
    /* file input styling 
    input[type='file'] {
        clip: rect(0 0 0 0);
        position: absolute;
        width: 1px;
        height: 1px;
        overflow: hidden;
        margin: -1px;
        padding: 0;
        border: 0;
        white-space: nowrap;
    }
    input[type='file'] + label {
        display: -webkit-inline-box;
        display: -ms-inline-flexbox;
        display: inline-flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        height: 2.5rem;
        padding: 0 0.75rem;
        border-radius: 4px;
        outline: 0;
        text-decoration: none;
        white-space: nowrap;
        color: hsl(0, 0%, 25%);
        background-color: hsl(0, 0%, 90%);
        transition: background-color 0.1s ease-out;
        cursor: pointer;
        font-weight: normal;
    }
    input[type='file'] + label:hover:not(:disabled) {
        color: hsl(0, 0%, 25%);
        background-color: rgb(162, 162, 162);
    }*/
    .upload-file-button {
        height: 40px;
        width: 120px;
        text-align: center;
        margin: 20px 0;
        border: none;
        border-radius: 5px;
        box-shadow: 0 1px 3px #203F53;
        background-color: #203F53;
        color: white;
        font-size: 15px;
    }
    input:invalid {
        border-color: #00c0ef;
    }
    .osh-msg-box.-success {
        border-color: #00a65a;
        color: #00a65a;
    }
    .osh-msg-box.-danger {
        border-color: red;
        color: red;
    }
    .osh-msg-box {
        border-width: 1px;
        border-style: solid;
        padding: 2px 5px;
        margin: 10px 0;
    }
    @-webkit-keyframes blinker {
        from {opacity: 1.0;}
        to {opacity: 0.0;}
    }
    .blink{
        color: red;
        font-weight:bold;
        text-decoration: blink;
        -webkit-animation-name: blinker;
        -webkit-animation-duration: 0.8s;
        -webkit-animation-iteration-count:infinite;
        -webkit-animation-timing-function:ease-in-out;
        -webkit-animation-direction: alternate;
    }
</style>
<!--<link rel="stylesheet" href="<?php echo nl2br(e(asset('assets_admin/dist/css/form_design.css'))); ?>">-->
<section class="content-header">
    <h1>
        Product
        <small>Add</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Product</a></li>
        <li class="active">Add</li>
    </ol>
</section>

<form name="add_product" id="myform" action="<?php echo nl2br(e(url('/product-save'))); ?>" method="post" enctype="multipart/form-data">
    <?php echo nl2br(e(csrf_field())); ?>

    <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Add Product</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="row">
                        <div class="col-md-3">&nbsp;</div>
                        <div class="col-md-6">
                            <?php if(session('save')): ?>
                            <center>
                                <div class=" osh-msg-box -success"><?php echo nl2br(e(session('save'))); ?>

                                </div>
                            </center>
                            <?php endif; ?>    
                            <?php if(session('error')): ?>
                            <center>
                                <div class=" osh-msg-box -danger">
                                    <span class="blink">Error :: </span>&nbsp;&nbsp;
                                    <?php echo nl2br(e(session('error'))); ?>

                                </div>
                            </center>
                            <?php endif; ?>  
                        </div>
                        <div class="col-md-3">&nbsp;</div>
                        <div class="col-md-6" style="padding-left:50px;">
                            <fieldset style="margin:10px;  margin-top:44px; padding:5px 20px; border:2px solid #00c0ef44">
                                <legend style="padding:5px 20px; text-align:center; width:auto">Item Definitions</legend>
                                <div class="form-group">
                                    <label for="ddlprocat">Main Category</label>
                                    <select type="text" class="form-control"  name="ddlprocat" id="ddlprocat"  required>
                                        <option value="">-- select category --</option>
                                        <?php $__currentLoopData = $procat_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $procat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo nl2br(e($procat->procat_id)); ?>"><?php echo nl2br(e($procat->procat_name)); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="ddlprosubcat1">Sub Category</label>
                                    <select class="form-control" type="text" name="ddlprosubcat1" id="ddlprosubcat1" required>
                                        <option value="">-- select sub category --</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="txtproductname">Product Name</label>
                                    <input class="form-control" type="text" name="txtproductname" id="txtproductname" value="<?php echo nl2br(e(old('txtproductname'))); ?>" required>
                                    <span class="help-text"><?php echo nl2br(e($errors->first('txtproductname'))); ?></span>
                                </div>
                                <!--<div class="form-group">
                                        <label for="txtproductname">Product Barcode</label>
                                        <input class="form-control" type="text" name="product_barcode" id="product_barcode" value="<?php echo nl2br(e(old('product_barcode'))); ?>">
                                        <span class="help-text"><?php echo nl2br(e($errors->first('product_barcode'))); ?></span>
                                </div> --->
                                <div class="form-group">
                                    <label for="txtstyleref">Product Code</label>
                                    <input class="form-control" type="text" id="required-input" name="txtstyleref" id="txtstyleref" required>
                                    <span class="help-text"><?php echo nl2br(e($errors->first('txtstyleref'))); ?></span>
                                </div>
                                <div class="form-group">
                                    <label for="txtprice">Price</label>
                                    <input class="form-control" type="number"  name="txtprice" id="in-range-input" min="1" max="20000"  required>
                                    <span class="help-text"><?php echo nl2br(e($errors->first('txtprice'))); ?></span>
                                </div>
                                <div class="form-group">
                                    <label for="txtpricediscounted">Discount Percentage</label>
                                    <input class="form-control" type="number" name="txtpricediscounted" id="optional-input" min="0" max="200" >
                                    <span class="help-text"></span>
                                </div>
                                <div class="form-group">
                                        <label for="ddlfilter">Price Filter</label>
                                        <select class="form-control" name="ddlfilter" id="optional-input">
                                                <option>-- Select Filter --</option>
                                                <option value="high">High</option>
                                                <option value="medium">Medium</option>
                                                <option value="low">Low</option>
                                        </select>
                                        <span class="help-text"></span>
                                </div>
                                <div class="form-group">
                                    <label for="txtorder">Product Order</label>
                                    <input class="form-control" type="number" name="txtorder" id="optional-input" min="1" max="1000" value="<?php echo $total_product + 1; ?>" readonly>
                                    <span class="help-text"><!-- Only Numbers [ <strong>Total Product Added: <?php echo $total_product; ?></strong> ] ----></span>
                                </div>
                                <div class="form-group">
                                    <label for="check-option-1">Is Special</label>
                                    <div style="padding-left:10px"><input type="radio" name="campaign" id="check-option-1" value="8" style="width:16px;"/> Summer 2019</div>
                                    <div style="padding-left:10px"><input type="radio" name="campaign" id="check-option-1" value="2" style="width:16px;"/> Eid Collection 2019</div>
                                    <span class="help-text"></span>
                                </div>
                                <!--<div class="form-group">
                                    <label>Size Image</label>
                                    <input style="width:100%" type="file"  name="sizeguide_image" class="btn btn-info"/>
                                    <span class="help-text"></span>
                                </div> --->
                                
                                
                                <fieldset style="margin:10px;  margin-top:44px; padding:5px 20px; border:2px solid #00c0ef44">
                                    <legend style="padding:5px 20px; text-align:center; width:auto">Discription</legend>
                                    <div class="form-group">
                                       <label for="txtproductdetails">Product Description</label>
                                       <textarea class="form-control" type="text" name="txtproductdetails" id="optional-input" rows="3"></textarea><?php echo nl2br(e(old('txtproductdetails'))); ?>

                                       <span class="help-text"></span>
                                    </div>
                                    
                                    <div class="mi-product-field">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                   <label for="txtproductdetails">Field Name</label>
                                                   <input class="form-control" type="text" name="field_name[]" id="optional-input" />
                                                   <span class="help-text"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                 <div class="form-group">
                                                   <label for="txtproductdetails">Description</label>
                                                   <textarea class="form-control" type="text" name="description[]" id="optional-input" rows="3"></textarea>
                                                   <span class="help-text"></span>
                                                </div>
                                            </div>
                                       </div>
                                   </div>
                                   
                                   <div id="assain_desirption">
                                       
                                   </div>
                                   <a id="duplicate-field-button" class="btn bg-navy btn-flat btn-sm margin tdata" href="#" target="__blank"><i class="fa fa-fw fa-plus-circle"></i> Add More</a>
                                </fieldset>
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <div class="text-center"><a class="add_style submitbtn btn btn-primary" href="#">Add More Color/Style</a></div>
                            <input type="hidden" name="total_grp" id="myform" class="total_grp" value="">
                            <input type="hidden" name="blank" id="blank" value="">
                            <div class="clone_grp">
                                <div class="product_style">
                                    <div id="right_part">
                                        <div class="formcontainer">
                                            <fieldset style="margin:10px; padding:5px 20px; border:2px solid #00c0ef44">
                                                <legend style="padding:5px; 20px">Product Style <small class="sl"></small>: <a type="button" class="remove_style submitbtn btn btn-warning pull-right"><span class="glyphicon glyphicon-remove"></span> Remove</a></legend>
                                                <div id="size-list">
                                                    <span for="blank" style="display:none;">Available Product Size</span><span style="font-weight:700;padding-top:3px;">Available Product Size for Add </span></br>
                                                    <?php
                                                    $i = 0;
                                                    foreach ($avail_size as $size) {
                                                        $i++;
                                                        ?>
                                                        <span style="border:1px solid #00c0ef33; min-width:100px; display:inline-block; margin:5px; padding:5px 2px;"><input type="checkbox" class="chksize clone_field" rel="size<?php echo $i ?>"  name="size<?php echo $i ?>" value="<?php echo $size->prosize_name; ?>" style="width:39px !important;"/> <span rel="input_size<?php echo $i ?>"  for="input_size<?php echo $i ?>" class="clone_field"><?php echo $size->prosize_name; ?></span></span>
                                                    <?php } ?>
                                                    <a class="btn bg-navy btn-flat btn-sm margin tdata" href="<?php echo nl2br(e(url('/create-size'))); ?>" target="__blank"><i class="fa fa-fw fa-plus-circle"></i> Need More</a>
                                                </div>
                                                <div class="form-group">
                                                    <label>Quantity</label>
                                                    <?php
                                                    $i = 0;
                                                    foreach ($avail_size as $size) {
                                                        $i++;
                                                        ?>
                                                        <div class="form-group" style="margin-bottom:0">
                                                            <label style="display:none"></label>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <input type="text" size="6" rel="input_size<?php echo $i ?>"  name="input_size<?php echo $i ?>" class="clone_field" id="required-input" style="display:none; margin-bottom:15px" placeholder="Qty for <?php echo $size->prosize_name; ?>" value="" />
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <input type="text" size="6" rel="barcode_input_size<?php echo $i ?>"  name="input_barcode<?php echo $i ?>" class="clone_field" id="required-input" style="display:none; margin-bottom:15px" placeholder="Barcode for <?php echo $size->prosize_name; ?>" value="" />
                                                                </div>
                                                            </div>												
                                                        </div>
                                                    <?php } session(['numberofavailabesize' => $i]); ?>
                                                </div>
                                                <div class="form-group">
                                                    <label rel="txtcolorname"  for="txtcolorname" class="clone_field">Color / Style Name</label>
                                                    <input type="text" rel="txtcolorname" name="txtcolorname" class="form-control txtfield clone_field" required>
                                                </div>
                                                <div class="form-group">
                                                    <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Color Thumbnail</label>
                                                    <input style="width:100%" type="file" rel="file_colorthm" name="file_colorthm" class="btn btn-info clone_field" required>
                                                    <small style="color:#06be1c;">[ Image Type: jpeg or jpg, Width: 81 Px Height: 62 PX ]</small>
                                                </div>
                                                <div class="form-group">
                                                    <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 1</label>
                                                    <input style="width:100%" type="file" rel="file_im1" name="file_im2" class="btn btn-info clone_field" required>
                                                    <small style="color:#06be1c;">[ Image Type: jpeg or jpg, Width: 1600 px X Height: 2400 px ]</small>
                                                </div>
                                                <div class="form-group">
                                                    <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 2</label>
                                                    <input style="width:100%" type="file" rel="file_im2" name="file_im2" class="btn btn-info clone_field">
                                                </div>
                                                <div class="form-group">
                                                    <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 3</label>
                                                    <input style="width:100%" type="file" rel="file_im3" name="file_im3" class="btn btn-info clone_field">
                                                </div>
                                                <div class="form-group">
                                                    <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 4</label>
                                                    <input style="width:100%" type="file" rel="file_im4" name="file_im4" class="btn btn-info clone_field">
                                                </div>
                                                <!--<div class="form-group">
                                                    <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 5</label>
                                                    <input style="width:100%" type="file" rel="file_im5" name="file_im5" class="btn btn-info clone_field">
                                                </div>
                                                <div class="form-group">
                                                    <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 6</label>
                                                    <input style="width:100%" type="file" rel="file_im6" name="file_im6" class="btn btn-info clone_field">
                                                </div> --->
                                            </fieldset>
                                        </div>

                                    </div>
                                    <hr>
                                </div>
                            </div>
                            <div class="dynamic">
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <div class="box-footer">
                    <button type="submit" name="btnsubmit" class="submitbtn btn btn-primary pull-right" style="height: 3.5rem;">Add Product</button>
                </div>
            </div>
    </section>
</form>
<script>
</script>
<script>

</script>
<link rel="stylesheet" href="<?php echo nl2br(e(asset('assets_admin/previewForm.css'))); ?>">
<script src="<?php echo nl2br(e(asset('assets_admin/previewForm.js'))); ?>"></script>
<!-- <script type="text/javascript" src="<?php echo nl2br(e(asset('assets_admin/dynamic_select_box.js'))); ?>"></script> --->
<script>
    $('#duplicate-field-button').on('click', function(e){
        e.preventDefault();
        var content = $('.mi-product-field').html();
        $('#assain_desirption').prepend(content);
    })
    $(document).ready(function () {
        $("#ddlprocat").change(function () {
            var procat = $("#ddlprocat").val();
            // alert(procat)
            var url_op = base_url + "/subpro-list/" + procat;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data) {
                    // alert(html);
                    // $('#CityList').html(html);
                    $('#ddlprosubcat1').empty();
                    $('#ddlprosubcat1').append('<option value="">Select Sub Category</option>');
                    $.each(data, function (index, supproobj) {
                        //   $('#ProductSizeList').append('<option value="' + subcatobj.productsize_id + '">' + subcatobj.productsize_size + '</option>');

                        $('#ddlprosubcat1').append('<option value="' + supproobj.subprocat_id + '">' + supproobj.subprocat_name + '</option>');
                    });
                }
            });
			
			var url_op_size = base_url + "/size-list/" + procat;
			$.ajax({
                url: url_op_size,
                type: 'GET',
                success: function (data) {
                     //alert(data);
				 $('#size-list').html(data);
				 var grp = $('.clone_grp').clone(true);
                $('.add_style').click(function () {
                    add_grp();
                    return false;
                });
                add_grp();
                $('.remove_style').click(function () {

                    remove(this);
                    return false;
                });
                function chksize_click(obj) {
                    var input = $(obj).attr('name');
                    //alert('input[name=input_'+ input +']');
                    if ($(obj).is(':checked')) {
                        $('input[name=input_' + input + ']').show();
                        $('input[name=input_' + input + ']').addClass('form-control');
                        $('input[name=barcode_input_' + input + ']').show();
                        $('input[name=barcode_input_' + input + ']').addClass('form-control');
                    } else {
                        $('input[name=input_' + input + ']').hide();
                        $('input[name=input_' + input + ']').val("");
                        $('input[name=barcode_input_' + input + ']').hide();
                        $('input[name=barcode_input_' + input + ']').val("");
                    }

                }
                ;
                function add_grp(obj) {
                    $('.dynamic').append(grp.html());
                    grp_arng();
                    $('.remove_style').unbind("click");
                    $('.remove_style').bind("click", function () {
                        remove(this);
                        return false;
                    });
                    $('.chksize').unbind("change");
                    $('.chksize   ').bind("change", function () {
                        chksize_click(this);
                        return false;
                    });
                }//add add_subgrp
                function remove(obj) {
                    $(obj).parent().parent().parent().parent().fadeOut('slow',
                            function () {
                                $(obj).parent().parent().parent().parent().remove();
                                grp_arng();
                            }
                    );
                }//remove

                function grp_arng() {
                    var i = 0;
                    $('.product_style').each(function () {
                        i++;
                        $(this).find('.sl').html(i);
                        $(this).find('.clone_field').each(function () {
                            var name = $(this).attr('rel');
                            //alert($(this).attr('for'));

                            if ($(this).attr('for') == null) {
                                $(this).attr('name', name + '_' + i);
                            } else {
                                $(this).attr('for', name + '_' + i);
                            }
                        });
                    }); //each clone_field prod    uc    t_st    yle
                    $('.total_grp').attr('value', i);
                }//grp_arng
                $('.clone_grp').remove();
                grp_arng();
                }
            });
        });
    });
    $(document).ready(
            function () {
                $('#isspecial').is(':checked');
                var grp = $('.clone_grp').clone(true);
                $('.add_style').click(function () {
                    add_grp();
                    return false;
                });
                add_grp();
                $('.remove_style').click(function () {

                    remove(this);
                    return false;
                });
                function chksize_click(obj) {
                    var input = $(obj).attr('name');
                    //alert('input[name=input_'+ input +']');
                    if ($(obj).is(':checked')) {
                        $('input[name=input_' + input + ']').show();
                        $('input[name=input_' + input + ']').addClass('form-control');
                        $('input[name=barcode_input_' + input + ']').show();
                        $('input[name=barcode_input_' + input + ']').addClass('form-control');
                    } else {
                        $('input[name=input_' + input + ']').hide();
                        $('input[name=input_' + input + ']').val("");
                        $('input[name=barcode_input_' + input + ']').hide();
                        $('input[name=barcode_input_' + input + ']').val("");
                    }

                }
                ;
                function add_grp(obj) {
                    $('.dynamic').append(grp.html());
                    grp_arng();
                    $('.remove_style').unbind("click");
                    $('.remove_style').bind("click", function () {
                        remove(this);
                        return false;
                    });
                    $('.chksize').unbind("change");
                    $('.chksize   ').bind("change", function () {
                        chksize_click(this);
                        return false;
                    });
                }//add add_subgrp
                function remove(obj) {
                    $(obj).parent().parent().parent().parent().fadeOut('slow',
                            function () {
                                $(obj).parent().parent().parent().parent().remove();
                                grp_arng();
                            }
                    );
                }//remove

                function grp_arng() {
                    var i = 0;
                    $('.product_style').each(function () {
                        i++;
                        $(this).find('.sl').html(i);
                        $(this).find('.clone_field').each(function () {
                            var name = $(this).attr('rel');
                            //alert($(this).attr('for'));

                            if ($(this).attr('for') == null) {
                                $(this).attr('name', name + '_' + i);
                            } else {
                                $(this).attr('for', name + '_' + i);
                            }
                        });
                    }); //each clone_field prod    uc    t_st    yle
                    $('.total_grp').attr('value', i);
                }//grp_arng
                $('.clone_grp').remove();
                grp_arng();
            });
    $(document).ready(function () {
        $('#myform').previewForm();

    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>