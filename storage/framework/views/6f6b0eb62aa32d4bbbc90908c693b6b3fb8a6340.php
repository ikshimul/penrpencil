<?php $__env->startSection('title','Manage sub Category'); ?>
<?php $__env->startSection('content'); ?>
<section class="content-header">
    <h1>
        Category
        <small>Manage Sub Category</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Category</a></li>
        <li class="active">Manage Sub Category</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Browse All Product Sub-Category</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php if(session('error')): ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i> Deleted!</h4>
                        <?php echo nl2br(e(session('error'))); ?>

                    </div>
                    <?php endif; ?>
                    <!--<form name="singleform" action="#" method="get">
                        <div class="form-group" style="width:50%;">
                            <label>Select Category</label>
                            <div class="input-group">
                                <select name="pcat"  class="form-control select2" style="width: 100%;" onChange="form.submit();">
                                    <option selected="selected" value="0">Select Category</option>
                                    <?php $__currentLoopData = $main_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $main): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo $main->procat_id; ?>"><?php echo $main->procat_name; ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <div class="input-group-addon">
                                    <input style="background-color: white; border: none;" type="submit" name="btnsubmit" value="Search"  /> 
                                </div>
                            </div>
                        </div>
                    </form>    --->
                    <div class="table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Product Category</th>
                                    <th>Product Sub Category</th>
                                    <th>Image</th>
                                    <th>Order</th>
<!--                                    <th>Last Update</th>-->
                                    <th style="text-align:center;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $sub_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td style="width:2%;"><?php echo nl2br(e($sub->subprocat_id)); ?></td>
                                    <td><?php echo $sub->subprocat_name; ?></td>
                                    <td><?php echo $sub->procat_name; ?></td>
                                    <td><img class="thumbnail  img-responsive" src="<?php echo nl2br(e(URL::to(''))); ?>/storage/app/productbanner/subcategory/<?php echo $sub->subprocat_img; ?>" alt="<?php echo $sub->subprocat_name; ?>" /></td>
                                    <td><?php echo $sub->subprocat_order; ?></td>
                                    <td>
                                        <a class="btn bg-olive btn-flat btn-sm margin tdata" href="#">Edit</a> 
                                        <a class="btn btn-danger btn-flat btn-sm tdata" href="#" onclick='confirm_delete("<?php echo nl2br(e(url("/delete-subcat/{$sub->subprocat_id}"))); ?>")'>Delete</a>
                                    </td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div> 
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- Ajax modal ---->
<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="text-align:center;color:black;">Are you sure to delete this?</h4>
            </div>
            <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                <a href="#" class="btn btn-sm btn-danger" id="delete_link">Delete</a>
                <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!--- Ajax modal end ---->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>