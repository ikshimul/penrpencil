<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Bkash Payment Receive Confirmation</title>
    </head>
    <style>
         body{
            font-family: myriad-pro;
        }
        p{
            font-family: myriad-pro;
        }
        .im {
            color: black;
          }
    </style>
    <body>
    <div>
        <table style="width: 100%;">
          <tr>
            <td></td>
            <td bgcolor="#FFFFFF ">
              <div style="padding: 15px; max-width: 600px;margin-left:5px ;display: block; border-radius: 0px;padding: 0px; border: 1px solid #291d88;">
                <table style="width: 100%;background: #291d88;">
                  <tr>
                    <td></td>
                    <td>
                      <div>
                        <table width="100%">
                          <tr>
                            <td rowspan="2" style="text-align:center;padding:2px;">
    							<img style="float:left;height:46px;"  src="http://pride-limited.com/storage/app/public/img/logo.png" /> 
    							
    							<span style="color:white;float:right;font-size: 13px;font-style: italic;margin-top: 18px; padding:0px; font-size: 14px; font-weight:normal;font-family: myriad-pro;">
    							A Pride <sup>&reg;</sup> Group Venture<span></span></span></td>
                          </tr>
                        </table>
                      </div>
                    </td>
                    <td></td>
                  </tr>
                </table>
                <table style="padding: 10px;font-size:14px; width:100%;">
                  <tr>
                    <td style="padding:10px;font-size:14px; width:100%;">
                        <p>Dear <?php echo $registeruser_firstname . ' ' . $registeruser_lastname; ?>,</p>
                        <p><br /> Thank you for sending us the payment for your order <a href="https://pride-limited.com/track-order" target="_blank"><?php echo $conforder_tracknumber;?></a> via bKash mobile payment service.We will now process your order for delivery.Please expect your order within 2-5 business days.</p>
                        <p>To track your order please  <a href="https://pride-limited.com/track-order" target="_blank">click here</a>
                        </p>
                        <p>Thanks for shopping with us!.</p>
                        <p>Pride Limited Customer Support,</p>
                        <p>Pride Limited Team.</p>
                     </td>
                  </tr>
    			  <tr>
    			  <td>
    				 <div align="center" style="font-size:12px; margin-top:20px; padding:5px; width:100%; background:#eee;">
                        © 2018 <a href="{{url('/')}}" target="_blank" style="color:#333; text-decoration: none;">pride-limited.com</a>
                      </div>
                    </td>
    			  </tr>
            </table>
        </div>
    </body>
</html>