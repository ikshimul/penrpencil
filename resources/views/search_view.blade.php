<?php
use App\Http\Controllers\product\ProductController;
?>
@extends('layouts.app')
@section('title','Search Result')
@section('content')

<style>
    .nav-block {
        position: relative;
        left: 0;
        right: 0;
        top: 100%;
        background: #fff;
        -webkit-box-shadow: 0 0 1px 0 rgba(127, 127, 127, 0.2);
        box-shadow: 0 0 1px 0 rgba(127, 127, 127, 0.2);
    }
    .icon-down:before {
        content: "\f107";
        font-family: FontAwesome;
        font-size: 15px;
    }
    .product-items .product-item-photo .product-image-wrapper {
        display: block;
        overflow: hidden;
        position: relative;
        border: 0px solid #00000012;
    }
    /* Pagination style */
	.pagination>li>a, .pagination>li>span {
		border-radius:50%;
		margin-left:20px;
		border:1px solid #FFF;
		color:#000;
	}
	.pagination>li:first-child>a, .pagination>li:first-child>span {
		border-radius:50%;
		border:1px solid #000;
	}
	.pagination>li:last-child>a, .pagination>li:last-child>span {
		border-radius:50%;
		border:1px solid #000;
	}
	.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
		background-color:#291d88;
		border-color:#291d88;
	}
</style>
<style>
  .custom-carousel {
	  position:relative;
	  display:inline-block;
	  width:100%;
	  padding-bottom:150%;
	  border: 1px solid #eee;
  }
  .custom-carousel .item{
	  opacity:0;
	  -webkit-transition:opacity 1s;
	  transition:opacity 1s;
	  position:absolute;
	  width:100%;
	  height:100%;
	  left:0;
  }
  .custom-carousel .item.active {
	  opacity:1;
  }
  /*Active filering */
  .sidebar .filter-holder ul li .m-filter-item-list li a.active:after {
    background:#000;
  }
</style>
<main id="maincontent" class="page-main">
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container"><div class="row"><div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Search Title</a>
                            </li>
                            <li class="item">
                                <a href="{{url("/search-view/{$key}")}}" title=""><?php echo $key;?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="sidebar">
                    <div class="block block-banner">
                        <div class="block-content">
                            <a href="#" class="opener-filter">filters</a>
                            <div class="filter-holder">
                                <div class="filter-content">
                                    <ul class="list-inline">
                                        <!--<li>
                                            <a class="opener-cate" href="#"> Category <span class="icon-down"></span></a> 
                                            <div class="filter-block">

                                                <ol class="m-filter-item-list">
                                                    
                                                </ol>
                                            </div>
                                        </li>-->
                                        <li id='filter-price'>
                                            <a class="opener-cate" href="#"> Price <span class="icon-down"></span></a>
                                            <div class="filter-block">
                                                <?php
                                                    if(!isset($lower_price))
                                                        $lower_price = 0;
                                                    if(!isset($upper_price))
                                                        $upper_price = 0;
                                                ?>
                                                <ol class="m-filter-item-list">
                                                    <li class="item"><a class="<?php if($lower_price == 500 && $upper_price == 1000) echo 'active'; ?>" href="{{ url("search-by-price/{$key}/500/1000") }}"><span>Tk 500</span> - <span>Tk 1,000</span></a></li>
                                                    <li class="item"><a class="<?php if($lower_price == 1000 && $upper_price == 1500) echo 'active'; ?>" href="{{ url("search-by-price/{$key}/1000/1500") }}"><span>Tk 1,000</span> - <span>Tk 1,500</span></a></li>
                                                    <li class="item"><a class="<?php if($lower_price == 1500 && $upper_price == 2000) echo 'active'; ?>" href="{{ url("search-by-price/{$key}/1500/2000") }}"><span>Tk 1,500</span> - <span>Tk 2,000</span></a></li>
                                                    <li class="item"><a class="<?php if($lower_price == 2000 && $upper_price == 2500) echo 'active'; ?>" href="{{ url("search-by-price/{$key}/2000/2500") }}"><span>Tk 2,000</span> - <span>Tk 2,500</span></a></li>
                                                    <li class="item"><a class="<?php if($lower_price == 2500 && ($upper_price == 0 || $upper_price == 99999)) echo 'active'; ?>" href="{{ url("search-by-price/{$key}/2500") }}"><span>Tk 2,500</span> and above</a></li>
                                                </ol>
                                            </div>
                                        </li>
                                        <li id='filter-size'>
                                            <a class="opener-cate" href="#"> Size <span class="icon-down"></span></a>
                                            <div class="filter-block">
                                                <?php
                                                    if(!isset($size))
                                                        $size = null;
                                                ?>
                                                <ol class="m-filter-item-list">
                                                    <li class="item"><a class="<?php if($size == '38') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/38") }}"><span>38</span></a></li>
                                                    <li class="item"><a class="<?php if($size == '40') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/40") }}"><span>40</span></a></li>
                                                    <li class="item"><a class="<?php if($size == '42') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/42") }}"><span>42</span></a></li>
                                                    <li class="item"><a class="<?php if($size == '44') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/44") }}"><span>44</span></a></li>
                                                    <li class="item"><a class="<?php if($size == '46') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/46") }}"><span>46</span></a></li>
                                                    <li class="item"><a class="<?php if($size == '48') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/48") }}"><span>48</span></a></li>
                                                    <li class="item"><a class="<?php if($size == 'XS') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/XS") }}"><span>XS</span></a></li>
                                                    <li class="item"><a class="<?php if($size == 'S') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/S") }}"><span>S</span></a></li>
                                                    <li class="item"><a class="<?php if($size == 'M') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/M") }}"><span>M</span></a></li>
                                                    <li class="item"><a class="<?php if($size == 'L') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/L") }}"><span>L</span></a></li>
                                                    <li class="item"><a class="<?php if($size == 'XL') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/XL") }}"><span>XL</span></a></li>
                                                    <li class="item"><a class="<?php if($size == 'XXL') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/XXL") }}"><span>XXL</span></a></li>
                                                </ol>
                                            </div>
                                        </li>
                                        <li id='filter-color'>
                                            <a class="opener-cate" href="#"> Color <span class="icon-down"></span></a>
                                            <div class="filter-block">
                                                <?php
                                                    if(!isset($color_name))
                                                        $color_name = null;
                                                    if(!isset($color_names))
                                                        $color_names = [];
                                                ?>
                                                <ol class="m-filter-item-list">
                                                    @foreach($color_names as $color)
                                                    <li class="item"><a class="<?php if($color_name == $color) echo 'active'; ?>" href="{{ url("search-by-color/{$key}/{$color}") }}"><span>{{ $color }}</a></li>
                                                    @endforeach
                                                </ol>
                                            </div>
                                        </li>

                                    </ul>
                                </div>   
                            </div>



                        </div>
                    </div>



                </div>
            </div>
            <div class="col-md-10">
                <input name="form_key" type="hidden" value="" />
                <div id="authenticationPopup" data-bind="scope:'authenticationPopup'" style="display: none;">
                </div>



                <style>
                    .product-items .product-item-photo .product-image-wrapper {
                        display: block;
                        overflow: hidden;
                        position: relative;
                        border: 0px solid #00000012;
                    }
                </style>
                <div class="products wrapper grid products-grid">
                    <ol class="products list items product-items">
                        @foreach($search as $product)
                        <?php
                        $product_name = str_replace(' ', '-', $product->product_name);
                        $product_url = strtolower($product_name);
                        $color_album=str_replace('/','-',$product->productalbum_name);
                        $data = ProductController::GetProductColorAlbum($product->product_id);
                        // dd($data);
                        $sold_out = ProductController::ProductWiseQty($product->product_id);
                        foreach ($data as $pro_album) {
                            $colorwiseimg = ProductController::GetProductImageByColorAlbum($pro_album->productalbum_id);
                        }
                        ?>
                        <li class="item product product-item">                                
                            <div class="product-item-info" data-container="product-grid">
                                <a href="{{url("shop/{$product_url}/color-{$color_album}/{$product->product_id}")}}">
                                    <span class="product-image-container">
                                        <span class="product-image-wrapper" >
                                            <span class="custom-carousel" onmouseenter="fadeImages(this)" onmouseleave="removeTimer(this)">
                                                <?php $images = ProductController::productImages($pro_album->productalbum_id); ?>
                                                @php($i = 0)
                                                @foreach($images as $image)
                                                <img class="item large_img<?php if($i==0) echo ' active';?>" src="{{ URL::to('') }}/storage/app/public/pgallery/{{ $image->productimg_img }}" alt="No Image Found"/>
                                                @php($i++)
                                                @endforeach
                                            </span>
                                        </span>
                                    </span>
                                </a>

                                <div class="product details product-item-details">
                                    <div class="info-holder">
                                        <strong class="product name product-item-name">
                                            <a class="product-item-link"
                                               href="{{url("shop/{$product_url}/color-{$color_album}/{$product->product_id}")}}">
                                                {{$product->product_name}}
                                            </a>
                                        </strong>

                                    </div>
                                    <div class="info-holder">
                                        <div class="price-box price-final_price" data-role="priceBox" data-product-id="79644" data-price-box="product-id-79644">
                                            <span class="normal-price">
                                                <span class="price-container price-final_price tax weee"
                                                      >
                                                    <!--        <span class="price-label">As low as</span>-->
                                                    <span  id="product-price-79644"                data-price-amount="{{$product->product_price}}"
                                                           data-price-type="finalPrice"
                                                           class="price-wrapper "
                                                           >
                                                        <span>Tk,&nbsp;{{$product->product_price}}</span>    </span>
                                                </span>
                                            </span>
                                        </div>                                                        
                                        <div class="product-item-inner">
                                            <div class="product actions product-item-actions">
                                                <div class="actions-primary">
                                                    <form data-role="tocart-form" action="#" method="post">
                                                        <input type="hidden" name="product" value="">
                                                        <input type="hidden" name="uenc" value="">
                                                        <input name="form_key" type="hidden" value="" />                                                
                                                        <a href="#" class="action tocart primary"><span>Shop Now</a>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ol>
                </div>
                <div class="toolbar toolbar-products" data-mage-init='{""}'>
                    <div class="modes"></div>
                    <p class="toolbar-amount" id="toolbar-amount">
                        Items <span class="toolbar-number">1</span>-<span class="toolbar-number">18</span> of <span class="toolbar-number">42</span>    </p>
                    <div class="pages">
					     <center>  {{{ $search->links() }}} </center>
                    </div>
                   <!-- <div class="field limiter">
                        <label class="label" for="limiter">
                            <span>Show</span>
                        </label>
                        <div class="control">
                            <select id="limiter" data-role="limiter" class="limiter-options">
                                <option value="9">
                                    9                </option>
                                <option value="18"                    selected="selected">
                                    18                </option>
                                <option value="45">
                                    45                </option>
                            </select>
                        </div>
                        <span class="limiter-text">per page</span>
                    </div>

                    <div class="toolbar-sorter sorter">
                        <label class="sorter-label" for="sorter">Sort By</label>
                        <select id="sorter" data-role="sorter" class="sorter-options">
                            <option value="">Sort By</option>
                            <option value="position"
                                    >
                                Position            </option>
                            <option value="name"
                                    >
                                Product Name            </option>
                            <option value="price"
                                    >
                                Price            </option>
                            <option value="created_at"
                                    selected="selected"
                                    >
                                Date            </option>
                        </select>
                        <a title="Set Ascending Direction" href="#" class="action sorter-action sort-desc" data-role="direction-switcher" data-value="asc">
                            <span>Set Ascending Direction</span>
                        </a>
                    </div> --->
                </div>
            </div>
        </div>
    </div>
</main>
<script>
var timer1;
var timer2;
var counter=1;
var running = false;
(function() {
	
})();
function updateActive(items, i) {
	i = i%(items.length);
	items[i>0?i-1:items.length-1].classList.remove('active');
	items[i].classList.add('active');
	counter++;
	if(!running) {
    	timer2 = setInterval(function() {
    		updateActive(items, counter);
    	}, 1500);
    	running = true;
    	clearInterval(timer1);
	}
}
function fadeImages(element) {
	var items = element.getElementsByClassName('item');
	for (var i = 0; i < items.length; i++) {
		items[i].classList.remove('active');
	}
	items[1].classList.add('active');
	timer1 = setInterval(function() {
		updateActive(items, counter);
	}, 100);
}
function removeTimer(element) {
    clearInterval(timer1);
	clearInterval(timer2);
	counter=1;
	var items = element.getElementsByClassName('item');
	for (var i = 0; i < items.length; i++) {
		items[i].classList.remove('active');
	}
	items[0].classList.add('active');
	running = false;
}

/* Open filter category */
(function(){
    <?php if(!((isset($lower_price) && $lower_price !=0) || (isset($upper_price) && $upper_price !=0) || (isset($size) && $size != null) || (isset($color_name) && $color_name != null)))  {?>
        document.getElementById('filter-category').classList.add('active');
    <?php } elseif((isset($lower_price) || isset($upper_price)) && ($lower_price != 0) && ($upper_price != 0)) { echo 'console.log("'.$lower_price.' and '.$upper_price.'");';?>
        document.getElementById('filter-price').classList.add('active');
    <?php } elseif(isset($size) && $size != null) { ?>
        document.getElementById('filter-size').classList.add('active');
    <?php } elseif(isset($color_name) && $color_name != null) { ?>
        document.getElementById('filter-color').classList.add('active');
    <?php }?>
})();
</script>
@endsection