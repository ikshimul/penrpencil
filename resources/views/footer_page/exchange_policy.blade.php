@extends('layouts.app')
@section('title','Pride Limited | A Pride Group venture')
@section('content')
<style type="text/css">
    div.aboutus_mother_container2{
        width: 100%;
        height: auto;
        background-color: #FFFFFF;
        /*opacity:0.3;
        filter:alpha(opacity=30); /* For IE8 and earlier */
        min-height: 450px;
        text-align:center;
        color:#000000;
        font-size:16px;
    }
    .h1 {
        font-size: 24px;
    }
    .h2 {
        text-align:justify;
        font-size: 18px;
    }
    .top-heading{
        font-weight: 600;
        padding-top: 17px;
        padding-bottom: 30px;
        font-size: 19px;
        text-align: justify;
    }
    .heading{
        display:block;
        font-weight:600;
    }
    .body-letter{
        padding-left:10px;
        text-align: justify;
        /*line-height: 2.26; */
    }
    .module, .module-small {
        position: relative;
        padding: 10px 0;
        background-repeat: no-repeat;
        background-position: 50% 50%;
        background-size: cover;
    }
    p, ol, ul, blockquote {
        margin: 0 0 13px;
    }
    .padding-top{
        padding-top: 10px;
    }
</style>
<div class="container bg-grey" style="padding-top:70px;color:black;" id="search_result">
    <main class="main">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="layout">
                        <div class="module section">
                            <div class="three modules">
                               <!---
                                <h2 class="top-heading"> EXCHANGE POLICY</h2>
                                <p class="body-letter padding-top">
                                    Please keep in mind that products may show minor difference than photographs in real life.Pride Limited does not refund any products.However, we do exchange products if We have sent you the wrong product, size or color Faulty product. The exchange must be made within 15 day's of purchase and the product, product package and the hangtag must be intact.The original invoice must be presented during exchange. Call our customer service to exchange the product.
                                    You can return your items through courier service to our e-commerce warehouse at the following address and then we will ship the right sized/colored product to you.E-Commerce Warehouse Pride Group Level 3, Mirandel, House 3, Road 5, Block J, Baridhara,Dhaka, Bangladesh. We also encourage you to check your product instantly when the delivery is made. 
                                </p>
                                
                                <p class="body-letter">
                                    Our delivery man will wait a few minutes for you to check the product and if there are any issues, we urge you to take it up with the customer service and resolve immediately.
                                    This exchange policy is at the management's disposal and may be subject to change.
                                </p>
                               -->
                                <h2 class="top-heading">HOW TO MAKE AN EXCHANGE ONLINE</h2>
                                <span class="heading">DHAKA</span>
                                <p class="body-letter padding-top">
                                    You have 15 days from the shipping date to exchange your urban-truth.com purchase for which the standard delivery  will be required, payable in cash or bKash. You can exchange your item’s sizes or colors.
                                </p>
                                <p class="body-letter">
                                    If you prefer to exchange it for a different item, make a new purchase online, and return previous item to the delivery person. The hangtag, barcode and receipt must be intact, and the product condition must be same as when you received it, for all cases.
                                </p>
                                <p class="body-letter">
                                    If a mistake is made on the part of our Shipping & Logistics,such as a wrong size, color or item was sent than the one you have ordered, the exchange is free of delivery cost. 
                                </p>
                                <p class="body-letter" style="font-weight: 600;text-transform: uppercase;">
                                    Visit an Pride Limited Store that carries the same department from which your item comes and bring your printed or electronic receipt with you.
                                    You can exchange the item in store for free. 
                                </p>
                                <p class="body-letter">
                                    If you choose to request an exchange through urban-truth.com, access your order and select the item you wish to return and the size for which you want to exchange it. You have 14 days from the exchange request to return your merchandise. If this period has passed and we have not received the original items, you will be charged for the amount of the items from the second shipment via your original payment method.
                                </p>
                                <span class="heading">REMEMBER</span>
                                <p class="body-letter padding-top" style="font-weight: 600;text-transform: uppercase;">
                                    Only one exchange request can be done at a time for the same order. You may add up to 5 items per exchange request.
                                </p>
                                <p class="body-letter" style="font-weight: 600;text-transform: uppercase;">
                                    The exchange option will not be visible if the purchased item is no longer available.
                                </p>
                                <p class="body-letter">
                                    Online exchanges are not available for purchases made with a gift card.
                                </p>
                                <p class="body-letter">
                                    Any product purchased in stores cannot be exchanged with a product carried online.
                                </p>
                                <p class="body-letter" style="font-weight: 600;text-transform: uppercase;">
                                    If you are registered under the PRIDE INSIDER REWARDS Loyalty Program, your points will get adjusted with your exchange of products. 
                                </p>
                                <p class="body-letter" style="font-weight: 600;text-transform: uppercase;">
                                    Any purchase for which you have redeemed your points and/or received discount will not be eligible for an exchange. 
                                </p>
                                <p class="body-letter">
                                    Please see our PRIDE INSIDER LOYALTY PROGRAM Terms and Conditions for more information. <a target="__blank" href="{{url('/loyalty-terms-&-conditions')}}" style="color:blue;">CLick Here</a>
                                </p>
                                <span class="heading">OUT OF DHAKA</span>
                                <p class="body-letter padding-top">
                                    We currently do not offer any online exchanges for out of Dhaka city deliveries.
                                </p> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

@endsection