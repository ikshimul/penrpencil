@extends('layouts.app')
@section('title','Pride Limited | A Pride Group venture')
@section('content')
<style>
    .help-center-topic-list._block.-list {
        padding: 25px 0;
    }
    .help-center-topic-list._title {
        margin-bottom: 12px;
        font-size: 24px;
        font-weight: 300;
        padding-left:2px;
        color: #202020;
    }
    .help-center-topic-list._block.-list .help-center-topic-list._link {
        height: 58px;
    }
    .help-center-topic-list._link {
        -webkit-transition: all .35s ease-in-out;
        transition: all .35s ease-in-out;
        display: block;
        position: relative;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        border-radius: 4px;
        border: 1px solid #dadada;
    }
    .help-center-topic-list._block.-list .help-center-topic-list._icon-wrapper {
        left: -1px;
        top: -1px;
        height: 58px;
        width: 58px;
    }
    .help-center-topic-list._icon-wrapper {
        position: absolute;
    }
    .help-center-topic-list._block.-list .help-center-topic-list._icon {
        max-width: 30px;
        max-height: 30px;
    }
    .help-center-topic-list._icon {
        position: absolute;
        left: 50%;
        top: 50%;
        -webkit-transform: translate(-50%,-50%);
        transform: translate(-50%,-50%);
    }
    .help-center-topic-list._block.-list .help-center-topic-list._name {
        position: absolute;
        top: 50%;
        -webkit-transform: translateY(-50%);
        transform: translateY(-50%);
        font-size: 16px;
        width: calc(100% - 46px);
        height: 30px;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
        line-height: 30px;
        left: 58px;
    }
    .help-center-topic-list._block.-list .help-center-topic-list._item:not(:first-child) {
        margin-top: 12px;
    }
    .help-center-chat._title {
        font-size: 24px;
        font-weight: 300;
        color: #202020;
        margin-bottom: 12px;
        line-height: 29px;
    }
    .help-center-chat._advice {
        color: #202020;
        font-weight: 300;
        margin: 0 0 14px;
        line-height: 19px;
        font-size: 14px;
    }
    .help-center-chat._buttons {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        font-size: 0;
    }
    .help-center-chat._button.-ghost {
        border: 1px solid #291d88;
    }
    .help-center-chat._button.-blue {
        background-image: -webkit-gradient(linear,left top,left bottom,from(#1a9cb7),to(#1692ac));
        background-image: linear-gradient(180deg,#1a9cb7,#1692ac);
    }
    .help-center-chat._button-text {
        color: #fff;
        text-transform: uppercase;
        font-size: 14px;
        display: table-cell;
        vertical-align: middle;
        padding: 2px 10px;
        height: 40px;
    }
    .help-center-chat._button.-ghost {
        border: 1px solid #291d88;
    }
    .help-center-chat._button.-ghost .help-center-chat._button-text {
        color: #291d88;
        padding: 1px 9px;
        height: 40px;
    }
    .collapse_desgin {
        padding-top: 65px;
        padding-left: 4px;
    }
    .collapse-border {
        border: 1px solid #dadada;
        padding: 0px;
    }
    .carousel-control-next, .carousel-control-prev {
        position: absolute;
        top: 0;
        bottom: 0;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        width: 15%;
        color: black;
        text-align: center;
        opacity: 1;
    }
    .active{
        color: white !important;
        background: #291d88;
    }
    a{
        cursor: pointer;
    }
    h4 {
        font-size: 1.125rem;
        padding-left: 7px;
        padding-top: 7px;
        color: black;
    }
</style>
<main id="maincontent" class="page-main" style="padding-top: 62px;"><a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item cms_page">
                                <strong>How to Order</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container start -->
    <div class="container">
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    <?php
                    $useragent = $_SERVER['HTTP_USER_AGENT'];
                    if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
                        ?>
                        <style>
                            .collapse_desgin {
                                padding-top: 10px;
                                padding-left: 4px;
                            }
                        </style>
                        <div class="row">
                            <div class="col-md-12">  
                                <div class="col-md-1"></div>                     
                                <div class="col-md-10">
                                    <div class="panel-group" id="accordion">
                                        <div class="help-center _sidebar">
                                            <div class="help-center-topic-list _block -list">
                                                <h3 class="help-center-topic-list _title">How to Order</h3>
                                                <ul class="help-center-topic-list _items">
                                                    <li class="help-center-topic-list _item">
                                                        <a  class="showSingle accordion-toggle help-center-topic-list _link" target="1"  id="div1" >
                                                            <span class="help-center-topic-list _icon-wrapper">
                                                                <img class="help-center-topic-list _icon" src="{{url('/')}}/storage/app/public/img/faq/printing.svg" alt="Select Products">
                                                            </span>
                                                            <span class="help-center-topic-list _content">
                                                                <span class="help-center-topic-list _name">Select product</span>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <div id="collapse1" class="panel-collapse collapse">
                                                        <div class="collapse_desgin">
                                                            <div class="collapse-border">
                                                                <div class="targetDiv panel-body" id="div1">
                                                                    <h4>1. Select Product</h4>
                                                                    <video width="100%" height="100%" id="introVideo" class="vjs-16-9" autoplay loop muted>
                                                                        <source type="video/mp4" src="{{ URL::to('') }}/storage/app/public/img/faq/How to Select product On E-commerce Site.mp4" />
                                                                    </video>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <li class="help-center-topic-list _item">
                                                        <a class="showSingle accordion-toggle help-center-topic-list _link" target="2" id="div2">
                                                            <span class="help-center-topic-list _icon-wrapper">
                                                                <img class="help-center-topic-list _icon" src="{{url('/')}}/storage/app/public/img/faq/buy.svg" alt="Order"></span>
                                                            </span>
                                                            <span class="help-center-topic-list _content">
                                                                <span class="help-center-topic-list _name">Place order</span>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <div id="collapse2" class="panel-collapse collapse">
                                                        <div class="collapse_desgin">
                                                            <div class="collapse-border">
                                                                <div class="panel-body">
                                                                    <h4>2. Place order</h4>
                                                                    <video width="100%" height="100%" id="introVideo" class="vjs-16-9" autoplay loop muted>
                                                                        <source type="video/mp4" src="{{ URL::to('') }}/storage/app/public/img/faq/2.mp4" />
                                                                    </video>
                                                                </div>
                                                            </div> 
                                                        </div> 
                                                    </div>
                                                    <li class="help-center-topic-list _item">
                                                        <a class="showSingle accordion-toggle help-center-topic-list _link" target="3" id="div3">
                                                            <span class="help-center-topic-list _icon-wrapper">
                                                                <img class="help-center-topic-list _icon" src="{{url('/')}}/storage/app/public/img/faq/registration.svg" alt="Products &amp; Prices">
                                                            </span>
                                                            <span class="help-center-topic-list _content">
                                                                <span class="help-center-topic-list _name">Registration</span>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <div id="collapse3" class="panel-collapse collapse">
                                                        <div class="collapse_desgin">
                                                            <div class="collapse-border">
                                                                <div class="panel-body">
                                                                    <h4>3. Login or Register</h4>
                                                                    <video width="100%" height="100%" id="introVideo" class="vjs-16-9" autoplay loop muted>
                                                                        <source type="video/mp4" src="{{ URL::to('') }}/storage/app/public/img/faq/3.mp4" />
                                                                    </video>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <li class="help-center-topic-list _item">
                                                        <a class="showSingle accordion-toggle help-center-topic-list _link" target="4" id="div4">
                                                            <span class="help-center-topic-list _icon-wrapper">
                                                                <img class="help-center-topic-list _icon" src="{{url('/')}}/storage/app/public/img/faq/pay.svg" alt="payment"></span>
                                                            <span class="help-center-topic-list _content">
                                                                <span class="help-center-topic-list _name">Payment</span>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <div id="collapse4" class="panel-collapse collapse">
                                                        <div class="collapse_desgin">
                                                            <div class="collapse-border">
                                                                <div class="panel-body">
                                                                    <h4>4. Confirm Payment Method</h4>
                                                                    <video width="100%" height="100%" id="introVideo" class="vjs-16-9" autoplay loop muted>
                                                                        <source type="video/mp4" src="{{ URL::to('') }}/storage/app/public/img/faq/4.mp4" />
                                                                    </video>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <li class="help-center-topic-list _item">
                                                        <a class="showSingle accordion-toggle help-center-topic-list _link" target="5" id="div5">
                                                            <span class="help-center-topic-list _icon-wrapper">
                                                                <img class="help-center-topic-list _icon" src="{{url('/')}}/storage/app/public/img/faq/delivery-truck.svg" alt="Delivery"></span>
                                                            <span class="help-center-topic-list _content">
                                                                <span class="help-center-topic-list _name">Delivery</span>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <div id="collapse5" class="panel-collapse collapse">
                                                        <div class="collapse_desgin">
                                                            <div class="collapse-border">
                                                                <div class="panel-body">
                                                                    <h4>5. Delivery</h4>
                                                                    <p style="text-align:justify;padding: 11px;font-size: 11pt;">Our customer support will be in touch with you in 24-36 hours to confirm your order.<br><br>

                                                                        <span style="font-weight:bold;">Delivery times:</span> <br>
                                                                        We require 2-3 business days to deliver within Dhaka, and 3-5 days to deliver outside Dhaka. Same day deliveries within are possible once customer support is able to confirm your orders before 2pm on a work day. <br><br>

                                                                        <span style="font-weight:bold;">Delivery charges:</span> <br>
                                                                        For Dhaka, it is Tk 70. For locations out of Dhaka, it is Tk 150. If you shop Tk 3000 or more, shipping is free! <br>

                                                                        If you have any more questions, <br>
                                                                        you can contact us <a href="{{url('/contact-us')}}" style="color:blue;">here.</a></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ul>
                                            </div>
                                            <div class="help-center-chat _block">
                                                <div class="help-center-chat _container">
                                                    <div class="help-center-chat _advice">Our customer support is available Saturday-Thursday from<br>11am-8pm.<br>Click <a style="color:blue;text-decoration:underline;" target="__blank" href="{{url('/contact-us')}}">here</a> to contact us</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="row" id="accordion">
                            <div class="col-md-4 col-xs-12 col-sm-4 col-lg-4">
                                <div class="help-center _sidebar">
                                    <div class="help-center-topic-list _block -list">
                                        <h3 class="help-center-topic-list _title">How to Order</h3>
                                        <ul class="help-center-topic-list _items">
                                            <li class="help-center-topic-list _item">
                                                <a class="showSingle accordion-toggle help-center-topic-list _link" target="1"  id="div1">
                                                    <span class="help-center-topic-list _icon-wrapper">
                                                        <img class="help-center-topic-list _icon" src="{{url('/')}}/storage/app/public/img/faq/printing.svg" alt="Select product">
                                                    </span>
                                                    <span class="help-center-topic-list _content">
                                                        <span class="help-center-topic-list _name">Select product</span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li class="help-center-topic-list _item">
                                                <a class="showSingle accordion-toggle help-center-topic-list _link" target="2"  id="div2">
                                                    <span class="help-center-topic-list _icon-wrapper">
                                                        <img class="help-center-topic-list _icon" src="{{url('/')}}/storage/app/public/img/faq/buy.svg" alt="Order"></span>
                                                    <span class="help-center-topic-list _content"><span class="help-center-topic-list _name">Place order</span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li class="help-center-topic-list _item">
                                                <a class="showSingle accordion-toggle help-center-topic-list _link" target="3"  id="div3">
                                                    <span class="help-center-topic-list _icon-wrapper">
                                                        <img class="help-center-topic-list _icon" src="{{url('/')}}/storage/app/public/img/faq/registration.svg" alt="Products &amp; Prices">
                                                    </span>
                                                    <span class="help-center-topic-list _content">
                                                        <span class="help-center-topic-list _name">Registration</span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li class="help-center-topic-list _item">
                                                <a class="showSingle accordion-toggle help-center-topic-list _link" target="4"  id="div4">
                                                    <span class="help-center-topic-list _icon-wrapper">
                                                        <img class="help-center-topic-list _icon" src="{{url('/')}}/storage/app/public/img/faq/pay.svg" alt="payment"></span>
                                                    <span class="help-center-topic-list _content">
                                                        <span class="help-center-topic-list _name">Payment</span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li class="help-center-topic-list _item">
                                                <a class="showSingle accordion-toggle help-center-topic-list _link" target="5"  id="div5">
                                                    <span class="help-center-topic-list _icon-wrapper">
                                                        <img class="help-center-topic-list _icon" src="{{url('/')}}/storage/app/public/img/faq/delivery-truck.svg" alt="Delivery"></span>
                                                    <span class="help-center-topic-list _content">
                                                        <span class="help-center-topic-list _name">Delivery</span>

                                                    </span>
                                                </a>
                                            </li>
                                            <!--<li class="help-center-topic-list _item">
                                                <a class="accordion-toggle help-center-topic-list _link" data-toggle="collapse" data-parent="#accordion" href="#return">
                                                    <span class="help-center-topic-list _icon-wrapper">
                                                        <img class="help-center-topic-list _icon" src="{{url('/')}}/storage/app/public/img/faq/return.svg" alt="Returns &amp; Refunds"></span>
                                                    <span class="help-center-topic-list _content">
                                                        <span class="help-center-topic-list _name">Returns &amp; Refunds</span>
                                                    </span>
                                                </a></li> -->
                                            <!--<li class="help-center-topic-list _item">
                                                <a class="accordion-toggle help-center-topic-list _link" data-toggle="collapse" data-parent="#accordion" href="#what">
                                                    <span class="help-center-topic-list _icon-wrapper">
                                                        <img class="help-center-topic-list _icon" src="{{url('/')}}/storage/app/public/img/faq/question.svg" alt="Other Topics">
                                                    </span><span class="help-center-topic-list _content">
                                                        <span class="help-center-topic-list _name">Other Topics</span>
            
                                                    </span>
                                                </a>
                                            </li> -->
                                        </ul>
                                    </div>
                                    <div class="help-center-chat _block">
                                        <div class="help-center-chat _container">
                                            <div class="help-center-chat _advice">Our customer support is available Saturday-Thursday from<br>11am-8pm.<br>Click <a style="color:blue;text-decoration:underline;" target="__blank" href="{{url('/contact-us')}}">here</a> to contact us</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-8 col-lg-8 col-xs-12">
                                <div id="collapse3" class="panel-collapse collapse">
                                    <div class="collapse_desgin">
                                        <div class="collapse-border">
                                            <div class="panel-body">
                                                <h4>3. Login or Register</h4>
                                                <video width="100%" height="100%" id="introVideo" class="vjs-16-9" autoplay loop muted>
                                                    <source type="video/mp4" src="{{ URL::to('') }}/storage/app/public/img/faq/3.mp4" />
                                                </video>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse">
                                    <div class="collapse_desgin">
                                        <div class="collapse-border">
                                            <div class="panel-body">
                                                <h4>1. Select Product</h4>
                                                <video width="100%" height="100%" id="introVideo" class="vjs-16-9" autoplay loop muted>
                                                    <source type="video/mp4" src="{{ URL::to('') }}/storage/app/public/img/faq/How to Select product On E-commerce Site.mp4" />
                                                </video>
                                            </div>
                                        </div> 
                                    </div> 
                                </div>
                                <div id="collapse2" class="panel-collapse collapse">
                                    <div class="collapse_desgin">
                                        <div class="collapse-border">
                                            <div class="panel-body">
                                                <h4>2. Place order</h4>
                                                <video width="100%" height="100%" id="introVideo" class="vjs-16-9" autoplay loop muted>
                                                    <source type="video/mp4" src="{{ URL::to('') }}/storage/app/public/img/faq/2.mp4" />
                                                </video>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse">
                                    <div class="collapse_desgin">
                                        <div class="collapse-border">
                                            <div class="panel-body">
                                                <h4>4. Confirm Payment Method</h4>
                                                <video width="100%" height="100%" id="introVideo" class="vjs-16-9" autoplay loop muted>
                                                    <source type="video/mp4" src="{{ URL::to('') }}/storage/app/public/img/faq/4.mp4" />
                                                </video>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="collapse6" class="panel-collapse collapse">
                                    <div class="collapse_desgin">
                                        <div class="collapse-border">
                                            <div class="panel-body">
                                                <h4>Return</h4>
                                                <b>Coming soon ....</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse">
                                    <div class="collapse_desgin">
                                        <div class="collapse-border">
                                            <div class="panel-body">
                                                <h4>5. Delivery</h4>
                                                <p style="text-align:justify;padding: 11px;font-size: 11pt;">Our customer support will be in touch with you in 24-36 hours to confirm your order.<br><br>

                                                    <span style="font-weight:bold;">Delivery times:</span> <br>
                                                    We require 2-3 business days to deliver within Dhaka, and 3-5 days to deliver outside Dhaka. Same day deliveries within are possible once customer support is able to confirm your orders before 2pm on a work day. <br><br>

                                                    <span style="font-weight:bold;">Delivery charges:</span> <br>
                                                    For Dhaka, it is Tk 70. For locations out of Dhaka, it is Tk 150. If you shop Tk 3000 or more, shipping is free! <br><br>

                                                    If you have any more questions, <br>
                                                    you can contact us <a href="{{url('/contact-us')}}" style="color:blue;">here.</a></p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="what" class="panel-collapse collapse">
                                    <div class="collapse_desgin">
                                        <div class="collapse-border">
                                            <div class="panel-body">
                                                <h4>Question</h4>
                                                <b>Coming soon ....</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
    </div>
</main>
<!-- end container -->
<script>
    jQuery(document).ready(function () {
        jQuery('.showSingle').click(function () {
            jQuery('.panel-collapse').removeClass("show");
            jQuery('.showSingle').removeClass("active");
            jQuery('#collapse' + jQuery(this).attr('target')).addClass("show");
            jQuery('#div' + jQuery(this).attr('target')).addClass("active");
        });
    });
</script>

@endsection