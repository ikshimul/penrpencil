@extends('layouts.app')
@section('title','Pride Limited | A Pride Group venture')
@section('content')
<style type="text/css">
    .h7 {
        font-size: 28px;
        margin-bottom:20px;

    }
    .h2 {
        margin-top:20px;
        float:left;
        font-size: 18px;

        text-align:center;
        overflow:hidden;
    }
    .h1 {
        font-size: 26px;
    }
    .bnew2 {
        width: 250px;
        height: auto;
        padding: 3px 9px 3px 9px;
        text-align: center;
        color: #00000;
        font-size: 14px;
        cursor: pointer;
        float: left;
        border-right: 1px solid #999;
        margin-bottom: 10px;
        margin-left: 200px;
        margin-right: 10px;
    }
    .bnew {
        width: 250px;
        height: auto;

        padding:3px 9px 3px 9px;
        text-align:center;
        color:#00000;
        font-size:14px;
        cursor:pointer;
        float:left;
        border-right: 1px solid #999;
        margin-bottom:10px;
    }
    .imb{
        width:60px;
        height:60px;
        float:left;
        overflow:hidden;
        background:#fff;
    }
</style>
<main id="maincontent" class="page-main" style="padding-top: 62px;"><a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item cms_page">
                                <strong>Contact Us</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container"><!-- Container start -->
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    <center> 
                        <div class="h7">Get in Touch!
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                <div class="card border-0">
                                    <div class="card-body text-center">
                                        <i class="fa fa-map-marker fa-5x mb-3" aria-hidden="true"></i>
                                        <h4 class="text-uppercase mb-5">Store location</h4>
                                        <address><a href="{{url('/store-locator')}}">Store location</a></address>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                <div class="card border-0">
                                    <div class="card-body text-center">
                                        <i class="fa fa-phone fa-5x mb-3" aria-hidden="true"></i>
                                        <h4 class="text-uppercase mb-5">Call</h4>
                                        <p>0966-910-0216<br>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                <div class="card border-0">
                                    <div class="card-body text-center">
                                        <i class="fa fa-envelope fa-5x mb-3" aria-hidden="true"></i>
                                        <h4 class="text-uppercase mb-5">email</h4>
                                        <p><a href="mailto:care@pride-limited.com?Subject=Hello%20again" target="_top">care@pride-limited.com</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>
                </div>
            </div>
    </div><!-- end container -->
</main>
@endsection