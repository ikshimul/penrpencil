@extends('layouts.app')
@section('title','User Details')
@section('content')
<style>
    button {
        width: 100%;
        height: 30px;
        font-size: 10px;
        line-height: 28px;
        color: #fff;
        text-transform: uppercase;
        letter-spacing: 2px;
        position: relative;
        border: none;
        background: rgb(41, 30, 136);
        -webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -ms-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        transition: all 0.5s ease;
    }
    .omniauth-container {
        box-shadow: 0 0 0 1px #e5e5e5;
        border-bottom-right-radius: 2px;
        border-bottom-left-radius: 2px;
        padding: 15px;
    }
    .prepend-top-15 {
        margin-top: 25px;
    }
    .d-block {
        display: block !important;
    }
    label.label-bold {
        font-weight: 600;
    }
    .omniauth-btn {
        margin-bottom: 16px;
        width: 48%;
        padding: 8px;
    }
    .omniauth-container .omniauth-btn img {
        width: 18px;
        height: 18px;
        margin-right: 16px;
    }
    .btn, .project-buttons .stat-text {
        border-radius: 3px;
        font-size: 14px;
        height: 38px;
        font-weight: 400;
        padding: 6px 10px;
        background-color: #fff;
        border-color: #e5e5e5;
        color: #2e2e2e;
        color: #2e2e2e;
    }
    .text-left {
        text-align: left !important;
    }
    .align-items-center, .page-title-holder {
        align-items: center !important;
    }
    .btn, .project-buttons .stat-text {
        display: inline-block;
        font-weight: 400;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        user-select: none;

        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        line-height: 20px;
        border-radius: 0.25rem;
        transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    }
    img {
        vertical-align: middle;
        border-style: none;
    }
    .justify-content-between {
        justify-content: space-between !important;
    }
    .flex-wrap {
        flex-wrap: wrap !important;
    }
    .d-flex, .page-title-holder, .right-sidebar.build-sidebar .trigger-variables-btn-container {
        display: flex !important;
    }
    .login-container {
        max-width:600px;
    }
	.login-container:after {
        position: absolute;
        left: 50%;
        top: 0;
        bottom: 0;
        background: none;
        width: 1px;
        content: '';
}
.form-control {
    font-size:12px;
    color:#000;
}
</style>
<main id="maincontent" class="page-main" style="padding-top: 62px;"><a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item cms_page">
                                <strong>Create Account Here</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="authenticationPopup" data-bind="scope:'authenticationPopup'" style="display: none;">
                </div>
                <div class="login-container">
                    <div class="block">
                        <div class="block-title">
                            <strong id="block-new-customer-heading" role="heading" aria-level="2">Create Account Here</strong>
                        </div>
                        <form class="form create account form-create-account" action="{{url('/create/user')}}" method="post" id="form-validate" enctype="multipart/form-data" autocomplete="off">
                            {{ csrf_field() }}
                            <fieldset class="fieldset create info">
                                <div class="field">
                                    <label class="label" for="firstname"><span>First Name</span></label>
                                    <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                                       <input class="form-control" type="text" name="first_name"  value="{{ old('first_name') }}" required />
                                        <div class="help-block with-errors">{{ $errors->first('first_name') }}</div>
                                    </div>
                                </div>
                                <div class="field field-name-lastname required">
                                    <label class="label" for="lastname"><span>Last Name</span></label>
                                    <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                        <input type="text" id="lastname" class="form-control"
                                               name="last_name"
                                               value="{{ old('last_name') }}"
                                               title="Last&#x20;Name"
                                               class="input-text required-entry"  data-validate="{required:true}">
                                        <div class="help-block with-errors">{{ $errors->first('last_name') }}</div>
                                    </div>
                                </div>
                                <div class="field field-name-regi_email required">
                                    <label class="label" for="regi_email"><span>Email</span></label>
                                    <div class="form-group {{ $errors->has('regi_email') ? ' has-error' : '' }}">
                                        <input type="text" id="regi_email" class="form-control"
                                                name="regi_email"
                                                value="{{ old('regi_email') }}{{ $email or '' }}"
                                                title="Email&#x20;Name"
                                                class="input-text required-entry"  data-validate="{required:true}">
                                        <div class="help-block with-errors">{{ $errors->first('regi_email') }}</div>
                                    </div>
                                </div>
                                <!--<div class="field field-name-confirm_email required">
                                    <label class="label" for="confirm_email"><span>Confirm Email</span></label>
                                    <div class="form-group {{ $errors->has('confirm_email') ? ' has-error' : '' }}">
                                        <input type="text" id="confirm_email" class="form-control"
                                               name="confirm_email"
                                               value="{{ old('confirm_email') }}"
                                               title="Email&#x20;Name"
                                               placeholder="Confirm Email*"
                                               class="input-text required-entry"  data-validate="{required:true}">
                                        <div class="help-block with-errors">{{ $errors->first('confirm_email') }}</div>
                                    </div>
                                </div> --->
                            </fieldset>
							<fieldset class="fieldset create account" data-hasrequired="* Required Fields">
                                <div class="password-area">
                                    <div class="field password required" data-mage-init='{"passwordStrengthIndicator": {}}'>
                                        <label for="password" class="label"><span>Password</span></label>
                                        <div class="form-group {{ $errors->has('regi_password') ? ' has-error' : '' }}">
										<input class="form-control" id="" type="password" name="regi_password"  data-minlength="4" data-error="Minimum of 4 characters" value="" />
										  <div class="help-block with-errors"> {{ $errors->first('regi_password') }}</div>
                                        </div>
                                    </div>
                                    <div class="field password required" data-mage-init='{"passwordStrengthIndicator": {}}'>
                                        <label for="password" class="label"><span>Confirm Password</span></label>
                                        <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
										<input class="form-control" id="" type="password" name="password_confirmation"  data-minlength="4" data-error="Minimum of 4 characters" value="" />
										  <div class="help-block with-errors"> {{ $errors->first('password_confirmation') }}</div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="actions-toolbar">
                                <div class="primary">
                                    <button type="submit" class="action submit primary" title="Create an Account"><span>Complete Account</span></button>
                                    <p style="text-align:center; font-size:10px; color:#555; margin-top:40px;">By clicking 'Complete Account' I accept to the <a style="font-weight:bold; text-decoration: underline;" href="{{ url('privacy-cookies') }}" target="__blank">Privacy Policy</a></p>
                                </div>
                            </div>
                        </form>
<!--                        <script>
                            jQuery(function ($) {
                                var dataForm = $('#form-validate');
                                var ignore = 'input[id$="full"]';
                                dataForm.mage('validation', {
                                    errorPlacement: function (error, element) {
                                        if (element.prop('id').search('full') !== -1) {
                                            var dobElement = $(element).parents('.customer-dob'),
                                                    errorClass = error.prop('class');
                                            error.insertAfter(element.parent());
                                            dobElement.find('.validate-custom').addClass(errorClass)
                                                    .after('<div class="' + errorClass + '"></div>');
                                        } else {
                                            error.insertAfter(element);
                                        }
                                    },
                                    ignore: ':hidden:not(' + ignore + ')'
                                }).find('input:text').attr('autocomplete', 'off');
                            });
                        </script>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection