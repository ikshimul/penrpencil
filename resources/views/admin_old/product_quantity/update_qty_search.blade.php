@extends('admin.layouts.app')
@section('title', 'Product Quantity')
@section('content')
<section class="content-header">
    <h1>
        Product
        <small>Update Quantity</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Product</li>
    </ol>
</section>
<section class="content">
    <!-- Small boxes (Stat box) -->
    <form name="search_qty_product" action="{{url('/update-quantity')}}" method="post">
        {{ csrf_field() }}
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Search Product By Style</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-offset-1 col-md-6">
                        <center>
                            @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                            @endif
                        </center>
                        <center>
                            @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                            @endif
                        </center>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>Product Style Code</label>
                            <input type="text" class="form-control" name="product_style_code"  required/>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="col-md-offset-1">
                    <input type="submit" name="btnsubmit" class="btn bg-navy btn-flat margin" value="Search"/>
                </div>
            </div>
        </div>
    </form>  
</section>
@endsection