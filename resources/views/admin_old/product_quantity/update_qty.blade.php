<?php

use App\Http\Controllers\admin\product\QuantityController;
?>
@extends('admin.layouts.app')
@section('title', 'Edit Category')
@section('content')
<section class="content-header">
    <h1>
        Product
        <small>Quantity </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Product</li>
    </ol>
</section>
<section class="content">
    <!-- Small boxes (Stat box) {{url('/update-quantity-save')}} -->
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Update Quantity</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-offset-1 col-md-6">
                    <center>
                        @if (session('update'))
                        <div class="alert alert-success">
                            {{ session('update') }}
                        </div>
                        @endif
                    </center>
                    <input type="hidden" name="productid" value="<?php echo $product_info->product_id; ?>" />
                    <input type="hidden" name="productstyle" value="<?php echo $product_info->product_styleref; ?>" />
                    <!--  <div class="alert alert-info alert-dismissible">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                          <h4><i class="icon fa fa-info"></i></h4>
                          <p>Product Name</p>
                      </div> ---->
                    <table class="table table-striped">
                        <tr>
                            <th>Product Details</th>
                            <th>&nbsp;</th>
                        </tr>
                        <tr>
                            <td>Product Name : </td>
                            <td><?php echo $product_info->product_name; ?></td>
                        </tr>
                        <tr>
                            <td>Product Code : </td>
                            <td><?php echo $product_info->product_styleref; ?></td>
                        </tr>
                        <tr>
                            <td>Product Category : </td>
                            <td> <?php echo $product_info->procat_name; ?></td>
                        </tr>
                        <tr>
                            <td>Product Sub Category : </td>
                            <td><?php echo $product_info->subprocat_name; ?></td>
                        </tr>
                    </table>
                    <div class="row">
                        <div class="col-md-6">
                            <h4><strong>Update Quantity</strong></h4>
                            <table class="table table-condensed">
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>[Color] / [Size]</th>
                                    <th></th>
                                    <th style="width: 40px">Quantity</th>
                                </tr>
                                <?php
                                $i = 0;
                                foreach ($size_qty as $prosize) {
                                    $i++;
                                    ?>
                                    <tr>
                                        <td><?php echo $i; ?>.</td>
                                        <td><?php echo '[' . $prosize->color_name . ']' . ' / [' . $prosize->productsize_size . ']'; ?></td>
                                        <td>&nbsp; </td>
                                        <td>
                                            <input type="number" name="qty[]" id="qty_<?php echo $i; ?>" value="<?php echo $prosize->SizeWiseQty; ?>">
                                            <input type="hidden" name="prosize_id[]" id="prosize_<?php echo $i; ?>" value="<?php echo $prosize->productsize_id; ?>">
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
                            <input type="hidden" id="total_loop" value="<?php echo $i; ?>">
                        </div>
                        <div class="col-md-6">
                            <h4><strong>Add New Size and Quantity</strong></h4>
                            <table class="table table-condensed">
                                <tr>
                                    <th>Color</th>
                                    <th>Size</th>
                                    <th style="width: 25px">Quantity</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                $j = 0;
                                foreach ($color_wise_prosize as $color) {
                                    $j++;
                                    $prosize = QuantityController::GetExpectSize($color->product_id, $color->color_name);
                                    ?>
                                    <form action="{{url('/save-new-size-qty')}}" method="post">
                                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                        <input type="hidden" name="product_id" value="<?php echo $product_info->product_id; ?>" />
                                        <tr>
                                            <td>
                                                <input type="hidden" name="color_name" value=" <?php echo $color->color_name; ?>" />
                                                <?php echo $color->color_name; ?>
                                            </td>
                                            <td>
                                                <select name="productsize_size" required>
                                                    <option value="">Select size</option>
                                                    <?php foreach ($prosize as $sizes) { ?>
                                                        <option value="<?php echo $sizes->prosize_name; ?>"><?php echo $sizes->prosize_name; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>

                                            <td>
                                                <input type="number" name="SizeWiseQty" id="prosize_<?php echo $j; ?>" required/>
                                            </td>
                                            <td><input type="submit" id="add_size_<?php echo $j; ?>" value="Add"/></td>
                                        </tr>
                                    </form>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="col-md-offset-1">
                    <input name="btnsubmit" class="btn bg-navy btn-flat margin" id="update_qty" value="Update Quantity"/>
            </div>
        </div>
    </div>  
</section>
<script>
    $(document).ready(function () {
        var total_loop = $("#total_loop").val();
        // alert(total_loop);
        for (var i = 1; i <= total_loop; i++) {
            $('#qty' + '_' + i).on('blur', function () {
                var rownowithid = $(this).attr('id');
                var rowno = rownowithid.split('_');
                var qty = $('#qty' + '_' + rowno[1]).val();
                var prosize_id = $('#prosize' + '_' + rowno[1]).val();
                var url_op = base_url + "/update-quantity-save-by-ajax/" + prosize_id + '/' + qty;
                $.ajax({
                    url: url_op,
                    type: 'GET',
                    success: function (html) {
                      //  alert(html);
                    }
                });
            });
        }
        $("#update_qty").on('click',function(){
			alert("Quantity Add Successfully!");
		});
    });
</script>
@endsection