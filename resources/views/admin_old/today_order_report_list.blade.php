@extends('admin.layouts.app')
@section('title', 'Pride Limited')
@section('content')
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" media="all"/>
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet" media="all"/>
<style>
    fieldset {
    padding: .75em .825em .85em;
    margin: 0 2px;
    border: 1px solid silver;
}
fieldset {
    display: block;
    -webkit-margin-start: 2px;
    -webkit-margin-end: 2px;
    -webkit-padding-before: 0.35em;
    -webkit-padding-start: 0.75em;
    -webkit-padding-end: 0.75em;
    -webkit-padding-after: 0.625em;
    min-width: -webkit-min-content;
    border-width: 2px;
    border-style: groove;
    border-color: threedface;
    border-image: initial;
}
a:link, span.MsoHyperlink {
    mso-style-noshow: yes;
    mso-style-priority: 99;
    text-decoration: none !important;
    text-underline: single;
}

.margin_new {
    margin: 2px;
}
td, th {
    padding: 5px;
}
.text-design{
        color:red;
    }
    .label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: 600;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0px;
}
</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Order(s) Report</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div style="padding-bottom:15px;">
                         <fieldset>
                             <legend>Search Order</legend>
                                   <form name="filtering"  action="{{url('/order-report')}}" method="GET">
                                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                       <table>
                							<tr>
                								<td class="field_label">
                									Form Date:
                								</td>
                								<td>
                								    <input type="date" name="form_date" id="date" value="<?php echo $form_date;?>" autocomplete="off"/>
                									Date format (yyyy-m-d)&nbsp;
                								</td>
                							</tr>
                							<tr>
                								<td class="field_label">
                									To Date :
                								</td>
                								<td>
                									<input type="date" name="to_date" id="ToDate"  value="<?php echo $to_date;?>" autocomplete="off" />
                									Date format (yyyy-m-d)&nbsp;
                								</td>
                							</tr>
                							<tr>
                								<td class="field_label">
                								</td>
                								<td>
                								
                								</td>
                							</tr>
                							<tr>
                								<td class="field_label">
                								</td>
                								<td>
                								   <input type="submit" value="Search"/>&nbsp;
                								</td>
                							</tr>
                        			</table>
                        		</form>
                        </fieldset>
                    </div>
                        <?php if($form_date != ''){ ?><p style="font-weight:500;color:#08c708;">Search form <?php  $form=strtotime($form_date); echo date('Y-m-d',$form)?> to <?php $to=strtotime($to_date); echo date('Y-m-d',$to);?></p><?php } ?>
                        <center><span id="showinfo" style="color:green;"></span></center>
                    <div id="orders">
                        <table id="example" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">SL</th>
                                    <!--<th>Customer Name</th>
                                    <th>Order NO</th>  --->                      	
                                    <th>Order Placed</th> 
                                    <th>Product Barcode</th>
                                    <th>Product Color</th>
                                    <th>Product Size</th>
                                    <th>Qty</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                $check=count($all_order_info);
                                ?>
                                @foreach($all_order_info as $orderinfo)
                                <tr>                        	
                                    <td width="5%">{{$i++}}</td>
                                   <!-- <td>{{$orderinfo->registeruser_firstname}} {{$orderinfo->registeruser_lastname}}</td>
                                    <td>
                                        <a href="{{url("/pride-admin/order-details/{$orderinfo->conforder_id}")}}" target="__blank" class="info">{{$orderinfo->conforder_tracknumber}}</a>
                                    </td> --->
                                    <td>
                                       <?php $date=strtotime($orderinfo->conforder_lastupdte); echo date('Y-m-d',$date);?>
                                    </td>	
                                    <td><?php echo $orderinfo->product_name;?></td>
                                    <td><?php echo $orderinfo->productalbum_name;?></td>
                                    <td><?php echo $orderinfo->prosize_name;?></td>
                                    <td><?php echo $orderinfo->shoppinproduct_quantity;?></td>
                                    <td><?php echo $orderinfo->product_price;?></td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>    
                    </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php if($check > 0) { ?>
                    <a href="#" id="PDF" class="btn btn-sm btn-info btn-flat pull-center">Send Mail</a>
                    <?php } ?>
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
    </div>
    <!-- Ajax modal ---->
    <div class="modal fade" id="modal-delete">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top:100px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="text-align:center;color:black;">Are you sure to send this mail ?</h4>
                </div>
                <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                    <a href="#" class="btn btn-sm btn-danger" id="delete_link">Delete</a>
                    <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!--- Ajax modal end ---->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

   <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/3.0.0-alpha.1/jspdf.plugin.autotable.min.js"></script>
    <script>
        $(document).on("click", "#PDF", function () {
        if(confirm("Are you sure to send this email ?")){    
            var table = document.getElementById("example");
            var cols = [],
                data = [];

            function html() {
                var _token="{{ csrf_token() }}";
                var doc = new jsPDF('p', 'pt');
                var res = doc.autoTableHtmlToJson(table, true);
                doc.autoTable(res.columns, res.data);
                var pdf =doc.output(); //returns raw body of resulting PDF returned as a string as per the plugin documentation.
                var data = new FormData();
                data.append("data_table" , pdf);
                data.append("_token" , _token);
                var xhr = new XMLHttpRequest();
                xhr.open( 'post', '/pdf-report', true ); //Post to php Script to save to server
                xhr.send(data);
                document.getElementById("showinfo").innerHTML="Mail send successfully.";
            }
            html();
        }else{return;}
        });
    </script>
    @endsection


