@extends('admin.layouts.app')
@section('title', 'CashBook History')
@section('content')
<style>
    .label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: 600;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0px;
}
</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">CashBook History</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="5%">SL</th>
                                <th>Date</th>
                                <th>Transaction Type</th>
                                <th>Order NO#</th> 
                                <th>Customer name</th>
                                <th>Delivery By</th>
                                <th>Transaction Amount</th>
                                <th>Expense Amount</th>
                                <th>Total Received</th>
                                <th>Balance</th>
                                
                                <th>Issue By</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            ?>
                            @php($balance=0)
                            @foreach($cash_history as $cash)
                            <tr>                        	
                                <td width="5%">{{$i++}}</td>
                                <td style="color:black">
                                    <?php 
                                        $order_date=strtotime($cash->created_at); 
                                        echo date('d M , Y',$order_date);
                                    ?>
                                </td>
                                <td style="color:black">{{$cash->transaction_type}}</td>
                                <td>
                                    <a href="{{url("/order-details/{$cash->conforder_id}")}}" target="__self" class="info">{{$cash->conforder_tracknumber}}</a>
                                </td>
                                <td> {{ $cash->Shipping_txtfirstname  }} {{ $cash->Shipping_txtlastname }} </td>
                                <td>{{$cash->delivery_by}}</td>
                                <td>
                                     {{ number_format($cash->transation_amount) }}
                                </td>
                                <td>
                                     {{ number_format($cash->expense_amount) }}
                                </td>
                                <td>
                                    {{ $cash->transation_amount - $cash->expense_amount }}
                                </td>
                                <td>
                                    <?php 
                                        if ($cash->transaction_type === 'cash_out')
                                            $balance -= ($cash->transation_amount - $cash->expense_amount);
                                        else
                                            $balance += ($cash->transation_amount - $cash->expense_amount);
                                    ?>
                                    {{ number_format($balance) }}
                                </td>
                                
                                <td style="color:black">{{ $cash->admin_username }}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <td colspan="9" style="text-align:right;font-weight:600;">Current Balance</td>
                                <td>{{ number_format($balance) }}</td>
                                <td colspan="1"></td>
                            </tr>
                        </tbody>
                    </table>
                  </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <style>
                                .pagination {
                                    display: inline-block;
                                    padding-left: 15px;
                                    margin: 20px 0;
                                    border-radius: 4px;
                                }
                            </style>
                            <div class="font-alt">                         
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                   
                </div>
            </div>
        </div>
    </div>
@endsection
