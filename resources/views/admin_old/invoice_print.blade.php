<?php

use App\Http\Controllers\admin\ManageOrderController;

foreach ($total_incomplete_order_info as $orderinfo) {
    $selectdestination_id = $orderinfo->shipping_area;
}
$shippingdestnation = ManageOrderController::GetShippingDestinationName($selectdestination_id);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Pride Limited | Invoice</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="{{asset('assets_admin/bootstrap/css/bootstrap.min.css')}}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{asset('assets_admin/dist/css/AdminLTE.min.css')}}">

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <style>
            .lead {
                font-size: 14px;
                font-weight: 700;
            }
            .table > tbody > tr > td {
                vertical-align: middle;
            }
        </style>
    </head>
    <body onload="window.print();">
        <div class="wrapper">
            <!-- Main content -->
            <section class="invoice">
                <!-- title row -->
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                            PRIDE LIMITED
                            <small class="pull-right">Date: <?php
                                date_default_timezone_set("Asia/Dhaka");
                                echo date("M  d, Y H:i:s a");
                                ?></small>
                        </h2>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- info row -->
                <div class="row invoice-info">
                    <div class="col-sm-4 col-md-4 col-xs-4 invoice-col">
                        Shipping Address
                        <address>
                            <strong>{{ $shipping_address_details->Shipping_txtfirstname }} {{$shipping_address_details->Shipping_txtlastname }}</strong><br>
                            {{ $shipping_address_details->Shipping_txtaddressname }}<br>
                            {{ $shipping_address_details->Shipping_txtcity }}<br>
                            Phone: {{ $shipping_address_details->Shipping_txtphone }}<br>
                         <!--   Email: info@almasaeedstudio.com -->
                        </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-3 col-md-3 col-xs-3 invoice-col">
                        <strong>Shipping Destination</strong> 
                        <address>
                            <?php echo $shippingdestnation; ?><br>
                        </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-5 col-md-5 col-xs-5 invoice-col">
                        <b>{{ $shipping_address_details->conforder_tracknumber }}</b><br>
                        <br>
                        @foreach($total_incomplete_order_info as $orderinfo)
                        <?php  $dmselect = $orderinfo->deliveryMethod;?>
                         @endforeach
                        3pl Status: <b><?php  if($shipping_address_details->order_threepldlv ==''){
                            if ($dmselect == 'cs') {
                            echo 'Cash on delivery';
                        } else {
                            echo 'bKash';
                        }
                        }else{
                          echo $shipping_address_details->order_threepldlv;
                        }
                        ;?></b>
                        <br>
                        Order Status:
                        <b> <?php
                            if ($shipping_address_details->conforder_completed == 1) {
                                echo "Order Completed";
                            } else if ($shipping_address_details->conforder_completed == 0) {
                                $ststus=str_replace("_"," ",$shipping_address_details->conforder_status);
                                echo $ststus;
                            } else {
                                
                            }
                            ?></b>
                        <br>
                        Order Placed Date:<b> <?php $date=strtotime($shipping_address_details->conforder_placed_date); echo date('M d, Y', $date); ?></b><br>
                        Delivery Date: <b> <?php echo $shipping_address_details->conforder_deliverydate; ?> <?php
                                date_default_timezone_set("Asia/Dhaka");
                                echo date("M  d, Y");
                                ?></b>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
				<br>
                <!-- Table row -->
                <div class="row">
                    <div class="col-xs-12 table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Code</th>
                                    <th>Color</th>
                                    <th>Size</th>
                                    <th>Unit Price</th>
                                    <th>Qty</th>
                                    <th>Total Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $k = 1;
                                $subtotal = 0;
                                ?>
                                @foreach($total_incomplete_order_info as $orderinfo)   						
                                <tr>
                                    <?php
                                    $subtotal = $subtotal + ($orderinfo->shoppinproduct_quantity * $orderinfo->product_price);
                                    $shipping_charge = $orderinfo->Shipping_Charge;
                                    $selectdestination = $orderinfo->shipping_area;
                                    $dmselect = $orderinfo->deliveryMethod;
                                    ?>

                                    <td style="color:black"  width="5%"><img src="{{url('/')}}/storage/app/public/pgallery/{{$orderinfo->cart_image}}" width="100"/></td>
                                    <td style="color:black">{{$orderinfo->product_name}}</td>
                                    <td style="color:black">{{$orderinfo->product_styleref}}</td>
                                    <td style="color:black">{{$orderinfo->productalbum_name}}</td>
                                    <td>{{$orderinfo->prosize_name}}</td>
                                    <td>{{$orderinfo->product_price}}</td>
                                    <td>{{$orderinfo->shoppinproduct_quantity}}</td>
                                    <td><?php echo $orderinfo->product_price * $orderinfo->shoppinproduct_quantity;?></td>
                                </tr>
                                <?php $k++; ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                <?php if($shipping_address_details->employee_status==1){
                $subtotal=$subtotal-($subtotal*20/100);
              //  echo "Employee Discount";
            } ?>
                <div class="row">
                    <!-- accepted payments column -->
                    <div class="col-xs-6">
                        <p class="lead">Payment Methods:</p>
                        <?php
                        if ($dmselect == 'cs') {
                            echo 'Cash on delivery';
                        } else {
                            echo 'bKash';
                        }
                        ?>
                        <br/>
                        <br/>
                        <p><strong>Delivery Notes:</strong></p>
                        <?php echo $shipping_address_details->conforder_statusdetails;?>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-6">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th style="width:50%">Subtotal:</th>
                                    <td>Tk {{ $subtotal }}</td>
                                </tr>
                                <tr>
                                    <th>
                                        <?php
                                        if ($subtotal >= 3000) {
                                            echo "Shipping Charge";
                                        } else {
                                            if ($shipping_charge == 70) {
                                                echo "Shipping Charge(Inside Dhaka): ";
                                            } else {
                                                echo "Shipping Charge(Outside Dhaka): ";
                                            }
                                        }
                                        ?>
                                    </th>
                                    <td>
                                        <?php
                                        if ($subtotal >= 3000) {
                                            $shipping_charge = 0;
                                            echo 'Tk ' . $shipping_charge;
                                        } else {
                                            if ($shipping_charge == 70) {
                                                echo 'Tk ' . $shipping_charge;
                                            } else {
                                                echo 'Tk ' . $shipping_charge;
                                            }
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <?php 
         
                                    if($shipping_address_details->used_promo==1){ ?>
                                    <th>Total(Promo):</th>
                                    <td>Tk <?php echo $totalamount=$shipping_address_details->shoppingcart_total; ?></td>
                                    <?php }elseif($shipping_address_details->get_offer==1){ ?>
                                        <th>Total (10% Discount) :</th>
                                        <td>Tk <?php $totalamount=$subtotal + $shipping_charge; $discount=$totalamount*10/100; echo $totalamount=$totalamount-$discount; ?></td>
                                    <?php }else{ ?>
                                    <th>Total:</th>
                                    <td>Tk <?php echo $totalamount=$subtotal + $shipping_charge; ?></td>
                                    <?php } ?>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-6">
                        <br/>
                        <br/>
                        <p class="lead"  style="border-top: 1px solid #0f0f0f45;width: 126px;">Customer Signature</p>
                    </div>
                    <div class="col-xs-6">&nbsp;</div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- ./wrapper -->
    </body>
</html>
