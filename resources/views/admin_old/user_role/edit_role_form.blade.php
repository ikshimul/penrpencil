@extends('admin.layouts.app')
@section('title', 'Urban Truth')
@section('content')
<style>
    .user_access{
        border: 1px solid #33333330;
        padding: 25px;
        margin-left: 5px;
    }
    label {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 400;
  }
</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if(session('update'))
            <div class="alert alert-success" role="alert">
                {{session('update')}}
            </div>
            @endif
            @if(session('error_message'))
            <div class="alert alert-danger" role="alert">
                {{session('error_message')}}
            </div>
            @endif

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Role</h3>
                </div>
                <form role="form" action="{{url('/role-update/'.$role->id)}}" method="post">
                    {{csrf_field()}}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label >Role Name</label>
                                    <input type="text" name="role_name" value="{{$role->role_name}}" class="form-control" placeholder="Enter Role Name">
                                    <span class="text-danger">{{$errors->first('role_name')}}</span>
                                </div>
                            </div>
                            <div class="col-md-6">&nbsp;</div>
                            <div class="col-md-12">
                                <div class="col-md-12" style="font-weight: 700;">
                                    <input type="checkbox" id="ckbCheckAll" /> Check All
                                </div>
                                <p class="title">&nbsp;</p>
                                <div class="col-md-2">
                                    <div class="user_access">
                                        <h4>Dashboard</h4>
                                        <div class="form-group">
                                            <label style="font-weight: 700;">
                                                <input type="checkbox" name="permission[]" value="utadmin" class="checkBoxClass flat-red" @if(in_array('utadmin',$permission)) checked @endif>
                                                       Dashboard
                                            </label>
                                        </div>
                                         <div class="form-group">
                                            <label style="font-weight: 700;">
                                                <input type="checkbox" name="permission[]" value="site-user" class="checkBoxClass flat-red" @if(in_array('site-user',$permission)) checked @endif>
                                                       Site User
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label style="font-weight: 700;">
                                                <input type="checkbox" name="permission[]" value="phone-request" class="checkBoxClass flat-red" @if(in_array('phone-request',$permission)) checked @endif>
                                                       Phone Request
                                            </label>
                                        </div>
                                    </div>
                                 </div>
                                <div class="col-md-2">
                                    <div class="user_access">
                                        <h4>Admin User</h4>
                                        <div class="form-group">
                                            <label style="font-weight: 700;">
                                                <input type="checkbox" name="permission[]" value="user-menu" class="checkBoxClass flat-red" @if(in_array('user-menu',$permission)) checked @endif >
                                                       &nbsp;User Main Menu
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="add-user" class="checkBoxClass flat-red" @if(in_array('add-user',$permission)) checked @endif >
                                                       &nbsp;Add User
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="view-user" class="checkBoxClass flat-red" @if(in_array('view-user',$permission)) checked @endif>
                                                       &nbsp;View User
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="create-admin-user" class="checkBoxClass flat-red" @if(in_array('create-admin-user',$permission)) checked @endif>
                                                       &nbsp;Save User
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="user-edit" class="checkBoxClass flat-red" @if(in_array('user-edit',$permission)) checked @endif>
                                                       &nbsp;Edit User
                                            </label>
                                        </div> 
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="update-user" class="checkBoxClass flat-red" @if(in_array('update-user',$permission)) checked @endif>
                                                       &nbsp;Update User
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="delete-user" class="checkBoxClass flat-red" @if(in_array('delete-user',$permission)) checked @endif>
                                                       &nbsp;Delete User
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="add-role" class="checkBoxClass flat-red" @if(in_array('add-role',$permission)) checked @endif>
                                                       &nbsp;Add Role
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="create-role" class="checkBoxClass flat-red" @if(in_array('create-role',$permission)) checked @endif>
                                                       &nbsp; Save Role
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="view-role" class="checkBoxClass flat-red" @if(in_array('view-role',$permission)) checked @endif>
                                                       &nbsp; View Role
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="edit-role" class="checkBoxClass flat-red" @if(in_array('edit-role',$permission)) checked @endif>
                                                       &nbsp; Edit Role
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="role-update" class="checkBoxClass flat-red" @if(in_array('role-update',$permission)) checked @endif>
                                                       &nbsp; Update Role
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="delete-role" class="checkBoxClass flat-red" @if(in_array('delete-role',$permission)) checked @endif>
                                                       &nbsp;Delete Role
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="user_access">
                                        <h4>Category</h4>
                                        <div class="form-group">
                                            <label style="font-weight: 700;">
                                                <input type="checkbox" name="permission[]" value="category-menu" class="checkBoxClass flat-red" @if(in_array('category-menu',$permission)) checked @endif>
                                                      Category Main Menu
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="manage-category" class="checkBoxClass flat-red" @if(in_array('manage-category',$permission)) checked @endif>
                                                       Manage Category
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="edit-procate" class="checkBoxClass flat-red" @if(in_array('edit-procate',$permission)) checked @endif>
                                                       Edit Main Category
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="update-procat" class="checkBoxClass flat-red" @if(in_array('update-procat',$permission)) checked @endif>
                                                       Update Main Category
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="add-subcat" class="checkBoxClass flat-red" @if(in_array('add-subcat',$permission)) checked @endif>
                                                       Add Sub Category
                                            </label>
                                        </div>  
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="save-subpro" class="checkBoxClass flat-red" @if(in_array('save-subpro',$permission)) checked @endif>
                                                       Save Sub Category
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="manage-subpro" class="checkBoxClass flat-red" @if(in_array('manage-subpro',$permission)) checked @endif>
                                                       Manage Sub Category
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="user_access">
                                        <h4>Product</h4>
                                        <div class="form-group">
                                            <label style="font-weight: 700;">
                                                <input type="checkbox" name="permission[]" value="product-menu" class="checkBoxClass flat-red" @if(in_array('product-menu',$permission)) checked @endif>
                                                      Product Main Menu
                                            </label>
                                        </div> 
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="add-product" class="checkBoxClass flat-red" @if(in_array('add-product',$permission)) checked @endif>
                                                       Add Product
                                            </label>
                                        </div> 
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="product-save" class="checkBoxClass flat-red" @if(in_array('product-save',$permission)) checked @endif>
                                                       Product Save
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="product-manage" class="checkBoxClass flat-red" @if(in_array('product-manage',$permission)) checked @endif>
                                                       Product Manage
                                            </label>
                                        </div> 
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="edit-product" class="checkBoxClass flat-red" @if(in_array('edit-product',$permission)) checked @endif>
                                                       Edit Product
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="update-product" class="checkBoxClass flat-red" @if(in_array('update-product',$permission)) checked @endif>
                                                       Update Product
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="product-album-manage" class="checkBoxClass flat-red" @if(in_array('product-album-manage',$permission)) checked @endif>
                                                       Album Manage
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="add-product-style" class="checkBoxClass flat-red" @if(in_array('add-product-style',$permission)) checked @endif>
                                                       Add Product Style
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="save-product-style" class="checkBoxClass flat-red" @if(in_array('save-product-style',$permission)) checked @endif>
                                                       Save Product Style
                                            </label>
                                        </div> 
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="manage-product-gallery" class="checkBoxClass flat-red" @if(in_array('manage-product-gallery',$permission)) checked @endif>
                                                       Manage Product Gallery
                                            </label>
                                        </div> 
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="edit-album-image" class="checkBoxClass flat-red" @if(in_array('edit-album-image',$permission)) checked @endif>
                                                       Edit Album Image
                                            </label>
                                        </div> 
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="update-album-image" class="checkBoxClass flat-red" @if(in_array('update-album-image',$permission)) checked @endif>
                                                       Update Album Image
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="delete_album_image" class="checkBoxClass flat-red" @if(in_array('delete_album_image',$permission)) checked @endif>
                                                       Delete Album Image
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="product-deactive-active" class="checkBoxClass flat-red" @if(in_array('product-deactive-active',$permission)) checked @endif>
                                                       Product Active/Deactive
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="product-delete" class="checkBoxClass flat-red" @if(in_array('product-delete',$permission)) checked @endif>
                                                       Product Delete
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="user_access">
                                        <h4>Order</h4>
                                        <div class="form-group">
                                            <label style="font-weight: 700;">
                                                <input type="checkbox" name="permission[]" value="manage-order-menu" class="checkBoxClass flat-red" @if(in_array('manage-order-menu',$permission)) checked @endif>
                                                       Manage Order Main Menu
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="manage-incomplete-order" class="checkBoxClass flat-red" @if(in_array('manage-incomplete-order',$permission)) checked @endif>
                                                       Manage Incomplete Order
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="manage-all-order" class="checkBoxClass flat-red" @if(in_array('manage-all-order',$permission)) checked @endif>
                                                       Manage All Order
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="order-details" class="checkBoxClass flat-red" @if(in_array('order-details',$permission)) checked @endif>
                                                       Order Details
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="exchange" class="checkBoxClass flat-red" @if(in_array('exchange',$permission)) checked @endif>
                                                       Product Exchange
                                            </label>
                                        </div>

                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="update-quantity-search" class="checkBoxClass flat-red" @if(in_array('update-quantity-search',$permission)) checked @endif>
                                                       Update Quantity Search
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="update-quantity" class="checkBoxClass flat-red" @if(in_array('update-quantity',$permission)) checked @endif>
                                                       Update Quantity
                                            </label>
                                        </div>

                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="save-new-size-qty" class="checkBoxClass flat-red" @if(in_array('save-new-size-qty',$permission)) checked @endif>
                                                       Save New Size Quantity
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="invoice-print" class="checkBoxClass flat-red" @if(in_array('invoice-print',$permission)) checked @endif>
                                                       Invoice Print
                                            </label>
                                        </div>
                                    </div>
                                </div>
                           </div>
                       </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $("#ckbCheckAll").click(function () {
            $(".checkBoxClass").prop('checked', $(this).prop('checked'));
        });

        $(".checkBoxClass").change(function () {
            if (!$(this).prop("checked")) {
                $("#ckbCheckAll").prop("checked", false);
            }
        });
    });
</script>
@stop