@extends('admin.layouts.app')
@section('title', 'Urban Truth')
@section('content')
<section class="content">
        <div class="row">
            <div class="col-md-12">
                @if(session('success_message'))
                    <div class="alert alert-success" role="alert">
                        {{session('success_message')}}
                    </div>
                @endif

                @if(session('error_message'))
                    <div class="alert alert-danger" role="alert">
                        {{session('error_message')}}
                    </div>
                @endif

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">View Role</h3>
                    </div>

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Role Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($role as $role_list)
                                <tr>
                                    <td>{{$role_list->role_name}}</td>
                                    <td>
                                        <a href="{{url('/edit-role/'.$role_list->id)}}" class="btn btn-sm btn-primary">Edit</a>
                                        <a href="" onclick="" class="btn btn-sm btn-danger">Delete</a>
                                    </td>
                                </tr>
								@endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Role Name</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>


                </div>
            </div>
        </div>

    </section>
	@stop