@extends('admin.layouts.app')
@section('title', 'Pride Limited')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if (session('success'))
            <div class="pad margin no-print">
                <div class="callout callout-danger" style="margin-bottom: 0!important;">
                    <h4>{{ session('success') }}</h4> </div>
            </div>
            @endif
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">All Site User(s)</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="2%">SL</th>
                                <th>User Id</th>
                                <th>Name</th>
                                <th>Email</th>                        	
                                <th>Telephone</th>                                                        
                                <th>Details</th>                                                        
                                <th>Action</th>                            
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            ?>
                            @foreach($site_user as $user)
                            <tr>                        	
                                <td width="2%">{{$i++}}</td>
                                <td width="5%">{{$user->registeruser_id}}</td>
                                <td>{{$user->registeruser_firstname}} {{$user->registeruser_lastname}}</td>
                                <td>
                                    {{$user->registeruser_email}}
                                </td>
                                <td>
                                    {{$user->registeruser_phone}}
                                </td>																		
                                <td>
                                    <?php
                                    echo "
					Address 1 : " . $user->registeruser_address . "
                                        <br />
					Address 2 : " . $user->registeruser_address1 . "
					<br />
				        City: " . $user->registeruser_city . "
					<br />
					Zip Code: " . $user->registeruser_zipcode . "";
                                    ?>  
                                </td>
                                <td width="10%">
                                    <a href="{{url("/edit-site-user/{$user->registeruser_id}")}}" class="btn btn-sm btn-info btn-flat">Edit</a> &nbsp;  &nbsp; 
                                    <a class="btn btn-danger btn-flat btn-sm tdata" href="#" onclick="confirm_delete('{{url('/site-user/delete-user')}}/<?php echo $user->id; ?>')">Delete</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-delete">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top:100px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="text-align:center;color:black;">Are you sure to delete this?</h4>
                </div>
                <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                    <a href="#" class="btn btn-sm btn-danger" id="delete_link">Delete</a>
                    <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    @endsection