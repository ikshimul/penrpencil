@extends('admin.layouts.app')
@section('title', 'Product Edit Form')
@section('content')
<link rel="stylesheet" href="{{asset('assets_admin/input_style.css')}}">
<style>
  .osh-msg-box.-success {
        border-color: #00a65a;
        color: #00a65a;
    }
    .osh-msg-box.-danger {
        border-color: red;
        color: red;
    }
    .osh-msg-box {
        border-width: 1px;
        border-style: solid;
        padding: 2px 5px;
        margin: 10px 0;
    }
    @-webkit-keyframes blinker {
    from {opacity: 1.0;}
    to {opacity: 0.0;}
}
.blink{
    color: red;
    font-weight:bold;
    text-decoration: blink;
    -webkit-animation-name: blinker;
    -webkit-animation-duration: 0.8s;
    -webkit-animation-iteration-count:infinite;
    -webkit-animation-timing-function:ease-in-out;
    -webkit-animation-direction: alternate;
}
</style>
<section class="content-header">
    <h1>
        Product
        <small>Edit</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Product</a></li>
        <li class="active">Edit</li>
    </ol>
</section>

<form   action="{{url('/product-edit')}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="product_id"  value="{{ $product_info->product_id }}">
    <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Product</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">&nbsp;</div>
				<div class="col-md-6">
				   @if (session('update'))
					<center>
						<div class=" osh-msg-box -success">{{session('update')}}
						</div>
					</center>
					@endif    
					@if (session('error'))
					<center>
						<div class=" osh-msg-box -danger">
						 <span class="blink">Error :: </span>&nbsp;&nbsp;
						{{session('error')}}
						</div>
					</center>
					@endif  
				</div>
				<div class="col-md-3">&nbsp;</div>
                    <div class="col-md-6" style="padding-left:50px;">
                        <table>
                            <tr>
                                <th><label for="ddlprocat">Main Category</label></th>
                                <td>
                                    <select type="text"  name="ddlprocat" id="ddlprocat"  required>
                                        <option value="">-- select category --</option>
                                        @foreach($procat_list as $procat)
                                        <option value="{{$procat->procat_id}}">{{$procat->procat_name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td><span class="help-text"></span></td>
                            </tr>
                            <tr>
                                <th><label for="ddlprosubcat1">Sub Category</label></th>
                                <td>
                                    <select type="text" name="ddlprosubcat1" id="ddlprosubcat1" required>
                                        <option value="">-- select sub category --</option>
                                        @foreach($subprocat_list as $subprocat)
                                        <option value="{{$subprocat->subprocat_id}}">{{$subprocat->subprocat_name}}</option>
                                        @endforeach
                                    </select></td>
                                <td><span class="help-text"></span></td>
                            </tr>
                            <tr>
                                <th><label for="txtproductname">Product Name</label></th>
                                <td>
                                    <input type="text" name="txtproductname" id="txtproductname" value="{{ $product_info->product_name }}" required>
                                </td>
                                <td><span class="help-text"></span></td>
                            </tr>
                            <!--<tr>
                                <th><label for="txtproductname">Product Barcode</label></th>
                                <td>
                                    <input type="text" name="barcode" value="{{ $product_info->product_barcode }}">
                                </td>
                                <td><span class="help-text"></span></td>
                            </tr> -->
                            <tr>
                                <th><label for="txtstyleref">Style Code/Ref</label></th>
                                <td><input type="text" id="required-input" name="txtstyleref" id="txtstyleref" value="{{ $product_info->product_styleref }}" required></td>
                                <td><span class="help-text"></span></td>
                            </tr>
                            <tr>
                                <th><label for="txtprice">Price</label></th>
                                <td><input type="number"  name="txtprice" id="in-range-input" min="350" max="20000" value="{{ $product_info->product_price }}" required></td>
                                <td><span class="help-text">(value must be number)</span></td>
                            </tr>
                            <tr>
                                <th><label for="txtpricediscounted">Discount Price</label></th>
                                <td><input type="number" name="txtpricediscounted" id="optional-input" min="0" max="200" value="{{ $product_info->discount_product_price }}"></td>
                                <td><span class="help-text">(value must be number)</span></td>
                            </tr>
                            <tr>
                                <th><label for="ddlfilter">Price Filter</label></th>
                                <td><select name="ddlfilter" id="ddlfilter">
                                        <option>-- Select Filter --</option>
                                        <option value="high">High</option>
                                        <option value="medium">Medium</option>
                                        <option value="low">Low</option>
                                    </select></td>
                                <td><span class="help-text"></span></td>
                            </tr>
                            <tr>
                                <th><label for="txtorder">Product Order</label></th>
                                <td><input type="number" name="txtorder" id="optional-input" value="{{$product_info->product_order}}"></td>
                                <td><span class="help-text">Only Numbers [ <strong>Total Product Added: <?php echo $total_product; ?></strong> ] </span></td>
                            </tr>
                            <tr>
                                <th><label for="txtproductcare">Care Description</label></th>
                                <td><textarea type="text" name="txtproductcare" id="optional-input">{{{ $product_info->product_care }}}</textarea></td>
                                <td><span class="help-text">Care Description</span></td>
                            </tr>
                            <tr>
                                <th>Is Special</th>
                                <td><input type="checkbox" name="campaign" id="check-option-1" value="3" <?php
                                    if ($product_info->isSpecial == 3) {
                                        echo 'checked';
                                    } else {
                                        echo "";
                                    }
                                    ?> style="width: 16px;" />Puja 2018</td>
                                <td><span class="help-text">Is Special then check this box</span></td>
                            </tr>
                            <tr>
                                <th>Size Image</th>
                                <td>
                                    <div class="form-group">
                                        <div class="mb10">
                                            <span class="file-input">
                                                <div class="file-preview">
                                                    <div class="close fileinput-remove text-right"><a href="#" onclick="confirm_modal('#');">×</a></div>
                                                    <div class="file-preview-thumbnails">
                                                        <div class="file-preview-frame" id="preview">
                                                            <img class="img-responsive" src="{{ URL::to('') }}/storage/app/public/prothm/{{$product_info->product_sizeguide_img}}" class="file-preview-image" title="" alt="" style="width:auto;height:160px;">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>   
                                                    <div class="file-preview-status text-center text-success"></div>
                                                    <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                                </div>

                                            </span>
                                        </div>
                                    </div>
                                    <input  type="file"  name="sizeguide_image"/></td>
                                <td><span class="help-text"></span></td>
                            </tr>
                            <tr>
                                <th><label for="txtproductdetails">Product Description</label></th>
                                <td><textarea type="text" name="txtproductdetails" id="optional-input" rows="5">{{{ $product_info->product_description }}}</textarea></td>
                                <td><span class="help-text"></span></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <div class="box-footer">
                <input type="submit" name="btnsubmit" class="btn btn-primary" value="Update Product" />
            </div>
        </div>
    </section>
</form>
<script>
</script>
<script>
    document.getElementById("ddlprocat").value = "<?php echo $product_info->procat_id; ?>";
    document.getElementById("ddlprosubcat1").value = "<?php echo $product_info->subprocat_id; ?>";
    document.getElementById("ddlfilter").value = "<?php echo $product_info->product_pricefilter; ?>";
</script>
<script src="{{asset('assets_admin/add_product.js')}}"></script>
<!--<link rel="stylesheet" href="{{asset('assets_admin/previewForm.css')}}">
<script src="{{asset('assets_admin/previewForm.js')}}"></script>-->
<script type="text/javascript" src="{{asset('assets_admin/dynamic_select_box.js')}}"></script>
<script>
    $(document).ready(function () {
        $('#myform').previewForm();
    });
</script>
@endsection