<?php

use App\Http\Controllers\admin\product\QuantityController;
?>
@extends('admin.layouts.app')
@section('title', 'View All Product')
@section('content')
<section class="content-header">
    <h1>
        Product 
        <small>Manage Product</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Product</a></li>
        <li class="active">Manage Product</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- /.box -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Browse All Product </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <center>
                        @if (session('update'))
                        <div class="alert alert-success">
                            {{ session('update') }}
                        </div>
                        @endif
                    </center>
                    <center>
                        @if (session('delete'))
                        <div class="alert alert-danger">
                            {{ session('delete') }}
                        </div>
                        @endif
                    </center>
                    <table id="example2" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Name</th>
                                <th>Style Code</th>
                                <th>Category - Sub Category</th>
                                <th style="text-align:center;">Image</th>
                                <th>Deleted By</th>
                                <th style="text-align:center;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $k = 0;
                            foreach ($product_list as $product) {
                                $k++;
                                ?>
                                <tr>
                                    <td style="width:1%;text-align:center;"><?php echo $product->product_id; ?></td>
                                    <td style="width:15%;"><?php echo $product->product_name; ?></td>
                                    <td><?php echo $product->product_styleref; ?></td>
                                    <td style="width:15%;"><?php echo $product->procat_name . " - " . $product->subprocat_name; ?></td>
                                        <td style="width:10%;text-align:center;">
                                            <img class="thumbnail" src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->productimg_img_tiny; ?>" alt=""/>
                                        </td>
                                        <td>
                                            <strong><?php echo $product->admin_username;?></strong>
                                        </td>
                                        <td>
                                            <a class="btn btn-success btn-flat btn-sm margin tdata" href="#" onClick="confirm_restore('{{url("/product-restore/{$product->product_id}/{$product->subprocat_id}")}}')">Restore</a>
                                            <a class="btn btn-danger btn-flat btn-sm margin tdata" href="#" onClick="confirm_delete('{{url("/product-delete/{$product->product_id}/{$product->subprocat_id}")}}')">Delete</a>
                                        </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- Ajax modal ---->
<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="text-align:center;color:black;">Are you sure to delete this product ?</h4>
            </div>
            <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                <a href="#" class="btn btn-sm btn-danger" id="delete_link">Delete</a>
                <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!--- Ajax modal end ---->
<!-- Ajax modal ---->
<div class="modal fade" id="modal-restore">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="text-align:center;color:black;">Are you sure to restore this product ?</h4>
            </div>
            <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                <a href="#" class="btn btn-sm btn-success" id="restore_link">Restore</a>
                <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!--- Ajax modal end ---->
<script>

    $(document).ready(function () {
        
        $('a[data-toggle=modal], button[data-toggle=modal]').click(function () {

            var data_id = '';
        
            if (typeof $(this).data('id') !== 'undefined') {
        
              data_id = $(this).data('id');
            }
        
         //   alert(data_id);
            
        var total_loop = $("#total_loop").val();
       // alert(total_loop);
        for (var i = 1; i <= total_loop; i++) {
            $('#qty' + '_' + data_id + '_'+ i).on('blur', function () {
               // alert('ok');
                var rownowithid = $(this).attr('id');
                var rowno = rownowithid.split('_');
              //  alert(rowno[2]);
                var qty = $('#qty' + '_' + data_id + '_' + rowno[2]).val();
                var prosize_id = $('#prosize' + '_' + data_id + '_' + rowno[2]).val();
               // alert(prosize_id);
                var url_op = base_url + "/update-quantity-save-by-ajax/" + prosize_id + '/' + qty;
                $.ajax({
                    url: url_op,
                    type: 'GET',
                    success: function (html) {
                       alert(html);
                    }
                });
            });
        }
        $('#update_qty' + '_' + data_id).on('click',function(){
		//	alert("Quantity Add Successfully!");
		});
     });
     
    });
    function confirm_restore(delete_url){
        jQuery("#modal-restore").modal('show', {backdrop: 'static'});
        document.getElementById("restore_link").setAttribute('href', delete_url);
}
</script>
@endsection