@extends('layouts.app')
@section('title','Pride Limited | A Pride Group venture')
@section('content')
<style>
	.help-center-topic-list._block.-list {
		padding: 25px 0;
	}
    .help-center-topic-list._title {
        margin-bottom: 12px;
        font-size: 24px;
        font-weight: 300;
        color: #202020;
    }
    .help-center-topic-list._block.-list .help-center-topic-list._link {
        height: 58px;
    }
    .help-center-topic-list._link {
        -webkit-transition: all .35s ease-in-out;
        transition: all .35s ease-in-out;
        display: block;
        position: relative;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        border-radius: 4px;
        border: 1px solid #dadada;
    }
    .help-center-topic-list._block.-list .help-center-topic-list._icon-wrapper {
        left: -1px;
        top: -1px;
        height: 58px;
        width: 58px;
    }
    .help-center-topic-list._icon-wrapper {
        position: absolute;
    }
    .help-center-topic-list._block.-list .help-center-topic-list._icon {
        max-width: 30px;
        max-height: 30px;
    }
    .help-center-topic-list._icon {
        position: absolute;
        left: 50%;
        top: 50%;
        -webkit-transform: translate(-50%,-50%);
        transform: translate(-50%,-50%);
    }
    .help-center-topic-list._block.-list .help-center-topic-list._name {
        position: absolute;
        top: 50%;
        -webkit-transform: translateY(-50%);
        transform: translateY(-50%);
        font-size: 16px;
        width: calc(100% - 46px);
        height: 30px;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
        line-height: 30px;
        left: 58px;
    }
    .help-center-topic-list._block.-list .help-center-topic-list._item:not(:first-child) {
        margin-top: 12px;
    }
	.help-center-chat._title {
    font-size: 24px;
    font-weight: 300;
    color: #202020;
    margin-bottom: 12px;
    line-height: 29px;
}
.help-center-chat._advice {
    color: #202020;
    font-weight: 300;
    margin: 0 0 14px;
    line-height: 19px;
    font-size: 14px;
}
.help-center-chat._buttons {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
    font-size: 0;
}
.help-center-chat._button.-ghost {
    border: 1px solid #291d88;
}
.help-center-chat._button.-blue {
    background-image: -webkit-gradient(linear,left top,left bottom,from(#1a9cb7),to(#1692ac));
    background-image: linear-gradient(180deg,#1a9cb7,#1692ac);
}
.help-center-chat._button-text {
    color: #fff;
    text-transform: uppercase;
    font-size: 14px;
    display: table-cell;
    vertical-align: middle;
    padding: 2px 10px;
    height: 40px;
}
.help-center-chat._button.-ghost {
    border: 1px solid #291d88;
}
.help-center-chat._button.-ghost .help-center-chat._button-text {
    color: #291d88;
    padding: 1px 9px;
    height: 40px;
}
.collapse_desgin {
    padding-top: 62px;
    padding-left: 4px;
}
.collapse-border {
    border: 1px solid #dadada;
    padding: 22px;
}
.active{
	color: white !important;
    background: #291d88;
}
a{
   cursor: pointer;
}
</style>
<div class="container"><!-- Container start -->
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="row" id="accordion">
				  <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4">
                    <div class="help-center _sidebar">
						<div class="help-center-topic-list _block -list">
                            <h3 class="help-center-topic-list _title">How to Order</h3>
                            <ul class="help-center-topic-list _items">
							   <li class="help-center-topic-list _item">
                                    <a  class="showSingle accordion-toggle help-center-topic-list _link" target="1"  id="div1" >
                                        <span class="help-center-topic-list _icon-wrapper">
                                            <img class="help-center-topic-list _icon" src="{{url('/')}}/storage/app/public/img/faq/registration.svg" alt="Products &amp; Prices">
                                        </span>
                                        <span class="help-center-topic-list _content">
                                            <span class="help-center-topic-list _name">Registration</span>
                                        </span>
                                    </a>
                                </li>
                                <li class="help-center-topic-list _item">
                                    <a class="showSingle accordion-toggle help-center-topic-list _link" target="2" id="div2">
                                        <span class="help-center-topic-list _icon-wrapper">
                                            <img class="help-center-topic-list _icon" src="{{url('/')}}/storage/app/public/img/faq/printing.svg" alt="Products &amp; Prices">
                                        </span>
                                        <span class="help-center-topic-list _content">
                                            <span class="help-center-topic-list _name">Products &amp; Prices</span>
                                        </span>
                                    </a>
                                </li>
                                <li class="help-center-topic-list _item">
                                    <a class="showSingle accordion-toggle help-center-topic-list _link" target="3" id="div3">
                                        <span class="help-center-topic-list _icon-wrapper">
                                            <img class="help-center-topic-list _icon" src="{{url('/')}}/storage/app/public/img/faq/buy.svg" alt="Order"></span>
                                        <span class="help-center-topic-list _content"><span class="help-center-topic-list _name">Order</span>
                                        </span>
                                    </a>
                                </li>
                                <li class="help-center-topic-list _item">
                                    <a class="showSingle accordion-toggle help-center-topic-list _link" target="4" id="div4">
                                        <span class="help-center-topic-list _icon-wrapper">
                                            <img class="help-center-topic-list _icon" src="{{url('/')}}/storage/app/public/img/faq/pay.svg" alt="payment"></span>
                                        <span class="help-center-topic-list _content">
                                            <span class="help-center-topic-list _name">Payment</span>
                                        </span>
                                    </a>
                                </li>
                                <li class="help-center-topic-list _item">
                                    <a class="showSingle accordion-toggle help-center-topic-list _link" target="5" id="div5">
                                        <span class="help-center-topic-list _icon-wrapper">
                                            <img class="help-center-topic-list _icon" src="{{url('/')}}/storage/app/public/img/faq/return.svg" alt="Returns &amp; Refunds"></span>
                                        <span class="help-center-topic-list _content">
                                            <span class="help-center-topic-list _name">Returns &amp; Refunds</span>
                                        </span>
                                    </a></li>
                                <li class="help-center-topic-list _item">
                                    <a class="showSingle accordion-toggle help-center-topic-list _link" target="6" id="div6">
                                        <span class="help-center-topic-list _icon-wrapper">
                                            <img class="help-center-topic-list _icon" src="{{url('/')}}/storage/app/public/img/faq/delivery-truck.svg" alt="Delivery"></span>
                                        <span class="help-center-topic-list _content">
                                            <span class="help-center-topic-list _name">Delivery</span>

                                        </span>
                                    </a>
                                </li>
                                <li class="help-center-topic-list _item">
                                    <a class="showSingle accordion-toggle help-center-topic-list _link" target="7" id="div7">
                                        <span class="help-center-topic-list _icon-wrapper">
                                            <img class="help-center-topic-list _icon" src="{{url('/')}}/storage/app/public/img/faq/question.svg" alt="Other Topics">
                                        </span><span class="help-center-topic-list _content">
                                            <span class="help-center-topic-list _name">Other Topics</span>

                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="help-center-chat _block">
                            <div class="help-center-chat _container">
                                <h3 class="help-center-chat _title">Still Need Help?</h3>
                                <div class="help-center-chat _advice">We're always here to assist you, 24 hours a day, 7 days a week.</div>
                                <div class="help-center-chat _buttons">
                                    <a class="help-center-chat _button -ghost" href="{{url('/contact-us')}}">
                                        <span class="help-center-chat _button-text">Contact Us</span>
                                    </a></div>
                            </div>
                        </div>
                      </div>
				  	</div>
					<div class="col-md-8 col-sm-8 col-lg-8 col-xs-8">
					      <div id="collapse1" class="panel-collapse collapse">
						  <div class="collapse_desgin">
						    <div class="collapse-border">
							 <div class="targetDiv panel-body" id="div1">
							    <h4>Registration</h4>
								<b>1.	Do I Need To Register Before Placing An Order?</b>
								<p>You do not need to register before placing an order. You may register as soon as you have finished shopping, or check out as a Guest. Our registration process is fast, free, and will save you time for future purchases. <a href="/login-register.php">Click here</a> to register.</p>
								<b><p>2.	How Do I Register?</b></p>
								You can <a href="/login-register.php">Click here</a> to register.
								<b><p>3.	Why Should I Become A Registered User?</b></p>
								You will not be required to re-enter your shipping or billing addresses every time you order online. Whenever you place an order, it will be delivered to the registered address on file, unless you direct us otherwise. Additionally, you will be missing out on several benefits for registered users such as tracking your order. 
								<b><p>4.	How Do I Reset My Password?</b></p>
								Please <a href="forgetpassword.php">Click here</a>  to reset your password.
								<b><p>5.	How Do I Change My Account Information?</b></p>
								As soon as you <a href="login-register.php">sign in</a>  to your account with your email address and password, it will direct you to your account overview to update/edit your account information.
								<b><p>6.	I Cannot Find The Answers To My Questions, How Do I Reach Customer Service?</b></p>
								Please <a href="contact.php">Click here</a> to contact our Customer Service Team.
                                </div>
							    </div>
							   </div>
							</div>
							<div id="collapse2" class="panel-collapse collapse">
							<div class="collapse_desgin">
							 <div class="collapse-border">
							  <div class="panel-body">
							    <h4>Products & Price</h4>
								<b>Coming soon ....</b>
                              </div>
							 </div> 
							  </div> 
							</div>
							<div id="collapse3" class="panel-collapse collapse">
							 <div class="collapse_desgin">
							  <div class="collapse-border">
							   <div class="panel-body">
							   <h4>Ordring</h4>
								   <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
									<div class="carousel-inner" role="listbox">
										<div class="carousel-item active">
										  <a href="#"><img class="d-block img-fluid" src="{{url('/')}}/storage/app/public/ordering/Screenshot.png" alt="First slide"></a>
										</div>
										<div class="carousel-item">
											<a href="#"><img class="d-block img-fluid" src="{{url('/')}}/storage/app/public/ordering/details.png" alt="Second slide"></a>
										</div>
										<div class="carousel-item">
										   <a href="#"> <img class="d-block img-fluid" src="{{url('/')}}/storage/app/public/ordering/add_to_cart.png" alt="Third slide"></a>
										</div>
										<div class="carousel-item">
										   <a href="#"> <img class="d-block img-fluid" src="{{url('/')}}/storage/app/public/ordering/cart.png" alt="Third slide"></a>
										</div>
										<div class="carousel-item">
										   <a href="#"> <img class="d-block img-fluid" src="{{url('/')}}/storage/app/public/ordering/login_register.png" alt="Third slide"></a>
										</div>
										<div class="carousel-item">
										   <a href="#"> <img class="d-block img-fluid" src="{{url('/')}}/storage/app/public/ordering/orderpreview.png" alt="Third slide"></a>
										</div>
										<div class="carousel-item">
										   <a href="#"> <img class="d-block img-fluid" src="{{url('/')}}/storage/app/public/ordering/shipping_info.png" alt="Third slide"></a>
										</div>
									</div>
									<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
										<i class="ion-ios-arrow-left" style="font-size:25px;"></i>
										<span class="sr-only">Previous</span>
									</a>
									<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
										<i class="ion-ios-arrow-right" style="font-size:25px;"></i>
										<span class="sr-only">Next</span>
									</a>
								</div>
									
                                  </div>
							    </div>
							  </div>
							</div>
							<div id="collapse4" class="panel-collapse collapse">
							<div class="collapse_desgin">
							  <div class="collapse-border">
							  <div class="panel-body">
							  <h4>Payment</h4>
								<b>Coming soon ....</b>
                              </div>
							  </div>
							</div>
							</div>
							<div id="collapse5" class="panel-collapse collapse">
							 <div class="collapse_desgin">
							  <div class="collapse-border">
							  <div class="panel-body">
							  <h4>Return</h4>
								<b>Coming soon ....</b>
                              </div>
							  </div>
							</div>
							</div>
							<div id="collapse6" class="panel-collapse collapse">
							 <div class="collapse_desgin">
							  <div class="collapse-border">
							  <div class="panel-body">
							  <h4>Delivery</h4>
								<b>Coming soon ....</b>
                              </div>
							  </div>
							</div>
							</div>
							<div id="collapse7" class="panel-collapse collapse">
							<div class="collapse_desgin">
							  <div class="collapse-border">
							  <div class="panel-body">
							  <h4>Question</h4>
								<b>Coming soon ....</b>
                              </div>
							  </div>
							</div>
							</div>
					   </div>
                    </div>
                <div class="col-md-1"></div>
            </div>
        </div>
</div>
<script>
 $(document).ready(function() {
	 $('.showSingle').click(function(){
	  $('.panel-collapse').removeClass("show");
	  $('.showSingle').removeClass("active");
	  $('#collapse'+$(this).attr('target')).addClass("show");
	  $('#div'+$(this).attr('target')).addClass("active");
    });	
});
</script>

@endsection