@extends('layouts.app')
@section('title','Pride Limited | A Pride Group venture')
@section('content')
<div id="search_result">
<div class="container"><!-- Container start -->
            <section id="content">
                <div class="content-wrap">
                <div class="container clearfix">
                  
                 

 <center> 
 
   <div class="h1"> Press & Talent </div><br>
             
             
             <div class="h2"> Press</div>

 

<p>For press related inquiries please contact <a href="mailto:press@pride-grp.com">press@pride-grp.com</a></p>

                 <div class="h2"> Talent</div>


<p>We're always looking to collaborate with fresh new talent! If you think you have got what it takes to be part of our innovative and dynamic team, please email your resume to <a href="mailto:talent@pride-grp.com">talent@pride-grp.com</a></p> </center>
 

</div>
</div>
</div><!-- end container -->
</div>
@endsection