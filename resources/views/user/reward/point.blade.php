@extends('layouts.app')
@section('title', 'Reward Points')
@section('content')
<style>
    .sidebar {
        padding: 0 20px 9999px 0;
        font-size: 13px;
        margin: 0 0 -9999px;
        border-right: 1px solid #eee;
    }
    .osh-msg-box.-success {
        border-color: #B0DA7C;
        color: #B0DA7C;
    }
    .osh-msg-box.-danger {
        border-color: red;
        color: red;
    }
    .osh-msg-box {
        border-width: 1px;
        border-style: solid;
        padding: 2px 5px;
        margin: 10px 0;
    }
</style>
<main id="maincontent" class="page-main" style="padding-top: 62px;"><a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/my-account')}}" title="Go to account Page">My Account</a>
                            </li>
                            <li class="item cms_page">
                                <strong>Point Balance</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row" id="search_result">
            <div class="col-md-2">
                <div class="sidebar">
                    <div class="block block-collapsible-nav">
                        <div class="title block-collapsible-nav-title" data-mage-init='{"toggleAdvanced": {"toggleContainers": "#block-collapsible-nav", "selectorsToggleClass": "active"}}'>
                            <strong>Account Dashboard</strong>
                        </div>
                        <div class="content block-collapsible-nav-content" id="block-collapsible-nav">
                            <ul class="nav items">
                                <li class="nav item current"><a href="{{url('/my-account')}}"><strong>Account Dashboard</strong></a></li>
                                <li class="nav item">
                                    <span class="delimiter"></span>
                                </li>
                                <li class="nav item"><a href="{{url('/address-book')}}">Address Book</a></li>
                                <li class="nav item"><a href="{{url('/my-account-info')}}">Account Information</a></li>
                                <li class="nav item">
                                    <span class="delimiter"></span>
                                </li>
                                
                                <li class="nav item"><a href="{{url('/track-order')}}">My Orders</a></li>
                                <li class="nav item"><a href="{{url('/reward-points')}}">Point Balance</a></li> 
                                <!--<li class="nav item"><a href="">Gift Card</a></li>  -->
                                <li class="nav item"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									Logout
								</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
								</form></li>
                            </ul>    
                        </div>
                    </div>
                    <div class="block account-nav">
                        <div class="title account-nav-title" data-mage-init='{"toggleAdvanced": {"toggleContainers": "#account-nav", "selectorsToggleClass": "active"}}'>
                            <strong></strong>
                        </div>
                        <div class="content account-nav-content" id="account-nav">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10">

                <div class="block block-dashboard-info">
                    <div class="block-title"><strong>Point Balance</strong>
                        
                    </div>
                    <div class="col-md-6" style="padding-left: 0px;">
                        <div class="box mtl size1of2">
                            <br>
                            <div class="box box-bdr mtm pam myaccountBox2">
                                <h4 class="ui-borderBottom pbs fsml">Your Points</h4>
								<hr>
                                <div class="mtm">
                                    <div class="osh-address -widget">
                                        <strong id="show-points"></strong>
                                    </div>
                                    <br>
                                    <div class="osh-address -widget -pickupStation hidden"><div class="osh-row -name"></div><div class="osh-row -with-icon-left"><i class="osh-font-address"></i> <span class="osh-field -address1"></span></div></div>                        </div>
                                    <a class="mts  myaccountBox2-edit-shipping-address sel-edit-my-shipping-addresse" href="{{url('/points-history')}}" id="points-history"><i class="fa fa-fw">&#xF129;</i> Check History</a>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</main>
<!--- Delete modal ----->
<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">                                                                    
            <div class="modal-header">
                <h6 class="font-alt" style="text-align:center;">Are you sure to remove this item ?</h6>
                <a class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
            </div>
            <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                <a href="#"  id="delete_link" style="color:red;">Remove</a> &nbsp;&nbsp; | &nbsp;&nbsp;
                <a href="#" data-dismiss="modal">Cancel</a>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function ($) {
      // alert('ok');
      var url_op = base_url + "/get-users-points/";
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data)
                {
                 if(data ==0){
                     $('#show-points').append("<span style='cursor:pointer;color:red;'>0</span>");
                     $("#points-history").hide();
                 }else{
                     
                     $('#show-points').append("<span style='cursor:pointer;color:green;'>"+ data +"</span>");
                      console.log(data);
                   }
                }
            });
    });
</script>
@endsection

