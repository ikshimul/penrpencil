@extends('layouts.app')
@section('title', 'Track Order')
@section('content')
<style>
    .sidebar {
        padding: 0 20px 9999px 0;
        font-size: 13px;
        margin: 0 0 -9999px;
        border-right: 1px solid #eee;
    }
    .osh-msg-box.-success {
        border-color: #79d00d;
        color: #75c511;
    }
    .osh-msg-box.-danger {
        border-color: red;
        color: red;
    }
    .osh-msg-box {
        border-width: 1px;
        border-style: solid;
        padding: 2px 5px;
        margin: 10px 0;
    }
    .btn.btn-default {
        min-width: 40px;
        height: 34px;
        background: #291e88;
        border-color: #ccc;
        text-align: center;
        padding: 8px;
        font-size: 15px;
        color: #fff;
        letter-spacing: 2px;
    }
</style>
<main id="maincontent" class="page-main" style="padding-top: 62px;"><a id="contentarea" tabindex="-1"></a>
        <div class="beadcumarea">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12"><div class="breadcrumbs">
                            <ul class="items">
                                <li class="item home">
                                    <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                                </li>
                                <li class="item cms_page">
                                    <strong>Track Order</strong>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="sidebar">
                    <div class="block block-collapsible-nav">
                        <div class="title block-collapsible-nav-title" data-mage-init='{"toggleAdvanced": {"toggleContainers": "#block-collapsible-nav", "selectorsToggleClass": "active"}}'>
                            <strong>Account Dashboard</strong>
                        </div>
                        <div class="content block-collapsible-nav-content" id="block-collapsible-nav">
                            <ul class="nav items">
                                @guest
                                <li class="nav item "><a href="{{url('/my-account')}}">Account Dashboard</a></li>
                                <li class="nav item">
                                    <span class="delimiter"></span>
                                </li>
                                <li class="nav item"><a href="{{url('/my-account-info')}}">Account Information</a></li>
                                <li class="nav item">
                                    <span class="delimiter"></span>
                                </li>
                                <li class="nav item"><a href="{{url('/reward-points')}}">Point Balance</a></li> 
                                <li class="nav item current"><a href="{{url('/track-order')}}"><strong>Track Order</strong></a></li> 
                                @else
                                <li class="nav item"><a href="{{url('/my-account')}}">Account Dashboard</a></li>
                                <li class="nav item">
                                    <span class="delimiter"></span>
                                </li>
                                <li class="nav item"><a href="{{url('/address-book')}}">Address Book</a></li>
                                <li class="nav item"><a href="{{url('/my-account-info')}}">Account Information</a></li>
                                <li class="nav item">
                                    <span class="delimiter"></span>
                                </li>
                                <li class="nav item"><a href="{{url('/user-orders')}}">My Orders</a></li>
                                <li class="nav item current"><a href="{{url('/track-order')}}"><strong>Track Order</strong></a></li> 
                                <li class="nav item"><a href="{{url('/reward-points')}}">Point Balance</a></li> 
                                <!--<li class="nav item"><a href="">Gift Card</a></li>  -->
                                <li class="nav item">
								<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									Logout
								</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
								</form></li>
								@endguest
                            </ul>    
                        </div>
                    </div>
                    <div class="block account-nav">
                        <div class="title account-nav-title" data-mage-init='{"toggleAdvanced": {"toggleContainers": "#account-nav", "selectorsToggleClass": "active"}}'>
                            <strong></strong>
                        </div>
                        <div class="content account-nav-content" id="account-nav">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <div class="block block-dashboard-info">
                   <div class="block-title"><strong>Track Your Order</strong></div>
                    <hr>
                    <div class="box box-bdr mtm pam myaccountBox2">
                     <label class="mts mts1 required" for="AddressForm_last_name">Tracking Number<span class="required">*</span></label>
    				<div class="input-group">
    					<input type="text" class="form-control" placeholder="Enter Your Tracking Number" id="ecr_number">
    					<div class="input-group-btn">
    					  <button class="btn btn-default" type="submit" id="track_order_ecr">
    						<i class="fa fa-search" aria-hidden="true"></i>
    					  </button>
    					</div>
    				  </div>
    				  <br>
    				</div>
    				<div id="order-status"></div>
    				<div id="order-status-date"></div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>

    jQuery(function () {
       jQuery("#track_order_ecr").on('click', function () {
           var ecr=jQuery("#ecr_number").val();
		   if(ecr == ''){
			    document.getElementById("ecr_number").focus();
			   alert('Enter ECR number');
		   }else{
		      // alert(ecr);
			   var url_op = base_url + "/track-order-byecr/";
				jQuery.ajax({
					url: url_op,
					type: 'GET',
					data: {ecr: ecr},
					success: function (data = false) {
					    if(data =='No Data Found'){
					        var ecr=jQuery("#ecr_number").val();
					        var url_op = base_url + "/track-order-status/";
				            jQuery.ajax({
				                url: url_op,
            					type: 'GET',
            					data: {ecr: ecr},
            					success: function (result = false) {
            					    if(result == 0){
            					        document.getElementById('order-status').innerHTML = '<h1 class="text-danger">Tracking number not found</h1>';
            					    }else{
            					       document.getElementById('order-status').innerHTML = '<h1 class="text-success">'+ result +'</h1>'; 
            					    }
            					}
				            });
					      //  document.getElementById('order-status').innerHTML = '<h1 class="text-danger">No Data Found</h1>';
					    } else {
						    document.getElementById('order-status').innerHTML = '<h1 class="text-success">'+data[0].status[0][0]+'</h1>';
						    document.getElementById('order-status-date').innerHTML = '<h1 class="text-success">'+data[0].status[0][2]+'</h1>';
						} 
					}
				});
		   }
       });
    });
</script>
@endsection

