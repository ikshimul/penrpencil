<?php

use App\Http\Controllers\product\ProductController;
use App\Http\Controllers\HomeController;
?>
@extends('layouts.app')
@section('title','Pride Limited | A Pride Group venture')
@section('content')
<!-- Breadcrumb Area -->
<div class="mi-breadcrumb-area mi-bggrey">
    <div class="mi-container">
        <div class="mi-breadcrumb text-center">
            <h2>Categories</h2>
        </div>
    </div>
    <div class="mi-breadcrumb-path">
        <div class="mi-container">
            <ul>
                <li><a href="{{url('/')}}">Home</a></li>
                <li><a href='{{url("/$title/")}}'>{{$title}}</a></li>
                <li>Sub Category</li>
            </ul>
        </div>
    </div>
</div>
<!--// Breadcrumb Area -->
<!-- Category Area -->
<div class="mi-category-area mi-section pt-0 mb-50">
    <div class="mi-container">
        <div class="mi-category-wrapper">
            <?php
            $category = HomeController::GetMainCategory();
            foreach ($category as $cat) {
                ?>
                <div class="mi-category">
                    <a href='{{url("/$cat->procat_name/$cat->procat_id")}}'>
                        <span class="mi-category-icon">
                            <?php echo html_entity_decode($cat->procat_icon, ENT_QUOTES, "UTF-8"); ?>
                        </span>
                        <h6>{{$cat->procat_name}}</h6>
                    </a>
                </div>
            <?php } ?>            
        </div>
    </div>
</div>
<!--// Category Area -->

<!-- Single Category Block -->
<div id="category-school-supplies" class="mi-category-block mb-50">
    <div class="mi-container">
        <div class="mi-title">
            <span class="mi-title-icon"><i class="fa fa-thumbs-o-up"></i></span>
            <h5>School Suppliers</h5>
            <a href="products.html" class="mi-button mi-button-small mi-button-style2">View All</a>
        </div>
        <div class="mi-mostpopular-slider mi-product-slider">

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-2.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-3.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-4.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-5.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-6.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!--// Single Category Block -->

<!-- Single Category Block -->
<div id="category-writing-supplies" class="mi-category-block mb-50">
    <div class="mi-container">
        <div class="mi-title">
            <span class="mi-title-icon"><i class="fa fa-thumbs-o-up"></i></span>
            <h5>Writing Suppliers</h5>
            <a href="products.html" class="mi-button mi-button-small mi-button-style2">View All</a>
        </div>
        <div class="mi-mostpopular-slider mi-product-slider">

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-2.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-3.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-4.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-5.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-6.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!--// Single Category Block -->

<!-- Single Category Block -->
<div id="category-office-supplies" class="mi-category-block mb-50">
    <div class="mi-container">
        <div class="mi-title">
            <span class="mi-title-icon"><i class="fa fa-thumbs-o-up"></i></span>
            <h5>Office Suppliers</h5>
            <a href="products.html" class="mi-button mi-button-small mi-button-style2">View All</a>
        </div>
        <div class="mi-mostpopular-slider mi-product-slider">

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-2.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-3.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-4.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-5.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-6.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!--// Single Category Block -->

<!-- Single Category Block -->
<div id="category-notebook-khata" class="mi-category-block mb-50">
    <div class="mi-container">
        <div class="mi-title">
            <span class="mi-title-icon"><i class="fa fa-thumbs-o-up"></i></span>
            <h5>Notebook / Khata</h5>
            <a href="products.html" class="mi-button mi-button-small mi-button-style2">View All</a>
        </div>
        <div class="mi-mostpopular-slider mi-product-slider">

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-2.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-3.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-4.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-5.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-6.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!--// Single Category Block -->

<!-- Single Category Block -->
<div id="category-paper" class="mi-category-block mb-50">
    <div class="mi-container">
        <div class="mi-title">
            <span class="mi-title-icon"><i class="fa fa-thumbs-o-up"></i></span>
            <h5>Papers</h5>
            <a href="products.html" class="mi-button mi-button-small mi-button-style2">View All</a>
        </div>
        <div class="mi-mostpopular-slider mi-product-slider">

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-2.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-3.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-4.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-5.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-6.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!--// Single Category Block -->

<!-- Single Category Block -->
<div id="category-cleaning-item" class="mi-category-block mb-50">
    <div class="mi-container">
        <div class="mi-title">
            <span class="mi-title-icon"><i class="fa fa-thumbs-o-up"></i></span>
            <h5>Cleaning Items</h5>
            <a href="products.html" class="mi-button mi-button-small mi-button-style2">View All</a>
        </div>
        <div class="mi-mostpopular-slider mi-product-slider">

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-2.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-3.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-4.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-5.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-6.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="mi-product-slideritem">
                <div class="mi-product">
                    <a href="product-details.html" class="mi-product-inside">
                        <div class="mi-product-top">
                            <img src="assets/images/products/product-image-1.png" alt="product image">
                            <span class="mi-product-discount">30% <br> Off</span>
                        </div>
                        <div class="mi-product-bottom">
                            <h6 class="mi-product-title">Double A A4 1 rim</h6>
                            <span class="mi-product-price"><del>TK. 545</del> TK. 445</span>
                            <div class="mi-product-ratings">
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span class="active"><i class="fa fa-star-o"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="mi-product-actions">
                        <ul>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#product-quickview-popup" data-toggle="modal"><i
                                        class="fa fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!--// Single Category Block -->
@endsection