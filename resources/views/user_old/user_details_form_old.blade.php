@extends('layouts.app')
@section('title','User Details')
@section('content')
<style>
    .with-errors{
        color: red;
    }
    .cms-index-index .page.messages {
        display: block;
    }
    .has-error{
        border-bottom: 1px solid red;
    }
    .login-container .block-new-customer {
        float: left;
        width: 70%;
        padding: 20px 0 20px 50px;
    }
    .login-container:after {
        position: absolute;
        left: 50%;
        top: 0;
        bottom: 0;
        background: none;
        width: 0px;
        content: '';
    }
</style>
<main id="maincontent" class="page-main" style="padding-top: 64px;">
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea"><div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page messages">
                        <div data-placeholder="messages">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="login-container">
                    <div class="block block-new-customer">
                        <div class="block-title">
                            <strong id="block-new-customer-heading" role="heading" aria-level="2">User Details Information</strong>
                        </div>

                        <form class="form create account form-create-account" action="{{route('UserDetailsSave')}}" method="post" id="form-validate" enctype="multipart/form-data" autocomplete="off">
                            {{ csrf_field() }}
                            <input type="hidden" name="login_user_id" value="{{$login_user_id}}"/>
                            <input type="hidden" name="registeruser_id" value="{{$user_info->registeruser_id}}"/>
                            <span class="text-danger">
                                {{ $errors->first('registeruser_id') }}
                            </span>
                            <fieldset class="fieldset create info">
                                <div class="field field-name-firstname required">
                                    <label class="label" for="firstname"><span>First Name</span></label>
                                    <div class="control {{ $errors->has('first_name') ? ' has-error' : '' }}">
                                        <input type="text" id="firstname"
                                               name="first_name"  value="{{ $user_info->registeruser_firstname }}" title="First&#x20;Name" class="input-text required-entry"  data-validate="{required:true}">
                                        @if ($errors->has('first_name'))
                                        <div id="password-strength-meter-container" data-role="password-strength-meter">
                                            <div id="password-strength-meter" class="password-strength-meter">
                                                {{ $errors->first('first_name') }}
                                                <span id="password-strength-meter-label" data-role="password-strength-meter-label" >
                                                </span>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="field field-name-lastname required">
                                    <label class="label" for="lastname"><span>Last Name</span></label>
                                    <div class="control {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                        <input type="text" id="lastname"
                                               name="last_name" value="{{ $user_info->registeruser_lastname }}"
                                               title="Last&#x20;Name"
                                               class="input-text required-entry"  data-validate="{required:true}">
                                        @if ($errors->has('last_name'))
                                        <div id="password-strength-meter-container" data-role="password-strength-meter">
                                            <div id="password-strength-meter" class="password-strength-meter">
                                                {{ $errors->first('last_name') }}
                                                <span id="password-strength-meter-label" data-role="password-strength-meter-label" >
                                                </span>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="field gender">
                                    <label class="label" for="gender"><span>Country</span></label>
                                    <div class="control">
                                        <select id="country" name="Shipping_ddlcountry" title="country">
                                            <option value="" selected="selected"> </option>
                                            <option value="Bangladesh">Bangladesh</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="field gender">
                                    <label class="label" for="gender"><span>City</span></label>
                                    <div class="control {{ $errors->has('registeruser_city') ? ' has-error' : '' }}">
                                        <input type="text" name="registeruser_city"  value="{{ old('registeruser_city') }}" id="registeruser_address" title="address" class="input-text">
                                        @if ($errors->has('registeruser_city'))
                                        <div id="password-strength-meter-container" data-role="password-strength-meter">
                                            <div id="password-strength-meter" class="password-strength-meter">
                                                {{ $errors->first('registeruser_city') }}
                                                <span id="password-strength-meter-label" data-role="password-strength-meter-label" >
                                                </span>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="fieldset create account" data-hasrequired="* Required Fields">
                                <div class="field required">
                                    <label for="email_address" class="label"><span>Address</span></label>
                                    <div class="control {{ $errors->has('registeruser_address') ? ' has-error' : '' }}">
                                        <input type="text" name="registeruser_address"  value="{{ old('registeruser_address') }}" id="registeruser_address" title="address" class="input-text">
                                        @if ($errors->has('registeruser_address'))
                                        <div id="password-strength-meter-container" data-role="password-strength-meter">
                                            <div id="password-strength-meter" class="password-strength-meter">
                                                {{ $errors->first('registeruser_address') }}
                                                <span id="password-strength-meter-label" data-role="password-strength-meter-label" >
                                                </span>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="password-area">
                                    <div class="field password required" data-mage-init='{"passwordStrengthIndicator": {}}'>
                                        <label for="zipcode" class="label"><span>Zip Code</span></label>
                                        <div class="control {{ $errors->has('registeruser_zipcode') ? ' has-error' : '' }}">
                                            <input type="text" name="registeruser_zipcode" id="password"
                                                   title="Password"
                                                   class="input-text"
                                                   data-password-min-length="8"
                                                   data-password-min-character-sets="1"
                                                   data-validate="{required:true, 'validate-customer-password':true}"
                                                   autocomplete="off">
                                            @if ($errors->has('registeruser_zipcode'))
                                            <div id="password-strength-meter-container" data-role="password-strength-meter">
                                                <div id="password-strength-meter" class="password-strength-meter">
                                                    {{ $errors->first('registeruser_zipcode') }}
                                                    <span id="password-strength-meter-label" data-role="password-strength-meter-label" >
                                                    </span>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="field confirmation required">
                                        <label for="password-confirmation" class="label"><span>Mobile No.</span></label>
                                        <div class="control {{ $errors->has('registeruser_phone') ? ' has-error' : '' }}">
                                            <input type="text" name="registeruser_phone" title="Phone" id="registeruser_phone" class="input-text" autocomplete="off">
                                            @if ($errors->has('registeruser_phone'))
                                            <div id="password-strength-meter-container" data-role="password-strength-meter">
                                                <div id="password-strength-meter" class="password-strength-meter">
                                                    {{ $errors->first('registeruser_phone') }}
                                                    <span id="password-strength-meter-label" data-role="password-strength-meter-label" >
                                                    </span>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="actions-toolbar">
                                <div class="primary">
                                    <button type="submit" class="action submit primary" title="Create an Account"><span>Go To Next</span></button>
                                </div>
                            </div>
                        </form>
                        <script>
                            jquery(function ($) {

                                var dataForm = $('#form-validate');
                                var ignore = 'input[id$="full"]';
                                dataForm.mage('validation', {
                                    errorPlacement: function (error, element) {
                                        if (element.prop('id').search('full') !== -1) {
                                            var dobElement = $(element).parents('.customer-dob'),
                                                    errorClass = error.prop('class');
                                            error.insertAfter(element.parent());
                                            dobElement.find('.validate-custom').addClass(errorClass)
                                                    .after('<div class="' + errorClass + '"></div>');
                                        } else {
                                            error.insertAfter(element);
                                        }
                                    },
                                    ignore: ':hidden:not(' + ignore + ')'
                                }).find('input:text').attr('autocomplete', 'off');
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection