<style>
/* HIDE RADIO */
[type=checkbox] { 
  position: absolute;
  opacity: 0;
  width: 0;
  height: 0;
}

/* IMAGE STYLES */
[type=checkbox] + img {
  cursor: pointer;
}

/* CHECKED STYLES */
[type=checkbox]:checked + img {
  outline: 2px solid #2ccc19;
}
.select-product{
    outline: 2px solid #2ccc19;
}
</style>
<img src="https://pride-limited.com/storage/app/public/logo.png" style="width: 0px;height: auto;border: 1px solid #eee;">
@foreach($productlist as $procat)
     <div class="col-md-2 col-xs-4 col-lg-2">
		<label id="select-product" style="cursor: pointer;">
		     <input type="checkbox" name="related_product_id[]" value="{{$procat->product_id}}" />
		     <img src="{{url('/')}}/storage/app/public/pgallery/{{$procat->productimg_img}}" style="width: 100px;height: auto;border: 1px solid #eee;">
		</label>
	  </div>
@endforeach