@extends('layouts.app')
@section('title', 'Track Order')
@section('content')
<style>
    .error-parent {
        height:60vh;
        position:relative;
    }
    .error-parent h1 {
        position:absolute;
        margin:0;
        left:50%;
        top:50%;
        transform:translate(-50%, -50%);
    }
    
</style>
<div class="container">
    <div class="error-parent">
        <h1>Error 404! Page not found. <a href="{{url('/')}}" style="text-decoration:underline">Go to Home</a></h1>
    </div>
</div>
@endsection