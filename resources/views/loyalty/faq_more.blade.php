@extends('layouts.app')
@section('title', 'Pride Limited | PRIDE INSIDER REWARDS')
@section('content')
<link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/sephora.css')}}/?11" />
<style>
    .title {
    text-transform: uppercase;
    font-weight: 600;
}
</style>
<main data-v-7cf67cfb="" id="page" class="page-container container">
    <div data-v-72bbedf8="" data-v-7cf67cfb="" class="bp-container">
        <div data-v-72bbedf8="" class="bp-section">
            <div data-v-72bbedf8="" class="header-section" style="background-image: url(&quot;{{ url('/') }}/storage/app/public/loyalty/banner.jpg&quot;);">
                <div data-v-72bbedf8="" class="logo">
                </div>
                
            </div>
            <div data-v-72bbedf8="" class="content-section">

			<div data-v-0f0c09c8="" data-v-72bbedf8="" class="faqs-container">
				<div data-v-0f0c09c8="" class="h3 title">FAQs</div>
				<div data-v-0f0c09c8="" class="faqs">
					<div data-v-0f0c09c8="" aria-multiselectable="true" role="tablist" class="faq accordion">
						<div data-v-0f0c09c8="" onclick="" data-target="#faq-0" data-parent="#faq-0-accordion" role="tab" data-toggle="collapse" class="collapsed"><i data-v-0f0c09c8="" class="collapse-arrow"></i>
							<div data-v-0f0c09c8="" class="question">How do I redeem Rewards?</div>
						</div>
						<div data-v-0f0c09c8="" id="faq-0" role="tabpanel" class="collapse">
							<div data-v-0f0c09c8="" class="answer">All Rewards can be redeemed only upon purchase of merchandise, at any showroom in Dhaka. Currently, rewards cannot be redeemed online.

							</div>
						</div>
					</div>
					<div data-v-0f0c09c8="" aria-multiselectable="true" role="tablist" class="faq accordion">
						<div data-v-0f0c09c8="" onclick="" data-target="#faq-1" data-parent="#faq-1-accordion" role="tab" data-toggle="collapse" class="collapsed"><i data-v-0f0c09c8="" class="collapse-arrow"></i>
							<div data-v-0f0c09c8="" class="question">How do I sign up for the Pride Insider Rewards Program?</div>
						</div>
						<div data-v-0f0c09c8="" id="faq-1" role="tabpanel" class="collapse">
							<div data-v-0f0c09c8="" class="answer">
							    In stores, if you purchase Tk 2000 or more of retail products, you automatically get signed up in the Program. You will be assisted by our Sales Personnel at the counter, who will give you  digital form to sign up. 
							</div>
						</div>
					</div>
					<div data-v-0f0c09c8="" aria-multiselectable="true" role="tablist" class="faq accordion">
						<div data-v-0f0c09c8="" onclick="" data-target="#faq-2" data-parent="#faq-2-accordion" role="tab" data-toggle="collapse" class="collapsed"><i data-v-0f0c09c8="" class="collapse-arrow"></i>
							<div data-v-0f0c09c8="" class="question">Do I need to pay a fee for the Pride Insider Rewards Program?</div>
						</div>
						<div data-v-0f0c09c8="" id="faq-2" role="tabpanel" class="collapse">
							<div data-v-0f0c09c8="" class="answer">No, Pride Insider Rewards membership is complimentary with a purchase of Tk 2000 or more of retail products. 
							</div>
						</div>
					</div>
					<div data-v-0f0c09c8="" aria-multiselectable="true" role="tablist" class="faq accordion">
						<div data-v-0f0c09c8="" onclick="" data-target="#faq-3" data-parent="#faq-3-accordion" role="tab" data-toggle="collapse" class="collapsed"><i data-v-0f0c09c8="" class="collapse-arrow"></i>
							<div data-v-0f0c09c8="" class="question">How do I check my points balance?</div>
						</div>
						<div data-v-0f0c09c8="" id="faq-3" role="tabpanel" class="collapse">
							<div data-v-0f0c09c8="" class="answer">
							   To view your Insider Rewards point balance online, simply log into your online account. You may view your points balance, as well as your point activity in the ‘Point History’ section in the tab 'My Accounts'. You may also approach any of our Sales staff in Pride stores or Urban Truth shop-in-shops, to find out your points balance.

							</div>
						</div>
						
						
					</div>
				<div data-v-0f0c09c8="" aria-multiselectable="true" role="tablist" class="faq accordion">
						<div data-v-0f0c09c8="" onclick="" data-target="#faq-4" data-parent="#faq-4-accordion" role="tab" data-toggle="collapse" class="collapsed"><i data-v-0f0c09c8="" class="collapse-arrow"></i>
							<div data-v-0f0c09c8="" class="question">Do the points I accumulate expire?</div>
						</div>
						<div data-v-0f0c09c8="" id="faq-4" role="tabpanel" class="collapse">
							<div data-v-0f0c09c8="" class="answer">
							    When the next 12 months of your membership account begins, your points are not carried forward. If your account has been inactive for a period of 18 months, you will be demoted by one tier. 
							</div>
						</div>
					</div>
					
					<div data-v-0f0c09c8="" aria-multiselectable="true" role="tablist" class="faq accordion">
						<div data-v-0f0c09c8="" onclick="" data-target="#faq-5" data-parent="#faq-5-accordion" role="tab" data-toggle="collapse" class="collapsed"><i data-v-0f0c09c8="" class="collapse-arrow"></i>
							<div data-v-0f0c09c8="" class="question">What happens to my points when I exchange items?</div>
						</div>
						<div data-v-0f0c09c8="" id="faq-5" role="tabpanel" class="collapse">
							<div data-v-0f0c09c8="" class="answer">
							   Points will be adjusted from your account when merchandise is exchanged.
							</div>
						</div>
					</div>
					
					<div data-v-0f0c09c8="" aria-multiselectable="true" role="tablist" class="faq accordion">
						<div data-v-0f0c09c8="" onclick="" data-target="#faq-6" data-parent="#faq-6-accordion" role="tab" data-toggle="collapse" class="collapsed"><i data-v-0f0c09c8="" class="collapse-arrow"></i>
							<div data-v-0f0c09c8="" class="question">What do I do if my Pride Insider Rewards Membership Card is lost or stolen?</div>
						</div>
						<div data-v-0f0c09c8="" id="faq-6" role="tabpanel" class="collapse">
							<div data-v-0f0c09c8="" class="answer">
							  In the event that your Pride Insider Rewards Card has been stolen or misplaced, you may approach our Sales staff at any store to cancel your card and re-apply for a new card. Upon verification, your points balance will be transferred to your new card within 7-10 working days.

							</div>
						</div>
					</div>
					
					<div data-v-0f0c09c8="" aria-multiselectable="true" role="tablist" class="faq accordion">
						<div data-v-0f0c09c8="" onclick="" data-target="#faq-7" data-parent="#faq-7-accordion" role="tab" data-toggle="collapse" class="collapsed"><i data-v-0f0c09c8="" class="collapse-arrow"></i>
							<div data-v-0f0c09c8="" class="question">How can I redeem my Birthday Gift?</div>
						</div>
						<div data-v-0f0c09c8="" id="faq-7" role="tabpanel" class="collapse">
							<div data-v-0f0c09c8="" class="answer">
							  Platinum and Gold Pride Insider Rewards members are eligible to redeem one birthday gift per year during their birthday month. No purchase is necessary when you are redeeming your birthday gift in-store. <br><br>
Due to limited quantities, substitution of gifts may occur. Gifts will be available while stocks last. This offer is subject to change, alteration, or termination by Pride Limited at its sole discretion at any time.

							</div>
						</div>
					</div>
					
					<div data-v-0f0c09c8="" aria-multiselectable="true" role="tablist" class="faq accordion">
						<div data-v-0f0c09c8="" onclick="" data-target="#faq-8" data-parent="#faq-8-accordion" role="tab" data-toggle="collapse" class="collapsed"><i data-v-0f0c09c8="" class="collapse-arrow"></i>
							<div data-v-0f0c09c8="" class="question">What are Pride Insider Store Events?</div>
						</div>
						<div data-v-0f0c09c8="" id="faq-8" role="tabpanel" class="collapse">
							<div data-v-0f0c09c8="" class="answer">
							  Pride Insider Rewards members may receive exclusive invitations to events in-store. As space is limited at these events, prior registration is required. Please follow the instructions you receive in your invitation email to register for the event. You will be asked to show your RSVP Confirmation Invite at the event. We regret that we may be unable to register you for an event once it reaches full capacity, due to space limitations.
							</div>
						</div>
					</div>
					
					<div data-v-0f0c09c8="" aria-multiselectable="true" role="tablist" class="faq accordion">
						<div data-v-0f0c09c8="" onclick="" data-target="#faq-9" data-parent="#faq-9-accordion" role="tab" data-toggle="collapse" class="collapsed"><i data-v-0f0c09c8="" class="collapse-arrow"></i>
							<div data-v-0f0c09c8="" class="question">How do I change or update my personal details?</div>
						</div>
						<div data-v-0f0c09c8="" id="faq-9" role="tabpanel" class="collapse">
							<div data-v-0f0c09c8="" class="answer">
							 Please log into your online account to update your Name, Email and Mobile Number. You may email customer service at hello@pride-grp.com to update your address details.
							</div>
						</div>
					</div>
					
					<div data-v-0f0c09c8="" aria-multiselectable="true" role="tablist" class="faq accordion">
						<div data-v-0f0c09c8="" onclick="" data-target="#faq-10" data-parent="#faq-10-accordion" role="tab" data-toggle="collapse" class="collapsed"><i data-v-0f0c09c8="" class="collapse-arrow"></i>
							<div data-v-0f0c09c8="" class="question">Will I receive points when I purchase a Pride Gift Card?</div>
						</div>
						<div data-v-0f0c09c8="" id="faq-10" role="tabpanel" class="collapse">
							<div data-v-0f0c09c8="" class="answer">
							 No points will be awarded when you buy a Pride Gift Card. Gift Cards can only be purchased in Pride Limited store in Banani and can only be redeemed there. Gift Cards must be redeemed in one purchase. Unused value on the Gift Card cannot be redeemed afterwards. Points will be awarded once on Gift Card redemptions.

							</div>
						</div>
					</div>
					
				</div>
				
			</div>
		</div>
	</div>
</main>
@endsection