@extends('layouts.app')
@section('title', 'Pride Limited | TERMS & CONDITIONS “PRIDE INSIDER REWARDS” LOYALTY PROGRAM')
@section('content')
<style>
    .top-heading{
        font-weight: 600;
        padding-top: 17px;
        padding-bottom: 30px;
        font-size: 19px;
        text-align: justify;
    }
    .heading{
        display:block;
        font-weight:600;
    }
    .body-letter{
        padding-left:10px;
        text-align: justify;
        /*line-height: 2.26; */
    }
    .module, .module-small {
        position: relative;
        padding: 10px 0;
        background-repeat: no-repeat;
        background-position: 50% 50%;
        background-size: cover;
    }
    p, ol, ul, blockquote {
        margin: 0 0 16px;
    }
    .padding-top{
        padding-top: 10px;
    }
</style>
<div class="container bg-grey" style="padding-top:70px;color:black;" id="search_result">
    <main class="main">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="layout">
                        <div class="module section">
                            <div class="three modules">
                                <h2 class="top-heading">TERMS & CONDITIONS “PRIDE INSIDER REWARDS” LOYALTY PROGRAM</h2>

                                <span class="heading">1. INTRODUCTION</span>
                                <p class="body-letter padding-top">
                                    1.1 Welcome to the PRIDE INSIDER REWARDS loyalty program ("Loyalty Program").<br>
                                </p>
                                <p class="body-letter">
                                    1.2 Membership is free of charge and a purchase of Tk 2000 or more at our Retail stores of our Retail products is necessary to become a member.<br>
                                </p>
                                <p class="body-letter">
                                    1.3 By being a member of the Loyalty Program, you can earn points through Eligible Purchases (defined below) which qualify you to different membership tiers where you can take part in offers, services, events and much more offered by Pride Group brands. You will find more information about current rewards and offers under your membership account ("Account") at the websites pride-limited.com and urban-truth.com. <br>
                                </p>
                                <p class="body-letter">
                                    1.4 You can find information regarding how Pride Group will process your personal data in the Privacy Notice here.<br>
                                </p>
                                <p class="body-letter">These terms and conditions ("Terms & Conditions") form the agreement (the "Agreement") between you ("Member" or "you") and Pride Group with respect to the Loyalty Program. When accessing the Loyalty Program or Pride Group’s online channels for Bangladesh (the, "Website"), you are subject to the Website's  Terms of Use [insert link]. In the event of any conflict between this Agreement and the Website's Terms of Use, this Agreement will control.</p>

                                <span class="heading">2. MEMBERSHIP</span>
                                <p class="body-letter padding-top">
                                    2.1 The Loyalty Program is open to Bangladeshi residents within Dhaka city, who have a current and valid email account and are at least 18 years of age (or 16 years of age with the consent of a parent, guardian or legal representative as applicable). Pride Group reserves the right to request written confirmation of such consent.  Employees, officers, directors, agents and representatives of Pride Group are eligible for Membership but may be excluded from certain promotions.
                                </p>
                                <p class="body-letter">
                                    2.2 By participating in the Loyalty Program, you represent that you are at least 18 years of age (or 16 years of age and have the consent of your parent(s), guardian or legal representative, as applicable) and that you agree to these Terms and Conditions. Pride Group reserves the right to disqualify Members who have violated any of the Terms and Conditions.
                                </p>
                                <p class="body-letter">
                                    2.3 Your Membership is personal, non-transferable and subject to these Terms and Conditions as well as any other rules, regulations, policies, and procedures adopted by Pride Group as approved by you upon taking part of offers, rewards, making purchases and other related services. Membership is limited to individuals only and limited to one account per individual.
                                </p>
                                <p class="body-letter">
                                    2.4 Companies, groups, associations or other entities, or others making commercial or bulk purchases are not eligible for Membership. The membership may not be used for reselling or profit.
                                </p>
                                <p class="body-letter">
                                    2.5 It is your responsibility, and a condition for your membership, to keep your email and contact details up to date in your Account.
                                </p>
                                <span class="heading">3. Pride Group LOYALTY PROGRAM IN DHAKA</span>

                                <p class="body-letter padding-top">
                                    3.1 The Loyalty Program is a national program where Members will receive points ("Points") on their Account through Eligible Purchases and participation in other special programs and promotional offers that may be announced by Pride Group or its third-party partners from time to time.
                                </p>
                                <p class="body-letter">
                                    3.2  Eligible Purchases means purchases of merchandise (after promotional offers have been applied, before taxes and shipping charges have been applied and minus returns, refunds or credit adjustments, rounded to the nearest taka) made at select Pride Group stores in Dhaka("Store") or online on the Pride Limited and Urban Truth Websites and shipping to Bangladesh. Eligible Purchases count toward Member's Point accrual. Members will receive one Point for each hundred taka (BDT 100) of Eligible Purchases. In all instances, number of Points earned subject to change; check back here for details.
                                </p>
                                <p class="body-letter">
                                    3.3 Points awarded for purchases that are returned, refunded or otherwise adjusted will be deducted from your Points total.
                                    However, we do not accept returns, only exchanges. To view our Exchange Policy, click here. 
                                </p>
                                <p class="body-letter">
                                    3.4 Please note that you cannot use Membership rewards, or earn Points on purchases, in Pride Group stores outside Dhaka at the moment.
                                </p>
                                <p class="body-letter">
                                    Exclusion from Eligible Purchases: Points will not be awarded on unauthorized or fraudulent purchases, or the purchase of gift cards, or wholesale purchases, or purchases made with the intent to resell. The amount of a purchase made with a gift card, merchandise credit or rewards as method of payment will not be applied to Member Point accrual. Points will not be awarded if, in Pride Group’s reasonable opinion, the merchandise or services purchases will be used for resale or commercial use and any Points awarded on such purchases will be forfeited.
                                </p>

                                <span class="heading">4.  POINTS, TIERS AND VOUCHERS</span>
                                <p class="body-letter padding-top">
                                    4.1 To receive Points for Eligible Purchases, Members must provide the Member ID associated with his/her Membership to the sales associate at a participating Store or sign into their online account before checkout for online transactions on the Website. Members may view their Point balance at any time by visiting their Account.
                                </p>
                                <p class="body-letter">
                                    4.3 Points will accumulate toward rewards ("Rewards"), the means by which are described in these Terms and Conditions and/or related promotional offers. All new accounts are opened at the first tier of membership (100 to 500 Points = "Silver" tier). The Points you earn also count toward the second tier membership (600 to 1000 Points = "Gold" tier). The Points you earn then count toward the third tier membership ( 1100 and more points = "Platinum" tier). The Platinum tier offers additional benefits, such as free standard shipping on all Pride Group online orders.
                                </p>
                                <p class="body-letter">
                                    4.4 The date on which you become a Member will constitute the “Start Date” of each Membership Year. The “Membership Year” is the 12 months following the Start Date of each year. The tier level of your membership is based on the Points earned during the current or previous Membership Year, whichever is the highest. If you are a new Silver Member or do not have sufficient Points to qualify for the Gold Member tier, your membership will start at the Silver Member tier. If you reach the Gold Member tier during a Membership Year, you will stay at this tier for the rest of the Membership Year and the following Membership Year. If, during the following Membership Year, you do not accumulate sufficient Points to qualify for the Gold Member tier, you will hold your status as Gold Member but will return to rewards applicable for Silver tier for the following Membership Year. This applies for the Platinum tier as well.
                                </p>
                                <p class="body-letter">
                                    4.5 After the end of each Membership Year all points counting towards the next membership tier level will be reset and start over at 0.
                                </p>
                                <p class="body-letter">
                                    4.6 Points accumulated during a Membership Year are valid until the next Start Date.
                                </p>
                                <p class="body-letter">
                                    4.7 For every 100 Points you earn on your Silver  Account, you will receive a Reward value of BDT 1 for that Account; for Gold Account, you will receive reward value of BDT 1.50 paisa; for Platinum Account, you will receive reward value of BDT 2. You can redeem a Reward by either: (1) presenting the Reward in store from your Account (access via the LOYALTY CARD), or (2) online at the Website by selecting the Reward at checkout.  Each Reward is for one-time use only and once per day and may not be combined with one promotional offer nor used to purchase items on sale. Pride Group may, at any time and without notice, change the Points earning and Rewards procedures and offerings, including the conversion rate between Points and Reward value.
                                </p>
                                <p class="body-letter">
                                    4.8 Rewards will be issued at any time for your next purchase after the date that the sufficient Point limit is reached. You will have 90 days from the date of issuance to redeem your Rewards.
                                </p>
                                <p class="body-letter">
                                    Points and Rewards are purely promotional, have no cash value and are not exchangeable for cash or cash equivalent. When redeeming your Reward, the value of your purchase must be greater than the value of the Reward. Non-Commercial goods, such as shopping bags in store, does not count towards this sum.  If you exchange merchandise purchased with a Reward, the Reward will be adjusted accordingly.
                                </p>
                                <span class="heading">5. CHANGES, TERMINATION AND/OR REMOVAL FROM Pride Group  LOYALTY PROGRAM</span>
                                <p class="body-letter padding-top">
                                    5.1 You may cancel your membership at any time through your Account at the websites or by contacting customer service. Please note that cancelling your membership means that you will only be able to shop at Pride Group websites after renewing your registration.
                                </p>
                                <p class="body-letter">
                                    5.2 Rewards cannot be used for purchasing gift cards or applied to past purchases.
                                </p>
                                <p class="body-letter">
                                    5.4 If you cancel your membership any Points which remain in such account will be forfeited.
                                </p>
                                <p class="body-letter">
                                    Pride Group may, at its sole discretion, terminate, alter, limit, suspend or modify the Pride Group Loyalty Program, and/or the Terms and Conditions at any time, for any reason, without prior notice. Pride Group will post any additional Loyalty Program details and updates  (including these Terms and Conditions) here and then update the "Last Updated" date above. Pride Group may also email you about any such changes. Your continued participation in the Loyalty Program will confirm your acceptance of such changes.  As a result of changes to applicable laws, changes in services provided by Pride Group or the introduction of new services.
                                </p>

                                <span class="heading">6. LIMITATION OF LIABILITY </span>
                                <p class="body-letter padding-top">
                                    6.1 Pride Group reserves the right to revoke the membership of any Member in the Loyalty Program and/or revoke any or all benefits the Member may be entitled to, if in the sole opinion of Pride Group, a Member abuses any Loyalty Program privileges, fraudulently uses the Loyalty Program, fails to comply with these Terms and Conditions, does not use the Membership account for more than 36 months, or otherwise earns benefits through deception, forgery and/or fraud. In addition, Loyalty Program Points, benefits or Member accounts may not be merged, transferred, purchased, sold, assigned, auctioned or traded. Doing so, or attempts to do so, will void the Member account. In the event that Pride Group cancels your membership or terminates the Loyalty Program for any reason, all Points reward or other benefits earned on your Member account will be forfeited.
                                </p>

                                <span class="heading"> 7. LIMITATION OF LIABILITY </span>
                                <p class="body-letter padding-top">
                                    7.1 Pride Group will not be liable for any system failure or malfunction of the Pride Group Loyalty Program or any consequences thereof. To the fullest extent permissible under applicable law, Pride Group is not responsible or liable for any direct, indirect, incidental, consequential or any other damages under any contract, negligence, strict liability or other theory arising out of or relating in any way, directly or indirectly, to Members' participation in the Loyalty Program. This applies even if foreseeable or even if Pride Group has been advised of the possibility of such damages.
                                </p>

                                <span class="heading"> 8. APPLICABLE LAW AND JURISDICTION</span>
                                <p class="body-letter padding-top">
                                    8.1 These Terms & Conditions, the relationship between you and Pride Group, and Pride Group's Loyalty Program, shall be governed by, construed and enforced in accordance with the laws of Bangladesh, without giving effect to any conflicts of law provisions. Any dispute, claim or controversy arising or relating to these Terms and Conditions or membership shall be resolved by the applicable courts in Bangladesh unless otherwise stipulated in applicable, mandatory consumer protection laws.
                                    Member is responsible and liable for any applicable national or local income, sales, use or other taxes which may result from Member's participation in the Loyalty Program.
                                </p>

                                <span class="heading"> 9. COMMUNICATIONS </span>
                                <p class="body-letter padding-top">
                                    9.1 If you have any questions regarding the Pride Group Loyalty Program  you may contact customer service at loyalty@pride-grp.com. All correspondence regarding the Pride Group Loyalty Program should be addressed to the above address. 
                                </p>
                                <p class="body-letter">
                                    You agree that Pride Group may contact you via mail, email, and other channels about marketing, special Loyalty Program promotions, offers, surveys and more. Pride Group will also use these channels to notify Members when they are eligible for a reward, communicate Loyalty Program membership information, Loyalty Program changes and more at Pride Group's discretion. You may opt out of marketing, promotional and survey communications by unsubscribing via the method listed in the communication or contacting customer service. Please note that even if you opt out of receiving marketing or promotional communications, Pride Group may continue to send you non-marketing or non-promotional emails, such as those about your account, Pride Group Loyalty Program membership or our ongoing business relations.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>


@endsection