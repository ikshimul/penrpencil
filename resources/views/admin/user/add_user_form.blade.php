 @extends('admin.layouts.app')
@section('title', 'Pride Limited')
@section('content')
 <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Add User</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-2"></div>
                <form action="<?php echo url('/create-admin-user')?>" method="post" enctype="multipart/form-data" >
                    {{csrf_field()}}
                    <div class="col-md-5">
                        @if(session('save'))
                        <div class="alert alert-success" role="alert">
                            <?php session('save') ?>
                        </div>
                        @endif
                        @if(session('error'))
                        <div class="alert alert-success" role="alert">
                            <?php session('error') ?>
                        </div>
                        @endif
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="first_name" value="<?php old('first_name') ?>" class="form-control">
                            <span class="text-danger"><?php $errors->first('first_name') ?></span>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" value="<?php ('email') ?>" class="form-control">
                            <span class="text-danger"><?php $errors->first('email') ?></span>
                        </div>
                        <div class="form-group">
                            <label>User Name</label>
                            <input type="text" name="user_name" value="<?php old('user_name')?>" class="form-control">
                            <span class="text-danger"><?php $errors->first('user_name') ?></span>
                        </div>
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" name="phone_number" value="<?php old('user_name')?>" class="form-control">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <textarea type="text" name="address" class="form-control" rows="3"></textarea>
                            <span class="text-danger"><?php $errors->first('address') ?></span>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password"  class="form-control">
                            <span class="text-danger"><?php $errors->first('password') ?></span>
                        </div>
                        <div class="form-group">
                            <label>Confirm Password</label>
                            <input type="password" name="c_password" class="form-control">
                            <span class="text-danger"><?php $errors->first('c_password')?></span>
                        </div>
                        <div class="form-group">
                            <label>Role</label>
                            <select name="role_id" class="form-control">
                                <option value="">---- select role ----</option>
                                <?php foreach($role as $role_nma) { ?>
                                <option value="<?php echo $role_nma->id ?>"> <?php echo $role_nma->role_name ?></option>
                               <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Profile Image</label>
                            <input type="file" name="photo" class="form-control">
                            <span class="text-danger"><?php $errors->first('photo') ?></span>
                        </div>
                    </div>
                    <div class="col-md-5"></div>
            </div>
        </div>
        <div class="box-footer">
            <input type="submit" name="btn" class="btn btn-success" value="Add User">
        </div>
        <form>
    </div>        
    @stop