@extends('admin.layouts.app')
@section('title', 'Urban Truth')
@section('content')
<section class="content">
    @if(session('save'))
    <div class="alert alert-success" role="alert">
        {{session('save')}}
    </div>
    @endif
    @if(session('error'))
    <div class="alert alert-success" role="alert">
        {{session('error')}}
    </div>
    @endif
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Update User</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-2"></div>
                <form action="{{url('/update-user')}}" method="post" name="edit_user">
                    {{csrf_field()}}
                    <input type="hidden" name="admin_id" value="{{$user->admin_id}}">
                    <input type="hidden" name="employe_id" value="{{$user->employe_id}}">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" name="first_name" value="{{$user->employe_name}}" class="form-control">
                            <span class="text-danger">{{ $errors->first('first_name') }}</span>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" value="{{$user->employe_email}}" class="form-control">
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        </div>
                        <div class="form-group">
                            <label>User Name</label>
                            <input type="text" name="admin_username" value="{{$user->admin_username}}" class="form-control">
                            <span class="text-danger">{{ $errors->first('admin_username') }}</span>
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <textarea type="text" name="address" class="form-control" rows="3">{{$user->employe_details}}</textarea>
                            <span class="text-danger">{{ $errors->first('address') }}</span>
                        </div>
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" name="phone_number" value="{{$user->employe_telephone}}" class="form-control">
                            <span class="text-danger"></span>
                        </div>
                     <!--   <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password"  class="form-control">
                            <span class="text-danger">{{ $errors->first('password') }}</span>
                        </div>
                        <div class="form-group">
                            <label>Confirm Password</label>
                            <input type="password" name="c_password" class="form-control">
                            <span class="text-danger">{{$errors->first('c_password')}}</span>
                        </div> --->
                        <div class="form-group">
                            <label>Role</label>
                            <select type="text" name="role_id" class="form-control">
                                <option value="">---- select role ----</option>
                                @foreach($role as $role_list)
                                <option value="{{$role_list->id}}">{{$role_list->role_name}}</option>
                                @endforeach
                            </select>
                            <span class="text-danger">{{$errors->first('role_id')}}</span>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="submit" name="btn" class="btn btn-success" value="Update User">
        </div>
        <form>
    </div>
    <script>
        document.forms['edit_user'].elements['role_id'].value = '{{$user->role_id}}';
    </script>
    @stop