@extends('admin.layouts.app')
@section('title', 'Manage Category')
@section('content')
<section class="content-header">
    <h1>
        Category
        <small>Manage Category</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Category</a></li>
        <li class="active">Manage Category</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Browse All Main Category</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if (session('error'))
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i> Deleted!</h4>
                        {{ session('error') }}
                    </div>
                    @endif
                    <a class="btn bg-navy btn-flat btn-sm margin tdata" data-toggle="modal" data-target="#main_category" href="#"><i class="fa fa-fw fa-plus-circle"></i> Add New</a>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Category Name</th>
                                <th>Category Icon</th>
                                <th>Last Update</th>
                                <th>Update By</th>
                                <th style="text-align:center;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($main_category as $category)
                            <tr>
                                <td style="width:2%;">{{$category->procat_id}}</td>
                                <td>{{$category->procat_name}}</td>
                                <td><span><?php echo html_entity_decode($category->procat_icon, ENT_QUOTES, "UTF-8"); ?></span></td>
                                <td><?php
                                    $up_date = strtotime($category->procat_lastupdate);
                                    echo date('M d, Y h:m a', $up_date);
                                    ?></td>
                                <td>{{$category->employe_name}}</td>
                                <td>
                                    <a class="btn btn-info btn-flat btn-sm margin tdata" href="{{url("/edit-procate/{$category->procat_id}")}}">Edit</a>
                                    <a class="btn btn-danger btn-flat btn-sm tdata" href="#" onclick='confirm_delete("{{url("/delete-procat/{$category->procat_id}")}}")'>Delete</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    <!-- Modal -->
    <div class="modal fade" id="main_category" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Main Category</h4>
                </div>
                <div class="modal-body">
                    <form name="add_subpro" action="{{url('/save-procat')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Category Name</label>
                            <input type="text" class="form-control" name="procat_name" required/>
                        </div>
                        <div class="form-group">
                            <label>Category Icon</label>
                            <input type="text" class="form-control" name="procat_icon" required/>
                            <span class="help-block" style="color:#f39c12;">Only Numbers</span>
                        </div>
                        <div class="form-group">
                            <label>Category Order</label>
                            <input type="number" class="form-control" name="procat_order" min="1" max="100"/>
                            <span class="help-block" style="color:#f39c12;">Only Numbers</span>
                        </div>
                        <div class="form-group">
                            <label>Category Banner</label>
                            <input id="input-upload-img1" type="file" class="file" name="filename" data-preview-file-type="text">
                            <span class="help-block" style="color:#f39c12;">only .jpg image is allowed Size (Width: 118px X Height: 143px)</span>  
                        </div>
                </div>
                <div class="box-footer">
                    <div class="col-md-offset-1">
                        <input type="submit" name="btnsubmit" class="btn bg-navy btn-flat margin" value="Add Category"/>
                    </div>
                </div>
                </form>  
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <!-- Ajax modal ---->
    <div class="modal fade" id="modal-delete">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top:100px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="text-align:center;color:black;">Are you sure to delete this?</h4>
                </div>
                <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                    <a href="#" class="btn btn-sm btn-danger" id="delete_link">Delete</a>
                    <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!--- Ajax modal end ---->
</section>
@endsection