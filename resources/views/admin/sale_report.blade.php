@extends('admin.layouts.app')
@section('title', 'Pride Limited')
@section('content')
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" media="all"/>
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet" media="all"/>
<?php
// use App\Http\Controllers\Admin\ReportController;
?>
<style>
    fieldset {
        padding: .75em .825em .85em;
        margin: 0 2px;
        border: 1px solid silver;
    }
    fieldset {
        display: block;
        -webkit-margin-start: 2px;
        -webkit-margin-end: 2px;
        -webkit-padding-before: 0.35em;
        -webkit-padding-start: 0.75em;
        -webkit-padding-end: 0.75em;
        -webkit-padding-after: 0.625em;
        min-width: -webkit-min-content;
        border-width: 2px;
        border-style: groove;
        border-color: threedface;
        border-image: initial;
    }
    a:link, span.MsoHyperlink {
        mso-style-noshow: yes;
        mso-style-priority: 99;
        text-decoration: none !important;
        text-underline: single;
    }

    .margin_new {
        margin: 2px;
    }
    td, th {
        padding: 5px;
    }
    .text-design{
        color:red;
    }
    .label {
        display: inline;
        padding: .2em .6em .3em;
        font-size: 75%;
        font-weight: 600;
        line-height: 1;
        color: #fff;
        text-align: center;
        white-space: nowrap;
        vertical-align: baseline;
        border-radius: 0px;
    }
</style>
<!-- Content Header (Page header) -->
<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Sales Report</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div style="padding-bottom:15px;">
                    <fieldset><legend>Search Order</legend>
                            <form name="filtering" id="filtering"  action="{{ route('pride_admin.sale_reports') }}" method="GET">
                                
                                <table>
                                    <tr>
                                        <td class="field_label">
                                            Form Date:
                                        </td>
                                        <td>
                                            <input type="date" name="from" id="date" value="<?php echo isset($from)?date('Y-m-d',strtotime($from)):''; ?>" autocomplete="off" style="height:25px; width:230px;"/>
                                            (mm/dd/yyyy)&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field_label">
                                            To Date :
                                        </td>
                                        <td>
                                            <input type="date" name="to" id="ToDate"  value="<?php echo isset($to)?date('Y-m-d',strtotime($to)):''; ?>" autocomplete="off" style="height:25px; width:230px;"/>
                                            (mm/dd/yyyy)&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field_label">
                                            Order status
                                        </td>
                                        <td>
                                            <select name="order_status" id="order_status" style="height:25px; width:230px;">
                                                <option value="">--Select--</option>
                                                <option value="Invalidate">Invalidate</option>
                                                <option value="Pending_Dispatch">Verified</option>
                                                <option value="Dispatched">Confirm Dispatch</option>
                                                <option value="Payment_Pending">Delivered</option>
                                                <option value="Closed">Payment Received</option>
                                                <option value="Bkash_Payment_Receive">Bkash Payment Received</option>
                                                <option value="Cancelled">Cancelled</option>
                                                <option value="Exchange_Pending">Exchange Pending</option>
                                                <option value="Exchange_Dispatched">Exchange Dispatched</option>
                                                <option value="Exchanged">Exchanged</option>
                                                <option value="Returned">Returned</option>
                                                <option value="Order_Verification_Pending">Order Verification Pending</option>
                                                <option value="%%">All Orders</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field_label">
                                            Payment Mode
                                        </td>
                                        <td>
                                            <select name="ddlthreepldlv" id="ddlthreepldlv" style="height:25px; width:230px;">
                                                <option value="">--Select--</option>
                                                <option value="Pathao">Bank</option>
                                                <option value="Sundorbon">Bkash</option>
                                                <option value="Hand Delivery">Hand Delivery</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field_label">
                                        </td>
                                        <td>
                                           <input type="submit" value="Search"/>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </fieldset>
                </div>
                <div id="print_div">
                    <table id="exam" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>User Track Number</th>
                                <th>Customer Name</th>
                                <th>Order Placed</th>
                                <th>Pos Date</th>
                                <th>Delivery Date</th>
                                <th>Payment</th>
                                <th>Delivery Charge</th>
                                <th>Expense</th>
                                <th>Sub Total</th>
                                <th>Total</th>
                                <th>Discounted(10%)</th> 
                                <th>Order Status</th> 
                            </tr>
                        </thead>
                        @php($total_amount = 0)
                        @php($total_delivery = 0)
                        @php($subtotal = 0)
                        @php($total_expense = 0)
                        @php($discounted_total = 0)
                        @php($i = 1)
                        <tbody>
                            @foreach($sale_reports as $report)
                            <tr>
                                <td>{{ $i }}</td>
                                <td><a href="{{url("/order-details/{$report->conforder_id}")}}" target="__self" class="info">{{$report->conforder_tracknumber}}</a></td>
                                <td>{{ $report->registeruser_firstname }} {{ $report->registeruser_lastname }}</td>
                                <td><?php echo date('M j, Y', strtotime($report->conforder_placed_date)); ?></td>
                                <td><?php if(isset($report->pos_entry_date) && !(empty($report->pos_entry_date))) echo date('M j, Y', strtotime($report->pos_entry_date)); ?></td>
                                <td><?php if(isset($report->conforder_deliverydate) && !(empty($report->conforder_deliverydate))) echo date('M j, Y', strtotime($report->conforder_deliverydate)); ?></td>
                                <td>{{ $report->order_threepldlv }}</td>
                                <td>{{ $report->Shipping_Charge }}</td>
                                <td>{{ $report->amount }}</td>
                                <td>{{ $report->shoppingcart_subtotal }}</td>
                                <td>{{ $report->Shipping_Charge + $report->shoppingcart_subtotal }}</td>
                                <td>
                                <?php
                                    $offer = $report->get_offer;
                                    $total = $report->shoppingcart_subtotal;
                                    if ($offer == 1) {
                                        $dis = $total * 10 / 100;
                                        $distotal = $total - $dis + $report->Shipping_Charge;
                                    } else {
                                        $distotal = $total + $report->Shipping_Charge;
                                    }
                                    echo $distotal;
                                    $discounted_total += $distotal;
                                ?>
                                </td>
                                <td>
                                    <?php if ($report->conforder_status == "Order_Verification_Pending") { ?>
                                        <span class="label label-primary" style="font-size:12px;"><?php echo str_replace("_"," ",$report->conforder_status); ?></span>
                                    <?php } else if ($report->conforder_status == "Pending_Dispatch") {
                                        ?>
                                        <span class="label label-warning" style="font-size:12px;"><?php echo str_replace("_"," ",$report->conforder_status); ?></span>
                                    <?php } else if ($report->conforder_status == "Dispatch" || $report->conforder_status == "Closed") {
                                        ?>
                                        <span class="label label-success" style="font-size:12px;"><?php echo str_replace("_"," ",$report->conforder_status); ?></span>
                                        <?php
                                    } else if($report->conforder_status == "Cancelled" || $report->conforder_status == "Invalidate") {?>
                                        <span class="label label-danger" style="font-size:12px;"><?php echo str_replace("_"," ",$report->conforder_status); ?></span>
                                    <?php }else if($report->conforder_status == "Bkash_Payment_Receive") { ?>
                                         <span class="label label-info" style="font-size:12px;"><?php echo str_replace("_"," ",$report->conforder_status); ?></span>
                                    <?php }else{
                                      echo str_replace("_"," ",$report->conforder_status);
                                      }
                                    ?>
                                </td>
                               
                                @php($total_delivery += $report->Shipping_Charge)
                                @php($subtotal += $report->shoppingcart_subtotal)
                                @php($total_amount= $total_delivery+$subtotal)
                                @php($total_expense = $total_expense+$report->amount)
                                @php($i++)
                            </tr>
                            @endforeach
                            <tr>
                                <td colspan="5"></td>
                                <td></td>
                                <td style="font-weight: bold">Total Amount</td>
                                <td style="font-weight: bold">{{ $total_delivery }}</td>
                                <td style="font-weight: bold">{{ $total_expense }}</td>
                                <td style="font-weight: bold">{{ $subtotal }}</td>
                                <td style="font-weight: bold">{{ $total_amount }}</td>
                                <td style="font-weight: bold">{{ $discounted_total }}</td>  
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <a href="#" class="btn btn-sm btn-info btn-flat pull-left margin" onclick="printDiv()">Print Sales Report</a>
                <!--<a href="#" id="PDF" class="btn btn-sm btn-success btn-flat pull-center margin">Generate PDF Report</a>
                <!--<a href="{{url('/utadmin/manage-all-order')}}" class="btn btn-sm btn-default btn-flat pull-right">Go to all orders page</a> -->
            </div>
            <!-- /.box-footer -->
        </div>
    </div>
</div>
<!-- /.content -->
<!--- Ajax modal end ---->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/3.0.0-alpha.1/jspdf.plugin.autotable.min.js"></script>
<script>
$(document).on("click", "#PDF", function () {
    var table = document.getElementById("sales");
    var cols = [],
            data = [];
    function html() {
        var _token = "{{ csrf_token() }}";
        var doc = new jsPDF('p', 'pt');
        var res = doc.autoTableHtmlToJson(table, true);
        doc.autoTable(res.columns, res.data);
        var pdf = doc.output(); //returns raw body of resulting PDF returned as a string as per the plugin documentation.
        var data = new FormData();
        data.append("data_table", pdf);
        data.append("_token", _token);
        var xhr = new XMLHttpRequest();
        xhr.open('post', '/pdf-sales-report', true); //Post to php Script to save to server
        xhr.send(data);
        document.getElementById("showinfo").innerHTML = "pdf create successfully.";
    }
    html();
});
</script>
<script type="text/javascript">
    function printDiv() {
        var printContents = document.getElementById('print_div').innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>
<script>
    (function() {
        var select_value = '{{$order_status}}';
        if(select_value)
            document.getElementById('order_status').value = '{{$order_status}}';
    })();
    (function() {
        var select_value = '{{$order_threepldlv}}';
        if(select_value)
            document.getElementById('ddlthreepldlv').value = '{{$order_threepldlv}}';
    })();
</script>
@endsection