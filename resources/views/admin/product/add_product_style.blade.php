@extends('admin.layouts.app')
@section('title', 'Product Add Form')
@section('content')
<link rel="stylesheet" href="{{asset('assets_admin/input_style.css')}}">
<style>
    input:invalid {
        border-color: #3c8dbc;
    }
    .upload-field {
        position: relative;
        cursor: pointer;
        width: 300px;
        height: 30px;
        line-height: 28px;
        padding: 5px 10px;
        border: 1px solid #203F53;
        border-radius: 5px;
    }

    .icon-folder {
        background-image: url("../images/icon-folder.png");
        height: 30px;
        width: 30px;
        background-size: cover;
        background-color: transparent;
        border: none;
        float: right;
    }
    .upload-file-button {
        height: 40px;
        width: 120px;
        text-align: center;
        margin: 20px 0;
        border: none;
        border-radius: 5px;
        box-shadow: 0 1px 3px #203F53;
        background-color: #203F53;
        color: white;
        font-size: 15px;
    }
    input:valid {
        border-color: #3c8dbc;
        background-image: none;
    }
	
</style>
<section class="content-header">
    <h1>
        Product Style
        <small>Add</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Product Style</a></li>
        <li class="active">Add</li>
    </ol>
</section>
<form name="add_product" id="myform" action="{{url('/save-product-style')}}" method="post" enctype="multipart/form-data">
    {{ csrf_field()}}
    <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Add Product Style</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <center>
                @if (session('save'))
                <div class="alert alert-success">
                    {{ session('save')}}
                </div>
                @endif
            </center>
            <center>
                @if (session('error'))
                <div class="alert alert-success">
                    {{ session('error')}}
                </div>
                @endif
            </center>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <input type="hidden" name="total_grp" id="myform" class="total_grp" value="">
                        <input type="hidden" name="blank" id="blank" value="">
                        <div class="clone_grp">
                            <div class="product_style">
                                <div id="right_part">
                                    <div class="formcontainer">
                                        <div class="form-group formheader">
                                            <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"/>
                                        </div>
                                        <div>
                                            <span for="blank" style="display:none;">Available Product Size</span><span style="font-weight:700;">Available Product Size for Add </span></br>                    
                                            <?php
                                            $i = 0;
                                            foreach ($avail_size as $size) {
                                                $i++;
                                                ?>
                                               <span rel="input_size<?php echo $i ?>"  for="input_size<?php echo $i ?>" class="clone_field"><?php echo $size->prosize_name; ?></span><input type="checkbox" class="chksize clone_field" rel="size<?php echo $i ?>"  name="size<?php echo $i ?>" value="<?php echo $size->prosize_name; ?>" style="width:39px !important;"/> 
                                            <?php } ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Quantity</label>
                                            <?php
                                            $i = 0;
                                            foreach ($avail_size as $size) {
                                                $i++;
                                                ?>
                                                <div class="form-group">
												<label style="display:none"></label>
                                                   <input type="text" size="6" rel="input_size<?php echo $i ?>"  name="input_size<?php echo $i ?>" class="clone_field" id="required-input" style="display:none" placeholder="Qty for <?php echo $size->prosize_name; ?>" value="" />
												</div>
                                            <?php } session(['numberofavailabesize' => $i]); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Color / Style Name</label>
                                            <input type="text" rel="txtcolorname" name="txtcolorname" class="txtfield clone_field" required>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>
                        </div>
                        <div class="dynamic">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <table>

                            <tr>
                                <th>Color Thumbnail</th>
                                <td><input type="file" rel="file_colorthm" name="file_colorthm" class="btn btn-info clone_field" required></td>
                                <td>[ Image Type: jpeg or jpg, Width: 81 Px Height: 62 PX ]</td>
                            </tr>
                            <tr>
                                <th>Image 1</th>
                                <td>
                                    <input type="file" rel="file_im1" name="file_im1" class="btn btn-info clone_field" required>
                                </td>
                                <td>[ Image Type: jpeg or jpg, Width: 1600 px X Height: 2400 px ]</td>
                            </tr>
                            <tr>
                                <th>Image 2</th>
                                <td><input type="file" rel="file_im2" name="file_im2" class="btn btn-info clone_field"></td>
                                <td>[ Image Type: jpeg or jpg, Width: 1600 px X Height: 2400 px ]</td>
                            </tr>
                            <tr>
                                <th>Image 3</th>
                                <td><input type="file" rel="file_im3" name="file_im3" class="btn btn-info clone_field"></td>
                                <td>[ Image Type: jpeg or jpg, Width: 1600 px X Height: 2400 px ]</td>
                            </tr>
                            <tr>
                                <th>Image 4</th>
                                <td><input type="file" rel="file_im4" name="file_im4" class="btn btn-info clone_field"></td>
                                <td>[ Image Type: jpeg or jpg, Width: 1600 px X Height: 2400 px ]</td>
                            </tr>
                            <tr>
                                <th>Image 5</th>
                                <td><input type="file" rel="file_im5" name="file_im5" class="btn btn-info clone_field"></td>
                                <td>[ Image Type: jpeg or jpg, Width: 1600 px X Height: 2400 px ]</td>
                            </tr>
                            <tr>
                                <th>Image 6</th>
                                <td><input type="file" rel="file_im6" name="file_im6" class="btn btn-info clone_field"></td>
                                <td>[ Image Type: jpeg or jpg, Width: 1600 px X Height: 2400 px ]</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <input type="submit" name="btnsubmit" class="btn btn-primary" value="Add Color/Style" />
            </div>
        </div>
</form>
</section>
<script src="{{asset('assets_admin/add_product.js')}}"></script>
<link rel="stylesheet" href="{{asset('assets_admin/previewForm.css')}}">
<script src="{{asset('assets_admin/previewForm.js')}}"></script>
<script type="text/javascript" src="js/dynamic_select_box.js"></script>
<script>
$(document).ready(function () {
    $('#myform').previewForm();
});
</script>
@endsection