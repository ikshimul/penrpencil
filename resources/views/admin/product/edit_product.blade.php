@extends('admin.layouts.app')
@section('title', 'Product Edit Form')
@section('content')
<link rel="stylesheet" href="{{asset('assets_admin/input_style.css')}}">
<style>
	input[type='text'], input[type='password'], input[type='url'], input[type='email'], input[type='tel'], input[type='search'], input[type='number'], input[type='date'], input[type='month'], input[type='week'], input[type='datetime-local'], input[type='color'] {
		height:34px;
	}
	label {
		font-size:16px;
		font-weight:600;
	}
</style>
<section class="content-header">
    <h1>
        Product
        <small>Edit</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Product</a></li>
        <li class="active">Edit</li>
    </ol>
</section>

<form name="add_product" id="myform" action="{{url('/product-edit')}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="product_id"  value="{{ $product_info->product_id }}" required>
    <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
				
                    <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
					<fieldset style="margin:10px;  margin-top:44px; padding:5px 20px; border:2px solid #00c0ef44">
					<legend style="padding:5px 20px; text-align:center; width:auto">Edit Product</legend>
                        <center>
                            @if (session('update'))
                            <div class="alert alert-success">
                                {{ session('update') }}
                            </div>
                            @endif
                        </center>
                        <center>
                            @if (session('error'))
                            <div class="alert alert-success">
                                {{ session('error') }}
                            </div>
                            @endif
                        </center>
						<div class="form-group">
							<label for="ddlprocat">Main Category</label>
							<select class="form-control" type="text"  name="ddlprocat" id="ddlprocat"  required>
								<option value="">-- select category --</option>
								@foreach($procat_list as $procat)
								<option value="{{$procat->procat_id}}">{{$procat->procat_name}}</option>
								@endforeach
							</select>
							<span class="help-text"></span>
						</div>
						<div class="form-group">
							<label for="ddlprosubcat1">Sub Category</label>
							<select class="form-control" type="text" name="ddlprosubcat1" id="ddlprosubcat1" required>
								<option value="">-- select sub category --</option>
								@foreach($subprocat_list as $subprocat)
								<option value="{{$subprocat->subprocat_id}}">{{$subprocat->subprocat_name}}</option>
								@endforeach
							</select>
							<span class="help-text"></span>
						</div>
						<div class="form-group">
							<label for="txtproductname">Product Name</label>
							<input type="text" class="form-control" name="txtproductname" id="txtproductname" value="{{ $product_info->product_name }}" required>
							<span class="help-text"></span></td>
						</div>
						<div class="form-group">
							<label for="txtstyleref">Style Code/Ref</label>
							<input class="form-control" type="text" id="required-input" name="txtstyleref" value="{{ $product_info->product_styleref }}" required>
							<span class="help-text"></span>
						</div>
						<div class="form-group">
							<label for="txtstyleref">Product Barcode</label>
							<input class="form-control" type="text" id="required-input" name="product_barcode" id="txtstyleref" value="{{ $product_info->product_barcode }}">
							<span class="help-text"></span></td>
						</div>
						<div class="form-group">
							<label for="txtprice">Price</label>
							<input type="number" class="form-control"  name="txtprice" id="in-range-input" min="350" max="20000" value="{{ $product_info->product_price }}" required>
							<span class="help-text">(value must be number)</span></td>
						</div>
						<div class="form-group">
							<label for="txtpricediscounted">Discount Percentange</label>
							<input  class="form-control" type="number" name="txtpricediscounted" id="optional-input" min="0" max="200" value="{{ $product_info->discount_product_price }}">
							<span class="help-text">(value must be number)</span></td>
						</div>
                           <!--- <tr>
                                <th><label for="ddlfilter">Price Filter</label></th>
                                <td><select name="ddlfilter" id="ddlfilter">
                                        <option>-- Select Filter --</option>
                                        <option value="high">High</option>
                                        <option value="medium">Medium</option>
                                        <option value="low">Low</option>
                                    </select></td>
                                <td><span class="help-text"></span></td>
                            </tr> --->
						<div class="form-group">
							<label for="txtorder">Product Order</label>
							<input class="form-control" type="number" name="txtorder" id="optional-input" min="1" max="1000" value="{{$product_info->product_order}}">
							<span class="help-text">Only Numbers [ <strong>Total Product Added: <?php echo $total_product; ?></strong> ] </span>
						</div>
						<!--<div class="form-group">
							<label for="txtproductcare">Care Description</label>
							<textarea class="form-control" type="text" name="txtproductcare" id="optional-input">{{$product_info->product_care}}</textarea>
							<span class="help-text">Care Description</span>
						</div>-->
						<div class="form-group">
							<label for="check-option-1">Is Special</label>
							<!--<input class="form-control" type="checkbox" name="campaign" id="check-option-1" value="2"/> Eid --->
							<!-- <input class="form-control" type="checkbox" name="campaign" id="check-option-1" value="3" style="width: 16px;"/> Puja 2018 -->
                            <div style="padding-left:10px"><input type="radio" name="campaign" id="check-option-1" value="9" <?php
								if ($product_info->isSpecial == 9) {
									echo 'checked';
								} else {
									echo "";
								}
								?> style="width:16px;"/>Pride Girls Capsule Collection 2019
							</div>
							<div style="padding-left:10px"><input type="radio" name="campaign" id="check-option-1" value="8" <?php
								if ($product_info->isSpecial == 8) {
									echo 'checked';
								} else {
									echo "";
								}
								?> style="width:16px;"/>Summer 2019
							</div>
							<div style="padding-left:10px"><input type="radio" name="campaign" id="check-option-1" value="2" <?php
								if ($product_info->isSpecial == 2) {
									echo 'checked';
								} else {
									echo "";
								}
								?> style="width:16px;"/>Eid Collection 2019
							</div>
							<div style="padding-left:10px"><input type="radio" name="campaign" id="check-option-1" value="7" <?php
								if ($product_info->isSpecial == 7) {
									echo 'checked';
								} else {
									echo "";
								}
								?> style="width:16px;"/>Pohela Boishak 1426
							</div>
							<div style="padding-left:10px"><input type="radio" name="campaign" id="check-option-1" value="6" <?php
								if ($product_info->isSpecial == 6) {
									echo 'checked';
								} else {
									echo "";
								}
								?> style="width:16px;"/>Indepandance Day
							</div>
							<div style="padding-left:10px"><input type="radio" name="campaign" id="check-option-1" value="5" <?php
								if ($product_info->isSpecial == 5) {
									echo 'checked';
								} else {
									echo "";
								}
								?> style="width:16px;"/>Amar Ekushay Collection 2019
							</div>
							<div style="padding-left:10px"><input type="radio" name="campaign" id="check-option-1" value="4" <?php
								if ($product_info->isSpecial == 4) {
									echo 'checked';
								} else {
									echo "";
								}
								?> style="width:16px;"/>Falgun 2019
							</div>
							<span class="help-text"></span>
						</div>
						<div class="form-group">
							<label for="txtproductcare">Size Image</label>
								<!--<div class="form-group">
									<div class="mb10">
										<span class="file-input">
											<div class="file-preview">
												<div class="close fileinput-remove text-right"><a href="#" onclick="confirm_modal('#');">×</a></div>
												<div class="file-preview-thumbnails">
													<div class="file-preview-frame" id="preview">
														<img class="img-responsive" src="{{ URL::to('') }}/storage/app/public/prothm/{{$product_info->product_sizeguide_img}}" class="file-preview-image" title="" alt="" style="width:auto;height:160px;">
													</div>
												</div>
												<div class="clearfix"></div>   
												<div class="file-preview-status text-center text-success"></div>
												<div class="kv-fileinput-error file-error-message" style="display: none;"></div>
											</div>

										</span>
									</div>
								</div>-->
							<input class="form-control"  type="file"  name="sizeguide_image"/>
							<span class="help-text"></span>
						</div>
						<div class="form-group">
							<label for="txtproductdetails">Product Description</label>
							<textarea class="form-control" type="text" name="txtproductdetails" id="optional-input" rows="3" cols="50">{{$product_info->product_description}}</textarea>
						</div>
						<div class="form-group">
							<label for="fabric">Fabric Composition</label>
							<!--<textarea type="text" name="fabric" id="optional-input" rows="3">{{ old('fabric') }}</textarea> -->
							<select class="form-control" name="fabric" id="fabric">
							  <option value=""> -- Select Fabric -- </option>
							  <option value="WOVEN 100% COTTON"> WOVEN 100% COTTON</option>
							  <option value="VISCOSE WOVEN (90/120 GSM)">VISCOSE WOVEN (90/120 GSM)</option>
							  <option value="KNIT 100% VISCOSE">KNIT 100% VISCOSE</option>
							  <option value="KNIT S/J PC (65% POLYESTER 35% COTTON)">KNIT S/J PC (65% POLYESTER 35% COTTON)</option>
							  <option value="KNIT S/J (50% COTTON 50% MODAL)">KNIT S/J (50% COTTON 50% MODAL)</option>
							  <option value="KNIT LYCRA S/J (95% COTTON 5% ELASTANE)">KNIT LYCRA S/J (95% COTTON 5% ELASTANE)</option>
							  <option value="KNIT S/J POLY VISCOSE ELASTANE (62% POLY 33% VIS 5% ELASTANE)">KNIT S/J POLY VISCOSE ELASTANE (62% POLY 33% VIS 5% ELASTANE)</option>
							</select>
						</div>
						<div class="form-group">
							<label for="txtproductcare">Care Description</label>
							<div style="padding-left:10px">
							<!--<textarea type="text" name="txtproductcare" id="optional-input" rows="3">{{ old('txtproductcare') }}</textarea> -->
							<input type="radio" name="txtproductcare" style="width: 16px;" id="care_1" value="Hand Wash Cold. Do Not Bleach. Do Not Tumble Dry.150°c (Medium Heat).Do not dry clean"/> Hand Wash Cold. Do Not Bleach. Do Not Tumble Dry.150°c (Medium Heat).Do not dry clean<br>
							<input type="radio" name="txtproductcare" style="width: 16px;" id="care_2" value="Hand Wash Cold. Do Not Bleach. Flat dry. 110˚c (Medium Heat). Do not dry clean"/> Hand Wash Cold. Do Not Bleach. Flat dry. 110˚c (Medium Heat). Do not dry clean<br>
							<input type="radio" name="txtproductcare" style="width: 16px;" id="care_3" value="Hand Wash Cold. Do Not Bleach. Flat dry. 110˚c (Medium Heat). Do not dry clean" /> Hand Wash Cold. Do Not Bleach. Flat dry. 110˚c (Medium Heat). Do not dry clean<br>
							<input type="radio" name="txtproductcare" style="width: 16px;" id="care_4" value="Machine Wash Cold. Do Not Bleach. TUMBLE DRY. 150˚c (Medium Heat). Do not dry clean" /> Machine Wash Cold. Do Not Bleach. TUMBLE DRY. 150˚c (Medium Heat). Do not dry clean<br>
							<input type="radio" name="txtproductcare" style="width: 16px;" id="care_5" value="Machine Wash Cold. Do Not Bleach. Line Dry. 150˚c (Medium Heat). Do not dry clean" /> Machine Wash Cold. Do Not Bleach. Line Dry. 150˚c (Medium Heat). Do not dry clean<br>
							<input type="radio" name="txtproductcare" style="width: 16px;" id="care_6" value="Machine Wash Cold. Do Not Bleach. TUMBLE DRY. Y 150˚c (Medium Heat). Do not dry clean" /> Machine Wash Cold. Do Not Bleach. TUMBLE DRY. Y 150˚c (Medium Heat). Do not dry clean<br>
							<input type="radio" name="txtproductcare" style="width: 16px;" id="care_7" value="Machine Wash Cold Permanent Press. Do Not Bleach. TUMBLE DRY LOW HEAT. 150˚c (Medium Heat).Dry Clean Any Solvent Except Trichloroethylene." /> Machine Wash Cold Permanent Press. Do Not Bleach. TUMBLE DRY LOW HEAT. 150˚c (Medium Heat).Dry Clean Any Solvent Except Trichloroethylene.
							</div>
						</div>
						<div class="box-footer">
							<input type="submit" name="btnsubmit" class="btn btn-primary pull-right" value="Update Product" />
						</div>
						</fieldset>
					</div>
					
				</div>
			</div>
			<!-- /.row -->
		</div>
    </section>
</form>
<script>
    document.getElementById("ddlprocat").value = "<?php echo $product_info->procat_id; ?>";
    document.getElementById("ddlprosubcat1").value = "<?php echo $product_info->subprocat_id; ?>";
    document.getElementById("ddlfilter").value = "<?php echo $product_info->product_pricefilter; ?>";
    document.getElementById("fabric").value = "<?php echo $product_info->fabric; ?>";
</script>
<script src="{{asset('assets_admin/add_product.js')}}"></script>
<!--<link rel="stylesheet" href="{{asset('assets_admin/previewForm.css')}}">
<script src="{{asset('assets_admin/previewForm.js')}}"></script>-->
<script type="text/javascript" src="{{asset('assets_admin/dynamic_select_box.js')}}"></script>
<script>
    $(document).ready(function () {
        var fabric=$("#fabric").val();
		if(fabric == 'WOVEN 100% COTTON'){
			$("#care_1").attr('checked', 'checked');
			$("#care_2").attr('checked', false);
			$("#care_3").attr('checked', false);
			$("#care_4").attr('checked', false);
			$("#care_5").attr('checked', false);
			$("#care_6").attr('checked', false);
			$("#care_7").attr('checked', false);
		}else if(fabric == 'VISCOSE WOVEN (90/120 GSM)'){
            $("#care_1").attr('checked', false);
			$("#care_2").attr('checked', 'checked');
			$("#care_3").attr('checked', false);
			$("#care_4").attr('checked', false);
			$("#care_5").attr('checked', false);
			$("#care_6").attr('checked', false);
			$("#care_7").attr('checked', false);
		}else if(fabric == 'KNIT 100% VISCOSE'){
	     	$("#care_1").attr('checked', false);
			$("#care_2").attr('checked', false);
			$("#care_3").attr('checked', 'checked');
			$("#care_4").attr('checked', false);
			$("#care_5").attr('checked', false);
			$("#care_6").attr('checked', false);
			$("#care_7").attr('checked', false);
		}else if(fabric == 'KNIT S/J PC (65% POLYESTER 35% COTTON)'){
			$("#care_1").attr('checked', false);
			$("#care_2").attr('checked', false);
			$("#care_3").attr('checked', false);
			$("#care_4").attr('checked', 'checked');
			$("#care_5").attr('checked', false);
			$("#care_6").attr('checked', false);
			$("#care_7").attr('checked', false);
		}else if(fabric == 'KNIT S/J (50% COTTON 50% MODAL)'){
			$("#care_1").attr('checked', false);
			$("#care_2").attr('checked', false);
			$("#care_3").attr('checked', false);
			$("#care_4").attr('checked', false);
			$("#care_5").attr('checked', 'checked');
			$("#care_6").attr('checked', false);
			$("#care_7").attr('checked', false);
		}else if(fabric == 'KNIT LYCRA S/J (95% COTTON 5% ELASTANE)'){
			$("#care_1").attr('checked', false);
			$("#care_2").attr('checked', false);
			$("#care_3").attr('checked', false);
			$("#care_4").attr('checked', false);
			$("#care_5").attr('checked', false);
			$("#care_6").attr('checked', 'checked');
			$("#care_7").attr('checked', false);
		}else if(fabric == 'KNIT S/J POLY VISCOSE ELASTANE (62% POLY 33% VIS 5% ELASTANE)'){
			$("#care_1").attr('checked', false);
			$("#care_2").attr('checked', false);
			$("#care_3").attr('checked', false);
			$("#care_4").attr('checked', false);
			$("#care_5").attr('checked', false);
			$("#care_6").attr('checked', false);
			$("#care_7").attr('checked', 'checked');
		}
		$("#fabric").change(function(){
		var fabric=$("#fabric").val();
		if(fabric == 'WOVEN 100% COTTON'){
			$("#care_1").attr('checked', 'checked');
			$("#care_2").attr('checked', false);
			$("#care_3").attr('checked', false);
			$("#care_4").attr('checked', false);
			$("#care_5").attr('checked', false);
			$("#care_6").attr('checked', false);
			$("#care_7").attr('checked', false);
		}else if(fabric == 'VISCOSE WOVEN (90/120 GSM)'){
            $("#care_1").attr('checked', false);
			$("#care_2").attr('checked', 'checked');
			$("#care_3").attr('checked', false);
			$("#care_4").attr('checked', false);
			$("#care_5").attr('checked', false);
			$("#care_6").attr('checked', false);
			$("#care_7").attr('checked', false);
		}else if(fabric == 'KNIT 100% VISCOSE'){
	     	$("#care_1").attr('checked', false);
			$("#care_2").attr('checked', false);
			$("#care_3").attr('checked', 'checked');
			$("#care_4").attr('checked', false);
			$("#care_5").attr('checked', false);
			$("#care_6").attr('checked', false);
			$("#care_7").attr('checked', false);
		}else if(fabric == 'KNIT S/J PC (65% POLYESTER 35% COTTON)'){
			$("#care_1").attr('checked', false);
			$("#care_2").attr('checked', false);
			$("#care_3").attr('checked', false);
			$("#care_4").attr('checked', 'checked');
			$("#care_5").attr('checked', false);
			$("#care_6").attr('checked', false);
			$("#care_7").attr('checked', false);
		}else if(fabric == 'KNIT S/J (50% COTTON 50% MODAL)'){
			$("#care_1").attr('checked', false);
			$("#care_2").attr('checked', false);
			$("#care_3").attr('checked', false);
			$("#care_4").attr('checked', false);
			$("#care_5").attr('checked', 'checked');
			$("#care_6").attr('checked', false);
			$("#care_7").attr('checked', false);
		}else if(fabric == 'KNIT LYCRA S/J (95% COTTON 5% ELASTANE)'){
			$("#care_1").attr('checked', false);
			$("#care_2").attr('checked', false);
			$("#care_3").attr('checked', false);
			$("#care_4").attr('checked', false);
			$("#care_5").attr('checked', false);
			$("#care_6").attr('checked', 'checked');
			$("#care_7").attr('checked', false);
		}else if(fabric == 'KNIT S/J POLY VISCOSE ELASTANE (62% POLY 33% VIS 5% ELASTANE)'){
			$("#care_1").attr('checked', false);
			$("#care_2").attr('checked', false);
			$("#care_3").attr('checked', false);
			$("#care_4").attr('checked', false);
			$("#care_5").attr('checked', false);
			$("#care_6").attr('checked', false);
			$("#care_7").attr('checked', 'checked');
		}
	});
        $('#myform').previewForm();
    });
</script>
@endsection