@extends('admin.layouts.app')
@section('title', 'Product Add Form')
@section('content')
<section class="content-header">
    <h1>
        Manage Album
        <small>Product </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Product</li>
        <li class="active">Manage Album</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Manage Product Colors for '<?php echo $title->product_name; ?>'</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <a href="{{url("/add-album/{$product_id}/{$subpro_id}")}}" class="tdata">+ Add New Color</a> &nbsp; | &nbsp; 
                    <a href="manage_product.php?pcode=<?php echo base64_encode($title->product_styleref); ?>" class="tdata">&laquo; Back To Product Page</a></p>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="5%">SL</th>
                                <th width="20%">Color Name</th>
                                <th width="35%">Color Image</th>
                                <th>Placement Order</th>                            
                                <th width="25%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            foreach ($album as $product_album) {
                                ?>                       
                                <!-- data -->
                                <tr>
                                    <td width="5%"><?php echo $i; ?></td>
                                    <td><?php echo $product_album->productalbum_name; ?></td>                            
                                    <td><img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $product_album->productalbum_img; ?>" alt="Banner" /></td>							
                                    <td><?php echo $product_album->productalbum_order; ?></td>
                                    <td width="20%">
                                        <a class="btn btn-sm btn-flat btn-primary margin tdata" href="{{url("edit-swatch/{$product_album->productalbum_id}/{$product_id}/{$subpro_id}")}}">Edit</a>
                                        <a class="btn btn-sm btn-flat btn-danger margintdata" href="#" onClick="confirm_delete('{{url("/delete-product-album/{$product_album->productalbum_id}")}}')">Delete</a>
                                        <hr /><a class="btn btn-sm btn-flat btn-default margin tdata" href="{{url("manage-product-gallery/{$product_album->productalbum_id}/{$product_id}")}}">Manage Color Images</a>
                                    </td>
                                </tr>
                                <!-- data -->
                                <?php
                                $i++;
                            } // end of while	
                            ?>
                            <?php if ($i < 1) { ?>
                                <tr>
                                    <td colspan="5">No Color Style found For &lsquo;<?php echo $title->product_name; ?>&rsquo;</td>                            
                                </tr>	
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- Ajax modal ---->
<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="text-align:center;color:black;">Are you sure to delete this album ?</h4>
            </div>
            <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                <a href="#" class="btn btn-sm btn-danger" id="delete_link">Delete</a>
                <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!--- Ajax modal end ---->
@endsection