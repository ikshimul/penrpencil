<?php

use App\Http\Controllers\admin\product\QuantityController;
?>
@extends('admin.layouts.app')
@section('title', 'Product Add Form')
@section('content')
<section class="content-header">
    <h1>
        Product 
        <small>Manage Product</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Product</a></li>
        <li class="active">Manage Product</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- /.box -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Browse All Product </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <center>
                        @if (session('update'))
                        <div class="alert alert-success">
                            {{ session('update') }}
                        </div>
                        @endif
                    </center>
                    <center>
                        @if (session('delete'))
                        <div class="alert alert-danger">
                            {{ session('delete') }}
                        </div>
                        @endif
                    </center>
                    <form name="singleform" action="{{url('/product-manage')}}" method="GET">
                        <div class="form-group">
                            <label>Select Category</label>
                            <div class="input-group">
                                <select name="scid" id="scid"  class="form-control select2">
                                    <option value=""> --- select Category --- </option>
                                    <?php
                                    foreach ($sub_category as $sub) {
                                        ?>
                                        <option  value="<?php echo $sub->subprocat_id; ?>"><?php echo "(" . $sub->procat_name . ") " . $sub->subprocat_name; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <div class="input-group-addon">
                                    <input style="background-color: white; border: none;" type="submit" name="btnsubmit" value="Search"  /> 
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                    </form>   
                    <div class="table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Name</th>
                                    <!--<th>Style Code</th> --->
                                    <th>Category - Sub Category</th>
                                    <th style="text-align:center;">Image</th>
                                    <th style="text-align:center;">Manage</th>
                                    <th style="text-align:center;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($product_list as $product) {
                                    $i++;
                                    ?>
                                    <tr>
                                        <td style="width:1%;text-align:center;"><?php echo $i; ?></td>
                                        <td style="width:15%;"><?php echo $product->product_name; ?></td>
                                        <td style="width:15%;"><?php echo $product->procat_name . " - " . $product->subprocat_name; ?></td>
                                        <?php if ($product->product_active_deactive == 0) { ?>
                                            <td style="width:10%;text-align:center;">
                                                <img class="thumbnail  img-responsive" src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->productimg_img_tiny; ?>" alt=""  />
                                            </td>
                                            <td>
                                                <a class="btn bg-navy btn-flat btn-sm margin tdata" href="{{url("product-album-manage/{$product->product_id}/{$product->subprocat_id}")}}">Manage Product Gallery</a>                                             
                                                <a class="btn btn-danger btn-flat btn-sm margin tdata" href="{{url("product-deactive-active/{$product->procat_id}/{$product->subprocat_id}/{$product->product_id}/{$product->product_active_deactive}")}}">Deactive</a>
                                            </td>
                                            <td>
                                                <a class="btn bg-olive btn-flat btn-sm margin tdata" href="{{url("/edit-product/{$product->product_id}")}}">Edit</a> 
                                                <a class="btn btn-danger btn-flat btn-sm margin tdata" href="#" onClick="confirm_delete('{{url("/product-in-trash/{$product->product_id}/{$product->subprocat_id}")}}')">Delete</a>
                                                <a class="btn bg-orange btn-flat btn-sm margin tdata" href="{{url("/add-product-style/{$product->product_id}")}}">Add More</a>
                                                <a class="btn bg-navy btn-flat btn-sm margin tdata" href="{{url("/add-related-product/{$product->product_id}")}}">Add Related Product</a>
                                                <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#product_<?php echo $product->product_id; ?>" data-id="<?php echo $product->product_id; ?>">Update Qty</button></br>
                                                <!-- Modal -->
                                                <div id="product_<?php echo $product->product_id; ?>" class="modal fade" role="dialog">
                                                    <div class="modal-dialog modal-lg">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title">Update Quantity</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <section class="content">
                                                                    <!-- Small boxes (Stat box) {{url('/update-quantity-save')}} -->
                                                                    <!-- /.box-header -->
                                                                    <div class="box-body">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <center>
                                                                                    @if (session('update'))
                                                                                    <div class="alert alert-success">
                                                                                        {{ session('update') }}
                                                                                    </div>
                                                                                    @endif
                                                                                </center>
                                                                                <input type="hidden" name="productid" value="<?php echo $product->product_id; ?>" />
                                                                                <input type="hidden" name="productstyle" value="<?php echo $product->product_styleref; ?>" />
                                                                                <!--  <div class="alert alert-info alert-dismissible">
                                                                                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                                                      <h4><i class="icon fa fa-info"></i></h4>
                                                                                      <p>Product Name</p>
                                                                                  </div> ---->
                                                                                <center><img class="thumbnail  img-responsive" src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->product_img_big; ?>" alt=""  height="200" width="160" /></center>
                                                                                <table class="table table-striped">
                                                                                    <tr>
                                                                                        <th>Product Details</th>
                                                                                        <th>&nbsp;</th>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Product Name : </td>
                                                                                        <td><?php echo $product->product_name; ?></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Product Code : </td>
                                                                                        <td><?php echo $product->product_styleref; ?></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Product Category : </td>
                                                                                        <td> <?php echo $product->procat_name; ?></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Product Sub Category : </td>
                                                                                        <td><?php echo $product->subprocat_name; ?></td>
                                                                                    </tr>
                                                                                </table>
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <fieldset style="margin:10px;  margin-top:44px; padding:5px 20px; border:2px solid #00c0ef44">
                                                                                            <legend style="padding:5px 20px; text-align:center; width:auto">Update Quantity</legend>
                                                                                            <table class="table table-condensed">
                                                                                                <tr>
                                                                                                    <th style="width: 10px">#</th>
                                                                                                    <th>[Color] / [Size]</th>
                                                                                                    <th>Barcode</th>
                                                                                                    <th style="width: 40px">Quantity</th>

                                                                                                    <th></th>
                                                                                                </tr>
                                                                                                <?php
                                                                                                $i = 0;
                                                                                                $size_qty = QuantityController::GetSizeQty($product->product_id);
                                                                                                foreach ($size_qty as $prosize) {
                                                                                                    $i++;
                                                                                                    ?>
                                                                                                    <tr>
                                                                                                        <td><?php echo $i; ?>.</td>
                                                                                                        <td><?php echo '[' . $prosize->color_name . ']' . ' / [' . $prosize->productsize_size . ']'; ?></td>
                                                                                                        <td>
                                                                                                            <input class="form-control" type="text" value="{{ $prosize->barcode }}" name="barcode[]" id="barcode_<?php echo $product->product_id; ?>_<?php echo $i; ?>" placeholder="Barcode" required/>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <input class="form-control" type="number" name="qty[]" id="qty_<?php echo $product->product_id; ?>_<?php echo $i; ?>" value="<?php echo $prosize->SizeWiseQty; ?>">
                                                                                                            <input class="form-control" type="hidden" name="prosize_id[]" id="prosize_<?php echo $product->product_id; ?>_<?php echo $i; ?>" value="<?php echo $prosize->productsize_id; ?>">
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <a class="btn btn-danger btn-flat btn-sm tdata" href="#" onClick="confirm_delete('{{url("/delete-product-size/{$prosize->productsize_id}")}}')">Delete</a>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                <?php } ?>
                                                                                            </table>
                                                                                            <input type="hidden" id="total_loop" value="<?php echo $i; ?>">
                                                                                        </fieldset>
                                                                                        <div class="box-footer">
                                                                                            <div class="col-sm-12 text-right">
                                                                                                <a href="#" class="btn bg-navy btn-flat margin" id="update_qty_<?php echo $product->product_id; ?>">Update Quantity </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <fieldset style="margin:10px;  margin-top:44px; padding:5px 20px; border:2px solid #00c0ef44">
                                                                                            <legend style="padding:5px 20px; text-align:center; width:auto">Add New Size and Quantity</legend>
                                                                                            <table class="table table-condensed">
                                                                                                <tr>
                                                                                                    <th>Color</th>
                                                                                                    <th>Size</th>
                                                                                                    <th style="width: 25px">Quantity</th>
                                                                                                    <th>Barcode</th>
                                                                                                    <th>&nbsp;</th>
                                                                                                </tr>
                                                                                                <?php
                                                                                                $j = 0;
                                                                                                $color_wise_prosize = QuantityController::GetColorProSize($product->product_id);
                                                                                                foreach ($color_wise_prosize as $color) {
                                                                                                    $j++;
                                                                                                    $prosize = QuantityController::GetExpectSize($color->product_id, $color->color_name);
                                                                                                    ?>
                                                                                                    <form action="{{url('/save-new-size-qty')}}" method="post">
                                                                                                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                                                                                        <input type="hidden" name="product_id" value="<?php echo $product->product_id; ?>" />
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <input type="hidden" name="color_name" value=" <?php echo $color->color_name; ?>" />
                                                                                                                <?php echo $color->color_name; ?>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <select name="productsize_size" class="form-control" required>
                                                                                                                    <option value="">Select size</option>
                                                                                                                    <?php foreach ($prosize as $sizes) { ?>
                                                                                                                        <option value="<?php echo $sizes->prosize_name; ?>"><?php echo $sizes->prosize_name; ?></option>
                                                                                                                    <?php } ?>
                                                                                                                </select>
                                                                                                            </td>

                                                                                                            <td>
                                                                                                                <input class="form-control" type="number" name="SizeWiseQty" placeholder="0" id="prosize_<?php echo $j; ?>" required/>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <input class="form-control" type="text" name="barcode" placeholder="Barcode" required/>
                                                                                                            </td>
                                                                                                            <td><input type="submit" id="add_size_<?php echo $j; ?>" value="Add"/></td>
                                                                                                        </tr>
                                                                                                    </form>
                                                                                                <?php } ?>
                                                                                            </table>
                                                                                        </fieldset>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!-- /.row -->
                                                                    </div>
                                                                    <!-- /.box-body name="btnsubmit" -->

                                                                </section>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </td>
                                        <?php } else { ?>
                                            <td><img class="thumbnail  img-responsive" src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->productimg_img_tiny; ?>" /></td>
                                            <td>
                                                <a class="btn bg-navy btn-flat btn-sm margin tdata" href="{{url("product-album-manage/{$product->product_id}/{$product->subprocat_id}")}}">Manage Product Gallery</a> 
                                                <a class="btn bg-olive btn-flat btn-sm margintdata" href="{{url("product-deactive-active/{$product->procat_id}/{$product->subprocat_id}/{$product->product_id}/{$product->product_active_deactive}")}}">Active</a>
                                            </td>
                                            <td>
                                                <a  class="btn bg-olive btn-flat btn-sm margin tdata" href="{{url("/edit-product/{$product->product_id}")}}">Edit Product</a> 
                                                <a class="btn btn-danger btn-flat btn-sm margin tdata" href="#" onClick="confirm_delete({{url("/product-in-trash/{$product->product_id}/{$product->subprocat_id}")}})">Delete</a>
                                            </td>
                                        <?php }
                                        ?>
                                    </tr>
                                <?php } ?>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>SL</th>
                                    <th>Name</th>
                                    <th>Category - Sub Category</th>
                                    <th style="text-align:center;">Image</th>
                                    <th style="text-align:center;">Manage</th>
                                    <th style="text-align:center;">Action</th>
                                </tr>
                            </tfoot> 
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- Ajax modal ---->
<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="text-align:center;color:black;">Are you sure to delete this product ?</h4>
            </div>
            <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                <a href="#" class="btn btn-sm btn-danger" id="delete_link">Delete</a>
                <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!--- Ajax modal end ---->
<script>
    document.getElementById("scid").value = "<?php echo $sub_pro_selected; ?>";</script>
<script>
    $(document).ready(function () {
    $('a[data-toggle=modal], button[data-toggle=modal]').click(function () {
    var data_id = '';
    if (typeof $(this).data('id') !== 'undefined') {

    data_id = $(this).data('id');
    }
    //   alert(data_id);

    var total_loop = $("#total_loop").val();
    // alert(total_loop);
    for (var i = 1; i <= total_loop; i++) {
    $('#qty' + '_' + data_id + '_' + i + ', #barcode' + '_' + data_id + '_' + i).on('blur', function () {
    // alert('ok');
    var rownowithid = $(this).attr('id');
    var rowno = rownowithid.split('_');
    //  alert(rowno[2]);
    var qty = $('#qty' + '_' + data_id + '_' + rowno[2]).val();
    var prosize_id = $('#prosize' + '_' + data_id + '_' + rowno[2]).val();
    var barcode = $('#barcode' + '_' + data_id + '_' + rowno[2]).val();
    //alert(barcode);
    var url_op = base_url + "/update-quantity-save-by-ajax/" + prosize_id + '/' + qty + '/' + barcode;
    $.ajax({
    url: url_op,
            type: 'GET',
            success: function (html) {
            //   alert(html);
            }
    });
    });
    }
    $('#update_qty' + '_' + data_id).on('click', function(){
    alert("Quantity Add Successfully!");
    });
    });
    });
</script>
@endsection