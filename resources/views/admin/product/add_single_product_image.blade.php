@extends('admin.layouts.app')
@section('title', 'Product Gallery')
@section('content')
<style>
    .with-errors{
        color:red;
    }
</style>
<section class="content-header">
    <h1>
        Product Image
        <small>Add </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Product</li>
         <li>Image</li>
          <li class="active">Add</li>
    </ol>
</section>
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Add Product Image Album for '<?php echo $album_name; ?>':</h3>
            <p style="width: 99%; text-align:right;">
                <a class="tdata" href="{{url("/manage-product-gallery/{$albumid}/{$pid}")}}">&laquo; Go Back to Album</a>
                &nbsp;|  &nbsp; <a class="tdata" href="{{url("/product-album-manage/{$pid}/{$subcatid}")}}">&laquo; Back to Album List</a>
            </p>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-offset-1 col-md-6">
                    <center>
                        @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                        @endif
                    </center>
                    <center>
                        @if (session('image_error'))
                        <div class="alert alert-danger">
                            {{ session('image_error') }}
                        </div>
                        @endif
                    </center>
                    <form name="formname" action="{{url('/add-new-album-image')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="hidden" name="product_id" class="form-control" value="<?php echo $pid; ?>" />
                            <input type="hidden" name="productalbum_id" value="<?php echo $albumid; ?>"/>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>Image Order</label>
                            <input type="text" name="productimg_order" class="form-control" class="txtfield" />
                            <div class="help-block with-errors">{{ $errors->first('productimg_order') }}</div>
                        </div>
                        <div class="form-group {{ $errors->has('filename') ? ' has-error' : '' }}">
                            <label>Chose Image</label>
                            <input id="input-upload-img1" type="file" name="filename" class="file" data-preview-file-type="text"/>
                            <span class="help-block" style="color:#f39c12;">only .jpg image is allowed Size (Width: 1300px X Height: 1667px MAX: 250 KB)</span>  
                            <div class="help-block with-errors">{{ $errors->first('filename') }}</div>
                        </div>
                        <!-- /.form-group -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="col-md-offset-1">
                <input type="submit" name="btnsubmit" class="btn bg-navy btn-flat margin" value="Add Image"/>
            </div>
        </div>
        </form>    
    </div>
    <!-- /.row -->
    <!-- Main row -->
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    List of current images of the album
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="5%">SL</th>
                                <th width="25%">Image Tag</th>
                                <th width="35%">Image</th>                            
                                <th>Image Order</th>                            
                                <th width="20%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            foreach ($product_img as $img) {
                                ?>                       
                                <!-- data -->
                                <tr>
                                    <td width="5%"><?php echo $i; ?></td>
                                    <td><?php echo $img->productimg_title; ?></td>                            
                                    <td><img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $img->productimg_img_tiny; ?>" alt="Banner" /></td>							
                                    <td><?php echo $img->productimg_order; ?></td>
                                    <td width="20%">
                                        <a class="btn bg-olive btn-flat btn-sm margin tdata" href="{{url("/edit-album-image/{$img->productimg_id}/{$pid}/{$albumid}/{$i}")}}">Edit</a> 
                                    </td>
                                </tr>
                                <!-- data -->
                                <?php
                                $i++;
                            } // end of while	
                            ?> 

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
@endsection