@extends('admin.layouts.app')
@section('title', 'Send Message')
@section('content')
<section class="content-header">
    <h1>
        Manage Customer
        <small>Send Message </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Manage Customer</li>
        <li class="active">Send Message</li>
    </ol>
</section>
<section class="content">
    <div class="box box-info">
        <div class="box-header">
          <i class="fa fa-envelope"></i>
          <h3 class="box-title">Quick Email</h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
               <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
               <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
          <!-- /. tools -->
        </div>
        <div class="box-body">
            @if (session('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i>Success!</h4>
                {{ session('success') }}
            </div>
            @endif
          <form action="{{url('/customer-send-message')}}" method="post"  enctype="multipart/form-data">
              {{ csrf_field() }}
            <div class="form-group">
              <input type="email" class="form-control" name="emailto" id="q" placeholder="Email to:">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="subject" placeholder="Subject">
            </div>
             <div class="form-group">
                <label>File Attach</label>
                <input id="input-upload-img1" type="file" class="file" name="file[]" data-preview-file-type="text">
                <span class="help-block" style="color:#f39c12;"></span>  
            </div>
            <div>
                <div class="form-group">
                  <textarea class="textarea" placeholder="Message" name="message"
                            style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </div>
            </div>
          
        </div>
        <div class="box-footer clearfix">
          <button type="submit" class="pull-right btn btn-default" id="sendEmail">Send
            <i class="fa fa-arrow-circle-right"></i></button>
        </div>
        </form>
    </div>
</section>
<script>
    $(document).ready(function(){
        $("#q").autocomplete({
    	  source: "search-customer",
    	  minLength: 1,
    	  select: function(event, ui) {
    	  	$('#q').val(ui.item.value);
    	  	$('#idno').val(ui.item.idno);
    	  }
    	});
    });
</script>
@endsection