@extends('admin.layouts.app')
@section('title', 'Pride Limited | User List')
@section('content')

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" media="all"/>
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet" media="all"/>
<section class="content-header">
    <h1>
        Analytics Reports 
        <small>Most View</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Analytics Reports</a></li>
        <li class="active">Most View</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- /.box -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Browse Last 100 Most View Products </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <center>
                        @if (session('update'))
                        <div class="alert alert-success">
                            {{ session('update') }}
                        </div>
                        @endif
                    </center>
                    <center>
                        @if (session('delete'))
                        <div class="alert alert-danger">
                            {{ session('delete') }}
                        </div>
                        @endif
                    </center>
                    <table id="example" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="2%">SL</th>
                                <th>Name</th>
                                <th>Email</th>                        	
                                <th>Telephone</th>                                                        
                                <th>Address</th>                                                        
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            ?>
                            @foreach($site_user as $user)
                            <tr>                        	
                                <td width="2%">{{$i++}}</td>
                                <td>{{$user->registeruser_firstname}} {{$user->registeruser_lastname}}</td>
                                <td>
                                    {{$user->registeruser_email}}
                                </td>
                                <td>
                                    {{$user->registeruser_phone}}
                                </td>																		
                                <td>
                                    <?php
                                    echo "
                					  " . $user->registeruser_address . " , " . $user->registeruser_city . " - " . $user->registeruser_zipcode . "";
                                    ?>  
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
                            <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
                                <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script>
    $(document).ready(function() {
    $('#example').DataTable( {
        "ordering": false,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if (session('success'))
            <div class="pad margin no-print">
                <div class="callout callout-danger" style="margin-bottom: 0!important;">
                    <h4>{{ session('success') }}</h4> </div>
            </div>
            @endif
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">All Site User(s)</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="2%">SL</th>
                                <th>User Id</th>
                                <th>Name</th>
                                <th>Email</th>                        	
                                <th>Telephone</th>                                                        
                                <th>Details</th>                                                        
                                <th>Action</th>                            
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            ?>
                            @foreach($site_user as $user)
                            <tr>                        	
                                <td width="2%">{{$i++}}</td>
                                <td width="5%">{{$user->registeruser_id}}</td>
                                <td>{{$user->registeruser_firstname}} {{$user->registeruser_lastname}}</td>
                                <td>
                                    {{$user->registeruser_email}}
                                </td>
                                <td>
                                    {{$user->registeruser_phone}}
                                </td>																		
                                <td>
                                    <?php
                                    echo "
					Address 1 : " . $user->registeruser_address . "
                                        <br />
					Address 2 : " . $user->registeruser_address1 . "
					<br />
				        City: " . $user->registeruser_city . "
					<br />
					Zip Code: " . $user->registeruser_zipcode . "";
                                    ?>  
                                </td>
                                <td width="10%">
                                    <a href="{{url("/edit-site-user/{$user->registeruser_id}")}}" class="btn btn-sm btn-info btn-flat">Edit</a> &nbsp;  &nbsp; 
                                    <a class="btn btn-danger btn-flat btn-sm tdata" href="#" onclick="confirm_delete('{{url('/site-user/delete-user')}}/<?php echo $user->id; ?>')">Delete</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                   </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-delete">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top:100px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="text-align:center;color:black;">Are you sure to delete this?</h4>
                </div>
                <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                    <a href="#" class="btn btn-sm btn-danger" id="delete_link">Delete</a>
                    <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    @endsection