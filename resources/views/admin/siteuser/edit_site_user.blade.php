@extends('admin.layouts.app')
@section('title', 'Urban Truth | Edit Site User')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if (session('success'))
             <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                {{ session('success') }}
             </div>
           @endif
            
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Site User Info</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
        <form name="add_subpro" action="{{url('/update-site-user-info')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" class="form-control" name="registeruser_id" value="{{$site_user->registeruser_id}}"/>
            <input type="hidden" class="form-control" name="registeruser_login_id" value="{{$site_user->id}}"/>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-offset-1 col-md-6">
                        <center>
                            @if (session('save'))
                            <div class="alert alert-success">
                                {{ session('save') }}
                            </div>
                            @endif
                        </center>
                        <center>
                            @if (session('error'))
                            <div class="alert alert-success">
                                {{ session('error') }}
                            </div>
                            @endif
                        </center>
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control" name="registeruser_firstname" value="{{$site_user->registeruser_firstname}}"/>
                        </div>
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" class="form-control" name="registeruser_lastname" value="{{$site_user->registeruser_lastname}}"/>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="registeruser_email" name="registeruser_email" value=" {{$site_user->registeruser_email}}"/>
                        </div>
                        <div class="form-group">
                            <label>Phone</label>
                            <input type="number" class="form-control" name="registeruser_phone" value="{{$site_user->registeruser_phone}}" required/>
                        </div>
                        <div class="form-group">
                            <label>City</label>
                            <input type="text" class="form-control" name="registeruser_city" value="{{$site_user->registeruser_city}}" />
                        </div>
                        <div class="form-group">
                            <label>Zip Code</label>
                            <input type="number" class="form-control" name="registeruser_zipcode" value="{{$site_user->registeruser_zipcode}}" step="5"/>
                        </div>
                        <div class="form-group">
                            <label>Address 1</label>
                            <textarea type="text" name="registeruser_address" class="form-control" name="txtorder" rows="3">{{$site_user->registeruser_address}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Address 2</label>
                            <textarea type="text" name="registeruser_address1" class="form-control" name="txtorder" rows="3">{{$site_user->registeruser_address1}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="col-md-offset-1">
                    <input type="submit" name="btnsubmit" class="btn bg-navy btn-flat margin" value="Update Info"/>
                </div>
            </div>
        </form>    
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-delete">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top:100px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="text-align:center;color:black;">Are you sure to delete this?</h4>
                </div>
                <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                    <a href="#" class="btn btn-sm btn-danger" id="delete_link">Delete</a>
                    <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    @endsection