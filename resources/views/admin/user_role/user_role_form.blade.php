@extends('admin.layouts.app')
@section('title', 'Urban Truth')
@section('content')
<style>
    .user_access{
        border: 1px solid #33333330;
        padding: 25px;
        margin-left: 5px;
    }
    label {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 400;
  }
</style>
<section class="content">
    @if(session('save'))
    <div class="alert alert-success" role="alert">
        {{session('save')}}
    </div>
    @endif
    @if(session('error'))
    <div class="alert alert-danger" role="alert">
        {{session('error')}}
    </div>
    @endif
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Add New Role</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <form action="{{url('/create-role')}}" method="post">
                    {{csrf_field()}}
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Role Name</label>
                            <input type="text" name="role_name" value="{{old('role_name')}}" class="form-control">
                            <span class="text-danger">{{ $errors->first('role_name') }}</span>
                        </div>
                    </div>
                    <div class="col-md-6">&nbsp;</div>
                    <div >
                        <div class="col-md-12">
                            <div class="col-md-12" style="font-weight: 700;">
                                <input type="checkbox" id="ckbCheckAll" /> Check All
                            </div>
                            <p class="title">&nbsp;</p>
                            <div class="col-md-2">
                            <div class="user_access">
                                <h4>Dashboard</h4>
                                <input type="checkbox" id="DashboardCheckAll" /> Check All
                                <br/><br/>
                                <div class="form-group">
                                    <label style="font-weight: 700;">
                                        <input type="checkbox" name="permission[]" value="utadmin" class="checkBoxClass checkBoxDashboard flat-red">
                                        Dashboard
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label style="font-weight: 700;">
                                        <input type="checkbox" name="permission[]" value="site-user" class="checkBoxClass checkBoxDashboard flat-red">
                                        Site User
                                    </label>
                                </div> 
                                 <div class="form-group">
                                    <label>
                                        <input type="checkbox" name="permission[]" value="edit-site-user" class="checkBoxClass checkBoxDashboard flat-red">
                                        Edit Site User
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" name="permission[]" value="update-site-user-info" class="checkBoxClass checkBoxDashboard flat-red">
                                        Update Site User
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" name="permission[]" value="delete-site-user" class="checkBoxClass checkBoxDashboard flat-red">
                                        Delete Site User
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label style="font-weight: 700;">
                                        <input type="checkbox" name="permission[]" value="phone-request" class="checkBoxClass checkBoxDashboard flat-red">
                                        Phone Request
                                    </label>
                                </div>
                             </div>
                            </div>
                            <div class="col-md-2">
                                <div class="user_access">
                                    <h4>Admin User</h4>
                                    <input type="checkbox" id="AdminCheckAll" /> Check All
                                    <br/><br/>
                                    <div class="form-group">
                                        <label style="font-weight: 700;">
                                            <input type="checkbox" name="permission[]" value="user-menu" class="checkBoxClass checkBoxAdmin flat-red">
                                            User Main Menu
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="add-user" class="checkBoxClass checkBoxAdmin flat-red">
                                            Add User Form
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="create-admin-user" class="checkBoxClass checkBoxAdmin flat-red">
                                            Save Add User
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="view-user" class="checkBoxClass checkBoxAdmin flat-red">
                                            View User
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="user-edit" class="checkBoxClass checkBoxAdmin flat-red">
                                            Edit User
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="update-user" class="checkBoxClass checkBoxAdmin flat-red">
                                            Update User
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="delete-user" class="checkBoxClass checkBoxAdmin flat-red">
                                            Delete User
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="add-role" class="checkBoxClass checkBoxAdmin flat-red">
                                            Add Role
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="create-role" class="checkBoxClass checkBoxAdmin flat-red">
                                            Save Role
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="view-role" class="checkBoxClass checkBoxAdmin flat-red">
                                            View Role
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="edit-role" class="checkBoxClass checkBoxAdmin flat-red">
                                            Edit Role
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="role-update" class="checkBoxClass checkBoxAdmin flat-red">
                                            Update Role
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="delete-role" class="checkBoxClass checkBoxAdmin flat-red">
                                            Delete Role
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="user_access">
                                    <h4>Category</h4>
                                    <input type="checkbox" id="CategoryCheckAll" /> Check All
                                    <br/><br/>
                                    <div class="form-group">
                                        <label style="font-weight: 700;">
                                            <input type="checkbox" name="permission[]" value="category-menu" class="checkBoxClass checkBoxCategory flat-red">
                                              Category Main Menu
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="manage-category" class="checkBoxClass checkBoxCategory flat-red">
                                            Manage Category
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="edit-procate" class="checkBoxClass checkBoxCategory flat-red">
                                            Edit Main Category
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="update-procat" class="checkBoxClass checkBoxCategory flat-red">
                                            Update Main Category
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="add-subcat" class="checkBoxClass checkBoxCategory flat-red">
                                            Add Sub Category
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="save-subpro" class="checkBoxClass checkBoxCategory flat-red">
                                            Save Sub Category
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="manage-subpro" class="checkBoxClass checkBoxCategory flat-red">
                                            Manage Sub Category
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="user_access">
                                    <h4>Product</h4>
                                    <input type="checkbox" id="ProductCheckAll" /> Check All
                                    <br/><br/>
                                    <div class="form-group">
                                        <label style="font-weight: 700;">
                                            <input type="checkbox" name="permission[]" value="product-menu" class="checkBoxClass checkBoxproduct flat-red">
                                               Product Main Menu
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="add-product" class="checkBoxClass checkBoxproduct flat-red">
                                            Add Product
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="product-save" class="checkBoxClass checkBoxproduct flat-red">
                                            Product Save
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="product-manage" class="checkBoxClass checkBoxproduct flat-red">
                                            Product Manage
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="edit-product" class="checkBoxClass checkBoxproduct flat-red">
                                            Edit Product
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="update-product" class="checkBoxClass checkBoxproduct flat-red">
                                            Update Product
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="product-album-manage" class="checkBoxClass checkBoxproduct flat-red">
                                            Album Manage
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="add-product-style" class="checkBoxClass checkBoxproduct flat-red">
                                            Add Product Style
                                        </label>
                                    </div> 
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="save-product-style" class="checkBoxClass checkBoxproduct flat-red">
                                            Save Product Style
                                        </label>
                                    </div> 
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="manage-product-gallery" class="checkBoxClass checkBoxproduct flat-red">
                                            Manage Product Gallery
                                        </label>
                                    </div> 
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="edit-album-image" class="checkBoxClass checkBoxproduct flat-red">
                                            Edit Album Image
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="update-album-image" class="checkBoxClass checkBoxproduct flat-red">
                                            Update Album Image
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="delete_album_image" class="checkBoxClass checkBoxproduct flat-red">
                                            Delete Album Image
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="product-deactive-active" class="checkBoxClass checkBoxproduct flat-red">
                                            Product Active/Deactive
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="product-delete" class="checkBoxClass checkBoxproduct flat-red">
                                            Product Delete
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="user_access">
                                    <h4>Order Details</h4>
                                    <input type="checkbox" id="OrderCheckAll" /> Check All
                                    <br/><br/>
                                    <div class="form-group">
                                        <label style="font-weight: 700;">
                                            <input type="checkbox" name="permission[]" value="manage-order-menu" class="checkBoxClass checkBoxorder flat-red">
                                            Manage Order Main Menu
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="manage-incomplete-order" class="checkBoxClass checkBoxorder flat-red">
                                            Manage Incomplete Order
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="manage-all-order" class="checkBoxClass checkBoxorder flat-red">
                                            Manage All Order
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="order-details" class="checkBoxClass checkBoxorder flat-red">
                                            Order Details
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="invoice-print" class="checkBoxClass checkBoxorder flat-red">
                                            Invoice Print
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="update-quantity-search" class="checkBoxClass checkBoxorder flat-red">
                                            Update Quantity Search
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="update-quantity" class="checkBoxClass checkBoxorder flat-red">
                                            Update Quantity
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="save-new-size-qty" class="checkBoxClass checkBoxorder flat-red">
                                            Save New Size Quantity
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="permission[]" value="exchange" class="checkBoxClass checkBoxorder flat-red">
                                            Product Exchange
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        <div class="box-footer">
            <input type="submit" name="btn" class="btn btn-success" value="Add Role">
        </div>
        <form>
    </div>
</section>  
<script>
    $(document).ready(function () {
        $("#ckbCheckAll").click(function () {
            $(".checkBoxClass").prop('checked', $(this).prop('checked'));
        });

        $(".checkBoxClass").change(function () {
            if (!$(this).prop("checked")) {
                $("#ckbCheckAll").prop("checked", false);
            }
        });
        $("#AdminCheckAll").click(function () {
            $(".checkBoxAdmin").prop('checked', $(this).prop('checked'));
        });
        $("#CategoryCheckAll").click(function () {
            $(".checkBoxCategory").prop('checked', $(this).prop('checked'));
        });
        $("#ProductCheckAll").click(function(){
             $(".checkBoxproduct").prop('checked', $(this).prop('checked'));
        });
        $("#OrderCheckAll").click(function(){
             $(".checkBoxorder").prop('checked', $(this).prop('checked'));
        });
        $("#DashboardCheckAll").click(function(){
             $(".checkBoxDashboard").prop('checked', $(this).prop('checked'));
        });
    });
</script>
@stop