<?php
use App\Http\Controllers\admin\product\QuantityController;
use App\Http\Controllers\admin\product\ProductController;
?>
@extends('admin.layouts.app')
@section('title', 'Set Discount')
@section('content')
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" media="all"/>
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet" media="all"/>
<style>
    fieldset {
    padding: .75em .825em .85em;
    margin: 0 2px;
    border: 1px solid silver;
}
fieldset {
    display: block;
    -webkit-margin-start: 2px;
    -webkit-margin-end: 2px;
    -webkit-padding-before: 0.35em;
    -webkit-padding-start: 0.75em;
    -webkit-padding-end: 0.75em;
    -webkit-padding-after: 0.625em;
    min-width: -webkit-min-content;
    border-width: 2px;
    border-style: groove;
    border-color: threedface;
    border-image: initial;
}
a:link, span.MsoHyperlink {
    mso-style-noshow: yes;
    mso-style-priority: 99;
    text-decoration: none !important;
    text-underline: single;
}

.margin_new {
    margin: 2px;
}
td, th {
    padding: 5px;
}
.text-design{
        color:red;
    }
    .label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: 600;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0px;
}
.dataTables_wrapper .dataTables_paginate .paginate_button {
    box-sizing: border-box;
    display: inline-block;
    min-width: 0.5em;
    padding: 0em 0em;
    margin-left: -2px;
    text-align: center;
    text-decoration: none !important;
    cursor: pointer;
    *cursor: hand;
    color: #333 !important;
    border: 1px solid transparent;
    border-radius: 2px;
}
button.dt-button, div.dt-button, a.dt-button {
    position: relative;
    display: inline-block;
    box-sizing: border-box;
    margin-right: 0.333em;
    margin-bottom: 0.333em;
    padding: 0.5em 1em;
    border: 1px solid #999;
    border-radius: 2px;
    cursor: pointer;
    font-size: 0.88em;
    line-height: 1.6em;
    color: black;
    white-space: nowrap;
    overflow: hidden;
    background-color: #00c0ef;
    border-color: #00acd6;
    background-image: -webkit-linear-gradient(top, #fff 0%, #e9e9e9 100%); 
    background-image: none;
    background-image: none;
    background-image: none;
    background-image: none; 
    filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,StartColorStr='white', EndColorStr='#e9e9e9');
    /* -webkit-user-select: none; */
    -moz-user-select: none;
    -ms-user-select: none;
    /* user-select: none; */
    /* text-decoration: none; */
    /* outline: none; */
}
button:hover{
    background-color: #00c0ef;
    border-color: #00acd6;
}
table.dataTable.no-footer {
    border-bottom: 0px solid #111;
}
table.dataTable thead th, table.dataTable thead td {
    padding: 10px 18px;
    border-bottom: 0px solid #111; 
}
</style>
<style>
    fieldset {
    padding: .75em .825em .85em;
    margin: 0 2px;
    border: 1px solid silver;
}
fieldset {
    display: block;
    -webkit-margin-start: 2px;
    -webkit-margin-end: 2px;
    -webkit-padding-before: 0.35em;
    -webkit-padding-start: 0.75em;
    -webkit-padding-end: 0.75em;
    -webkit-padding-after: 0.625em;
    min-width: -webkit-min-content;
    border-width: 2px;
    border-style: groove;
    border-color: threedface;
    border-image: initial;
}
a:link, span.MsoHyperlink {
    mso-style-noshow: yes;
    mso-style-priority: 99;
    text-decoration: none !important;
    text-underline: single;
}

.margin_new {
    margin: 2px;
}
td, th {
    padding: 5px;
}
.text-design{
        color:red;
    }
    .label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: 600;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0px;
}
table {
    font-size: 11pt;
}
td {
    text-align: center;
}
th {
    text-align: center;
}

</style>
</style>
<section class="content-header">
    <h1>
        Product 
        <small>Manage Product</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Product</a></li>
        <li class="active">Manage Product</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-12 col-lg-12">
            <!-- /.box -->
            <div class="box">
                
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row" id="notshow">
                        <div class="box-header">
                    <h3 class="box-title">Product Discount</h3>
                </div>
                        <center>
                        @if (session('update'))
                        <div class="alert alert-success">
                            {{ session('update') }}
                        </div>
                        @endif
                    </center>
                    <center>
                        @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                        @endif
                    </center>
                        <div class="col-md-12"  style="padding-bottom:5px;">
                          <a class="btn btn-sm btn-info btn-flat pull-right"  href=" {{ url()->previous() }}">Back</a>
                        </div>
                        <div class="col-md-12"  style="padding-bottom:15px;">
                        <fieldset><legend>Set Discount</legend>
                            <form name="filtering"  action="{{url('/set-product-discount')}}" method="POST">
                                {{ csrf_field() }}
                                <table>
                                    <tr>
                                        <td class="field_label">
                                            Discount Percentage : 
                                        </td>
                                        <td>
                                           <input type="number" name="percentage"   autocomplete="off"  min="0" max="70" style="width:230px;" required/> 
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td class="field_label">
                                        </td>
                                        <td>
                                           <input type="submit" value="Set Discount"/>
                                        </td>
                                    </tr>
                                </table>
                            
                        </fieldset>
                        </div>
                    </div>
                    <strong> <?php echo $count = count($product_list);?> Products are find in range. </strong>
                    <div class="box-body">
                     <table id="" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                 <th><input type="checkbox" id="ckbCheckAll" /> All(<?php echo $count;?>)</th>
                                <th>Category Nmae</th>
                                <th>Style Code</th> 
                                <th>Product Name</th> 
                                <th>Price</th>  
                                <th>Last Sell Date</th> 
                                <th>Product insert date</th>
                                <th>Stock</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            ?>
                            <?php foreach($product_list as $product){ 
                               $data = ProductController::GetLastSell($product->product_id);
                               $qty = ProductController::ProductTotalQty($product->product_id);
                               if($data==null){
                                   $data='0000-00-00 00:00:00';
                               }else{
                                 $data=$data;  
                               }
                                $date1 = $data;
                                $date2 = "2019-03-19";
                                $diff = abs(strtotime($date2) - strtotime($date1));
                                $years = floor($diff / (365*60*60*24));
                                $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
                                
                                if($months >=6){
                                    $i++;
                            ?>
                            <tr> 
                                <td><input type="checkbox" name="product_id[]" value="<?php echo $product->product_id; ?>" class="checkBoxClass flat-red"></td>
                                <td><?php echo $product->procat_name . " - " . $product->subprocat_name; ?></td>
                                <td><?php echo $product->product_styleref; ?></td>
                                <!--<td ><?php echo $product->product_id; ?></td> --->
                                <td><?php echo $product->product_name; ?></td>
                                <td><?php echo $product->product_price; ?></td>
                                <td><?php echo $data; ?></td>
                                 <td><?php echo $product->product_insertion_date; ?></td>
                                <td><?php echo $qty; ?></td>
                                <td><img class="thumbnail  img-responsive" src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->productimg_img_tiny; ?>" alt=""  /></td> 
                            </tr>
                            <?php }} ?>
                        </tbody>
                    </table>
                 </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <a class="btn btn-sm btn-info btn-flat pull-left" onclick="printDiv()" target="__blank">Print</a>
                    
                </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script>
    $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
    $("#ckbCheckAll").click(function () {
            $(".checkBoxClass").prop('checked', $(this).prop('checked'));
        });

        $(".checkBoxClass").change(function () {
            if (!$(this).prop("checked")) {
                $("#ckbCheckAll").prop("checked", false);
            }
        });
} );
</script>
@endsection