@extends('admin.layouts.app')
@section('title', 'Pride Limited')
@section('content')
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" media="all"/>
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet" media="all"/>
<style>
    fieldset {
    padding: .75em .825em .85em;
    margin: 0 2px;
    border: 1px solid silver;
}
fieldset {
    display: block;
    -webkit-margin-start: 2px;
    -webkit-margin-end: 2px;
    -webkit-padding-before: 0.35em;
    -webkit-padding-start: 0.75em;
    -webkit-padding-end: 0.75em;
    -webkit-padding-after: 0.625em;
    min-width: -webkit-min-content;
    border-width: 2px;
    border-style: groove;
    border-color: threedface;
    border-image: initial;
}
a:link, span.MsoHyperlink {
    mso-style-noshow: yes;
    mso-style-priority: 99;
    text-decoration: none !important;
    text-underline: single;
}

.margin_new {
    margin: 2px;
}
td, th {
    padding: 5px;
}
.text-design{
        color:red;
    }
    .label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: 600;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0px;
}
.dataTables_wrapper .dataTables_paginate .paginate_button {
    box-sizing: border-box;
    display: inline-block;
    min-width: 0.5em;
    padding: 0em 0em;
    margin-left: -2px;
    text-align: center;
    text-decoration: none !important;
    cursor: pointer;
    *cursor: hand;
    color: #333 !important;
    border: 1px solid transparent;
    border-radius: 2px;
}
button.dt-button, div.dt-button, a.dt-button {
    position: relative;
    display: inline-block;
    box-sizing: border-box;
    margin-right: 0.333em;
    margin-bottom: 0.333em;
    padding: 0.5em 1em;
    border: 1px solid #999;
    border-radius: 2px;
    cursor: pointer;
    font-size: 0.88em;
    line-height: 1.6em;
    color: black;
    white-space: nowrap;
    overflow: hidden;
    background-color: #00c0ef;
    border-color: #00acd6;
    background-image: -webkit-linear-gradient(top, #fff 0%, #e9e9e9 100%); 
    background-image: none;
    background-image: none;
    background-image: none;
    background-image: none; 
    filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,StartColorStr='white', EndColorStr='#e9e9e9');
    /* -webkit-user-select: none; */
    -moz-user-select: none;
    -ms-user-select: none;
    /* user-select: none; */
    /* text-decoration: none; */
    /* outline: none; */
}
button:hover{
    background-color: #00c0ef;
    border-color: #00acd6;
}
table.dataTable.no-footer {
    border-bottom: 0px solid #111;
}
table.dataTable thead th, table.dataTable thead td {
    padding: 10px 18px;
    border-bottom: 0px solid #111; 
}
</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">All Order(s)</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div style="padding-bottom:15px;">
                         <fieldset><legend>Search Order</legend>
                                   <form name="filtering"  action="{{url('/search-order')}}" method="POST">
                                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                       <table>
                							<tr>
                								<td class="field_label">
                									Form Date:
                								</td>
                								<td>
                								    <input type="date" name="form_date" id="date" value="<?php echo $form_date;?>" autocomplete="off"/>
                									(yyyy-m-d)&nbsp;
                								</td>
                							</tr>
                							<tr>
                								<td class="field_label">
                									To Date :
                								</td>
                								<td>
                									<input type="date" name="to_date" id="ToDate"  value="<?php echo $to_date;?>" autocomplete="off" />
                									(yyyy-m-d)&nbsp;
                								</td>
                							</tr>
                							<!---<tr>
                								<td class="field_label">
                									Tracking Number :
                								</td>
                								<td>
                									<input type="text" name="tracking_number"  id="TNO"  value="<?php echo $tracking_number;?>" autocomplete="off" />
                								</td>
                							</tr> --->
                							<tr>
                								<td class="field_label">
                									Order Status :
                								</td>
                								<td>
                									<select name="order_status" id="OS">
                                                        <option value=""> -- select -- </option>
                                                        <option value="Invalidate">Invalidate</option>
                                                        <option value="Pending_Dispatch">Verified</option>
                                                        <option value="Dispatched">Confirm_Dispatch</option>
                                                        <option value="Payment_Pending">Delivered</option>
                                                        <option value="Closed">Payment_Received</option>
                                                        <option value="Cancelled">Cancelled</option>
                                                        <option value="Exchange_Pending">Exchange_Pending</option>
                                                        <option value="Exchange_Dispatched">Exchange_Dispatched</option>
                                                        <option value="Exchanged">Exchanged</option>
                                                        <option value="Returned">Returned</option>
                                                    </select>
                									&nbsp;
                								</td>
                							</tr>
                							<tr>
                								<td class="field_label">
                									Offer :
                								</td>
                								<td>
                									<input type="radio" name="get_offer"  id="offer"  value="1" />&nbsp;10% off
                								</td>
                							</tr> 
                							<tr>
                								<td class="field_label">
                								</td>
                								<td>
                								
                								</td>
                							</tr>
                							<tr>
                								<td class="field_label">
                								</td>
                								<td>
                								   <input type="submit" value="Search"/>&nbsp;
                								   <input type="checkbox" name="match_type" value="1"/>Match all criteria
                								</td>
                							</tr>
                        			</table>
                        		</form>
                        </fieldset>
                    </div>
                    
                    <div class="col-md-12">
                        <?php if($form_date != ''){ ?><p style="font-weight:bold;">Search form <?php echo $form_date;?> to <?php echo $to_date;?></p><?php } ?>
                    </div>
                    <div id="orders">
                        <table id="example" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Order Track Number</th>
                                    <th>Customer Name</th>
                                    <th>Order Placed</th> 
                                    <th>Pos Entry</th>  
                                    <th>Delivery Date</th> 
                                    <th>Delivery charge</th>
                                    <th>Sub Total</th>
                                    <th>Total Amount</th>
                                    <th>Discounted Price</th>  
                                </tr>
                            </thead>
                                @php($total_amount = 0)
                                @php($total_delivery = 0)
                                @php($subtotal = 0)
                                @php($discounted_total = 0)
                                @php($i = 1)
                                <tbody>
                                    @foreach($all_order_info as $report)
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td><a href="{{url("/pride-admin/order-details/{$report->conforder_id}")}}" target="_self" class="info">{{$report->conforder_tracknumber}}</a></td>
                                        <td>{{ $report->registeruser_firstname }} {{ $report->registeruser_lastname }}</td>
                                        <td><?php echo date('M j, Y', strtotime($report->conforder_placed_date)); ?></td>
                                        <td><?php if($report->pos_entry_date !=''){ echo date('M j, Y', strtotime($report->pos_entry_date)); } ?></td>
                                        <td><?php echo date('M j, Y', strtotime($report->conforder_deliverydate)); ?></td>
                                        <td>{{ $report->Shipping_Charge }}</td>
                                        <td>{{ $report->shoppingcart_subtotal }}</td>
                                        <td>{{ $report->Shipping_Charge + $report->shoppingcart_subtotal }}</td>
                                        <td>
                                            <?php
                                            $offer = $report->get_offer;
                                            $total = $report->shoppingcart_subtotal;
                                            if ($offer == 1) {
                                                $dis = $total * 10 / 100;
                                                $distotal = $total - $dis + $report->Shipping_Charge;
                                            } else {
                                                $distotal = $total + $report->Shipping_Charge;
                                            }
                                            echo $distotal;
                                            $discounted_total += $distotal;
                                            ?>
                                        </td>
                                        @php($total_delivery += $report->Shipping_Charge)
                                        @php($subtotal += $report->shoppingcart_subtotal)
                                        @php($total_amount= $total_delivery+$subtotal)
                                        @php($i++)
                                    </tr>
        
                                    @endforeach
                                  <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td style="font-weight: bold">Total Amount</td>
                                        <td style="font-weight: bold">{{ $total_delivery }}</td>
                                        <td style="font-weight: bold">{{ $subtotal }}</td>
                                        <td style="font-weight: bold">{{ $total_amount }}</td>
                                        <td style="font-weight: bold">{{ $discounted_total }}</td>  
                                    </tr>
                            </tbody>
                        </table>
                    </div>    
                    </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                   
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
    </div>
    
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

   <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/3.0.0-alpha.1/jspdf.plugin.autotable.min.js"></script>
    <script>
        $(document).on("click", "#PDF", function () {
            var table = document.getElementById("example");
            var cols = [],
                data = [];

            function html() {
                var _token="{{ csrf_token() }}";
                var doc = new jsPDF('p', 'pt');
                var res = doc.autoTableHtmlToJson(table, true);
                doc.autoTable(res.columns, res.data);
                var pdf =doc.output(); //returns raw body of resulting PDF returned as a string as per the plugin documentation.
                var data = new FormData();
                data.append("data_table" , pdf);
                data.append("_token" , _token);
                var xhr = new XMLHttpRequest();
                xhr.open( 'post', '/pdf-report', true ); //Post to php Script to save to server
                xhr.send(data);
        
            }
            html();
        });
    </script>
    <script>
        $(document).ready(function() {
        $('#example1').DataTable();
        $('#example').DataTable( {
            dom: 'Bfrtip',
            ordering: false,
            "lengthChange": false,
            buttons: [
              //  'copy', 'csv', 'excel', 'pdf', 'print'
                  'excel', 'pdf', 'print'
            ]
        });
        $( "#Date" ).datepicker({
           dateFormat: 'mm-dd-yy',
           changeMonth: true,
           changeYear: true
       });
    });
    </script>
    @endsection


