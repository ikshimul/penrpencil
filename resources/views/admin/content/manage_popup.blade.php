@extends('admin.layouts.app')
@section('title','Manage Banner')
@section('content')
<section class="content-header">
    <h1>
        Popup
        <small>Manage Popup</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Manage Home Page</a></li>
        <li>Popup</li>
        <li class="active">Manage</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Browse All Popup</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <center>
                        @if (session('save'))
                        <div class="alert alert-success">
                            {{ session('save') }}
                        </div>
                        @endif
                    </center>
                    <center>
                        @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                        @endif
                    </center>
                    <div class="table-responsive">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Popup Image</th>
                                    <th>Banner Caption</th>
                                    <th>Banner Link</th>
                                    <th>Created By</th>
                                    <th>Created At</th>
                                    <th>Publication Status</th>
                                    <th style="text-align:center;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($popup_list as $popup)
                                <tr>
                                    <td style="width:2%;"><?php echo $popup->id; ?></td>
                                    <td style="width:10%;text-align:center;">
                                        <img class="thumbnail  img-responsive" src="{{ URL::to('') }}/storage/app/public/popup/<?php echo $popup->image; ?>" />
                                    </td>
                                    <td><?php echo $popup->title; ?></td>
                                    <td><?php echo $popup->link; ?></td>
                                    <td><?php echo $popup->admin_username; ?></td>
                                    <td>
                                        <?php 
                                        $time = strtotime($popup->created_at);
                                        echo date('d M, Y',$time); ?>
                                    </td>
                                    <td>
                                        <?php if($popup->status==0){ ?>
                                            <a class="btn btn-danger btn-flat btn-sm tdata" href="{{url("/active-popup/{$popup->id}")}}" title="Active">Deactive</a>
                                        <?php }else{ ?>
                                        <a class="btn btn-success btn-flat btn-sm tdata" href="{{url("/deactive-popup/{$popup->id}")}}" title="Deactive">Active</a>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <a class="btn bg-olive btn-flat btn-sm margin tdata" href="{{url("/edit-popup/{$popup->id}")}}">Edit</a> 
                                        <a class="btn btn-danger btn-flat btn-sm tdata" href="#" onclick='confirm_delete("{{url("/delete-popup/{$popup->id}")}}")'>Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- Ajax modal ---->
<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="text-align:center;color:black;">Are you sure to delete this?</h4>
            </div>
            <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                <a href="#" class="btn btn-sm btn-danger" id="delete_link">Delete</a>
                <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!--- Ajax modal end ---->
@endsection