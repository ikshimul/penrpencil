@extends('admin.layouts.app')
@section('title','Edit Banner')
@section('content')
<section class="content-header">
    <h1>
        Banner
        <small>Edit </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Manage Home Page</li>
        <li>Banner</li>
        <li class="active">Edit</li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">EDIT BANNER FORM</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <form name="add_subpro" action="{{url('/update-banner')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" class="form-control" name="banner_id" value="{{$banner->banner_id}}"/>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-offset-1 col-md-6">
                        <center>
                            @if (session('save'))
                            <div class="alert alert-success">
                                {{ session('save') }}
                            </div>
                            @endif
                        </center>
                        <center>
                            @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                            @endif
                        </center>
                        <div class="form-group {{ $errors->has('banner_title') ? ' has-error' : '' }}">
                            <label>Banner Title</label>
                            <input type="text" class="form-control" name="banner_title" value="{{$banner->banner_title}}"/>
                            <div class="help-block with-errors">{{ $errors->first('banner_title') }}</div>
                        </div>
                        <div class="form-group {{ $errors->has('banner_pos') ? ' has-error' : '' }}">
                            <label>Row</label>
                            <select class="form-control" name="banner_pos" id="banner_pos">
                                <option value=""> --- Select Row --- </option>
                                <option value="1">Row 1</option>
                                <option value="2">Row 2</option>
                                <option value="3">Row 3</option>
                            </select>
                            <div class="help-block with-errors">{{ $errors->first('banner_pos') }}</div>
                        </div>
                        <div class="form-group {{ $errors->has('banner_order') ? ' has-error' : '' }}">
                            <label>Banner Position</label>
                            <input type="number" class="form-control" name="banner_order" min="1" max="10" value="{{$banner->banner_order}}"/>
                            <span class="help-block" style="color:#06be1c;">Only Numbers</span>
                            <div class="help-block with-errors">{{ $errors->first('banner_order') }}</div>
                        </div>
                        <div class="form-group {{ $errors->has('banner_link') ? ' has-error' : '' }}">
                            <label>Banner Link</label>
                            <input type="url" class="form-control" name="banner_link" value="{{$banner->banner_link}}"/>
                            <span class="help-block" style="color:#06be1c;">Only Url</span>
                            <div class="help-block with-errors">{{ $errors->first('banner_link') }}</div>
                        </div>
                        <div class="form-group">
                            <label>Current Banner</label>
                            <div class="mb10">
                                <span class="file-input">
                                    <div class="file-preview">
                                        <div class="close fileinput-remove text-right"><a href="#" onclick="confirm_modal('#');">×</a></div>
                                        <div class="file-preview-thumbnails">
                                            <div class="file-preview-frame" id="preview">
                                                <img class="img-responsive" src="{{ URL::to('') }}/storage/app/public/banner/{{$banner->banner_image}}" class="file-preview-image" title="" alt="" style="width:auto;height:160px;">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>   
                                        <div class="file-preview-status text-center text-success"></div>
                                        <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                    </div>

                                </span>
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('banner_image') ? ' has-error' : '' }}">
                            <label>Banner Image</label>
                            <input id="input-upload-img1" type="file" class="file" name="banner_image" data-preview-file-type="text">
                            <span class="help-block" style="color:#06be1c">only .jpg image is allowed Size (Width: 1110px X Height: 550px)</span>  
                            <div class="help-block with-errors">{{ $errors->first('banner_image') }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="col-md-offset-1">
                    <input type="submit" name="btnsubmit" class="btn bg-navy btn-flat margin" value="Update Banner"/>
                </div>
            </div>
        </form>    
    </div>
</section>
<script>
     document.getElementById("banner_pos").value = "<?php echo $banner->banner_pos; ?>";
</script>
@endsection