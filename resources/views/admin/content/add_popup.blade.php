@extends('admin.layouts.app')
@section('title','Add Popup')
@section('content')
<section class="content-header">
    <h1>
        Popup
        <small>Add </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Manage Home Page</li>
        <li>Popup</li>
        <li class="active">Add</li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">ADD POPUP ALERT</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <form name="add_subpro" action="{{url('/save-popup')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-offset-1 col-md-6">
                        <center>
                            @if (session('save'))
                            <div class="alert alert-success">
                                {{ session('save') }}
                            </div>
                            @endif
                        </center>
                        <center>
                            @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                            @endif
                        </center>
                        <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                            <label>Popup Title</label>
                            <input type="text" class="form-control" name="title" value="{{old('title')}}"/>
                            <div class="help-block with-errors">{{ $errors->first('title') }}</div>
                        </div>
                        <div class="form-group {{ $errors->has('link') ? ' has-error' : '' }}">
                            <label>Popup Link</label>
                            <input type="url" class="form-control" name="link" value="{{old('link')}}"/>
                            <span class="help-block" style="color:#06be1c;">Only Url</span>
                            <div class="help-block with-errors">{{ $errors->first('link') }}</div>
                        </div>
                        <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                            <label>Popup Image</label>
                            <input id="input-upload-img1" type="file" class="file" name="image" data-preview-file-type="text" required />
                            <span class="help-block" style="color:#06be1c">only .jpg image is allowed Size (Width: 450px X Height: 570px)</span>  
                            <div class="help-block with-errors">{{ $errors->first('image') }}</div>
                        </div>
                        <div class="form-group">
                            <label for="check-option-1">Popup Status</label>
                            <div style="padding-left:10px"><input type="radio" name="status" id="check-option-1" value="1" style="width:16px;" required/> Show</div>
                            <div style="padding-left:10px"><input type="radio" name="status" id="check-option-1" value="0" style="width:16px;"/> Hide</div>
                            <span class="help-text">{{ $errors->first('status') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="col-md-offset-1">
                    <input type="submit" name="btnsubmit" class="btn bg-navy btn-flat margin" value="Add Popup"/>
                </div>
            </div>
        </form>    
    </div>
</section>
@endsection