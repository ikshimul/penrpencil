@extends('admin.layouts.app')
@section('title', 'CashBook History')
@section('content')
<style>
    .label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: 600;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0px;
}
</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">CashBook History</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive" id="print_div">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="5%">SL</th>
                                <th>Date</th>
                                <th>Transaction Type</th>
                                <th>Order NO#</th> 
                                <th>Customer name</th>
                                <th>Delivery By</th>
                                <th>Transaction Amount</th>
                                <th>Expense Amount</th>
                                <th>Total Received</th>
                                <th>Balance</th>
                                
                                <th>Issue By</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            ?>
                            @php($balance=0)
                            @foreach($cash_history as $cash)
                            <tr <?php  if($cash->rider_id == 4 || $cash->rider_id == 3 || $cash->rider_id == 5){ echo "style='background-color:#d2b7b7;color: black;'"; }else if($cash->payment_method == 'ssl'){echo "style='background-color:#d2b7b7;color: #a94442;'";}else if($cash->transaction_type === 'cash_out'){echo "style='background-color:#f7d8d8;color: #a94442;'";}?>>
                                <td width="5%">{{$i++}}</td>
                                <td>
                                    <?php 
                                        $order_date=strtotime($cash->created_at); 
                                        echo date('d M , Y',$order_date);
                                    ?>
                                </td>
                                <td>{{$cash->transaction_type}}</td>
                               <?php if($cash->transaction_type === 'cash_out'){ ?>
                                <td colspan="3">{{ $cash->description }}</td>
                               <?php }else{ ?>
                                <td>
                                    <a href="{{url("/order-details/{$cash->conforder_id}")}}" target="__self" class="info">{{$cash->conforder_tracknumber}}</a>
                                </td>
                                <td> {{ $cash->Shipping_txtfirstname  }} {{ $cash->Shipping_txtlastname }} </td>
                                <td>{{$cash->delivery_by}}</td>
                                <?php } ?>
                                <td>
                                     {{ number_format($cash->transation_amount) }}
                                </td>
                                <td>
                                     {{ number_format($cash->expense_amount) }}
                                </td>
                                <td>
                                    {{ $cash->transation_amount - $cash->expense_amount }}
                                </td>
                                <td>
                                    <?php 
                                    if($cash->rider_id == 4){
                                        $balance -=$cash->expense_amount;
                                    }elseif($cash->rider_id == 3){
                                        
                                    }else if($cash->rider_id == 5){
                                        
                                    }else if($cash->payment_method == 'ssl'){
                                        
                                    }else{
                                        if ($cash->transaction_type === 'cash_out')
                                            $balance -= ($cash->transation_amount - $cash->expense_amount);
                                        else
                                            $balance += ($cash->transation_amount - $cash->expense_amount);
                                      }
                                    ?>
                                    {{ number_format($balance) }}
                                </td>
                                
                                <td>{{ $cash->admin_username }}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <td colspan="9" style="text-align:right;font-weight:600;">Current Balance</td>
                                <td>{{ number_format($balance) }}</td>
                                <td colspan="1"></td>
                            </tr>
                        </tbody>
                    </table>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                   <a class="btn btn-sm btn-info btn-flat pull-left margin" target="__blank" onclick="printDiv()">Print Cashbook</a>
                </div>
            </div>
        </div>
    </div>
 <script type="text/javascript">
    function printDiv() {
        var printContents = document.getElementById('print_div').innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
 </script>
@endsection