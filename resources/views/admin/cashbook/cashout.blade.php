@extends('admin.layouts.app')
@section('title', 'CashBook History')
@section('content')
<section class="content-header">
    <h1>
        Cashbook
        <small>Cash Transfer </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Cashbook</li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Cash Transfer</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <form action="{{ route('store.cashout') }}" method="post">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-offset-1 col-md-6">
                        @if(session('save'))
                        <div class="alert alert-success" role="alert">
                            {{session('save')}}
                        </div>
                        @endif
                        @if(session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{session('error')}}
                        </div>
                        @endif
                       <?php
                        $balance=0;
                        foreach($cash_history as $cash){
                            if($cash->rider_id == 4){
                                $balance -=$cash->expense_amount;
                            }elseif($cash->rider_id == 3){
                                
                            }else{
                                if ($cash->transaction_type === 'cash_out')
                                    $balance -= ($cash->transation_amount - $cash->expense_amount);
                                else
                                    $balance += ($cash->transation_amount - $cash->expense_amount);
                              }
                        }
                      //  $remain=$opening_balance+$cash_in-$cash_out-$expenses;
                        ?>
                        <div class="form-group">
                            <label for="amount">Amount:</label>
                            <input type="hidden" name="available_amount" class="form-control" value="<?php echo $balance;?>" required>
                            <input type="number" name="amount" class="form-control" id="amount" max="<?php echo $balance;?>" required>[<strong> Available amount <?php echo $balance;?> </strong>]
                        </div>
                        <div class="form-group">
                            <label for="pwd">Transfer to:</label>
                            <input type="text"  name="transfer_to" class="form-control" id="transfer_to" required>
                        </div>
                        <div class="form-group">
                            <label for="pwd">Description:</label>
                            <textarea class="form-control" name="description" id="description"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="col-md-offset-1">
                    <input type="submit" name="btnsubmit" class="btn bg-navy btn-flat margin" value="Cash Transfer"/>
                </div>
            </div>
        </form>    
    </div>
</section>
@endsection