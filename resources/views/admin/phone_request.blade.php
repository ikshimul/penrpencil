@extends('admin.layouts.app')
@section('title', 'Phone Request')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if (session('success'))
            <div class="pad margin no-print">
                <div class="callout callout-danger" style="margin-bottom: 0!important;">
                    <h4>{{ session('success') }}</h4> </div>
            </div>
            @endif
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">All Phone Request(s)</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                   <div class="table-responsive">
                    <table id="example2" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="2%">SL</th>
                                <th>Name</th>
                                <th>Phone Number</th>                        	
                                <th>Calling Date</th>                                                        
                                <th>Message</th>                                                        
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            ?>
                            @foreach($phone_list as $phone)
                            <tr>                        	
                                <td width="2%">{{$i++}}</td>
                                <td>{{$phone->phnumber_name}}</td>
                                <td>{{$phone->phnumber_number}} </td>
                                <td>
                                    <?php $time=strtotime($phone->pnumber_time); echo date('d M, Y h:s a',$time); ?>
                                </td>
                                <td>
                                    {{$phone->comment_box}}
                                </td>																		
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
 @endsection