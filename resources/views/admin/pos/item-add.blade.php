<?php
use App\Http\Controllers\admin\ItemDefineController;
?>
@extends('admin.layouts.app')
@section('title', 'Product Add Form')
@section('content')
<style>
    .osh-msg-box.-success {
        border-color: #00a65a;
        color: #00a65a;
    }
    .osh-msg-box.-danger {
        border-color: red;
        color: red;
    }
    .osh-msg-box {
        border-width: 1px;
        border-style: solid;
        padding: 2px 5px;
        margin: 10px 0;
    }
</style>
<section class="content-header">
    <h1>
        Product 
        <small>Add Product</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Product</a></li>
        <li class="active">Add</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- /.box -->
            <div class="box">
                <div class="box-header">
                    
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <center>
                        @if (session('update'))
                        <div class="alert alert-success">
                            {{ session('update') }}
                        </div>
                        @endif
                    </center>
                    <center>
                        @if (session('delete'))
                        <div class="alert alert-danger">
                            {{ session('delete') }}
                        </div>
                        @endif
                    </center>
                    <form name="singleform" action="{{url('/add-product-pos')}}" method="GET">
                        <div class="form-group" style="width:50%;">
                            <label>Select Style Code</label>
                            <div class="input-group">
                                <select name="stylecode" id="stylecode"  class="form-control select2" required>
                                    <option  value=""> --- Select Style Code --- </option>
                                    <?php
                                    foreach ($unique_style as $item) {
                                        ?>
                                        <option  value="<?php echo $item->DesignRef; ?>"><?php echo $item->DesignRef; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <div class="input-group-addon">
                                    <input style="background-color: white; border: none;" type="submit" name="btnsubmit" value="Next" /> 
                                </div>
                            </div>
                        </div>
                    </form>    
                </div>
            </div>
        </div>
    </div>
<?php
$count = count($item_define);
//dd($item_define);
if ($count > 0) {
    ?>
    <form name="add_product" id="myform" action="{{url('/product-save-by-pos')}}" method="post" enctype="multipart/form-data">
      <!-- <form name="add_product" id="myform" action="{{url('/product-save')}}" method="post" enctype="multipart/form-data"> -->
        {{ csrf_field() }}
            <!-- SELECT2 EXAMPLE -->
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">ADD PRODUCT FORM </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
				    <div class="row">
					<div class="col-md-3">&nbsp;</div>
					<div class="col-md-6">
						@if (session('save'))
						<div class="alert alert-success alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<h4><i class="icon fa fa-check"></i> Success!</h4>
							{{session('save')}}
						  </div>
						@endif    
						@if (session('error'))
						<div class="alert alert-danger alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<h4><i class="icon fa fa-ban"></i> Image Error!</h4>
							There is a problem that we need to fix.{{session('error')}}
						 </div>
						@endif  
					</div>
					<div class="col-md-3">&nbsp;</div>
					</div>
                    <div class="row">
                        <div class="row">
                            <div class="col-md-6" style="padding-left:50px;">
							 <fieldset style="margin:10px;  margin-top:44px; padding:5px 20px; border:2px solid #00c0ef44">
							 <legend style="padding:5px 20px; text-align:center; width:auto">Item Definitions</legend>
							  <?php if($old_item == null){ ?>
        							  <div class="form-group">
        								<label for="ddlprocat">Main Category</label>
        								<select type="text" class="form-control"  name="ddlprocat" id="ddlprocat"  required>
        									<option value="">-- select category --</option>
        									@foreach($procat_list as $procat)
        									<option value="{{$procat->procat_id}}">{{$procat->procat_name}}</option>
        									@endforeach
        								</select>
        								<span class="help-text">{{ $errors->first('ddlprocat') }}</span>
        							</div>
        							<div class="form-group">
        								<label for="ddlprosubcat1">Sub Category</label>
        								<select class="form-control" type="text" name="ddlprosubcat1" id="ddlprosubcat1" required>
        									<option value="">-- select sub category --</option>
        								</select>
        								<span class="help-text">{{ $errors->first('ddlprosubcat1') }}</span>
        							</div>
                                    <!--<div class="form-group">
                                        <label for="txtproductname">Category</label>
                                        <input class="form-control" type="text" name="main_category" id="txtproductname" value="{{$item_define->Category}}" required>
                                        <span class="help-text">{{ $errors->first('txtproductname') }}</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="txtproductname">Sub Category</label>
                                        <input class="form-control" type="text" name="sub_category" id="txtproductname" value="{{$item_define->subcategory}}" required>
                                        <span class="help-text">{{ $errors->first('txtproductname') }}</span>
                                    </div> --->
                                    <div class="form-group">
                                        <label for="txtproductname">Product Name</label>
                                        <input class="form-control" type="text" name="txtproductname" id="txtproductname" value="{{ old('txtproductname') }}" required>
                                        <span class="help-text">{{ $errors->first('txtproductname') }}</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="txtstyleref">Style Code/Ref</label>
                                        <input class="form-control" type="text" id="required-input" name="txtstyleref" id="txtstyleref" value="<?php echo $item_define->DesignRef; ?>" readonly required>
                                        <span class="help-text">{{ $errors->first('txtstyleref') }}</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="txtprice">Price</label>
                                        <input class="form-control" type="number"  name="txtprice" id="in-range-input" min="1" max="20000"  value="<?php echo $item_define->SellRate; ?>" readonly required>
                                        <span class="help-text">{{ $errors->first('txtprice') }}</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="txtpricediscounted">Discount Percentage</label>
                                        <input class="form-control" type="number" name="txtpricediscounted" id="optional-input" min="0" max="100" value="0" />
                                        <span class="help-text"></span>
                                    </div>
                                    <!--<div class="form-group">
                                            <label for="ddlfilter">Price Filter</label>
                                            <select class="form-control" name="ddlfilter" id="optional-input">
                                                    <option>-- Select Filter --</option>
                                                    <option value="high">High</option>
                                                    <option value="medium">Medium</option>
                                                    <option value="low">Low</option>
                                            </select>
                                            <span class="help-text"></span>
                                    </div>-->
                                    <div class="form-group">
                                        <label for="txtorder">Product Order</label>
                                        <input class="form-control" type="number" name="txtorder" id="optional-input" min="1" max="1000" value="<?php echo $total_product + 1; ?>" readonly>
                                        <span class="help-text"><!-- Only Numbers [ <strong>Total Product Added: <?php echo $total_product; ?></strong> ] ----></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="check-option-1">Is Special</label>
                                        <div style="padding-left:10px"><input type="radio" name="campaign" id="check-option-1" value="9" style="width:16px;"/> Pride Girls Capsule Collection 2019</div>
                                        <div style="padding-left:10px"><input type="radio" name="campaign" id="check-option-1" value="8" style="width:16px;"/> Summer 2019</div>
                                        <div style="padding-left:10px"><input type="radio" name="campaign" id="check-option-1" value="2" style="width:16px;"/> Eid Collection 2019</div>
                                        <div style="padding-left:10px"><input type="radio" name="campaign" id="check-option-1" value="7" style="width:16px;"/> Pohela Boishak 1426</div>
        								<div style="padding-left:10px"><input type="radio" name="campaign" id="check-option-1" value="6" style="width:16px;"/> Independence Day</div>
        								<div style="padding-left:10px"><input type="radio" name="campaign" id="check-option-1" value="5" style="width:16px;"/> Amar Ekushay Collection</div>
        								<div style="padding-left:10px"><input type="radio" name="campaign" id="check-option-1" value="4" style="width:16px;"/> Falgun 2019</div>
                                        <span class="help-text"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="txtproductdetails">Product Description</label>
                                        <textarea class="form-control" type="text" name="txtproductdetails" id="optional-input" rows="3"><?php echo $item_define->ItemDescription; ?></textarea>
                                        <span class="help-text"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="fabric">Fabric Composition</label>
                                        <!--<textarea type="text" name="fabric" id="optional-input" rows="3">{{ old('fabric') }}</textarea> -->
                                        <select class="form-control" name="fabric" id="fabric">
                                            <option value=""> -- Select Fabric -- </option>
                                            <option value="WOVEN 100% COTTON"> WOVEN 100% COTTON</option>
                                            <option value="VISCOSE WOVEN (90/120 GSM)">VISCOSE WOVEN (90/120 GSM)</option>
                                            <option value="KNIT 100% VISCOSE">KNIT 100% VISCOSE</option>
                                            <option value="KNIT S/J PC (65% POLYESTER 35% COTTON)">KNIT S/J PC (65% POLYESTER 35% COTTON)</option>
                                            <option value="KNIT S/J (50% COTTON 50% MODAL)">KNIT S/J (50% COTTON 50% MODAL)</option>
                                            <option value="KNIT LYCRA S/J (95% COTTON 5% ELASTANE)">KNIT LYCRA S/J (95% COTTON 5% ELASTANE)</option>
                                            <option value="KNIT S/J POLY VISCOSE ELASTANE (62% POLY 33% VIS 5% ELASTANE)">KNIT S/J POLY VISCOSE ELASTANE (62% POLY 33% VIS 5% ELASTANE)</option>
                                            <option value="WOVEN SATIN"> WOVEN SATIN</option>
                                            <option value="100% KNIT POLYESTER WITH LUREX"> 100% KNIT POLYESTER WITH LUREX</option>
                                            
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="txtproductcare">Care Description</label>
                                        <div style="padding-left:10px">
                                        <!--<textarea type="text" name="txtproductcare" id="optional-input" rows="3">{{ old('txtproductcare') }}</textarea> -->
                                            <input type="radio" name="txtproductcare" style="width: 16px;" id="care_1" value="Hand Wash Cold. Do Not Bleach. Do Not Tumble Dry.150°c (Medium Heat).Do not dry clean"/> Hand Wash Cold. Do Not Bleach. Do Not Tumble Dry.150°c (Medium Heat).Do not dry clean<br>
                                            <input type="radio" name="txtproductcare" style="width: 16px;" id="care_2" value="Hand Wash Cold. Do Not Bleach. Flat dry. 110˚c (Medium Heat). Do not dry clean"/> Hand Wash Cold. Do Not Bleach. Flat dry. 110˚c (Medium Heat). Do not dry clean<br>
                                            <input type="radio" name="txtproductcare" style="width: 16px;" id="care_3" value="Hand Wash Cold. Do Not Bleach. Flat dry. 110˚c (Medium Heat). Do not dry clean" /> Hand Wash Cold. Do Not Bleach. Flat dry. 110˚c (Medium Heat). Do not dry clean<br>
                                            <input type="radio" name="txtproductcare" style="width: 16px;" id="care_4" value="Machine Wash Cold. Do Not Bleach. TUMBLE DRY. 150˚c (Medium Heat). Do not dry clean" /> Machine Wash Cold. Do Not Bleach. TUMBLE DRY. 150˚c (Medium Heat). Do not dry clean<br>
                                            <input type="radio" name="txtproductcare" style="width: 16px;" id="care_5" value="Machine Wash Cold. Do Not Bleach. Line Dry. 150˚c (Medium Heat). Do not dry clean" /> Machine Wash Cold. Do Not Bleach. Line Dry. 150˚c (Medium Heat). Do not dry clean<br>
                                            <input type="radio" name="txtproductcare" style="width: 16px;" id="care_6" value="Machine Wash Cold. Do Not Bleach. TUMBLE DRY. Y 150˚c (Medium Heat). Do not dry clean" /> Machine Wash Cold. Do Not Bleach. TUMBLE DRY. Y 150˚c (Medium Heat). Do not dry clean<br>
                                            <input type="radio" name="txtproductcare" style="width: 16px;" id="care_7" value="Machine Wash Cold Permanent Press. Do Not Bleach. TUMBLE DRY LOW HEAT. 150˚c (Medium Heat).Dry Clean Any Solvent Except Trichloroethylene." /> Machine Wash Cold Permanent Press. Do Not Bleach. TUMBLE DRY LOW HEAT. 150˚c (Medium Heat).Dry Clean Any Solvent Except Trichloroethylene.<br>
                                            <input type="radio" name="txtproductcare" style="width: 16px;" id="care_8" value="Hand Wash Cold. Do Not Bleach. Do Not Tumble Dry.150°c (Medium Heat).Do not dry clean"/> Hand Wash Cold. Do Not Bleach. Do Not Tumble Dry.150°c (Medium Heat).Do not dry clean.<br>
                                            <input type="radio" name="txtproductcare" style="width: 16px;" id="care_9" value="Machine Wash Cold. Do Not Bleach. TUMBLE DRY. 150˚c (Medium Heat). Do not dry clean" /> Machine Wash Cold. Do Not Bleach. TUMBLE DRY. 150˚c (Medium Heat). Do not dry clean
                                        </div>
                                    </div>
							<?php }else{ ?>
									<div class="alert alert-info alert-dismissible">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<h4><i class="icon fa fa-info"></i> Alert! <i class="fa fa-fw fa-hand-o-right"></i></h4>
										This item define already in our database.please add only images for new color.
									 </div>
									 <img class="thumbnail  img-responsive" src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $old_item->product_img_thm; ?>" alt=""  height="200" width="160" />
								    <div class="form-group">
                                        <label for="txtproductname">Category</label>
                                        <input class="form-control" type="text" name="main_category" id="txtproductname" value="{{$old_item->procat_name}}" readonly>
                                        <span class="help-text">{{ $errors->first('txtproductname') }}</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="txtproductname">Sub Category</label>
                                        <input class="form-control" type="text" name="sub_category" id="txtproductname" value="{{$old_item->subprocat_name}}" readonly>
                                        <span class="help-text">{{ $errors->first('txtproductname') }}</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="txtproductname">Product Name</label>
                                        <input class="form-control" type="text" name="txtproductname" id="txtproductname" value="{{$old_item->product_name}}" readonly>
                                        <span class="help-text">{{ $errors->first('txtproductname') }}</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="txtstyleref">Style Code/Ref</label>
                                        <input class="form-control" type="text" id="required-input" name="txtstyleref" id="txtstyleref" value="<?php echo $item_define->DesignRef; ?>" readonly required>
                                        <span class="help-text">{{ $errors->first('txtstyleref') }}</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="txtprice">Price</label>
                                        <input class="form-control" type="number"  name="txtprice" id="in-range-input" min="1" max="20000"  value="<?php echo $item_define->SellRate; ?>" readonly required>
                                        <span class="help-text">{{ $errors->first('txtprice') }}</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="txtpricediscounted">Discount Percentage</label>
                                        <input class="form-control" type="number" name="txtpricediscounted" id="optional-input" min="0" max="100" value="{{$old_item->discount_product_price}}" readonly />
                                        <span class="help-text"></span>
                                    </div>
									<div class="form-group">
                                        <label for="txtproductdetails">Product Description</label>
                                        <textarea class="form-control" type="text" name="txtproductdetails" id="optional-input" rows="3" readonly><?php echo $old_item->product_description; ?></textarea>
                                        <span class="help-text"></span>
                                    </div>
							<?php } ?>
                                </fieldset>
                            </div>
							<div style="text-align:center">
                            <?php
							$i = 0;
							foreach ($item_color as $color_list) {
								$i++;
							?>
								<span style="border:1px solid #00c0ef33; min-width:100px; display:inline-block; margin:5px; padding:5px 2px; text-align:center">
									<input onClick="colorClicked(this);" type="checkbox" class="chksize clone_field" rel="size"  name="color_{{ $i }}"  style="width:39px !important;" <?php if($i == 1 ) echo 'checked';?>/> 
									<span rel="input_size"  for="input_size" class="clone_field"><?php echo $color_list->color; ?></span>
								</span>
							<?php
							}
							?>
							</div>
							<?php
                            //dd($item_color);
                            $c=0;
                             foreach ($item_color as $color_list) { 
                                 $c++;
                            ?>
                                <div class="col-md-6" id = "color_{{ $c }}" style="display:<?php if($c == 1 ) echo 'block;'; else echo 'none'; ?>">
                                    <input type="hidden" name="total_grp" class="total_grp" value="<?php echo $c;?>">
                                    <input type="hidden" name="blank" id="blank" value="">
                                    <div class="clone_grp">
                                        <div class="product_style">
                                            <div id="right_part">
                                                <div class="formcontainer">
                                                    <fieldset style="margin:10px; padding:5px 20px; border:2px solid #00c0ef44">
                                                        <legend style="padding:5px 20px; text-align:center; width:auto">Product Style for <span style="color:<?php echo $color_list->color;?>; text-shadow: 0 0 5px #555;"><?php echo $color_list->color; ?></span></legend>
                                                        <?php  $data = ItemDefineController::GetSizeBarcode($item_define->DesignRef,$color_list->color);
                                                        $s=0;
                                                        foreach($data as $stock){ 
                                                           $s++; 
                                                            ?>
                                                        <div class="form-group" style="margin-bottom:0">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <label>Size</label>
                                                                         <input type="text" size="6" rel="input_size"  name="size<?php echo $s;?>_<?php echo $c;?>" class="form-control clone_field" id="required-input" style="margin-bottom:15px" placeholder="Qty for" value="<?php echo $stock->size;?>" required/>
                                                                    </div>
                                                                   <div class="col-md-4">
                                                                    <label>Stock</label>
                                                                     <input type="text" size="6" rel="input_size"  name="input_size<?php echo $s;?>_<?php echo $c;?>" class="form-control clone_field" id="required-input" style="margin-bottom:15px" placeholder="Qty for" value="<?php echo $stock->CurrentStock;?>"/>
                                                                   </div>
                                                                   <div class="col-md-4">
                                                                        <label>Barcode</label>
                                                                     <input type="text" size="6" rel="barcode_input_size"  name="barcode_input_size<?php echo $s;?>_<?php echo $c;?>" class="form-control clone_field" id="required-input" style="margin-bottom:15px" placeholder="Barcode for " value="<?php echo $stock->Barcode;?>" readonly/>
                                                                   </div>
                                                                </div>												
                                                        </div>
                                                    <?php } session(['numberofavailabesize' => 10]); ?>
                                                        <div class="form-group">
                                                            <label rel="txtcolorname"  for="txtcolorname" class="clone_field">Color / Style Name</label>
                                                            <input class="form-control txtfield clone_field" type="text" rel="txtcolorname" name="txtcolorname_<?php echo $c;?>" value="<?php echo $color_list->color;?>">
                                                        </div>
														<div class="form-group">
															<label>Color Thumbnail</label>
															<input id="input-upload-img1" type="file" class="file" name="file_colorthm_<?php echo $c;?>" data-preview-file-type="text">
															<span class="help-block" style="color:#00b161;">[ Image Type: jpeg or jpg, Width: 82 px X Height: 61 px ]</span>  
															<span class="text-danger">{{ $errors->first('filename') }}</span>
														</div>
                                                        <!--<div class="form-group">
                                                            <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Color Thumbnail</label>
                                                            <input style="width:100%" type="file" rel="file_colorthm" name="file_colorthm_<?php echo $c;?>" class="btn btn-info clone_field">
                                                            <small style="color:#06be1c;">[ Image Type: jpeg or jpg, Width: 81 Px Height: 62 PX ]</small>
                                                        </div> -->
														<div class="form-group">
															<label>Image 1</label>
															<input id="input-upload-img1" type="file" class="file" name="file_im1_<?php echo $c;?>" data-preview-file-type="text">
															<span class="help-block" style="color:#00b161;">[ Image Type: jpeg or jpg, Width: 1300 px X Height: 1667 px ]</span>  
														</div>
                                                        <!--<div class="form-group">
                                                            <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 1</label>
                                                            <input id="input-upload-img1" style="width:100%" type="file" rel="file_im1" name="file_im1_<?php echo $c;?>" class="btn btn-info clone_field">
                                                            <small style="color:#06be1c;">[ Image Type: jpeg or jpg, Width: 1600 px X Height: 2400 px ]</small>
                                                        </div> -->
														<div class="form-group">
															<label>Image 2</label>
															<input id="input-upload-img1" type="file" class="file" name="file_im2_<?php echo $c;?>" data-preview-file-type="text">
															<span class="help-block" style="color:#00b161;">[ Image Type: jpeg or jpg, Width: 1300 px X Height: 1667 px ]</span>  
														</div>
                                                        <!--<div class="form-group">
                                                            <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 2</label>
                                                            <input style="width:100%" type="file" rel="file_im2" name="file_im2_<?php echo $c;?>" class="btn btn-info clone_field">
                                                        </div> -->
														<div class="form-group">
															<label>Image 3</label>
															<input id="input-upload-img1" type="file" class="file" name="file_im3_<?php echo $c;?>" data-preview-file-type="text">
															<span class="help-block" style="color:#00b161;">[ Image Type: jpeg or jpg, Width: 1300 px X Height: 1667 px ]</span>  
														</div>
                                                       <!-- <div class="form-group">
                                                            <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 3</label>
                                                            <input style="width:100%" type="file" rel="file_im3" name="file_im3_<?php echo $c;?>" class="btn btn-info clone_field">
                                                        </div> --->
														<div class="form-group">
															<label>Image 4</label>
															<input id="input-upload-img1" type="file" class="file" name="file_im4_<?php echo $c;?>" data-preview-file-type="text">
															<span class="help-block" style="color:#00b161;">[ Image Type: jpeg or jpg, Width: 1300 px X Height: 1667 px ]</span>  
														</div>
                                                        <!--<div class="form-group">
                                                            <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 4</label>
                                                            <input style="width:100%" type="file" rel="file_im4" name="file_im4_<?php echo $c;?>" class="btn btn-info clone_field">
                                                        </div> -->
														<div class="form-group">
															<label>Image 5</label>
															<input id="input-upload-img1" type="file" class="file" name="file_im5_<?php echo $c;?>" data-preview-file-type="text">
															<span class="help-block" style="color:#00b161;">[ Image Type: jpeg or jpg, Width: 1300 px X Height: 1667 px ]</span>  
														</div>
                                                        <!--<div class="form-group">
                                                            <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 5</label>
                                                            <input style="width:100%" type="file" rel="file_im5" name="file_im5_<?php echo $c;?>" class="btn btn-info clone_field">
                                                        </div> -->
														<div class="form-group">
															<label>Image 6</label>
															<input id="input-upload-img1" type="file" class="file" name="file_im6_<?php echo $c;?>" data-preview-file-type="text">
															<span class="help-block" style="color:#00b161;">[ Image Type: jpeg or jpg, Width: 1300 px X Height: 1667 px ]</span>  
														</div>
                                                        <!--<div class="form-group">
                                                            <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 6</label>
                                                            <input style="width:100%" type="file" rel="file_im6" name="file_im6_<?php echo $c;?>" class="btn btn-info clone_field">
                                                        </div> -->
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dynamic">
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" name="btnsubmit" class="submitbtn btn btn-primary pull-right" style="height: 3.5rem;">Add Product</button>
                    </div>
                </div>
        </section>
    </form>
<?php } ?>
<script>
     document.getElementById("stylecode").value = "<?php echo $stylecode; ?>";
	(function() {
		document.querySelector('[name=file_colorthm_1]').required = true;
		document.querySelector('[name=file_im1_1]').required = true;
	})();
	function colorClicked(element, i) {
		if (element.checked) {
			document.getElementById(element.name).style.display="block";
			document.querySelector('[name=file_colorthm_'+i+']').required = true;
			document.querySelector('[name=file_im1_'+i+']').required = true;
		} else {
			document.getElementById(element.name).style.display="none";
			document.querySelector('[name=file_colorthm_'+i+']').required = false;
			document.querySelector('[name=file_im1_'+i+']').required = false;
		}
	}
	
$(document).ready(function () {
   $("#fabric").change(function(){
		var fabric=$("#fabric").val();
		if(fabric == 'WOVEN 100% COTTON'){
			$("#care_1").attr('checked', 'checked');
			$("#care_2").attr('checked', false);
			$("#care_3").attr('checked', false);
			$("#care_4").attr('checked', false);
			$("#care_5").attr('checked', false);
			$("#care_6").attr('checked', false);
			$("#care_7").attr('checked', false);
			$("#care_8").attr('checked', false);
			$("#care_9").attr('checked', false);
		}else if(fabric == 'VISCOSE WOVEN (90/120 GSM)'){
            $("#care_1").attr('checked', false);
			$("#care_2").attr('checked', 'checked');
			$("#care_3").attr('checked', false);
			$("#care_4").attr('checked', false);
			$("#care_5").attr('checked', false);
			$("#care_6").attr('checked', false);
			$("#care_7").attr('checked', false);
			$("#care_8").attr('checked', false);
			$("#care_9").attr('checked', false);
		}else if(fabric == 'KNIT 100% VISCOSE'){
	     	$("#care_1").attr('checked', false);
			$("#care_2").attr('checked', false);
			$("#care_3").attr('checked', 'checked');
			$("#care_4").attr('checked', false);
			$("#care_5").attr('checked', false);
			$("#care_6").attr('checked', false);
			$("#care_7").attr('checked', false);
			$("#care_8").attr('checked', false);
			$("#care_9").attr('checked', false);
		}else if(fabric == 'KNIT S/J PC (65% POLYESTER 35% COTTON)'){
			$("#care_1").attr('checked', false);
			$("#care_2").attr('checked', false);
			$("#care_3").attr('checked', false);
			$("#care_4").attr('checked', 'checked');
			$("#care_5").attr('checked', false);
			$("#care_6").attr('checked', false);
			$("#care_7").attr('checked', false);
			$("#care_8").attr('checked', false);
			$("#care_9").attr('checked', false);
		}else if(fabric == 'KNIT S/J (50% COTTON 50% MODAL)'){
			$("#care_1").attr('checked', false);
			$("#care_2").attr('checked', false);
			$("#care_3").attr('checked', false);
			$("#care_4").attr('checked', false);
			$("#care_5").attr('checked', 'checked');
			$("#care_6").attr('checked', false);
			$("#care_7").attr('checked', false);
			$("#care_8").attr('checked', false);
			$("#care_9").attr('checked', false);
		}else if(fabric == 'KNIT LYCRA S/J (95% COTTON 5% ELASTANE)'){
			$("#care_1").attr('checked', false);
			$("#care_2").attr('checked', false);
			$("#care_3").attr('checked', false);
			$("#care_4").attr('checked', false);
			$("#care_5").attr('checked', false);
			$("#care_6").attr('checked', 'checked');
			$("#care_7").attr('checked', false);
			$("#care_8").attr('checked', false);
			$("#care_9").attr('checked', false);
		}else if(fabric == 'KNIT S/J POLY VISCOSE ELASTANE (62% POLY 33% VIS 5% ELASTANE)'){
			$("#care_1").attr('checked', false);
			$("#care_2").attr('checked', false);
			$("#care_3").attr('checked', false);
			$("#care_4").attr('checked', false);
			$("#care_5").attr('checked', false);
			$("#care_6").attr('checked', false);
			$("#care_7").attr('checked', 'checked');
			$("#care_8").attr('checked', false);
			$("#care_9").attr('checked', false);
		}else if(fabric =='WOVEN SATIN'){
		    $("#care_1").attr('checked', false);
			$("#care_2").attr('checked', false);
			$("#care_3").attr('checked', false);
			$("#care_4").attr('checked', false);
			$("#care_5").attr('checked', false);
			$("#care_6").attr('checked', false);
			$("#care_7").attr('checked', false);
			$("#care_8").attr('checked', 'checked');
			$("#care_9").attr('checked', false);
		}else if(fabric =='100% KNIT POLYESTER WITH LUREX'){
		    $("#care_1").attr('checked', false);
			$("#care_2").attr('checked', false);
			$("#care_3").attr('checked', false);
			$("#care_4").attr('checked', false);
			$("#care_5").attr('checked', false);
			$("#care_6").attr('checked', false);
			$("#care_7").attr('checked', false);
			$("#care_8").attr('checked', false);
			$("#care_9").attr('checked', 'checked');
		}
	});
    $("#ddlprocat").change(function () {
        var procat = $("#ddlprocat").val();
        // alert(procat)
        var url_op = base_url + "/subpro-list/" + procat;
        $.ajax({
            url: url_op,
            type: 'GET',
            dataType: 'json',
            data: '',
            success: function (data) {
                // alert(html);
                // $('#CityList').html(html);
                $('#ddlprosubcat1').empty();
                $('#ddlprosubcat1').append('<option value="">Select Sub Category</option>');
                $.each(data, function (index, supproobj) {
                    //   $('#ProductSizeList').append('<option value="' + subcatobj.productsize_id + '">' + subcatobj.productsize_size + '</option>');

                    $('#ddlprosubcat1').append('<option value="' + supproobj.subprocat_id + '">' + supproobj.subprocat_name + '</option>');
                });
            }
        });
    });
});
</script>

@endsection