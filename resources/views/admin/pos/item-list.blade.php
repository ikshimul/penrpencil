@extends('admin.layouts.app')
@section('title', 'ITEM LIST')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">POS ITEM LIST</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>ItemMasterID</th>
                                <th>Style Code</th>                        	
                                <th>ItemDescription</th> 
                                <th>Barcode</th>   
							    <th>Color</th>
                                <th>Size</th>  
                                <th>CurrentRate</th>  		  
                                <th>Stock</th>
                                <th>Brand</th> 
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            ?>
                            @foreach($item_list as $item)
                            <tr>                        	
                                <td>{{$i++}}</td>
                                <td>{{$item->ItemMasterID}}</td>
                                <td>{{$item->DesignRef}}</td>																		
                                <td>{{$item->ItemDescription}}</td>
								<td>{{$item->Barcode}}</td>
								<td>{{$item->Color}}</td>
                                <td>{{$item->Size}}</td>
                                <td>{{$item->SellRate}}</td>
                                <td>{{$item->CurrentStock}}</td>
                                <td>{{$item->Brand}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="box-footer clearfix"></div>
            </div>
        </div>
    </div>
@endsection


