@extends('admin.layouts.app')
@section('title', 'Pride Limited | All Order')
@section('content')
<style>
    .label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: 600;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0px;
}
</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">All Order(s)</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    
                    @if(Session::has('error'))
                    <div class="alert alert-danger">
                    {{ Session::get('error') }}
                    </div>
                    @endif
                    
                  <div class="table-responsive">
                    <table id="order_all" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="5%">SL</th>
                                <th>Customer Name</th>
                                <th>Order NO#</th>                        	
                                <th>Order Placed</th>
                                <th>Pos Entry</th>
                                <th>Delivery Date</th>
                                 <th>Delivery By</th>
                                <th width="15%">Order Status</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            ?>
                            @foreach($all_order_info as $orderinfo)
                            <tr>                        	
                                <td width="5%">{{$i++}}</td>
                                <td style="color:black">{{$orderinfo->Shipping_txtfirstname}} {{$orderinfo->Shipping_txtlastname}}</td>
                                <td>
                                    <a href="{{url("/order-details/{$orderinfo->conforder_id}")}}" target="__self" class="info">{{$orderinfo->conforder_tracknumber}}</a>
                                </td>
                                <td>
                                     <?php 
                                            $order_date=strtotime($orderinfo->conforder_placed_date); 
                                            echo date('d M , Y',$order_date);
                                        ?>
                                </td>
                                <td>
                                     <?php if($orderinfo->pos_entry_date !=''){
                                            $order_date=strtotime($orderinfo->pos_entry_date); 
                                            echo date('d M , Y',$order_date);
                                     }
                                        ?>
                                </td>
                                <td style="color:black">
                                  <?php 
                                     if($orderinfo->conforder_deliverydate !=null){
                                            $order_date=strtotime($orderinfo->conforder_deliverydate); 
                                            echo date('d M , Y',$order_date);
                                     }else{
                                         
                                     }
                                        ?>
                                </td>
                                <td style="color:black">{{$orderinfo->name}}</td>
                                <td>
                                    <?php if ($orderinfo->conforder_status == "Order_Verification_Pending") { ?>
                                        <span class="label label-primary" style="font-size:12px;"><?php echo str_replace("_"," ",$orderinfo->conforder_status); ?></span>
                                    <?php } else if ($orderinfo->conforder_status == "Pending_Dispatch") {
                                        ?>
                                        <span class="label label-warning" style="font-size:12px;"><?php echo str_replace("_"," ",$orderinfo->conforder_status); ?></span>
                                    <?php } else if ($orderinfo->conforder_status == "Dispatch" || $orderinfo->conforder_status == "Closed") {
                                        ?>
                                        <span class="label label-success" style="font-size:12px;"><?php echo str_replace("_"," ",$orderinfo->conforder_status); ?></span>
                                        <?php
                                    } else if($orderinfo->conforder_status == "Cancelled" || $orderinfo->conforder_status == "Invalidate") {?>
                                        <span class="label label-danger" style="font-size:12px;"><?php echo str_replace("_"," ",$orderinfo->conforder_status); ?></span>
                                 <?php }else if($orderinfo->conforder_status == "Bkash_Payment_Receive") { ?>
                                         <span class="label label-info" style="font-size:12px;"><?php echo str_replace("_"," ",$orderinfo->conforder_status); ?></span>
                                   <?php }else{ ?>
                                           <span class="label label-default" style="font-size:12px;color:black;"><?php echo str_replace("_"," ",$orderinfo->conforder_status); ?></span>
                                   <?php }  ?>
                                </td>
                                <td> <a href='{{url("/invoice/{$orderinfo->conforder_id}")}}' target="_blank" class="btn btn-default btn-sm">Print Invoice</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                  </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <style>
                                .pagination {
                                    display: inline-block;
                                    padding-left: 15px;
                                    margin: 20px 0;
                                    border-radius: 4px;
                                }
                            </style>
                            <div class="font-alt">                         
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <a href="{{url('/utadmin/manage-incomplete-order')}}" class="btn btn-sm btn-info btn-flat pull-left">Go to incomplete order page</a>
                    <a href="{{url('/excelreport')}}" class="btn btn-sm btn-primary btn-flat pull-right">Excel Report</a>
                </div>
            </div>
        </div>
    </div>
    <script>
       $(document).ready(function () {
            $('#order_all').DataTable();
        });

    </script>
    @endsection
