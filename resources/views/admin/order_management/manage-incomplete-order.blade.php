@extends('admin.layouts.app')
@section('title', 'Pride Limited | Incomplete Order')
@section('content')
<style>
    .label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: 600;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0px;
}
</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Incomplete Order(s)</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                <div class="table-responsive">
                    <table id="order_all" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="5%" style="color:black;">SL</th>
                                <th style="color:black;">Customer Name</th>
                                <th style="color:black;">Order NO#</th>   
                                <th style="color:black;">Order Placed</th> 
                                <th>Pos Entry</th>
                                <th style="color:black;">Delivery Date</th>  
                                <th style="color:black;">Delivery By</th>  
                                <th width="25%" style="color:black;">Order Status</th>  
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            ?>
                            @foreach($total_incomplete_order_info as $orderinfo)
                            <tr>                        	
                                <td style="color:black" width="5%">{{$i++}}</td>
                                <td style="color:black">{{$orderinfo->Shipping_txtfirstname}} {{$orderinfo->Shipping_txtlastname}}</td>
                                <td style="color:black">
                                    <a href="{{url("/order-details/{$orderinfo->conforder_id}")}}" target="__self" class="info">{{$orderinfo->conforder_tracknumber}}</a>
                                </td>
                                <td style="color:black">
                                   <?php 
                                            $order_date=strtotime($orderinfo->conforder_placed_date); 
                                            echo date('d M , Y',$order_date);
                                        ?>
                                </td>
                                <td>
                                     <?php if($orderinfo->pos_entry_date !=''){
                                            $order_date=strtotime($orderinfo->pos_entry_date); 
                                            echo date('d M , Y',$order_date);
                                     }
                                        ?>
                                </td>
                                <td style="color:black">
                                  <?php 
                                     if($orderinfo->conforder_deliverydate !=null){
                                            $order_date=strtotime($orderinfo->conforder_deliverydate); 
                                            echo date('d M , Y',$order_date);
                                     }else{
                                         
                                     }
                                        ?>
                                </td>
                                <td style="color:black">{{$orderinfo->name}}</td>
                                <td>
                                    <?php if ($orderinfo->conforder_status == "Order_Verification_Pending") { ?>
                                        <span class="label label-danger" style="font-size:12px;"><?php echo str_replace("_"," ",$orderinfo->conforder_status); ?></span>
                                    <?php } else if ($orderinfo->conforder_status == "Pending_Dispatch") {
                                        ?>
                                        <span class="label label-warning" style="font-size:12px;"><?php echo str_replace("_"," ",$orderinfo->conforder_status); ?></span>
                                    <?php } else if ($orderinfo->conforder_status == "Dispatch") {
                                        ?>
                                        <span class="label label-success" style="font-size:12px;"><?php echo str_replace("_"," ",$orderinfo->conforder_status); ?></span>
                                        <?php
                                    } else if($orderinfo->conforder_status == "Bkash_Payment_Receive") { ?>
                                         <span class="label label-info" style="font-size:12px;"><?php echo str_replace("_"," ",$orderinfo->conforder_status); ?></span>
                                   <?php }else{ ?>
                                           <span class="label label-default" style="font-size:12px;color:black;"><?php echo str_replace("_"," ",$orderinfo->conforder_status); ?></span>
                                   <?php }  ?>
                                </td>
                                 <td> <a href='{{url("/invoice/{$orderinfo->conforder_id}")}}' target="_blank" class="btn btn-default btn-sm">Print Invoice</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                 </div>    
                    <div class="row">

                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <style>
                                .pagination {
                                    display: inline-block;
                                    padding-left: 15px;
                                    margin: 20px 0;
                                    border-radius: 4px;
                                }
                            </style>
                            <div class="font-alt">                         
                            </div>
                        </div>
                    </div>
                    
                    <!--   <div class="row">
                         <div class="col-md-11">
                             &nbsp;
                         </div>
                         
                          <div class="col-md-1">
                            <span class="" style="text-align:right;">
                               <label>	    
                              <a  id="PrintBTN1" href="javascript:void(0);" title="Excel" onClick="print_function('excel')" class="btn btn-primary">Excel</a>
                           </label>
                          </span>
                         </div>
                    </div> --->
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                </div>
                <!-- /.box-footer -->
                
                
                
            </div>
        </div>
    </div>
    
    <script>
       $(document).ready(function () {
            $('#order_all').DataTable();
        });
    </script>
   
    @endsection


