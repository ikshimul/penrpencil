<div style="background:#FFF; border:1px solid #EFEFEF; padding:10px 20px; border-radius:5px; letter-spacing:1px; text-align:justify">
<div style="text-align:center; font-size:16px; font-weight:600; padding:50px">{{ config('app.name') }}</div>
@if (! empty($greeting))
<h2>{{ $greeting }}</h2>
@else
@if ($level == 'error')
<h2>Whoops!</h2>
@else
<h2>Hello!</h2>
@endif
@endif
@foreach ($introLines as $line)
<p>{{ $line }}</p>
@endforeach
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
            $color = 'green';
            break;
        case 'error':
            $color = 'red';
            break;
        default:
            $color = 'blue';
    }
?>
<p><a href="{{ $actionUrl }}" class="button button-{{ $color or 'blue' }}" target="_blank">{{ $actionText }}</a></p>
@endisset
@foreach ($outroLines as $line)
<p>{{ $line }}</p>
@endforeach
@if (! empty($salutation))
<p>{{ $salutation }}</p>
@else
<br/><br/><br/>
Regards,<br>{{ config('app.name') }}
@endif
<br/>
@isset($actionText)
@component('mail::subcopy')
<p>If you’re having trouble clicking the "{{ $actionText }}" button, copy and paste the URL below
into your web browser: [{{ $actionUrl }}]({{ $actionUrl }})</p>
@endcomponent
@endisset
</div>
