<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Ordershipping extends Model
{
    //ordershipping
	protected $primaryKey = 'ordershipping_id';
    protected $table = 'ordershipping';
    
    public function SaveData($conforder_id,$shoppingcart_id,$user_id,$request){
        $ordershipping['conforder_id'] = $conforder_id;
        $ordershipping['shoppingcart_id'] = $shoppingcart_id;
        $ordershipping['registeruser_id'] = $user_id;
        $ordershipping['Shipping_txtaddressname'] = $request->address;
        $ordershipping['Shipping_ddlcountry'] = $request->country_id;
        $ordershipping['Shipping_txtfirstname'] = $request->firstname;
        $ordershipping['Shipping_txtlastname'] = $request->lastname;
        $ordershipping['Shipping_txtcity'] = $request->registeruser_city;
        $ordershipping['Shipping_txtzipcode'] = $request->registeruser_zipcode;
        $ordershipping['Shipping_txtphone'] = $request->mobile_no;
        DB::table('ordershipping')->insert($ordershipping);
    }
}
