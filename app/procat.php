<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class procat extends Model {

    protected $primaryKey = 'procat_id';
    protected $table = 'procat';

    public function submenus() {
        $submenu = DB::table('procat')->where('procat.status', 0)->get();
        return $submenu;
        //return $this->hasMany('subprocat');
    }

}
