<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Browser extends Model
{
	protected $primaryKey = 'browser_id';
    protected $table = 'browser';
}
