<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Subprocat extends Model {

    //subprocat
    protected $primaryKey = 'subprocat_id';
    protected $table = 'subprocat';

    public function menu() {
        return $this->belongsTo('procat');
    }

    public function Submenu($id) {
        $submemu = DB::table('subprocat')->where('procat_id', $id)->get();
        return $submemu;
    }

}
