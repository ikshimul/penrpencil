<?php
namespace App\Others;

use Illuminate\Notifications\Messages\MailMessage;


class MyClassMailMessage extends MailMessage {

    public function splitInLines($steps){
        $arrSteps = explode("\n", $steps);
        if(!empty($arrSteps)){
            foreach ($arrSteps as $line) {
               $this->with($line);
            }
          }
         return $this; 
    }
}