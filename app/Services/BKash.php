<?php
namespace App\Services;

use DB;

//Configuration data are available in 'storage/app/config.json'

class BKash
{

    public function GetToken(){
			$request_token=$this->bkash_Get_Token();
			$idtoken=$request_token['id_token'];
				
			$_SESSION['token']=$idtoken;
			$strJsonFileContents = file_get_contents("storage/app/config.json");
			$array = json_decode($strJsonFileContents, true);

			$array['token']=$idtoken;

			$newJsonString = json_encode($array);
			file_put_contents('storage/app/config.json',$newJsonString);

			echo $idtoken;
	}
	
	public function bkash_Get_Token(){

	$strJsonFileContents = file_get_contents("storage/app/config.json");
	$array = json_decode($strJsonFileContents, true);
	
	$post_token=array(
        'app_key'=>$array["app_key"],                                              
		'app_secret'=>$array["app_secret"]                  
	);	
    
    $url=curl_init($array["tokenURL"]);
	$proxy = $array["proxy"];
	$posttoken=json_encode($post_token);
	$header=array(
		'Content-Type:application/json',
		'password:'.$array["password"],                                                               
        'username:'.$array["username"]                                                           
    );				
    
    curl_setopt($url,CURLOPT_HTTPHEADER, $header);
	curl_setopt($url,CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($url,CURLOPT_RETURNTRANSFER, true);
	curl_setopt($url,CURLOPT_POSTFIELDS, $posttoken);
	curl_setopt($url,CURLOPT_FOLLOWLOCATION, 1);
	//curl_setopt($url, CURLOPT_PROXY, $proxy);
	$resultdata=curl_exec($url);
	curl_close($url);
	return json_decode($resultdata, true);    
}

 public function CreatePayment($request, $invoice){
	$strJsonFileContents = file_get_contents("storage/app/config.json");
	$array = json_decode($strJsonFileContents, true);
	$shopping_cart=DB::table('conforder')->where('conforder_id', $invoice)->where('conforder_status', 'Order_Verification_Pending')->first();
	$shopiing_cart_id=$shopping_cart->shoppingcart_id;
	$shopping_amount=DB::table('shoppingcart')->where('shoppingcart_id', $shopiing_cart_id)->first();
	$vaild_amount=$shopping_amount->shoppingcart_total;
	$amount = $request->shoppingcart_total;
	//$amount = 2;
	if($amount == $vaild_amount){
	$intent = "sale";
	$tracknumber = "PLORDER#100-" . $invoice;
	$proxy = $array["proxy"];
		$createpaybody=array('amount'=>$amount, 'currency'=>'BDT', 'merchantInvoiceNumber'=>$tracknumber,'intent'=>$intent);   
		$url = curl_init($array["createURL"]);
		$createpaybodyx = json_encode($createpaybody);
		$header=array(
			'Content-Type:application/json',
			'authorization:'.$array["token"],
			'x-app-key:'.$array["app_key"]
		);
		curl_setopt($url,CURLOPT_HTTPHEADER, $header);
		curl_setopt($url,CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($url,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($url,CURLOPT_POSTFIELDS, $createpaybodyx);
		curl_setopt($url,CURLOPT_FOLLOWLOCATION, 1);
		//curl_setopt($url, CURLOPT_PROXY, $proxy);
		
		$resultdata = curl_exec($url);
		curl_close($url);
		echo $resultdata;
		exit;
		}else{
			echo "2073";
			exit;
		}
   }
   
   public function ExecutePayment($paymentID){
	    $strJsonFileContents = file_get_contents("storage/app/config.json");
		$array = json_decode($strJsonFileContents, true);
		$proxy = $array["proxy"];

		$url = curl_init($array["executeURL"].$paymentID);

		$header=array(
			'Content-Type:application/json',
			'authorization:'.$array["token"],
			'x-app-key:'.$array["app_key"]              
		);	
			
		curl_setopt($url,CURLOPT_HTTPHEADER, $header);
		curl_setopt($url,CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($url,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($url,CURLOPT_FOLLOWLOCATION, 1);
		//curl_setopt($url, CURLOPT_PROXY, $proxy);

		$resultdatax=curl_exec($url);
		curl_close($url);
		echo $resultdatax;
		exit;
   }
   public function success($request){
	    $strJsonFileContents = file_get_contents("storage/app/config.json");
		$array = json_decode($strJsonFileContents, true);
		$proxy = $array["proxy"];
		$array["queryURL"] = 'https://checkout.sandbox.bka.sh/v1.2.0-beta/checkout/payment/query/';

		$url = curl_init($array["queryURL"].$request->paymentID);

		$header=array(
			'Content-Type:application/json',
			'authorization:'.$array["token"],
			'x-app-key:'.$array["app_key"]              
		);	
			
		curl_setopt($url,CURLOPT_HTTPHEADER, $header);
		curl_setopt($url,CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($url,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($url,CURLOPT_FOLLOWLOCATION, 1);
		//curl_setopt($url, CURLOPT_PROXY, $proxy);

		$resultdatax=curl_exec($url);
		
		curl_close($url);
		$resultdatax = json_decode($resultdatax);
		return $this->isValid($resultdatax->merchantInvoiceNumber);
   }
   public function isValid($conforder_id) {
       $row = DB::table('conforder')->where('conforder_tracknumber', $conforder_id)->where('conforder_status', 'Order_Verification_Pending')->first();
       if($row) {
           return $row->conforder_id;
       }
       return false;
   }
}