<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityList extends Model
{
    protected $primaryKey = 'CityId';
    protected $table = 't_city';
}
