<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDiscription extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'product_discriptions';
}
