<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminReview extends Model
{
     protected $table = 'admin_reviews';
	 protected $primaryKey = 'id';
	 
}
