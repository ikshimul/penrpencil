<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productimg extends Model
{
    //productimg
	protected $primaryKey = 'productimg_id';
    protected $table = 'productimg';
}
