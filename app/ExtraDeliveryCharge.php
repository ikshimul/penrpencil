<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtraDeliveryCharge extends Model {
    protected $fillable = ['charge', 'comment', 'conforder_id'];
    protected $hidden = ['created_at', 'updated_at'];
}
