<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Popups extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'popups';
}
