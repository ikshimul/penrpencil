<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShowroomList extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'showroom_lists';
}
