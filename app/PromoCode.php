<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class PromoCode extends Model
{
    //
    public function scopeIsExists($query, $code) {
        $row = $query->where('promo_code', $code)->first();
        if($row)
            return $row->promo_code;
        return false;
    }
    
    public function scopeisActive($query, $code){
        $row = $query->where('promo_code', $code)->where('is_active',0)->first();
        if($row)
            return $row->promo_code;
        return false;
    }
    public function scopeDeactivate($query, $code) {
        $row = $query->where('promo_code', $code)->update(['is_active'=> 1]);
        
        return $row;
    }
    
    public function PromocodeList(){
        $promocodes=DB::table('promo_codes')
        ->join('showroom_lists','promo_codes.showroom_id','=','showroom_lists.id')
        ->select('promo_codes.promo_code','showroom_lists.SalesPointName','showroom_lists.SPAddress','showroom_lists.Phone')->get();
        return $promocodes;
    }
}
