<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Coupon;
use App\ShowroomList;
use DB;
use App\PromoCode;

class CuponController extends Controller
{
	public function __construct() {
        date_default_timezone_set("Asia/Dhaka");
    }
	
	public function GenerateCoupon(){
	    $data['list']=ShowroomList::get();
		return view('admin.coupon.generate',$data);
	}
	
	public function ManageCoupon(){
	    $promo=new PromoCode();
	    $data['list']=$promo->PromocodeList();
	    return view('admin.coupon.manage',$data);
	}

	public function StorePromo(Request $request){
	    
		$no_of_coupons = $request->no_of_coupons;
        $coupons = [];
        for ($i = 0; $i < $no_of_coupons; $i++) {
            $temp = $this->Coupongenerate($request);
            $coupons[] = $temp;
        }
		foreach ($coupons as $key => $value) {
			$data['promo_code']=$value;
			$data['discount_percentage']="10";
			$data['showroom_id']=$request->showroom_id;
			DB::table('promo_codes')->insert($data);
		}
		 return redirect()->back()->with('code',"success $value");
	}

    public function Coupongenerate($request){
		$coupon = '';
		$uppercase    = ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M'];
        $lowercase    = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm'];
        $numbers      = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        $symbols      = ['`', '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '=', '+', '\\', '|', '/', '[', ']', '{', '}', '"', "'", ';', ':', '<', '>', ',', '.', '?'];
        $characters   = [];
		$characters = array_merge($characters, $lowercase, $uppercase);
		$characters = array_merge($characters, $numbers);
		//$characters = array_merge($characters, $symbols);
		for ($i = 0; $i < $request->length; $i++) {
					$coupon .= $characters[mt_rand(0, count($characters) - 1)];
				}
			//	return $request->prefix . $coupon . $request->suffix;
				return $request->prefix . $request->suffix;
			}
	}  

