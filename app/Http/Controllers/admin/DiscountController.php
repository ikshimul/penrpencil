<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class DiscountController extends Controller
{
    public function ProductFiltering(){
        
    $product_list=DB::table('product')
        ->leftjoin('shoppinproduct','product.product_id','=','shoppinproduct.product_id')
        ->leftjoin('conforder','shoppinproduct.shoppingcart_id','=','conforder.shoppingcart_id')
        ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
        ->join('procat', 'product.procat_id', '=', 'procat.procat_id')
        ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
        ->join('productimg', 'productimg.productalbum_id', '=', 'productalbum.productalbum_id')
        ->select('product.product_id','product.product_name','product.product_insertion_date','product.product_styleref','product.product_price','product.product_pricediscounted','product.discount_product_price','product.sale','product.product_img_thm','subprocat.subprocat_name', 'procat.procat_name','shoppinproduct.created_at','productimg.productimg_img_tiny','shoppinproduct.shoppingcart_date','conforder.conforder_placed_date')
        ->where('product.product_price','>=',2000)
        ->groupBy('product.product_id')
        ->get();
    $data['product_list'] = $product_list;
    return view('admin.discount.add_discount_set',$data);
    }
    
    public function UpdateProductDiscount(Request $request){
         $percentage = $request->percentage;
         $product_id = $request->product_id;
          if(isset($product_id) && isset($percentage)){
            foreach ($product_id as $p_id) {
            if($percentage > 0){
                DB::table('product')->where('product.product_id',$p_id)->update(['product_pricediscounted' => $percentage, 'discount_product_price' => DB::raw( 'product_price - product_price * '.$percentage.' / 100' )]); 
            }else{
                DB::table('product')->where('product.product_id',$p_id)->update(['product_pricediscounted' => $percentage, 'discount_product_price' => 0]);  
              }
          }
           return redirect()->back()->with('update', 'Product discount set successfull.');
          }else{
              return redirect()->back()->with('error', 'Please select atlest one product.');
          }
    }
}
