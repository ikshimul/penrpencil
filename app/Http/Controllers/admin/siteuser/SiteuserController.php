<?php

namespace App\Http\Controllers\admin\siteuser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Mail;
use Illuminate\Support\Facades\Input;
use Response;
use File;
use Storage;

class SiteuserController extends Controller {

    public function __construct() {
        $this->middleware('AdminAuth');
    }

    public function SiteUserList() {
        $data['site_user'] = DB::table('users')
                ->join('registeruser', 'users.id', '=', 'registeruser.registeruser_login_id')
                ->join('registeruserdetails', 'registeruser.registeruser_id', '=', 'registeruserdetails.registeruser_id')
                ->select('users.id', 'registeruser.registeruser_id', 'registeruser.registeruser_firstname', 'registeruser.registeruser_lastname', 'registeruser.registeruser_email', 'registeruser.created_at', 'registeruserdetails.registeruser_phone', 'registeruserdetails.registeruser_address', 'registeruserdetails.registeruser_address1', 'registeruserdetails.registeruser_city', 'registeruserdetails.registeruser_zipcode')
                ->where('registeruser.registeruser_active', 1)
                ->groupBy('users.id')
                ->orderBy('registeruser.registeruser_id', 'DESC')
                ->get();
        return view('admin.siteuser.site_user_list', $data);
    }
    

    public function DeleteUser($user_id) {
        $data['registeruser_active'] = 0;
        DB::table('registeruser')->where('registeruser_id', $user_id)->update($data);
        return redirect()->back()->with('success', 'User deleted!');
    }
    public function CustomerSendMessageForm(){
        return view('admin.siteuser.send_message');
    }
    
    public function CustomerSearch(){
        $term = Input::get('term');
    	$results = array();
    	$queries = DB::table('registeruser')
    	     ->join('registeruserdetails', 'registeruser.registeruser_id', '=', 'registeruserdetails.registeruser_id')
    		->where('registeruser_email', 'LIKE', '%'.$term.'%')
    		->orWhere('registeruser_firstname', 'LIKE', '%'.$term.'%')
    		->orWhere('registeruser_lastname', 'LIKE', '%'.$term.'%')
    		->orWhere('registeruser_phone', 'LIKE', '%'.$term.'%')
    		->groupby('registeruser.registeruser_id')
    		->take(10)->get();
        	foreach ($queries as $query)
        	{
        	    $results[] = array(
                    "label" => 'Customer Email : '.$query->registeruser_email.' / Customer Name : '.$query->registeruser_firstname.' '.$query->registeruser_lastname,
                    "value" =>$query->registeruser_email,
                    "idno" => $query->registeruser_id
                );
        	}
        return Response::json($results);
    }
    
    public function CustomerSendMessage(Request $request){
        $emailto=$request->emailto;
        /* $e_data=DB::table('registeruser')
            ->select('registeruser.registeruser_email','registeruser.registeruser_firstname','registeruser.registeruser_lastname')
            ->where('registeruser.registeruser_email',$emailto)
            ->first();
            $email_data['registeruser_email']=$e_data->registeruser_email;
            $email_data['registeruser_firstname']=$e_data->registeruser_firstname;
            $email_data['registeruser_lastname']=$e_data->registeruser_lastname; */
            $email_data['registeruser_email']= $emailto=$request->emailto;;
            $email_data['subject']=$request->subject;
            $message_body=html_entity_decode($request->message);
            $email_data['message_details']=$message_body;
          //  $files = $request->file('file');
            Mail::send('emails.send_message', $email_data, function($message) use($email_data) {
            $message->from('customer-service@pride-limited.com', 'Pride Limited');
            $message->to($email_data['registeruser_email']);
            $message->bcc('pride.orderlog@gmail.com', 'Pride Group');
            $message->subject($email_data['subject']);
           /* if(count($files > 0)) {
                foreach($files as $file) {
                    $message->attach($file->getRealPath(), array(
                       'as' => $file->getClientOriginalName(),  
                        )
                   );
                }
            } */
        });
       return redirect()->back()->with('success', 'Email send successfully.'); 
    }
    
    public function PhoneRequest(){
        $up_data['view']=1;
        DB::table('phnumber')->update($up_data);
        $data['phone_list']=DB::table('phnumber')->where('phone_status',1)->orderBy('phnumber_id','DESC')->get();
        return view('admin.phone_request',$data);
    }

}
