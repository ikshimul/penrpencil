<?php

namespace App\Http\Controllers\admin\user;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Role;
use App\RolePermission;

class RoleController extends Controller {

    public function __construct(Role $role, RolePermission $role_permission) {
         $this->middleware('AdminAuth');
       // $this->middleware('permission');
        $this->Role = $role;
       $this->RolePermission = $role_permission;
    }

    public function index() {
        return view('admin.user_role.user_role_form');
    }

    public function create(Request $request) {
        $this->validate($request, [
            'role_name' => 'required|unique:roles,role_name'
        ]);
        $data = [
            'role_name' => $request->role_name
        ];
        $result = DB::table('roles')->insertGetId($data);
        if ($result) {
            $permission = $request->permission;
            foreach ($permission as $pinfo) {
                $role_permission = new RolePermission();
                $role_permission->role_id = $result;
                $role_permission->permission = $pinfo;
                $role_permission->save();
            }
            $request->session()->flash('save', 'New role add successfully');
        } else {
            $request->session()->flash('error', 'New role not added');
        }
        return redirect()->back();
    }

    public function view() {
        $data['role'] = $this->Role->all();
        return view('admin.user_role.view_role')->with($data);
    }

    public function edit(Request $request) {
        $role_id = $request->id;
        $data['role'] = $this->Role->find($role_id);
        $data['permission'] = $this->RolePermission->get_permission($role_id);
        return view('admin.user_role.edit_role_form')->with($data);
    }

    public function update(Request $request) {
        $role_id = $request->id;
        $this->Role->where('id', $role_id)->update(['role_name' => $request->role_name]);
        $this->RolePermission->where('role_id', $role_id)->delete();
        $permission = $request->permission;
        foreach ($permission as $p_info) {
            //create method use and fillable must set in model 
            RolePermission::create([
                'role_id' => $role_id,
                'permission' => $p_info
            ]);
        }
         $request->session()->flash('update', 'Role  Eidted Successfully!.');
         return redirect()->back();
    }

}
