<?php

namespace App\Http\Controllers\admin\user;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Storage;
use Illuminate\Validation\Rule;
use App\RolePermission;

class usercontroller extends Controller {

    protected $redirectTo = '/login';

    public function __construct(RolePermission $role_permission) {
         $this->middleware('AdminAuth');
       //  $this->middleware('permission');
         $this->RolePermission = $role_permission;
    }

    public function index() {
        $data['role'] = DB::table('roles')->get();
        return view('admin.user.add_user_form', $data);
    }

    public function view() {
        $data['users'] = DB::table('admin')->select('admin.*', 'employe.*', 'roles.role_name')
                ->join('employe', 'admin.employe_id', '=', 'employe.employe_id')
                ->join('roles', 'admin.role_id', '=', 'roles.id')
                ->get();
        return view('admin.user.view_user', $data);
    }

    public function create(Request $request) {
        $vaildation = Validator::make($request->all(), [
                    'first_name' => 'required',
                    'user_name' => 'required|max:10|unique:admin,admin_username',
                    'password' => 'required|min:6',
                    'c_password' => 'required|same:password'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $file = $request->file('photo');
            $savename = time(10) . '_' . $file->getClientOriginalName();
            Storage::put('public/' . $savename, file_get_contents($request->file('photo')->getRealPath()));
            $employee = [
                'employe_name' => $request->first_name,
                'employe_email' => $request->email,
                'employe_telephone' => $request->phone_number,
                'employe_details' => $request->address,
                'employe_image' => $savename
            ];
            $employee_id = DB::table('employe')->insertGetId($employee);
            if ($employee_id) {
                $data = [
                    'admin_username' => $request->user_name,
                    'admin_password' => md5($request->password),
                    'employe_id' => $employee_id,
                    'role_id' => $request->role_id,
                ];
                $result = DB::table('admin')->insert($data);
                if ($result) {
                    return redirect()->back()->with('save', 'User insert successfully');
                } else {
                    return redirect()->back()->with('error', 'User not insert');
                }
            } else {
                return redirect()->back()->with('error', 'User not insert');
            }
        }
    }

    public function edit($id) {
        $data['user'] = DB::table('admin')->select('admin.*', 'employe.*', 'roles.role_name')
                ->join('employe', 'admin.employe_id', '=', 'employe.employe_id')
                ->join('roles', 'admin.role_id', '=', 'roles.id')
                ->where('admin.admin_id', $id)
                ->first();
        $data['role'] = DB::table('roles')->get();
        return view('admin.user.user_edit_form')->with($data);
    }

    public function update(Request $request) {
        $rules = [
            'first_name' => 'required',
            'admin_username' => Rule::unique('admin')->ignore($request->admin_id, 'admin_id'),
                //   'password' => 'required|min:6',
                //   'c_password' => 'required|same:password'
        ];
        $vaildation = Validator::make($request->all(), $rules);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $employee = [
                'employe_name' => $request->first_name,
                'employe_email' => $request->email,
                'employe_telephone' => $request->phone_number,
                'employe_details' => $request->address
            ];
            DB::table('employe')->where('employe_id', $request->employe_id)->update($employee);
            $data = [
                'admin_username' => $request->admin_username,
                // 'admin_password' => md5($request->password),
                'employe_id' => $request->employe_id,
                'role_id' => $request->role_id,
            ];
            $result = DB::table('admin')->where('admin_id', $request->admin_id)->update($data);
            if ($result) {
                return redirect()->back()->with('save', 'User updated');
            } else {
                return redirect()->back()->with('error', 'User updated');
            }
        }
    }

    public function delete($employee_id, $admin_id) {
        DB::table('employe')->where('employe_id', $employee_id)->delete();
        $result = DB::table('admin')->where('admin_id', $admin_id)->delete();
        if ($result) {
            return redirect()->back()->with('save', 'Delete successfully');
        } else {
            return redirect()->back()->with('error', 'User not delete');
        }
    }

}
