<?php

namespace App\Http\Controllers\admin\report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Mail;
use Response;

class ReportController extends Controller
{
    public function __construct() {
        $this->middleware('AdminAuth');
    }
    

    public function SearchOrder(Request $request){
       // dd($request);
        $form = $request->form_date;
        $to = $request->to_date;
        $tracking_number = $request->tracking_number;
        $order_status = $request->order_status;
        $get_offer = $request->get_offer;
        $match = $request->match_type;
                if($form !='' && $to !='' && $tracking_number!='' && $order_status!=''){
                   $form = $form.' '.'00:00:00';
                   $to= $to.' '.'23:59:00';
                    $all_order_info = DB::table('conforder')
                    ->join('registeruser', 'conforder.registeruser_id', '=', 'registeruser.registeruser_id')
                    ->join('shoppingcart','shoppingcart.shoppingcart_id', '=', 'conforder.shoppingcart_id')
                    ->join('shoppinproduct', 'shoppingcart.shoppingcart_id', 'shoppinproduct.shoppingcart_id', '=')
                    ->select('conforder.conforder_id','conforder.pos_entry_date', 'conforder.conforder_status','conforder.conforder_deliverydate','shoppingcart.shoppingcart_subtotal', 'conforder.conforder_lastupdte','conforder.order_threepldlv','conforder.conforder_placed_date', 'conforder.conforder_tracknumber', 'registeruser.registeruser_firstname', 'registeruser.registeruser_lastname','shoppinproduct.Shipping_Charge','shoppingcart.get_offer', 'shoppingcart.shoppingcart_subtotal', 'shoppingcart.shoppingcart_total', 'shoppingcart.shoppingcart_total')
                    ->whereBetween('conforder.conforder_lastupdte', [$form, $to])
                    ->where('conforder_tracknumber', $tracking_number)
                    ->where('conforder_status', $order_status)
                    ->orderBy('conforder.conforder_id', 'desc')
                    ->groupBy('conforder.conforder_id')
                    ->get();
                }else if($tracking_number!='' && $order_status!='' && $form =='' && $to ==''){
                    $all_order_info = DB::table('conforder')
                    ->join('registeruser', 'conforder.registeruser_id', '=', 'registeruser.registeruser_id')
                    ->join('shoppingcart','shoppingcart.shoppingcart_id', '=', 'conforder.shoppingcart_id')
                    ->join('shoppinproduct', 'shoppingcart.shoppingcart_id', 'shoppinproduct.shoppingcart_id', '=')
                    ->select('conforder.conforder_id','conforder.pos_entry_date', 'conforder.conforder_status','conforder.conforder_deliverydate','shoppingcart.shoppingcart_subtotal', 'conforder.conforder_lastupdte','conforder.order_threepldlv','conforder.conforder_placed_date', 'conforder.conforder_tracknumber', 'registeruser.registeruser_firstname', 'registeruser.registeruser_lastname','shoppinproduct.Shipping_Charge','shoppingcart.get_offer', 'shoppingcart.shoppingcart_subtotal', 'shoppingcart.shoppingcart_total', 'shoppingcart.shoppingcart_total')
                    ->where('conforder_tracknumber', $tracking_number)
                    ->where('conforder_status', $order_status)
                    ->orderBy('conforder.conforder_id', 'desc')
                    ->groupBy('conforder.conforder_id')
                    ->get();
                }elseif($form !='' && $to !='' && $tracking_number =='' && $order_status ==''){
                    $form = $form.' '.'00:00:00';
                    $to= $to.' '.'23:59:00';
                    $all_order_info = DB::table('conforder')
                    ->join('registeruser', 'conforder.registeruser_id', '=', 'registeruser.registeruser_id')
                    ->join('shoppingcart','shoppingcart.shoppingcart_id', '=', 'conforder.shoppingcart_id')
                    ->join('shoppinproduct', 'shoppingcart.shoppingcart_id', 'shoppinproduct.shoppingcart_id', '=')
                    ->select('conforder.conforder_id', 'conforder.pos_entry_date','conforder.conforder_status','conforder.conforder_deliverydate','shoppingcart.shoppingcart_subtotal', 'conforder.conforder_lastupdte','conforder.order_threepldlv','conforder.conforder_placed_date', 'conforder.conforder_tracknumber', 'registeruser.registeruser_firstname', 'registeruser.registeruser_lastname','shoppinproduct.Shipping_Charge','shoppingcart.get_offer', 'shoppingcart.shoppingcart_subtotal', 'shoppingcart.shoppingcart_total', 'shoppingcart.shoppingcart_total')
                    ->whereBetween('conforder.conforder_lastupdte', [$form, $to])
                    ->orderBy('conforder.conforder_id', 'desc')
                    ->groupBy('conforder.conforder_id')
                    ->get();
                }elseif($form !='' && $to !='' && $order_status !='' && $get_offer ==''){
                    $form = $form.' '.'00:00:00';
                    $to= $to.' '.'23:59:00';
                     $all_order_info = DB::table('conforder')
                    ->join('registeruser', 'conforder.registeruser_id', '=', 'registeruser.registeruser_id')
                    ->join('shoppingcart','shoppingcart.shoppingcart_id', '=', 'conforder.shoppingcart_id')
                    ->join('shoppinproduct', 'shoppingcart.shoppingcart_id', 'shoppinproduct.shoppingcart_id', '=')
                    ->select('conforder.conforder_id','conforder.pos_entry_date', 'conforder.conforder_status','conforder.conforder_deliverydate', 'shoppingcart.shoppingcart_subtotal','conforder.conforder_lastupdte','conforder.order_threepldlv','conforder.conforder_placed_date', 'conforder.conforder_tracknumber', 'registeruser.registeruser_firstname', 'registeruser.registeruser_lastname','shoppinproduct.Shipping_Charge','shoppingcart.get_offer', 'shoppingcart.shoppingcart_subtotal', 'shoppingcart.shoppingcart_total', 'shoppingcart.shoppingcart_total')
                    ->where('conforder.conforder_lastupdte','>=',$form)
                    ->where('conforder.conforder_lastupdte','<=',$to)
                    ->where('conforder_status', $order_status)
                    ->orderBy('conforder.conforder_id', 'desc')
                    ->groupBy('conforder.conforder_id')
                    ->get();
                }elseif($form !='' && $to !='' && $order_status !='' && $get_offer !=''){
                    $form = $form.' '.'00:00:00';
                    $to= $to.' '.'23:59:00';
                     $all_order_info = DB::table('conforder')
                    ->join('registeruser', 'conforder.registeruser_id', '=', 'registeruser.registeruser_id')
                    ->join('shoppingcart','shoppingcart.shoppingcart_id', '=', 'conforder.shoppingcart_id')
                    ->join('shoppinproduct', 'shoppingcart.shoppingcart_id', 'shoppinproduct.shoppingcart_id', '=')
                    ->select('conforder.conforder_id','conforder.pos_entry_date', 'conforder.conforder_status','conforder.conforder_deliverydate', 'shoppingcart.shoppingcart_subtotal','conforder.conforder_lastupdte','conforder.order_threepldlv','conforder.conforder_placed_date', 'conforder.conforder_tracknumber', 'registeruser.registeruser_firstname', 'registeruser.registeruser_lastname','shoppinproduct.Shipping_Charge','shoppingcart.get_offer', 'shoppingcart.shoppingcart_subtotal', 'shoppingcart.shoppingcart_total', 'shoppingcart.shoppingcart_total')
                    ->where('shoppingcart.get_offer', 1)
                    ->where('conforder.conforder_lastupdte','>=',$form)
                    ->where('conforder.conforder_lastupdte','<=',$to)
                    ->where('conforder_status', $order_status)
                    ->orderBy('conforder.conforder_id', 'desc')
                    ->groupBy('conforder.conforder_id')
                    ->get();
                    
                }elseif($form !='' && $to !='' && $get_offer !=''){
                    $form = $form.' '.'00:00:00';
                    $to= $to.' '.'23:59:00';
                     $all_order_info = DB::table('conforder')
                    ->join('registeruser', 'conforder.registeruser_id', '=', 'registeruser.registeruser_id')
                    ->join('shoppingcart','shoppingcart.shoppingcart_id', '=', 'conforder.shoppingcart_id')
                    ->join('shoppinproduct', 'shoppingcart.shoppingcart_id', 'shoppinproduct.shoppingcart_id', '=')
                    ->select('conforder.conforder_id','conforder.pos_entry_date', 'conforder.conforder_status','conforder.pos_entry_date','conforder.conforder_deliverydate','shoppingcart.shoppingcart_subtotal','conforder.conforder_lastupdte','conforder.order_threepldlv','conforder.conforder_placed_date', 'conforder.conforder_tracknumber', 'registeruser.registeruser_firstname', 'registeruser.registeruser_lastname','shoppinproduct.Shipping_Charge','shoppingcart.get_offer', 'shoppingcart.shoppingcart_subtotal', 'shoppingcart.shoppingcart_total', 'shoppingcart.shoppingcart_total')
                    ->where('shoppingcart.get_offer', 1)
                    ->where('conforder.conforder_lastupdte','>=',$form)
                    ->where('conforder.conforder_lastupdte','<=',$to)
                    ->groupBy('conforder.conforder_id')
                    ->orderBy('conforder.conforder_id', 'desc')
                    ->get();
                }elseif($tracking_number!='' && $order_status=='' && $form =='' && $to ==''){
                     $all_order_info = DB::table('conforder')
                    ->join('registeruser', 'conforder.registeruser_id', '=', 'registeruser.registeruser_id')
                    ->join('shoppingcart','shoppingcart.shoppingcart_id', '=', 'conforder.shoppingcart_id')
                    ->join('shoppinproduct', 'shoppingcart.shoppingcart_id', 'shoppinproduct.shoppingcart_id', '=')
                    ->select('conforder.conforder_id', 'conforder.pos_entry_date','conforder.conforder_status','conforder.pos_entry_date','conforder.conforder_deliverydate','shoppingcart.shoppingcart_subtotal', 'conforder.conforder_lastupdte','conforder.order_threepldlv','conforder.conforder_placed_date', 'conforder.conforder_tracknumber', 'registeruser.registeruser_firstname', 'registeruser.registeruser_lastname','shoppinproduct.Shipping_Charge','shoppingcart.get_offer', 'shoppingcart.shoppingcart_subtotal', 'shoppingcart.shoppingcart_total', 'shoppingcart.shoppingcart_total')
                    ->where('conforder_tracknumber', $tracking_number)
                    ->orderBy('conforder.conforder_id', 'desc')
                    ->groupBy('conforder.conforder_id')
                    ->get();
                }elseif($order_status!='' && $tracking_number=='' && $form =='' && $to ==''){
                    $all_order_info = DB::table('conforder')
                    ->join('registeruser', 'conforder.registeruser_id', '=', 'registeruser.registeruser_id')
                    ->join('shoppingcart','shoppingcart.shoppingcart_id', '=', 'conforder.shoppingcart_id')
                    ->join('shoppinproduct', 'shoppingcart.shoppingcart_id', 'shoppinproduct.shoppingcart_id', '=')
                    ->select('conforder.conforder_id', 'conforder.pos_entry_date','conforder.conforder_status','conforder.pos_entry_date','conforder.conforder_deliverydate','shoppingcart.shoppingcart_subtotal', 'conforder.conforder_lastupdte','conforder.order_threepldlv','conforder.conforder_placed_date', 'conforder.conforder_tracknumber', 'registeruser.registeruser_firstname', 'registeruser.registeruser_lastname','shoppinproduct.Shipping_Charge','shoppingcart.get_offer', 'shoppingcart.shoppingcart_subtotal', 'shoppingcart.shoppingcart_total', 'shoppingcart.shoppingcart_total')
                    ->where('conforder_status', $order_status)
                    ->orderBy('conforder.conforder_id', 'desc')
                    ->groupBy('conforder.conforder_id')
                    ->get();
                }elseif($get_offer!='' && $order_status=='' && $tracking_number=='' && $form =='' && $to ==''){
                     $all_order_info = DB::table('conforder')
                    ->join('registeruser', 'conforder.registeruser_id', '=', 'registeruser.registeruser_id')
                    ->join('shoppingcart','shoppingcart.shoppingcart_id', '=', 'conforder.shoppingcart_id')
                    ->join('shoppinproduct', 'shoppingcart.shoppingcart_id', 'shoppinproduct.shoppingcart_id', '=')
                    ->select('conforder.conforder_id', 'conforder.pos_entry_date','conforder.conforder_status','conforder.pos_entry_date','conforder.conforder_deliverydate','shoppingcart.shoppingcart_subtotal', 'conforder.conforder_lastupdte','conforder.order_threepldlv','conforder.conforder_placed_date', 'conforder.conforder_tracknumber', 'registeruser.registeruser_firstname', 'registeruser.registeruser_lastname','shoppinproduct.Shipping_Charge','shoppingcart.get_offer', 'shoppingcart.shoppingcart_subtotal', 'shoppingcart.shoppingcart_total', 'shoppingcart.shoppingcart_total')
                    ->where('shoppingcart.get_offer', 1)
                    ->orderBy('conforder.conforder_id', 'desc')
                    ->groupBy('conforder.conforder_id')
                    ->get();
                }else{
                $all_order_info = DB::table('conforder')
                ->join('registeruser', 'conforder.registeruser_id', '=', 'registeruser.registeruser_id')
                ->join('shoppingcart','shoppingcart.shoppingcart_id', '=', 'conforder.shoppingcart_id')
                ->join('shoppinproduct', 'shoppingcart.shoppingcart_id', 'shoppinproduct.shoppingcart_id', '=')
                ->select('conforder.conforder_id', 'conforder.pos_entry_date','conforder.conforder_status','conforder.pos_entry_date','conforder.conforder_deliverydate','conforder.conforder_lastupdte','shoppingcart.shoppingcart_subtotal', 'conforder.conforder_lastupdte','conforder.order_threepldlv','conforder.conforder_placed_date', 'conforder.conforder_tracknumber', 'registeruser.registeruser_firstname', 'registeruser.registeruser_lastname','shoppinproduct.Shipping_Charge','shoppingcart.get_offer', 'shoppingcart.shoppingcart_subtotal', 'shoppingcart.shoppingcart_total', 'shoppingcart.shoppingcart_total')
                ->orderBy('conforder.conforder_id', 'desc')
                ->groupBy('conforder.conforder_id')
                ->get();
                }
        $data['form_date']=$request->form_date;;
        $data['to_date']=$request->to_date;
        $data['tracking_number']=$tracking_number;
        $data['order_status']=$order_status;
        $data['all_order_info'] = $all_order_info;
        $data['get_offer'] = $get_offer;
        return view('admin.all_order_report_list', $data);
    }
    
    public function PdfReport(Request $request){
        if(!empty($request->data_table)){
            $data = $request->data_table;
            $fname = Carbon::now()."_report.pdf"; // name the file
            $file = fopen("storage/app/public/report/" .$fname, 'w'); // open the file path
            fwrite($file, $data); //save data
            fclose($file);
            $email_data['report_file']="storage/app/public/report/" .$fname;
             Mail::send('emails.pos', $email_data, function($message) use($email_data) {
                $fname = Carbon::now()."_report.pdf";
                $report_file="storage/app/public/report/" .$fname;
                $message->to('inzamamul_dsg@pride-grp.com', 'Inzamamul Karim');
                $message->cc('ikshimuluits@gmail.com', 'Inzamamul Karim');
                $message->subject('Online pos entry information');
                $message->attach($report_file);
            });
             DB::select(DB::raw("UPDATE `conforder` SET `mail_status` = '1' WHERE conforder_lastupdte >= CURDATE()"));
        } else {
            echo "No Data Sent";
        }
    }
    
    public function TodayReport(Request $request){
        $form=Input::get('form_date');
        $to=Input::get('to_date');
        if($form==null){
           $form = date('Y-m-d'); 
        }else{
           $form= $form;
        }
        if($to==null){
            $to = date('Y-m-d'); 
        }else{
            $to=$to;
        }
        $form = $form.' '.'00:00:00';
        $to= $to.' '.'23:59:00';
        $all_order_info = DB::table('conforder')
        ->join('registeruser', 'conforder.registeruser_id', '=', 'registeruser.registeruser_id')
        ->join('shoppingcart','shoppingcart.shoppingcart_id', '=', 'conforder.shoppingcart_id')
        ->join('shoppinproduct','shoppinproduct.shoppingcart_id','=','conforder.shoppingcart_id')
        ->join('product','product.product_id','=','shoppinproduct.product_id')
        ->select('conforder.conforder_id', 'conforder.conforder_status','shoppingcart.shoppingcart_subtotal', 'conforder.conforder_lastupdte','product.product_name','product.product_barcode','shoppinproduct.productalbum_name','shoppinproduct.prosize_name','shoppinproduct.shoppinproduct_quantity','shoppinproduct.product_price','conforder.order_threepldlv','conforder.conforder_placed_date', 'conforder.conforder_tracknumber', 'registeruser.registeruser_firstname', 'registeruser.registeruser_lastname')
        ->whereBetween('conforder.conforder_lastupdte', [$form, $to])
        ->where('conforder.conforder_status', 'Closed')
        ->where('conforder.mail_status',0)
        ->groupBy('shoppinproduct.shoppinproduct_id')
        ->orderBy('conforder.conforder_id', 'desc')
        ->get();
        $data['form_date']=Input::get('form_date');
        $data['to_date']=Input::get('to_date');
        $data['all_order_info'] = $all_order_info;
        //dd($data);
        return view('admin.today_order_report_list', $data);
    }
    
    public function SalesPdfReport(Request $request){
        if(!empty($request->data_table)){
            $data = $request->data_table;
            $fname = Carbon::now()."_report.pdf"; // name the file
            $file = fopen("storage/app/public/sales-report/" .$fname, 'w'); // open the file path
            fwrite($file, $data); //save data
            fclose($file);
            $headers = array(
              'Content-Type: application/pdf',
            );
            echo 'done';
            return Response::download($file, 'filename.pdf', $headers);
        } else {
            echo "No Data available";
        }
    }
}
