<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Validator;
use Image;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Session;
use Carbon\Carbon;

class ItemDefineController extends Controller {
    
    public function __construct() {
        $this->middleware('AdminAuth');
    }

    public static function GetSizeBarcode($style_code, $color) {
        $stock = DB::table('item_define')->where('DesignRef', $style_code)->where('color', $color)->where('add_status', 0)->get();
        return $stock;
    }

    public function ShowPosItemList() {
        $data['item_list'] = DB::connection('sqlsrv')
                ->table('itemdefination')
                ->join('itemeffectiverate', 'itemdefination.barcode', '=', 'itemeffectiverate.barcode')
                ->join('itemwisestock', 'itemdefination.barcode', '=', 'itemwisestock.barcode')
                ->where('itemdefination.Size', '!=', null)
                ->where('itemdefination.Color', '!=', null)
                ->where('itemdefination.Brand', 'Pride Limited')
                ->get();
        // dd($unique_style);
        return view('admin.pos.item-list', $data);
    }

    public function AddPosItemList() {
        $item_list = DB::connection('sqlsrv')
                ->table('itemdefination')
                ->join('itemeffectiverate', 'itemdefination.barcode', '=', 'itemeffectiverate.barcode')
                ->join('itemwisestock', 'itemdefination.barcode', '=', 'itemwisestock.barcode')
                ->where('itemdefination.Size', '!=', null)
                ->where('itemdefination.Color', '!=', null)
               ->where('itemdefination.Brand', 'Pride Limited')
                //->where('itemdefination.DesignRef','PG-BL009SD2')
                ->get();
     //   dd($item_list);
        foreach ($item_list as $item) {
            $check_duplicate = DB::table('item_define')->where('Barcode', $item->Barcode)->where('DesignRef', $item->DesignRef)->where('Color', $item->Color)->where('Size', $item->Size)->first();
            if ($check_duplicate === null) {
                $data['Barcode'] = $item->Barcode;
                $data['ItemMasterID'] = $item->ItemMasterID;
                $data['ItemDescription'] = $item->ItemDescription;
                $data['Category'] = $item->Category;
                $data['subcategory'] = $item->subcategory;
                $data['itemGroup'] = $item->itemGroup;
                $data['DesignRef'] = $item->DesignRef;
                $data['Department'] = $item->Department;
                $data['Color'] = $item->Color;
                $data['Size'] = $item->Size;
                $data['SellRate'] = $item->SellRate;
                $data['CurrentStock'] = $item->CurrentStock;
                $data['Brand'] = $item->Brand;
                DB::table('item_define')->insert($data);
            }
            $add_check = DB::table('product')->where('product_styleref', $item->DesignRef)->first();
            if ($add_check !== null) {
                $check=DB::table('productsize')->where('product_id', $add_check->product_id)->first();
				if($check){
                $product_barcodes = DB::table('productsize')->where('product_id', $add_check->product_id)->where('color_name', $item->Color)->where('barcode','!=','')->pluck('barcode')->toArray();
                if (count($product_barcodes) > 0 && !in_array($item->Barcode, $product_barcodes)) {
                    $added_stock['product_id'] = $add_check->product_id;
                    $added_stock['barcode'] = $item->Barcode;
                    $added_stock['productsize_size'] = $item->Size;
                    $added_stock['color_name'] = $item->Color;
                    $added_stock['SizeWiseQty'] = $item->CurrentStock;
                 //   DB::table('productsize')->insert($added_stock);
                    //Update status
                    $add_status_update['add_status'] = 1;
                    DB::table('item_define')->where('Barcode', $item->Barcode)->update($add_status_update);
                 }
				}else{
				$added_stock['product_id'] = $add_check->product_id;
				$added_stock['barcode'] = $item->Barcode;
				$added_stock['productsize_size'] = $item->Size;
				$added_stock['color_name'] = $item->Color;
				$added_stock['SizeWiseQty'] = $item->CurrentStock;
				DB::table('productsize')->insert($added_stock);
				//Update status
				$add_status_update['add_status'] = 1;
				DB::table('item_define')->where('Barcode', $item->Barcode)->update($add_status_update);
			  } 
                
            }
            $added_update['add_status'] = 1;
         //   DB::table('item_define')->where('Barcode', $item->Barcode)->update($added_update);
           // $data_stock['SizeWiseQty'] = $item->CurrentStock;
          //  DB::table('productsize')->where('barcode', $item->Barcode)->where('productsize_size',$item->Size)->update($data_stock);
		     $data_stock['CurrentStock'] = $item->CurrentStock;
          //  DB::table('productsize')->where('barcode', $item->Barcode)->where('productsize_size',$item->Size)->update($data_stock);
            DB::table('item_define')->where('Barcode', $item->Barcode)->update($data_stock);;
            $price_update['product_price'] = $item->SellRate;
         //   DB::table('product')->where('product_styleref', $item->DesignRef)->update($price_update);
        }
        echo "all data sync";
    }

    public function StockUpdate($product_id, $style_code, $color) {
        $already_product_sizes = DB::table('productsize')->where('product_id', $product_id)->where('color_name', $color)->get();
        $total_product_sizes = DB::table('item_define')->where('DesignRef', $style_code)->where('Color', $color)->get();
        foreach ($total_product_sizes as $total_s) {
            foreach ($already_product_sizes as $exits) {
                if ($exits->barcode != '' && $total_s->Barcode != $exits->barcode && $total_s->size != $exits->productsize_size) {
                    echo $product_id . '-' . $color . '-' . $exits->barcode;
                    echo $product_id . '-' . $color . '-' . $total_s->barcode;
                    exit();
                }
            }
        }
    }

    public function AddProductByPos() {
        $stylecode = Input::get('stylecode');
        $data['procat_list'] = DB::table('procat')->get();
        $data['total_product'] = DB::table('product')->count();
        $data['unique_style'] = DB::table('item_define')->where('add_status', 0)->groupBy('DesignRef')->get();
        $data['item_define'] = DB::table('item_define')->where('DesignRef', $stylecode)->first();
        $data['item_color'] = DB::table('item_define')->where('DesignRef', $stylecode)->where('add_status', 0)->groupBY('color')->get();
        $data['stylecode'] = $stylecode;
        $stye_check = DB::table('product')
		->join('procat','product.procat_id','=','procat.procat_id')
		->join('subprocat','product.subprocat_id','=','subprocat.subprocat_id')
		->select('procat.procat_name','subprocat.subprocat_name','product.product_name','product.product_price','product.discount_product_price','product.product_description','product.product_img_thm')
		->where('product_styleref', $stylecode)
		->where('product_active_deactive', 0)
		->first();
		if($stye_check){
			$data['old_item']=$stye_check;
		}else{
			$data['old_item']=null;
		}
        return view('admin.pos.item-add', $data);
    }

    public function ProductSave(Request $request) {
        $totalnumberofinserteddata = 0;
        $numberofboxedchecked = 0;
        $current_date = date("d-m-Y");
        $created_at = Carbon::now();
        $style_code = $request->txtstyleref;
        $stye_check = DB::table('product')->where('product_styleref', $style_code)->where('product_active_deactive', 0)->first();
        if ($stye_check) {
            $product_id = $stye_check->product_id;
        } else {
            $vaildation = Validator::make($request->all(), [
                        'txtproductname' => 'required|min:4',
                        'txtprice' => 'required',
                            // 'txtstyleref' => 'required|unique:product,product_styleref',
            ]);
            if ($vaildation->fails()) {
                return redirect()->back()->withErrors($vaildation)->withInput();
            } else {
                $discount = $request->txtpricediscounted;
                $original_price = $request->txtprice;
                if ($discount >= 1) {
                    $discount_price = ($discount / 100) * $original_price;
                    //  $discount_price = $original_price - $discount;
                    //  $discount_price = ($discount_price * 100) / $original_price;
                    $price_with_discount = $original_price - $discount_price;
                } else {
                    $discount_price = 0;
                    $price_with_discount = 0;
                }
                if ($request->campaign) {
                    $specialcollection = $request->campaign;
                } else {
                    $specialcollection = 0;
                }
                //category set
                if ($request->main_category == 'Women') {
                    $procat_id = 5;
                } elseif ($request->main_category == 'Men') {
                    $procat_id = 4;
                } else {
                    $procat_id = $request->main_category;
                }
                $get_sub = DB::table('subprocat')->where('procat_id', $procat_id)->where('subprocat_name', $request->sub_category)->first();
                if ($get_sub) {
                    $subprocat_id = $get_sub->subprocat_id;
                } else {
                    $subprocat_id = $request->sub_category;
                }
                $data['procat_id'] = $request->ddlprocat;
                $data['subprocat_id'] =  $request->ddlprosubcat1;
                $data['product_name'] = $request->txtproductname;
                $data['fabric'] = $request->fabric;
                $data['product_price'] = $request->txtprice;
                $data['discount_product_price'] = $request->txtpricediscounted;
                $data['product_pricediscounted'] = $price_with_discount;
                $data['product_pricefilter'] = $request->ddlfilter;
                $data['product_insertion_date'] = $current_date;
                $data['product_code'] = $request->txtstyleref;
                $data['product_styleref'] = $request->txtstyleref;
                // $data['product_barcode'] = $request->product_barcode;
                $data['product_order'] = $request->txtorder;
                $data['product_description'] = $request->txtproductdetails;
                $data['product_care'] = $request->txtproductcare;
                $data['created_by'] = session('pride_admin_id');
                $data['created_at'] = $created_at;
               // $data['insertion_login_ip'] = request()->ip();
                $data['isSpecial'] = $specialcollection;
                $product_id = DB::table('product')->insertGetId($data);
                $sold['sold'] = $product_id;
                DB::table('product')->where('product_id', $product_id)->update($sold);
                if ($product_id) {
                    if ($request->hasFile('sizeguide_image')) {
                        $file = $request->file('sizeguide_image');
                        $savename = time() . '_' . $file->getClientOriginalName();
                        //  Storage::put('productbanner/subcategory/' . $savename, file_get_contents($request->file('sizeguide_image')->getRealPath()));
                        $image = Image::make($request->file('sizeguide_image'))->resize(255, 383)->insert('assets/image/logo.png')->save('storage/app/public/prothm/' . $savename);
                        $image_data = [
                            'product_sizeguide_img' => $product_id . '_sizeguide_' . $savename
                        ];
                        DB::table('product')->where('product_id', $product_id)->update($image_data);
                    } else {
                        
                    }
                }
            }
        }
        $total_grp = $request->total_grp;
        for ($loopcountermain = 1; $loopcountermain <= $total_grp; $loopcountermain++) {
            $txtcolorvalname = "txtcolorname_" . $loopcountermain;
            $color_name_post = "txtcolorname_" . $loopcountermain;
            $txtcolovalue = $request->$color_name_post;
            $colorfilethm = "file_colorthm_" . $loopcountermain;
            //check click or not
            if ($request->hasFile($colorfilethm)) {
                $totalnumberofavailabesize = session('numberofavailabesize');
                for ($loopcountersize = 1; $loopcountersize <= $totalnumberofavailabesize; $loopcountersize++) {
                    $checkboxvarname = "size" . $loopcountersize . "_" . $loopcountermain;
                    $checkboxvarvalename = "input_size" . $loopcountersize . "_" . $loopcountermain;
                    $checkboxvarvalename_barcode = "barcode_input_size" . $loopcountersize . "_" . $loopcountermain;
                    if ($request->$checkboxvarname == TRUE) {
                        $productsizaname = $request->$checkboxvarname;
                        $productsizquantity = $request->$checkboxvarvalename;
                        $barcode = $request->$checkboxvarvalename_barcode;
                        $numberofboxedchecked++;
                        if ($productsizquantity > 0) {
                            
                        } else {
                            $productsizquantity = 0;
                        }
                        $product_size['product_id'] = $product_id;
                        $product_size['barcode'] = $barcode;
                        $product_size['productsize_size'] = $productsizaname;
                        $product_size['SizeWiseQty'] = $productsizquantity;
                        $product_size['color_name'] = $txtcolovalue;
                        $productsize_id = DB::table('productsize')->insertGetId($product_size);
                        $add_status_update['add_status'] = 1;
                        DB::table('item_define')->where('Barcode', $barcode)->update($add_status_update);
                        if ($productsize_id) {
                            $totalnumberofinserteddata++;
                        } else {
                            
                        }
                    } else {
                        
                    }
                }
                if ($totalnumberofinserteddata == $numberofboxedchecked) {
                    $productalbum['product_id'] = $product_id;
                    $productalbum['productalbum_name'] = $txtcolovalue;
                    $productalbum['ProductSizeId'] = 0;
                    $productalbum['productalbum_order'] = $loopcountermain;
                    $productalbum_id = DB::table('productalbum')->insertGetId($productalbum);
                    if ($productalbum_id) {
                        if ($request->hasFile($colorfilethm)) {
                            $file = $request->file($colorfilethm);
                            $savename = $productalbum_id . '_album_thm_' . $file->getClientOriginalName();
                            $image = Image::make($request->file($colorfilethm))->save('storage/app/public/pgallery/' . $savename);
                            if ($image) {
                                $productablumimage['productalbum_img'] = $savename;
                                DB::table('productalbum')->where('productalbum_id', $productalbum_id)->update($productablumimage);
                                for ($filecounter = 1; $filecounter <= 6; $filecounter++) {
                                    $imageid = NULL;
                                    $imgfieldname = "file_im" . $filecounter . "_" . $loopcountermain;
                                    if ($request->hasFile($imgfieldname)) {
                                        $file = $request->file($imgfieldname);
                                        $filename = $product_id . "_product_image_" . $filecounter . "_" . $file->getClientOriginalName();
                                        $image = Image::make($request->file($imgfieldname))->save('storage/app/public/pgallery/' . $filename);
                                        $productimg['productimg_title'] = '';
                                        $productimg['productalbum_id'] = $productalbum_id;
                                        $productimg['productimg_order'] = '';
                                        $productimg_id = DB::table('productimg')->insertGetId($productimg);
                                        if ($image) {
                                            $width = Image::make($request->file($imgfieldname))->width();
                                            $height = Image::make($request->file($imgfieldname))->height();
                                            if ($width >= 1590 && $width <= 1610 && $height >= 2390 && $height <= 2410) {
                                                //upload thm file
                                                $filenamethmb = $product_id . "_product_image_" . $filecounter . "_thm_" . $file->getClientOriginalName();
                                                $image = Image::make($request->file($imgfieldname))->resize(200, 300)->save('storage/app/public/pgallery/' . $filenamethmb);
                                                //upload tiny file
                                                $filenametiny = $product_id . "_product_image_" . $filecounter . "_tiny_" . $file->getClientOriginalName();
                                                $image = Image::make($request->file($imgfieldname))->resize(64, 96)->save('storage/app/public/pgallery/' . $filenametiny);
                                                //upload medium file
                                                $filenamemedium = $product_id . "_product_image_" . $filecounter . "_medium_" . $file->getClientOriginalName();
                                                $image = Image::make($request->file($imgfieldname))->resize(370, 555)->save('storage/app/public/pgallery/' . $filenamemedium);
                                                $productimg_update['productimg_img'] = $filename;
                                                $productimg_update['productimg_img_tiny'] = $filenametiny;
                                                $productimg_update['productimg_img_medium'] = $filenamemedium;
                                                $productimg_update['productimg_img_thm'] = $filenamethmb;
                                                $productimg_update['productimg_order'] = $filecounter;
                                                $productimg_query = DB::table('productimg')->where('productimg_id', $productimg_id)->update($productimg_update);
                                                if ($filecounter == 1 && $loopcountermain == 1) {
                                                    $product_update['product_img_thm'] = $filename;
                                                    DB::table('product')->where('product_id', $product_id)->update($product_update);
                                                } else {
                                                    
                                                }
                                            } else {
                                                $get_productimg = DB::table('productimg')
														->where('productimg_id', $productimg_id)
														->first();
												$product_tiny_image = $get_productimg->productimg_img_tiny;
												$product_medium_image = $get_productimg->productimg_img_medium;
												$productimg_img_thm = $get_productimg->productimg_img_thm;
												$productimg_img = $get_productimg->productimg_img;
												Storage::delete(public_path()."/pgallery/$product_tiny_image");
												Storage::delete(public_path()."/pgallery/$product_medium_image");
												Storage::delete(public_path()."/pgallery/$productimg_img_thm");
												Storage::delete(public_path()."/pgallery/$productimg_img");
												$result = DB::table('productimg')->where('productimg_id', $productimg_id)->delete();
                                                DB::table('productalbum')->where('productalbum_id', $productalbum_id)->delete();
												$get_barcode=DB::table('productsize')->where('product_id', $product_id)->where('color_name', $txtcolovalue)->get();
												foreach($get_barcode as $barcode){
												   $item_define['add_status'] = 0;
												   DB::table('item_define')->where('Barcode', $barcode->barcode)->update($item_define);
												}
												DB::table('productsize')->where('product_id', $product_id)->where('color_name', $txtcolovalue)->delete();
												if ($stye_check) {
													
												}else{
                                                DB::table('product')->where('product_id', $product_id)->delete();
												}
                                                // DB::table('product')->where('product_id',$product_id)->delete();
                                                return redirect()->back()->with('error', 'upload size must be greater than or equal 1600*2400');
                                            }
                                        }
                                    }
                                }
                            } else {
                                
                            }
                        } else {
                            
                        }
                    } else {
                        
                    }
                } else {
                    
                }
            }
        }
        return redirect()->back()->with('save', 'Product Upload Successfully!');
    }

}
