<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ReportController extends Controller
{
    public static function getOfferFlag($shopping_cart_id) {
        $offer=DB::table('shoppingcart')
        ->select('get_offer')
        ->where('shoppingcart_id',$shopping_cart_id)
        ->first();
        return $offer;
        
    }
   public function saleReport(Request $request) {
        $query_scope = DB::table('conforder')
                ->join('shoppingcart', 'conforder.shoppingcart_id', '=', 'shoppingcart.shoppingcart_id')
                ->join('registeruser', 'conforder.registeruser_id', '=', 'registeruser.registeruser_id')
                ->join('shoppinproduct', 'shoppingcart.shoppingcart_id', 'shoppinproduct.shoppingcart_id', '=')
                ->leftJoin('cash_expenses', 'conforder.conforder_id', '=', 'cash_expenses.conforder_id')
                ->select('conforder.conforder_id','conforder.conforder_tracknumber', 'conforder.conforder_deliverydate', 'conforder.conforder_status', 'conforder.conforder_placed_date','conforder.pos_entry_date','conforder.order_threepldlv', 'registeruser.registeruser_firstname', 'registeruser_lastname', 'shoppinproduct.Shipping_Charge','shoppingcart.get_offer', 'shoppingcart.shoppingcart_subtotal', 'shoppingcart.shoppingcart_total', 'cash_expenses.amount');
               // ->where('conforder.conforder_status','=', 'Order_Verification_Pending')
              //  ->where('conforder.conforder_status','=', 'Pending_Dispatch');
        $order_status = 'Order_Verification_Pending';
        if(isset($request->order_status)) {
            $order_status = $request->order_status == ''?'%%':$request->order_status;
        }
        if(isset($request->from) && isset($request->to) && !empty($request->from) && !empty($request->to) && empty($request->ddlthreepldlv)) {
            $from = $request->from;
            $to = $request->to;
            $order_threepldlv='';
            $sale_reports = $query_scope
                ->where('conforder_status', 'like', $order_status)
                ->whereBetween('conforder.conforder_placed_date', array($from, $to))
                ->orderBy('conforder.conforder_id', 'desc')
                ->groupBy('conforder.conforder_id')
                ->get();
            return view('admin.sale_report', compact('sale_reports', 'from', 'to','order_threepldlv', 'order_status'));
        }elseif(isset($request->from) && isset($request->to) && isset($request->ddlthreepldlv) && !empty($request->from) && !empty($request->to) && !empty($request->ddlthreepldlv)){
            $from = $request->from;
            $to = $request->to;
            $order_threepldlv=$request->ddlthreepldlv;
            $sale_reports = $query_scope
                ->where('conforder_status', 'like', $order_status)
                ->where('conforder.order_threepldlv', 'like', $order_threepldlv)
                ->whereBetween('conforder.conforder_placed_date', array($from, $to))
                ->orderBy('conforder.conforder_id', 'desc')
                ->groupBy('conforder.conforder_id')
                ->get();
            return view('admin.sale_report', compact('sale_reports', 'from', 'to','order_threepldlv', 'order_status'));
        } elseif(isset($request->from) && !empty($request->from)) {
            $from = $request->from;
            $order_threepldlv='';
            $sale_reports = $query_scope
                ->where('conforder_status', 'like', $order_status)
                ->where('conforder.conforder_placed_date', '>=', $from)
                ->orderBy('conforder.conforder_id', 'desc')
                ->groupBy('conforder.conforder_id')
                ->get();
            return view('admin.sale_report', compact('sale_reports', 'from', 'to','order_threepldlv', 'order_status'));
        }elseif(isset($request->ddlthreepldlv) && !empty($request->ddlthreepldlv)){
             $order_threepldlv=$request->ddlthreepldlv;
             $sale_reports = $query_scope
                ->where('conforder_status', 'like', $order_status)
                ->where('conforder.order_threepldlv', 'like', $order_threepldlv)
                ->orderBy('conforder.conforder_id', 'desc')
                ->groupBy('conforder.conforder_id')
                ->get();
            return view('admin.sale_report', compact('sale_reports','order_threepldlv','order_status'));
        } else {
            $order_threepldlv='';
            $sale_reports = $query_scope
                ->where('conforder_status', 'like', $order_status)
                ->orderBy('conforder.conforder_id', 'desc')
                ->groupBy('conforder.conforder_id')
                ->get();
          //  dd($sale_reports);
            return view('admin.sale_report', compact('sale_reports','order_threepldlv', 'order_status'));
        }
        
        
    }
}
