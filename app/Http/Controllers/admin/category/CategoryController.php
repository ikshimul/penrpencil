<?php

namespace App\Http\Controllers\admin\category;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Storage;
use App\procat;

class CategoryController extends Controller {

    public function __construct() {
        $this->middleware('AdminAuth');
    }

    //
    public function ManageCategory() {
        $main_category_list = DB::table('procat')
                ->join('employe', 'procat.employe_id', '=', 'employe.employe_id')
                ->select('procat.procat_id', 'procat.procat_name', 'procat.procat_icon', 'procat.procat_lastupdate', 'employe.employe_name')
                ->orderBy('procat_id', 'ASC')
                ->where('status', 0)
                ->get();
        $data['main_category'] = $main_category_list;
        return view('admin.category.manage_category', $data);
    }

    public function AddProCat(Request $request) {
        //  dd($request);
        $pro_cat = New procat();
        if ($request->hasFile('filename')) {
            $file = $request->file('filename');
            $originalimage = time() . '_' . $file->getClientOriginalName();
            Storage::put('/public/category_banner/' . $originalimage, file_get_contents($request->file('filename')->getRealPath()));
            $pro_cat->procat_name = $request->procat_name;
            $pro_cat->procat_order = $request->procat_order;
            $pro_cat->procat_icon = $request->procat_icon;
            $pro_cat->employe_id = 2;
            $pro_cat->procat_banner = $originalimage;
            $result = $pro_cat->save();

            if ($result) {
                return redirect()->back()->with('update', 'Procat added successfully');
            } else {
                return redirect()->back()->with('error', 'Procat not update');
            }
        } else {
            $pro_cat->procat_name = $request->procat_name;
            $pro_cat->procat_order = $request->procat_order;
            $pro_cat->procat_icon = $request->procat_icon;
            $pro_cat->employe_id = 2;
            $result = $pro_cat->save();
            if ($result) {
                return redirect()->back()->with('update', 'Procat added successfully');
            } else {
                return redirect()->back()->with('error', 'Procat not update');
            }
        }
    }

    public function EditProcat($procat_id) {
        $procat = DB::table('procat')->where('procat_id', $procat_id)->first();
        $data['procat'] = $procat;
        return view('admin.category.edit_procat', $data);
    }

    public function UpdateProcat(Request $request) {
        //  dd($request);
        if ($request->hasFile('filebanner')) {
            $file = $request->file('filebanner');
            $originalimage = time() . '_' . $file->getClientOriginalName();
            Storage::put('productbanner/' . $originalimage, file_get_contents($request->file('filebanner')->getRealPath()));
            $updatedata = [
                'procat_name' => $request->procat_name,
                'procat_banner' => $originalimage,
                'employe_id' => 2
            ];
            $result = DB::table('procat')->where('procat_id', $request->procat_id)->update($updatedata);
            if ($result) {
                return redirect()->back()->with('update', 'Procat update successfully');
            } else {
                return redirect()->back()->with('error', 'Procat not update');
            }
        } else {
            $updatedata = [
                'procat_name' => $request->procat_name,
                'employe_id' => 2
            ];
            $result = DB::table('procat')->where('procat_id', $request->procat_id)->update($updatedata);
            if ($result) {
                return redirect()->back()->with('update', 'Procat update successfully');
            } else {
                return redirect()->back()->with('error', 'Procat not update');
            }
        }
    }

    public function deleteProcat($id) {
        DB::table('procat')->where('procat_id', $id)->update(['status' => 1]);
        return redirect()->back()->with('error', 'Category deleted successfully.');
    }

    public function AddSubCat() {
        $main_category = DB::table('procat')->get();
        $data['main_category'] = $main_category;
        return view('admin.category.add_subcat', $data);
    }

    public function SaveSubPro(Request $request) {

        //    $file = $request->file('filename');
        //   $savename = time() . '_' . $file->getClientOriginalName();
        //   Storage::put('productbanner/subcategory/' . $savename, file_get_contents($request->file('filename')->getRealPath()));
        $data = [
            'procat_id' => $request->procat_id,
            'subprocat_name' => $request->txtsubcategoryname,
            'subprocat_order' => $request->txtorder,
                // 'subprocat_img' => $savename
        ];
        $result = DB::table('subprocat')->insert($data);
        if ($result) {
            return redirect('/add-subcat')->with('save', 'Subcate insert successfully');
        } else {
            return redirect('/add-subcat')->with('error', 'data not inserted');
        }
    }

    public function ManageSubPro() {
        $main_category = DB::table('procat')->get();
        $sub_pro = DB::table('subprocat')
                ->join('procat', 'procat.procat_id', '=', 'subprocat.procat_id')
                ->select('subprocat.subprocat_id','subprocat.subprocat_name', 'subprocat.subprocat_order', 'subprocat.subprocat_img', 'procat.procat_id', 'subprocat.procat_id', 'procat.procat_name')
                ->where('subprocat.status',0)
                ->get();
        $data['main_category'] = $main_category;
        $data['sub_category'] = $sub_pro;
        return view('admin.category.manage_subcat', $data);
    }

    public function deleteSubPro($id) {
        DB::table('subprocat')->where('subprocat_id', $id)->update(['status' => 1]);
        return redirect()->back()->with('error', 'Sub category deleted successfully.');
    }

    public function GetProWiseSubpro($id) {
        $subpro = DB::table('subprocat')
                ->join('procat', 'procat.procat_id', '=', 'subprocat.procat_id')
                ->select('subprocat.subprocat_id', 'subprocat.subprocat_name', 'subprocat.subprocat_order', 'subprocat.subprocat_img', 'procat.procat_id', 'subprocat.procat_id', 'procat.procat_name')
                ->where('subprocat.procat_id', $id)
                ->get();
        return json_encode($subpro);
    }
	
	public function GetProWiseSizes($procat_id){
		$data['sizes']=DB::table('prosize')->where('procat_id',$procat_id)->get();
		return view('admin.product.size_list',$data);
	}

}
