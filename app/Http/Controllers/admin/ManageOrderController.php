<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Conforder;
use App\Registeruser;
use App\Productsize;
use App\Ordershipping;
use App\Shoppinproduct;
use App\Shoppingcart;
use DB;
use App\Rider;
use Carbon\Carbon;
use Mail;
use App\ExtraDeliveryCharge;

class ManageOrderController extends Controller {

    public function __construct() {
        $this->middleware('AdminAuth');
		date_default_timezone_set("Asia/Dhaka");
    }
    
    public static function getNewPhoneRequest(){
       $request_numbers= DB::table('phnumber')->where('view',0)->where('phone_status',1)->count();
       return $request_numbers;
     }
     
      public static function getNewPhoneRequestList(){
       $request_numbers_list= DB::table('phnumber')->where('view',0)->where('phone_status',1)->get();
       return $request_numbers_list;
     }

    public function manageIncompleteOrder() {
        $total_incomplete_order_info = DB::table('conforder')
                 ->join('ordershipping', 'conforder.conforder_id', '=', 'ordershipping.conforder_id')
                 ->leftJoin('riders','conforder.delivery_by','=','riders.id')
                ->select('conforder.conforder_id', 'conforder.conforder_status', 'conforder.conforder_lastupdte','conforder.conforder_placed_date','conforder.pos_entry_date','conforder.created_at','conforder.delivery_by','conforder.conforder_deliverydate', 'conforder.conforder_tracknumber', 'ordershipping.Shipping_txtfirstname', 'ordershipping.Shipping_txtlastname','riders.name')
                ->where('conforder_completed', '=', 0)
                ->where('conforder_status', '!=', 'Closed')
                ->where('conforder_status', '!=', 'Cancelled')
                ->where('conforder_status', '!=', 'Exchanged')
                ->where('conforder_status', '!=', 'Invalidate')
                ->where('conforder_status', '!=', 'Returned')
                ->orderBy('conforder.conforder_id', 'desc')
                ->get();
        $data['total_incomplete_order_info'] = $total_incomplete_order_info;
        return view('admin.order_management.manage-incomplete-order', $data);
    }
    
    public function manageAllOrder() {
        $all_order_info = DB::table('conforder')
                ->join('ordershipping', 'conforder.conforder_id', '=', 'ordershipping.conforder_id')
                ->leftJoin('riders','conforder.delivery_by','=','riders.id')
                ->select('conforder.conforder_id', 'conforder.conforder_status', 'conforder.conforder_lastupdte','conforder.conforder_placed_date','conforder.pos_entry_date','conforder.delivery_by','conforder.conforder_deliverydate', 'conforder.conforder_tracknumber', 'ordershipping.Shipping_txtfirstname', 'ordershipping.Shipping_txtlastname','riders.name')
                ->orderBy('conforder.conforder_id', 'desc')
                ->groupBy('conforder.conforder_id')
                ->get();
        $data['all_order_info'] = $all_order_info;
        return view('admin.order_management.manage-all-order', $data);
    }

    public function OrderFilteringView() {
       $all_order_info = DB::table('conforder')
                ->join('registeruser', 'conforder.registeruser_id', '=', 'registeruser.registeruser_id')
                ->join('shoppingcart','shoppingcart.shoppingcart_id', '=', 'conforder.shoppingcart_id')
                ->join('shoppinproduct', 'shoppingcart.shoppingcart_id', 'shoppinproduct.shoppingcart_id', '=')
                ->select('conforder.conforder_id', 'conforder.conforder_status','conforder.pos_entry_date','conforder.conforder_deliverydate','conforder.conforder_lastupdte','conforder.pos_entry_date','shoppingcart.shoppingcart_subtotal', 'conforder.conforder_lastupdte','conforder.order_threepldlv','conforder.conforder_placed_date', 'conforder.conforder_tracknumber', 'registeruser.registeruser_firstname', 'registeruser.registeruser_lastname','shoppinproduct.Shipping_Charge','shoppingcart.get_offer', 'shoppingcart.shoppingcart_subtotal', 'shoppingcart.shoppingcart_total', 'shoppingcart.shoppingcart_total')
                ->orderBy('conforder.conforder_id', 'desc')
                ->groupBy('conforder.conforder_id')
                ->get();
        $data['form_date']='';
        $data['to_date']='';
        $data['tracking_number']='';
        $data['all_order_info'] = $all_order_info;
        return view('admin.manage-all-order', $data);
    }

    public static function getTotlaIncomplte_order() {
        $total_incomplete_order = DB::table('conforder')
                ->where('conforder_completed', 0)
                ->where('conforder_status', '!=', 'Closed')
                ->where('conforder_status', '!=', 'Cancelled')
                ->where('conforder_status', '!=', 'Exchanged')
                ->where('conforder_status', '!=', 'Invalidate')
                ->where('conforder_status', '!=', 'Returned')
                ->count();
        return $total_incomplete_order;
    }

    public function OrderDetails($order_id) {
        $data['shipping_address_details'] = $this->getShippingAddressDetails($order_id);
        $data['total_incomplete_order_info'] = $this->getTotalOrderInfo($this->getShippingAddressDetails($order_id)->shoppingcart_id);
        $data['extra_delivery_charge'] = ExtraDeliveryCharge::where('conforder_id', $order_id)->first();
        $data['riders'] = Rider::all()->where('status',0);
        $data['conforder'] = DB::table('conforder')
            ->join('cash_expenses', 'conforder.conforder_id', '=', 'cash_expenses.conforder_id')
            ->select('conforder.delivery_by', 'cash_expenses.amount')
            ->where('conforder.conforder_id', $order_id)
            ->first();
        $data['delivery_by'] = DB::table('conforder')
            ->select('delivery_by')
            ->where('conforder_id', $order_id)
            ->first();
        $data['order_id'] = $order_id;
        //$data['package_list']=$this->eCourierPackageList();
		$data['package_list']=DB::table('ecourier_packages')->where('status',0)->get();
		/*foreach($data['package_list'] as $package){
			$p_data['package_code']=$package->package_code;
			$p_data['package_name']=$package->package_name;
			DB::table('ecourier_packages')->insert($p_data);
		} */
        return view('admin.order-details', $data);
    }
    
    public function eCourierPackageList(){
        $curl = curl_init();
        $packagelist = [
                'parcel' => 'packagelist',
            ];
		curl_setopt_array($curl, array(
                CURLOPT_URL => "https://ecourier.com.bd/apiv2/",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $packagelist,
                CURLOPT_HTTPHEADER => array(
                    "API_SECRET:zReJG",
                    "API_KEY: 66eY",
                    "USER_ID:I7155",
					"Content-Type:multipart/form-data"
                ),
            ));
			
			$package_list = curl_exec($curl); 
			return json_decode($package_list);
    }

    public function InvoicePrint($order_id) {
        $data['shipping_address_details'] = $this->getShippingAddressDetails($order_id);
        $data['total_incomplete_order_info'] = $this->getTotalOrderInfo($this->getShippingAddressDetails($order_id)->shoppingcart_id);
        $data['extra_delivery_charge'] = ExtraDeliveryCharge::where('conforder_id', $order_id)->first();
        $data['order_id'] = $order_id;
        return view('admin.invoice_print', $data);
    }
    
    public function Invoice($order_id) {
        $data['shipping_address_details'] = $this->getShippingAddressDetails($order_id);
        $data['extra_delivery_charge'] = ExtraDeliveryCharge::where('conforder_id', $order_id)->first();
        $data['total_incomplete_order_info'] = $this->totalOrderInfo($this->getShippingAddressDetails($order_id)->shoppingcart_id);
        $data['order_id'] = $order_id;
        return view('admin.invoice', $data);
    }
    
    public function totalOrderInfo($shoppingcartid) {
        $total_incomplete_order_info = DB::table('product')
                ->join('shoppinproduct', 'product.product_id', '=', 'shoppinproduct.product_id')
                ->join('shoppingcart', 'shoppingcart.shoppingcart_id', '=', 'shoppinproduct.shoppingcart_id')
                ->join('conforder', 'conforder.shoppingcart_id', '=', 'shoppingcart.shoppingcart_id')
                ->leftJoin('riders','conforder.delivery_by','=','riders.id')
                ->join('registeruser', 'conforder.registeruser_id', '=', 'registeruser.registeruser_id')
                ->select('product.product_name', 'product.product_img_thm','product.product_id', 'product.product_price as original_price','product.product_styleref','shoppinproduct.product_barcode', 'shoppinproduct.shoppinproduct_id', 'shoppinproduct.shoppinproduct_quantity', 'shoppinproduct.prosize_name', 'shoppinproduct.cart_image', 'shoppinproduct.productalbum_name', 'shoppinproduct.Shipping_Charge', 'shoppinproduct.shipping_area', 'shoppinproduct.deliveryMethod', 'shoppinproduct.product_price', 'shoppinproduct.product_id', 'shoppinproduct.productalbum_name','riders.name','registeruser.registeruser_email')
                ->where('shoppinproduct.shoppingcart_id', '=', $shoppingcartid)
                ->get();
        return $total_incomplete_order_info;
    }

    public function UpdateOrderStatus(Request $request) {
       // dd($request);
	    $order_status=$request->ddlstatus;
	    $confoorderid=$request->conforder_id;
	    $get_delivery_date=$request->delivery_date;
		$today=date("Y-m-d");
		
		if($order_status =='Pending_Dispatch'){
			//$this->SendSMS($request);
		 }
 
        $totalamount=$request->total_amount;
        $orderstatusnote=$request->orderstatusnote;
        $data['conforder_status'] = $order_status;
        $data['conforder_statusdetails']=$orderstatusnote;
        $data['updated_at']=Carbon::now();
        $data['delivery_by'] = $request->delivered_by;
        $data['conforder_deliverydate'] = $request->delivery_date;
        $data['pos_entry_date'] = $request->pos_entry_date;
        $data['order_threepldlv'] = $request->ddlthreepldlv;
        $order_status_result = DB::table("conforder")->where('conforder_id', $confoorderid)->update($data);
        DB::table("registeruser")->where('registeruser_id', $request->registeruser_id)->update(['admin_comment' => $request->admin_comment]);
        
        
        $data['conforder'] = DB::table('conforder')
            ->join('cash_expenses', 'conforder.conforder_id', '=', 'cash_expenses.conforder_id')
            ->select('conforder.delivery_by', 'cash_expenses.amount')
            ->where('conforder.conforder_id', $confoorderid)
            ->first();
        $data['delivery_by'] = DB::table('conforder')
            ->select('delivery_by')
            ->where('conforder_id', $confoorderid)
            ->first();
        if ($order_status_result) {
            $shipping_address_details = $this->getShippingAddressDetails($confoorderid);
            $total_incomplete_order_info = $this->getTotalOrderInfo($shipping_address_details->shoppingcart_id);
            if ($order_status == 'Invalidate' || $order_status == 'Returned' || $order_status == 'Cancelled') {
                foreach ($total_incomplete_order_info as $order_info) {
                    $result = $this->getProductInfoByProductSize($order_info->product_id, $order_info->prosize_name, $order_info->productalbum_name);
                    $pro_SizeWiseQty = $result->SizeWiseQty + $order_info->shoppinproduct_quantity;
                    //echo $pro_SizeWiseQty.'==========';exit;
                    
                    $update_qty = $this->update_qty($order_info->product_id, $result->color_name, $result->productsize_size, $pro_SizeWiseQty);
                    if ($update_qty) {
                      //  echo "Success";
                    } else {
                     //   echo "Failed!!";
                    }
                }
            } else {
                
           /* foreach ($total_incomplete_order_info as $order_info) {
                    $result = $this->getProductInfoByProductSize($order_info->product_id, $order_info->prosize_name, $order_info->productalbum_name);
                    $pro_SizeWiseQty = $result->SizeWiseQty - $order_info->shoppinproduct_quantity;
                    $update_qty = $this->update_qty($order_info->product_id, $result->color_name, $result->productsize_size, $pro_SizeWiseQty);
                    if ($update_qty) {
                        echo "Success";
                    } else {
                        echo "Failed!!";
                    }
                } */
            }
        } else {
            
        }
        
        if ($order_status == 'Bkash_Payment_Receive'){
            $e_data=DB::table('conforder')
            ->join('registeruser','conforder.registeruser_id','=','registeruser.registeruser_id')
            ->select('registeruser.registeruser_email','registeruser.registeruser_firstname','registeruser.registeruser_lastname','conforder.conforder_tracknumber')
            ->where('conforder_id',$confoorderid)
            ->first();
            $email_data['registeruser_email']=$e_data->registeruser_email;
            $email_data['registeruser_firstname']=$e_data->registeruser_firstname;
            $email_data['registeruser_lastname']=$e_data->registeruser_lastname;
            $email_data['conforder_tracknumber']=$e_data->conforder_tracknumber;
            Mail::send('emails.bkash_payment', $email_data, function($message) use($email_data) {
            $message->from('admin@pride-limited.com','Pride Limited');
            $message->to($email_data['registeruser_email']);
            $message->bcc('pride.orderlog@gmail.com', 'Pride Limited');
            $message->subject('bKash Payment Comfirmation');
            });
         } elseif($order_status == 'Closed'){
             
             //cash in
             $cash_transation['transaction_type']='cash_in';
             $cash_transation['transfer_to']='online';
             $cash_transation['conforder_id']=$confoorderid;
             $cash_transation['amount']=$totalamount;
             $cash_transation['issued_by']= session('pride_admin_id');
             $cash_transation['created_at'] = Carbon::now();
             $dupli_cash_check=DB::table('cash_transactions')->where('conforder_id',$confoorderid)->first();
             if(!$dupli_cash_check){
             DB::table('cash_transactions')->insert($cash_transation);
             }else{
               $up_cash_transation['amount']=$totalamount;  
               DB::table('cash_transactions')->where('conforder_id',$confoorderid)->update($up_cash_transation);
             }
             //expenses
             $cash_expenses['conforder_id']=$confoorderid;
             if($request->expenses_amount ==''){
                  $cash_expenses['amount']=0;
             }else{
             $cash_expenses['amount']=$request->expenses_amount;
             }
             $cash_expenses['expense_type']='delivery_charge';
             $cash_expenses['issued_by']= session('pride_admin_id');
             $dupli_check=DB::table('cash_expenses')->where('conforder_id',$confoorderid)->first();
            if(!$dupli_check){
                 DB::table('cash_expenses')->insert($cash_expenses);
            }else{
                $up_cash_expenses['amount']=$request->expenses_amount;
                DB::table('cash_expenses')->where('conforder_id',$confoorderid)->update($up_cash_expenses);
            }
         }
         
        $tplorderid=$request->ddlthreepldlv;
        if ($tplorderid == 'Pathao') {
		$eC_data['3pl_name']=$tplorderid;
		$eC_data['conforder_id']=$confoorderid;
		$eC_data['assign_date']=Carbon::now();
		DB::table('3pl_history')->insert($eC_data);
        $re_p_data['delivery_by']=3;
        DB::table('conforder')->where('conforder_id', $confoorderid)->update($re_p_data);
        $get_pathao_check=DB::table('conforder')->select('pathao_check')->where('conforder_id', $confoorderid)->first();
        $pathao_check=$get_pathao_check->pathao_check;
        //echo $pathao_check;exit;
        if($pathao_check!=1){
        $tplorderid=$request->ddlthreepldlv;
        $p_data['order_threepldlv'] = $tplorderid;
        $p_data['pathao_check'] = 1;
        $p_data['delivery_by']=3;
        $tplstatus_result = DB::table('conforder')->where('conforder_id', $confoorderid)->update($p_data);
        $shipping_address_details = $this->getShippingAddressDetails($confoorderid);
       // $data['order_threepldlv'] = $tplorderid;
     //   $tplstatus_result = DB::table('conforder')->where('conforder_id', $confoorderid)->update($data);
        /*    $curl = curl_init();
            $token_postdata = [
                'client_id' => 'a853162dbc0b3d4a1cd0c63fd8f75073',
                'client_secret' => '9d5e877c280325a270291a3da60eb924',
                'username' => 'farwah_tasnim@pride-grp.com',
                'password' => 'asdf7890',
                'scope' => 'create_user_delivery',
                'grant_type' => 'password'
            ];
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.pathao.com/v1/oauth/access_token", //api url call here
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($token_postdata),
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/json"
                ),
            ));

            $token_response = curl_exec($curl);
            $result = json_decode($token_response);
            $access_token = $result->access_token;
            $delivery_postdata = [
                'receiver_name' => $shipping_address_details->Shipping_txtfirstname . ' ' . $shipping_address_details->Shipping_txtlastname,
                'receiver_address' => $shipping_address_details->Shipping_txtaddressname,
                'receiver_number' => $shipping_address_details->Shipping_txtphone,
                'recipient_email' => 'ikshimuluits@gmail.com',
                'cost' => $totalamount,
                'instructions' => $shipping_address_details->conforder_tracknumber,
                'package_description' => 'Medium Packet'
            ];

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.pathao.com/v1/me/deliveries",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($delivery_postdata),
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/json",
                    "Authorization: Bearer " . $access_token
                ),
            ));

            $delivery_response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
			*/
			$err=false;
            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                 return redirect("/pride-admin/order-details/$confoorderid")->with('update', 'Pathao status  updated');
            }
        } else {
            return redirect("/pride-admin/order-details/$confoorderid")->with('update', 'already add Pathao service for this order.');
        }
    } elseif ($tplorderid == 'Sundorbon') {
		$eC_data['3pl_name']=$tplorderid;
		$eC_data['conforder_id']=$confoorderid;
		$eC_data['assign_date']=Carbon::now();
		DB::table('3pl_history')->insert($eC_data);
        $datasundorbon['delivery_by']=4;
        DB::table('conforder')->where('conforder_id', $confoorderid)->update($datasundorbon);
        return redirect("/pride-admin/order-details/$confoorderid")->with('update', 'Sundorbon status updated');
    }elseif($tplorderid == 'eCourier'){
		$eCourier_check=DB::table('3pl_history')->where('conforder_id', $confoorderid)->first();
		//dd($eCourier_check);
        //$eCourier_check=DB::table('conforder')->where('conforder_id', $confoorderid)->first();
        if($eCourier_check){
			return redirect("/pride-admin/order-details/$confoorderid")->with('update', 'Already added to eCourier.');
		}else{
        $eCourier_data['delivery_by']=5;
        $eCourier_data['order_threepldlv'] = $tplorderid;
        DB::table('conforder')->where('conforder_id', $confoorderid)->update($eCourier_data);
		$order_data=DB::table('conforder')->where('conforder_id', $confoorderid)->first();
        $curl = curl_init();
		   $shipping_address_details = $this->getShippingAddressDetails($confoorderid);
		   $track_number=$order_data->conforder_tracknumber;
		   $package_code=$request->package_code;
		   $payment_method=$request->payment_method;
		   $number_of_product=$request->total_loop;
			$eCdelivery_postdata = [
                'parcel' => 'insert',
                'recipient_name' => $shipping_address_details->Shipping_txtfirstname . ' ' . $shipping_address_details->Shipping_txtlastname,
                'recipient_mobile' => $shipping_address_details->Shipping_txtphone,
                'recipient_city' => $shipping_address_details->Shipping_txtphone,
				'recipient_area' => $shipping_address_details->CityName,
                'recipient_address' => $shipping_address_details->Shipping_txtaddressname,
                'package_code' => $package_code,
                'product_price' => $shipping_address_details->shoppingcart_total,
				'payment_method' => $payment_method,
				'product_id' => $track_number,
				'number_of_item' => $number_of_product
            ];
			
			curl_setopt_array($curl, array(
               // CURLOPT_URL => "http://ecourier.com.bd/apiv2/",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 3000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $eCdelivery_postdata,
                CURLOPT_HTTPHEADER => array(
                    "API_SECRET:zReJG",
                    "API_KEY: 66eY",
                    "USER_ID:I7155",
					"Content-Type:multipart/form-data"
                ),
            ));
			$delivery_response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
				// dd($delivery_response);
				$json = json_decode($delivery_response);
				$ecr_number=$json->ID;
				$eC_data['3pl_name']=$tplorderid;
				$eC_data['ecr_number']=$ecr_number;
				$eC_data['conforder_id']=$confoorderid;
				$eC_data['assign_date']=Carbon::now();
				$eC_data['package_code']=$package_code;
				DB::table('3pl_history')->insert($eC_data);
				dd($eC_data);
				return redirect("/pride-admin/order-details/$confoorderid")->with('update', 'eCourier status updated');
			}
		//	echo 'eCourier';
        }	
        
    }else{
        return redirect("/pride-admin/order-details/$confoorderid")->with('update', 'status updated');
     }
    }

    public function UpdateTPLStatus(Request $request) {
        $tplorderid=$request->ddlthreepldlv;
        $data['order_threepldlv'] = $tplorderid;
        $tplstatus_result = DB::table('conforder')->where('conforder_id', $confoorderid)->update($data);
        $shipping_address_details = $this->getShippingAddressDetails($confoorderid);
        $data['order_threepldlv'] = $tplorderid;
        $tplstatus_result = DB::table('conforder')->where('conforder_id', $confoorderid)->update($data);

        if ($tplorderid == 'Pathao') {
            $curl = curl_init();
            $token_postdata = [
                'client_id' => 'a853162dbc0b3d4a1cd0c63fd8f75073',
                'client_secret' => '9d5e877c280325a270291a3da60eb924',
                'username' => 'farwah_tasnim@pride-grp.com',
                'password' => 'asdf7890',
                'scope' => 'create_user_delivery',
                'grant_type' => 'password'
            ];
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.pathao.com/v1/oauth/access_token", //api url call here
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($token_postdata),
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/json"
                ),
            ));

            $token_response = curl_exec($curl);
            $result = json_decode($token_response);
            $access_token = $result->access_token;
            $delivery_postdata = [
                'receiver_name' => $shipping_address_details->Shipping_txtfirstname . ' ' . $shipping_address_details->Shipping_txtlastname,
                'receiver_address' => $shipping_address_details->Shipping_txtaddressname,
                'receiver_number' => $shipping_address_details->Shipping_txtphone,
                'recipient_email' => 'ikshimuluits@gmail.com',
                'cost' => $totalamount,
                'instructions' => $shipping_address_details->conforder_tracknumber,
                'package_description' => 'Medium Packet'
            ];

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.pathao.com/v1/me/deliveries",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($delivery_postdata),
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/json",
                    "Authorization: Bearer " . $access_token
                ),
            ));

            $delivery_response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                
            }
        } else {
            echo 0;
        }
    }

    public function CanceIndivisuallQty($product_id, $ProColor, $ProSize, $ProQty, $shoppinproduct_id) {
        $get_shoppingcart_id=DB::table('shoppinproduct')->where('shoppinproduct_id', $shoppinproduct_id)->first();
        $shopping_cart_id=$get_shoppingcart_id->shoppingcart_id;
        $dmselect=$get_shoppingcart_id->deliveryMethod;
        $data['shoppinproduct_quantity'] = 0;
        $update_result = DB::table("shoppinproduct")->where('shoppinproduct_id', $shoppinproduct_id)->where('productalbum_name', $ProColor)->where('prosize_name', $ProSize)->update($data);
        if ($update_result) {
            
             $subtotal_sql = DB::select(DB::raw("select SUM(product_price*shoppinproduct_quantity)as total from shoppinproduct where shoppingcart_id='$shopping_cart_id' and shoppinproduct_quantity!=0"));
              foreach ($subtotal_sql as $cart_update_total) {
                         $subtotal = $cart_update_total->total;
             }
             
            $product_info = DB::table('productsize')
                    ->select('productsize.SizeWiseQty')
                    ->where('productsize.product_id', '=', $product_id)
                    ->where('productsize.productsize_size', '=', $ProSize)
                    ->where('productsize.color_name', '=', $ProColor)
                    ->first();

            $data2['SizeWiseQty'] = $ProQty + $product_info->SizeWiseQty;
            $update_Prosize_result = DB::table("productsize")->where('product_id', $product_id)->where('color_name', $ProColor)->where('productsize_size', $ProSize)->update($data2);
            
            if($subtotal < 3000){
                if($dmselect=='cs'){
                   $shiping_charge=70; 
                }elseif($dmselect=='bk'){
                    $shiping_charge=150; 
                }
                
            }else{
                $shiping_charge=0;
            }
            $update_Shipping_Charge['Shipping_Charge']=$shiping_charge;
            DB::table("shoppinproduct")->where('shoppingcart_id', $shopping_cart_id)->update($update_Shipping_Charge);
            
            $update_shopping_cart = ['shoppingcart_subtotal' => $subtotal, 'shoppingcart_total' => $subtotal+$shiping_charge];
            DB::table("shoppingcart")->where('shoppingcart_id', $shopping_cart_id)->update($update_shopping_cart);
            
            if ($update_Prosize_result) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    /* public function CanceIndivisuallQty($product_id,$ProColor,$ProSize,$ProQty,$shoppinproduct_id){

      //$data['productalbum_name'] = $ProColor;
      //$data['prosize_name'] = $ProSize;

      $data['shoppinproduct_quantity'] = 0;
      $update_result = DB::table("shoppinproduct")->where('shoppinproduct_id',$shoppinproduct_id)->update($data);
      if($update_result){
      echo 1;
      }else{
      echo 0;
      }

      }

      $cancle_shoppinproduct_id=$_POST['shoppinproduct_id'];
      $cancle_product_id=$_POST['product_id'];
      $cancle_color=$_POST['pre_color'];
      $cancle_size=$_POST['pre_size'];
      $quantity=$_POST['qty'];
      $cancle_quantity=0;
      $sqlcancleupdate = "UPDATE shoppinproduct SET productalbum_name='$cancle_color', prosize_name ='$cancle_size',shoppinproduct_quantity='$cancle_quantity' WHERE shoppinproduct_id='$cancle_shoppinproduct_id'";
      $cancleupdate = mysql_query($sqlcancleupdate, $con) or die("Cannot Update: " . mysql_error());
      if($cancleupdate){
      $pre_change_sql="select productsize_size,SizeWiseQty,color_name,product_id  FROM productsize WHERE product_id='$cancle_product_id'
      AND color_name='$cancle_color' AND productsize_size='$cancle_size'";
      $pre_change_res=mysql_query($pre_change_sql,$con);
      $pre_result=mysql_fetch_assoc($pre_change_res);
      $pre_product_id= $pre_result['product_id'];
      $pre_productsize_size = $pre_result['productsize_size'];
      $pre_color_name = $pre_result['color_name'];
      $pre_SizeWiseQty = $pre_result['SizeWiseQty'];
      $pre_new_qty =$pre_SizeWiseQty + $quantity;
      $pre_sql = "UPDATE productsize SET SizeWiseQty='$pre_new_qty' WHERE product_id='$pre_product_id' AND color_name='$pre_color_name' AND productsize_size='$pre_productsize_size'";
      $pre_res = mysql_query($pre_sql,$con) or die("<br>Error: ".mysql_error($con));
      }

     */


    /* public function CanceIndivisuallQty($product_id, $ProColor, $ProSize, $ProQty, $shoppinproduct_id) {

      //$data['productalbum_name'] = $ProColor;
      //$data['prosize_name'] = $ProSize;

      $data['shoppinproduct_quantity'] = 0;
      $update_result = DB::table("shoppinproduct")->where('shoppinproduct_id', $shoppinproduct_id)->update($data);
      if ($update_result) {
      echo 1;
      } else {
      echo 0;
      }
      } */


    /*
      $cancle_shoppinproduct_id=$_POST['shoppinproduct_id'];
      $cancle_product_id=$_POST['product_id'];
      $cancle_color=$_POST['pre_color'];
      $cancle_size=$_POST['pre_size'];
      $quantity=$_POST['qty'];
      $cancle_quantity=0;
      $sqlcancleupdate = "UPDATE shoppinproduct SET productalbum_name='$cancle_color', prosize_name ='$cancle_size',shoppinproduct_quantity='$cancle_quantity' WHERE shoppinproduct_id='$cancle_shoppinproduct_id'";
      $cancleupdate = mysql_query($sqlcancleupdate, $con) or die("Cannot Update: " . mysql_error());
      if($cancleupdate){
      $pre_change_sql="select productsize_size,SizeWiseQty,color_name,product_id  FROM productsize WHERE product_id='$cancle_product_id'
      AND color_name='$cancle_color' AND productsize_size='$cancle_size'";
      $pre_change_res=mysql_query($pre_change_sql,$con);
      $pre_result=mysql_fetch_assoc($pre_change_res);
      $pre_product_id= $pre_result['product_id'];
      $pre_productsize_size = $pre_result['productsize_size'];
      $pre_color_name = $pre_result['color_name'];
      $pre_SizeWiseQty = $pre_result['SizeWiseQty'];
      $pre_new_qty =$pre_SizeWiseQty + $quantity;
      $pre_sql = "UPDATE productsize SET SizeWiseQty='$pre_new_qty' WHERE product_id='$pre_product_id' AND color_name='$pre_color_name' AND productsize_size='$pre_productsize_size'";
      $pre_res = mysql_query($pre_sql,$con) or die("<br>Error: ".mysql_error($con));
      }

     */

    /* Function Description here */

    public function getShippingAddressDetails($order_id) {
        $shipping_address_details = DB::table('conforder')
                ->select('conforder.conforder_id','conforder.employee_status', 'conforder.conforder_tracknumber', 'conforder.conforder_completed', 'conforder.conforder_status', 'conforder.order_threepldlv', 'conforder.conforder_deliverynotes','conforder.conforder_statusdetails', 'conforder.conforder_placed_date','conforder.created_at', 'conforder.conforder_deliverydate', 'conforder.pos_entry_date', 'shoppingcart.shoppingcart_id','shoppingcart.used_promo','shoppingcart.get_offer','shoppingcart.shoppingcart_total', 'ordershipping.Shipping_txtfirstname', 'ordershipping.Shipping_txtlastname', 'ordershipping.Shipping_txtaddressname', 'ordershipping.Shipping_txtcity', 'ordershipping.Shipping_txtzipcode', 'ordershipping.Shipping_txtphone', 'shoppingcart.shoppingcart_id', 'ordershipping.ordershipping_id', 'ordershipping.registeruser_id',  't_city.CityId', 't_city.CityName', 'shoppinproduct.shoppinproduct_id','registeruser.admin_comment')
                ->join('shoppingcart', 'conforder.shoppingcart_id', '=', 'shoppingcart.shoppingcart_id')
                ->join('ordershipping', 'conforder.conforder_id', '=', 'ordershipping.conforder_id')
                ->join('shoppinproduct', 'shoppingcart.shoppingcart_id', '=', 'shoppinproduct.shoppingcart_id')
                ->join('t_city', 'shoppinproduct.shipping_area', '=', 't_city.CityId')
                ->leftjoin('registeruser', 'ordershipping.registeruser_id', '=', 'registeruser.registeruser_id')
                ->where('conforder.conforder_id', '=', $order_id)
                ->first();
        return $shipping_address_details;
    }

    public function getTotalOrderInfo($shoppingcartid) {
        $total_incomplete_order_info = DB::table('product')
                ->join('shoppinproduct', 'product.product_id', '=', 'shoppinproduct.product_id')
                ->select('product.product_name', 'product.product_img_thm','product.product_id', 'product.product_price as original_price', 'product.product_styleref', 'shoppinproduct.shoppinproduct_id', 'shoppinproduct.shoppinproduct_quantity', 'shoppinproduct.prosize_name', 'shoppinproduct.cart_image', 'shoppinproduct.productalbum_name', 'shoppinproduct.Shipping_Charge','shoppinproduct.shipping_area','shoppinproduct.deliveryMethod', 'shoppinproduct.product_price', 'shoppinproduct.product_id', 'shoppinproduct.productalbum_name')
                ->where('shoppinproduct.shoppingcart_id', '=', $shoppingcartid)
                ->get();
        return $total_incomplete_order_info;
    }

    public function getProductInfoByProductSize($product_id, $prosize, $color) {
        $product_info = DB::table('productsize')
                ->select('productsize.color_name', 'productsize.SizeWiseQty', 'productsize.productsize_size')
                ->where('productsize.product_id', '=', $product_id)
                ->where('productsize.productsize_size', '=', $prosize)
                ->where('productsize.color_name', '=', $color)
                ->first();
        return $product_info;
    }

    public function update_qty($product_id, $pro_color, $prosize, $pro_SizeWiseQty) {
        $data['SizeWiseQty'] = $pro_SizeWiseQty;
        $update_result = DB::table('productsize')->where('product_id', $product_id)->where('color_name', $pro_color)->where('productsize_size', $prosize)->update($data);
        if ($update_result) {
            return 1;
        } else {
            return 0;
        }
    }

    //SELECT productalbum_id, productalbum_name FROM productalbum WHERE product_id='$pid' ORDER BY productalbum_id ASC
    public static function GetColorListByProductId($product_id) {
        $color_list = DB::table('productsize')
                ->select('productsize.color_name')
                ->where('productsize.product_id', '=', $product_id)
                ->get();
        return $color_list;
    }

    public static function GetSizeListByProductId($product_id, $productalbum_name) {
        $Size_list = DB::table('productsize')
                ->select('productsize.productsize_size')
                ->where('productsize.product_id', '=', $product_id)
                ->where('productsize.color_name', '=', $productalbum_name)
                ->get();
        return $Size_list;
    }

    public static function GetQtyProductId($product_id, $productalbum_name, $prosize_name) {
        $qtys = DB::table('productsize')
                ->where('productsize.product_id', '=', $product_id)
                ->where('productsize.color_name', '=', $productalbum_name)
                ->where('productsize.productsize_size', '=', $prosize_name)
                ->sum('productsize.SizeWiseQty');
        return $qtys;
    }

    public static function GetShippingDestinationName($city_id) {
		if (is_numeric($city_id)) {
		   $city_name = DB::table('t_city')->where('CityId', $city_id)->first();
          $name=$city_name->CityName;
        return $name;
		} else {
		   return $city_id;
		}
        
    }

    public function ExchangeUpdateSave(Request $request) {
        
        $product_id = $request->product_id;
        $shoppinproduct_id = $request->shoppinproduct_id;
        $shoppinproduct['productalbum_name'] = $request->new_productalbum_name;
        $shoppinproduct['prosize_name'] = $request->new_prosize_name;
        $shoppinproduct['shoppinproduct_quantity'] = $request->new_shoppinproduct_quantity;
        //UPDATE shoppinproduct SET productalbum_name='$color', prosize_name ='$size',shoppinproduct_quantity='$quantity' WHERE shoppinproduct_id='$exchange_shoppinproduct_id'
        $exchange = DB::table('shoppinproduct')->where('shoppinproduct_id', $shoppinproduct_id)->update($shoppinproduct);
        if ($exchange) {
            //select productsize_size,SizeWiseQty,color_name,product_id  FROM productsize WHERE product_id='$product_id_exchange'
            //  AND color_name='$precolor' AND productsize_size='$presize'
            $productalbum_name = $request->productalbum_name;
            $productsize_size = $request->prosize_name;
            $pre_change = DB::table('productsize')
                    ->where('product_id', $product_id)
                    ->where('color_name', $productalbum_name)
                    ->where('productsize_size', $productsize_size)
                    ->first();
                   
            $shoppinproduct_quantity = $request->shoppinproduct_quantity;
            $product_id = $pre_change->product_id;
            $pre_productsize_size = $pre_change->productsize_size;
            $pre_color_name = $pre_change->color_name;
            $pre_SizeWiseQty = $pre_change->SizeWiseQty;
            $request->shoppinproduct_quantity.' '.$pre_SizeWiseQty;
            $update_qty = $pre_SizeWiseQty + $shoppinproduct_quantity;

            $pre_change_update_data['SizeWiseQty'] = $update_qty;
            //UPDATE productsize SET SizeWiseQty='$pre_new_qty' WHERE product_id='$pre_product_id' AND color_name='$pre_color_name' AND productsize_size='$pre_productsize_size'
            $pre_change_update = DB::table('productsize')
                    ->where('product_id', $product_id)
                    ->where('color_name', $pre_color_name)
                    ->where('productsize_size', $pre_productsize_size)
                    ->update($pre_change_update_data);
            if ($pre_change_update) {
                $new_productalbum_name= $request->new_productalbum_name;
                $new_productsize_size = $request->new_prosize_name;
                $new_change = DB::table('productsize')
                        ->where('product_id', $product_id)
                        ->where('color_name', $new_productalbum_name)
                        ->where('productsize_size', $new_productsize_size)
                        ->first();
                $new_shoppinproduct_quantity = $request->new_shoppinproduct_quantity;
                $product_id = $new_change->product_id;
                $new_productsize_size = $new_change->productsize_size;
                $new_color_name = $new_change->color_name;
                $new_SizeWiseQty = $new_change->SizeWiseQty;
                $new_update_qty = $new_SizeWiseQty - $new_shoppinproduct_quantity;
                $new_change_update_data['SizeWiseQty'] = $new_update_qty;
                $new_change_update = DB::table('productsize')
                        ->where('product_id', $product_id)
                        ->where('color_name', $new_color_name)
                        ->where('productsize_size', $new_productsize_size)
                        ->update($new_change_update_data);
                if ($new_change_update) {
                    return redirect()->back()->with('update', 'Exchange success');
                } else {
                    return redirect()->back()->with('update', 'Exchange not success');
                }
            }
            
        }
         return redirect()->back()->with('update', 'Exchange not success');
    }
    
    public function updateAddress(Request $request) {
        DB::table('ordershipping')->where('ordershipping_id', $request->ordershipping_id)->update([
            'Shipping_txtfirstname' => $request->firstname,
            'Shipping_txtlastname' => $request->lastname,
            'Shipping_txtaddressname' => $request->address,
            'Shipping_txtcity' => $request->city,
            'Shipping_txtzipcode' => $request->zip,
            'Shipping_txtphone' => $request->phone
        ]);
        /*
        DB::table('t_city')
                ->where('CityId', $request->destination_id)
                ->update(['CityName' => $request->destination]);
        */
    }
    
   public function OrderFiltering(){
       $order_status = Input::get('order_status');
       if($order_status !=''){
           $all_order_info = DB::table('conforder')
                 ->join('ordershipping', 'conforder.conforder_id', '=', 'ordershipping.conforder_id')
                 ->leftJoin('riders','conforder.delivery_by','=','riders.id')
                ->select('conforder.conforder_id', 'conforder.conforder_status', 'conforder.conforder_lastupdte','conforder.conforder_placed_date','conforder.delivery_by','conforder.conforder_deliverydate', 'conforder.conforder_tracknumber', 'ordershipping.Shipping_txtfirstname', 'ordershipping.Shipping_txtlastname','riders.name')
                ->orderBy('conforder.conforder_deliverydate', 'ASC')
                ->where('conforder.conforder_status',$order_status)
                ->get();
        $data['all_order_info'] = $all_order_info;
        $data['filter_by']=$order_status;
       } else {
            $all_order_info = DB::table('conforder')
                 ->join('ordershipping', 'conforder.conforder_id', '=', 'ordershipping.conforder_id')
                 ->leftJoin('riders','conforder.delivery_by','=','riders.id')
                ->select('conforder.conforder_id', 'conforder.conforder_status', 'conforder.conforder_lastupdte','conforder.conforder_placed_date','conforder.delivery_by','conforder.conforder_deliverydate', 'conforder.conforder_tracknumber', 'ordershipping.Shipping_txtfirstname', 'ordershipping.Shipping_txtlastname','riders.name')
                ->orderBy('conforder.conforder_id', 'DESC')
                ->where('conforder.conforder_status','Order_Verification_Pending')
                ->get();
           $data['all_order_info'] = $all_order_info;
           $data['filter_by']='Order_Verification_Pending';
       }
       return view('admin.order_management.order_filter',$data);
   }
   
   public function updateCityInProductDetails(Request $request) {
       
        DB::table('shoppinproduct')
                ->where('shoppingcart_id', $request->shoppingcart_id)
                ->update([
                    'shipping_area' => $request->route_city
                ]);
        return $request->shippingcart_id;
   }
   public function orderLog() {
        $data['order_log'] = DB::select(DB::raw("SELECT shoppinproduct.shoppinproduct_id,conforder.conforder_id,conforder.conforder_status,conforder.conforder_placed_date,conforder.conforder_tracknumber,conforder.conforder_deliverydate,ordershipping.Shipping_txtfirstname,ordershipping.Shipping_txtlastname,ordershipping.Shipping_txtaddress1,ordershipping.Shipping_txtaddress2,ordershipping.Shipping_txtphone,product.product_name,product.product_styleref,shoppinproduct.productalbum_name,shoppinproduct.prosize_name,shoppinproduct.shoppinproduct_quantity,shoppinproduct.Shipping_Charge,product.product_price,product.product_pricediscounted,riders.name,shoppinproduct.dmselect
            FROM conforder
            LEFT JOIN ordershipping
            ON conforder.conforder_id=ordershipping.conforder_id
            LEFT JOIN shoppingcart
            ON conforder.shoppingcart_id=shoppingcart.shoppingcart_id
            LEFT JOIN shoppinproduct
            ON shoppingcart.shoppingcart_id=shoppinproduct.shoppingcart_id
            LEFT JOIN product
            ON shoppinproduct.product_id=product.product_id
            LEFT JOIN riders
            ON conforder.delivery_by=riders.id
            WHERE conforder.conforder_status !='Invalidate' AND conforder.conforder_status !='Cancelled' AND conforder.conforder_status !='Order_Verification_Pending'
            GROUP BY shoppinproduct.shoppinproduct_id ORDER BY shoppinproduct.shoppinproduct_id DESC"));
       return view('admin.order_management.order_log',$data);
   }
   
   public function eCourierCancelOrder(){
            $curl = curl_init();
			$cancel_postdata = [
                'parcel' => 'cancel_order',
                'tracking_no' => 'ECR0001809191383',
                'comment' => 'test order',
            ];
			
			curl_setopt_array($curl, array(
                CURLOPT_URL => "http://ecourier.com.bd/apiv2/",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 3000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $cancel_postdata,
                CURLOPT_HTTPHEADER => array(
                    "API_SECRET:zReJG",
                    "API_KEY: 66eY",
                    "USER_ID:I7155",
					"Content-Type:multipart/form-data"
                ),
            ));
			$cancel_response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
				 dd($cancel_response);
			}
   }
   
   public function SendSMS($request){
	 
	$order_status=$request->ddlstatus;
	$confoorderid=$request->conforder_id;
	$get_delivery_date=$request->delivery_date;
	$today=date("Y-m-d");
	$bitly_link = app('bitly')->getUrl('https://pride-limited.com/track-order'); 
	if($order_status =='Pending_Dispatch'){
		//conforder details
		$order_detials = Conforder::find($confoorderid);
		//get guest customer details
		$user_details = Ordershipping::where('conforder_id', $confoorderid)->first();
		if($get_delivery_date !=''){
			$delivery_date=$order_detials->conforder_deliverydate;
			if($delivery_date == NULL){
			   //send sms for delivery date set || new date set
			   $date=strtotime($request->delivery_date);
			   $order_date=date('d M,',$date);
			   //dd($order_date);
				$user_mobile = "88" . $user_details->Shipping_txtphone;
				if($request->ddlthreepldlv == 'Pathao'){
				$sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
				Your order $order_detials->conforder_tracknumber has been dispatched with PATHAO.
				Please be available on your phone to receive any calls from our logistics.";
				}else if($request->ddlthreepldlv == 'eCourier'){
				$sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
				Your order $order_detials->conforder_tracknumber has been dispatched with E-COURIER.
				Track your order here: $bitly_link";
				}else if($request->ddlthreepldlv == 'Sundorbon'){
				$sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
				Your order $order_detials->conforder_tracknumber has been dispatched with Sundorbon.
				Please be available on your phone to receive any calls from our logistics.";
				}else{
				$sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
				Your order $order_detials->conforder_tracknumber is verified and will be delivered on the $order_date, at any time before 8pm BST.
				Please be available on your phone to receive any calls from our logistics.";
				}
				$user = "Pride";
				$pass = "xA33I127";
				$sid = "PrideLtdEng";
				$url = "http://sms.sslwireless.com/pushapi/dynamic/server.php";
				$param = "user=$user&pass=$pass&sms[0][0]=$user_mobile&sms[0][1]=" . urlencode($sms_text) . "&sms[0][2]=1234567890&sid=$sid";
				$crl = curl_init();
				curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, 2);
				curl_setopt($crl, CURLOPT_URL, $url);
				curl_setopt($crl, CURLOPT_HEADER, 0);
				curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($crl, CURLOPT_POST, 1);
				curl_setopt($crl, CURLOPT_POSTFIELDS, $param);
				$response = curl_exec($crl);
				curl_close($crl);
			  //end send sms 
				
			}else if($get_delivery_date == $delivery_date){
				//sms not send || today equal delivery date
				
			}else if($get_delivery_date != $delivery_date && $get_delivery_date > $delivery_date){
				//send sms update delivery time || update date
			   $date=strtotime($request->delivery_date);
			   $order_date=date('d M',$date);
			   //dd($order_date);
				$user_mobile = "88" . $user_details->Shipping_txtphone;
				if($request->ddlthreepldlv == 'Pathao'){
				$sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
				Your order $order_detials->conforder_tracknumber has been dispatched with PATHAO.
				Please be available on your phone to receive any calls from our logistics.";
				}else if($request->ddlthreepldlv == 'eCourier'){
				$sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
				Your order $order_detials->conforder_tracknumber has been dispatched with E-COURIER.
				Please be available on your phone to receive any calls from our logistics.";
				}else if($request->ddlthreepldlv == 'Sundorbon'){
				$sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
				Your order $order_detials->conforder_tracknumber has been dispatched with Sundorbon.
				Please be available on your phone to receive any calls from our logistics.";
				}else{
				$sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
				Your order $order_detials->conforder_tracknumber is verified and will be delivered on the $order_date, at any time before 8pm BST.
				Please be available on your phone to receive any calls from our logistics.";
				}
				$user = "Pride";
				$pass = "xA33I127";
				$sid = "PrideLtdEng";
				$url = "http://sms.sslwireless.com/pushapi/dynamic/server.php";
				$param = "user=$user&pass=$pass&sms[0][0]=$user_mobile&sms[0][1]=" . urlencode($sms_text) . "&sms[0][2]=1234567890&sid=$sid";
				$crl = curl_init();
				curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, 2);
				curl_setopt($crl, CURLOPT_URL, $url);
				curl_setopt($crl, CURLOPT_HEADER, 0);
				curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($crl, CURLOPT_POST, 1);
				curl_setopt($crl, CURLOPT_POSTFIELDS, $param);
				$response = curl_exec($crl);
				curl_close($crl);
			  //end send sms 
			 }
	   }
	 }
   }
   
   

}