<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class AjaxCallController extends Controller {

    public function getSizeByColor($product_id, $color_name) {
        $sizes = DB::table('productsize')
                ->select('productsize_id', 'productsize_size')
                ->where('product_id', $product_id)
                ->where('color_name', $color_name)
                ->get();
        return json_encode($sizes);
    }
    
    public function AddNumber($number,$message, $name) {
        date_default_timezone_set('Asia/Dhaka');
        $data['phnumber_name']=$name;
        $data['phnumber_number']=$number;
        $data['comment_box']=$message;
        $data['pnumber_time']=Carbon::now();
        $result = DB::table('phnumber')->insert($data);
        if ($result) {
            echo 'ok';
        } else {
            
        }
    }

    public function getQuantityByColor($product_id, $product_size, $color_name) {
        $color_name=str_replace("-","/",$color_name);
        $qty = DB::select(DB::raw("SELECT sum(SizeWiseQty) as SizeWiseQty  from productsize where product_id='$product_id'  AND productsize_size='$product_size' AND color_name='$color_name'"));
        return json_encode($qty);
    }
    
    public function GetProductBarcodeBySize($product_id, $product_size, $color_name){
        $color_name=str_replace("-","/",$color_name);
        $get_barcode=DB::table('productsize')->where('product_id',$product_id)->where('productsize_size',$product_size)->where('color_name',$color_name)->first();
        echo $get_barcode->barcode;
    }

    public function ProductFiltering($category_id, $subpro_id) {
        $productlist = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('procat', 'product.procat_id', '=', 'procat.procat_id')
                ->select('product.product_id', 'procat.procat_name', 'product.product_name', 'product.product_price', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                ->where('product.procat_id', $category_id)
                ->whereIn('subprocat_id', [$subpro_id])
                // ->where('subprocat_id', $subpro_id)
                ->where('product_active_deactive', 0)
                ->orderBy('product.product_order', 'ASC')
                ->orderBy('product.product_id', 'desc')
                ->groupBy('product.product_id')
                ->get();
        return json_encode($productlist);
    }

    public function getImageByColor($product_id, $product_color) {
        $sproduct = DB::select(DB::raw("select productalbum.productalbum_id from product inner join productalbum on product.product_id=productalbum.product_id WHERE product.product_id='$product_id' AND productalbum.productalbum_name='$product_color'"));
        foreach ($sproduct as $sproduct) {
            $productalbum_id = $sproduct->productalbum_id;
        }
        $singleproductmultiplepic = DB::select(DB::raw("select productimg.productimg_img_thm,productimg.productimg_img,productimg.productimg_img_medium,productimg.productimg_img_tiny from productimg WHERE productimg.productalbum_id='$productalbum_id'"));
       // dd($singleproductmultiplepic);
        return json_encode($singleproductmultiplepic);
    }
    
    public function geBrowserData() {
        $brosernum = DB::table('browser')
                ->select('Firefox', 'Chrome', 'Opera', 'Safari', 'IE', 'others')
                ->first();

        $output = array();
        $series = array();

        $output[0]['name'] = 'Firefox';
        $output[0]['y'] = $brosernum->Firefox;
        $output[1]['name'] = 'Chrome';
        $output[1]['y'] = $brosernum->Chrome;
        $output[2]['name'] = 'Opera';
        $output[2]['y'] = $brosernum->Opera;

        $output[3]['name'] = 'Safari';
        $output[3]['y'] = $brosernum->Safari;

        $output[4]['name'] = 'IE';
        $output[4]['y'] = $brosernum->IE;

        $output[4]['name'] = 'Others';
        $output[4]['y'] = $brosernum->others;
        return json_encode($output);
    }
    
        public function getmonthlysalesdata($syear){
                $totals = DB::select("SELECT sum(product_price*shoppinproduct_quantity) as monthlytotal from conforder 
                inner join shoppinproduct on conforder.shoppingcart_id=shoppinproduct.shoppingcart_id
                where conforder.conforder_status='Closed' AND conforder.month !=0 AND conforder.year=$syear
                group by conforder.month 
                order by conforder.month asc");
                
               $count = count($totals);
               $ext_mon = 12-$count;
               $i = 1;
               $j = 0;
               $comma = ',';
               $back = ']';
               $data='[';
                foreach($totals as $total){
                       $data.=$total->monthlytotal.$comma;
                }
                
                for($j=1;$j<=$ext_mon;$j++){
                    $zero = 0;
                    if($j==$ext_mon){
                        $data.=$zero.$back;
                    }else{
                        $data.=$zero.$comma;
                    }
                    
                }
                echo json_encode($data);

    }
    
    public function GetProductListByCategory($subpro_id)
	{
		$data['productlist'] = DB::table('product')
                 ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img_medium', 'productimg.productimg_img', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                ->where('product.subprocat_id', $subpro_id)
                // ->where('subprocat_id', $subpro_id)
                ->where('product_active_deactive', 0)
                ->orderBy('product.product_order', 'ASC')
                ->orderBy('product.product_id', 'desc')
                ->groupBy('product.product_id')
                ->get();
		return view('select_option',$data);
      //  return json_encode($productlist);
	}
	
	public function UpdateQuantitySaveByAjax($prosize_id, $qty) {
        $data['SizeWiseQty'] = $qty;
        $update = DB::table('productsize')->where('productsize_id', $prosize_id)->update($data);
        if ($update) {
            echo "Product quantity updated successfully!";
        } else {
            echo "Product quantity not updated.";
        }
    }

}
