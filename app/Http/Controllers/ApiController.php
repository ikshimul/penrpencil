<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Http\Controllers\OrderPreviewController;
use Cart;
use DB;
use App\Registeruser;
use App\Shoppingcart;
use App\Shoppinproduct;
use App\Conforder;
use App\Ordershipping;
use Auth;
use Mail;
use Validator;
use Session;
use Carbon\Carbon;
use App\Services\IPay;
use App\Services\SSLCommerz;
use App\PromoCode;

class ApiController extends OrderPreviewController {
    
    public function search(Request $request) {
        if($request->name) {
            $products = Product::whereHas('category', function($q) use($request) {
                $q->where('procat_name', 'like', "%$request->name%");
            })
            ->get();
            if(count($products) == 0) {
                $products = Product::where('product_name', 'like', "%$request->name%")
                ->orWhere('product_description', 'like', "%$request->name%")
                ->get();
            }
            
            return $products;
        }
        return Product::all();
    }
}

