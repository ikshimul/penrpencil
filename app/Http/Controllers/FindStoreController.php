<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FindStore;
use DB;

class FindStoreController extends Controller
{
	public function ShowroomCityList(){
	$city_list=DB::connection('sqlsrv')
	->table('ShowroomDetails')
	->where('district','!=',null)
	//->groupBy('district')
	->get();
	//showroom_cities
	foreach($city_list as $city){
		$check_duplicate=DB::table('showroom_cities')->where('city',$city->city)->where('district',$city->district)->first();
		if ($check_duplicate === null) {
		$data['district']=$city->district;
		$data['city']=$city->city;
		DB::table('showroom_cities')->insert($data);
	   }
	}
	dd($city_list);
	}
	public function ShowroomStock($district, $barcode){
    $showroom_stock=DB::connection('sqlsrv')
	->table('ShowroomDetails')
	->join('Showroomwiseitemstock', 'ShowroomDetails.SalesPointID', '=', 'Showroomwiseitemstock.salespointid')
	->select('ShowroomDetails.SalesPointName','ShowroomDetails.SPAddress','ShowroomDetails.district','ShowroomDetails.city','ShowroomDetails.OpeningTime','ShowroomDetails.ClosingTime','Showroomwiseitemstock.Barcode','Showroomwiseitemstock.CurrentStock')
	->where('ShowroomDetails.district', $district)
	->where('Showroomwiseitemstock.Barcode',$barcode)
	->where('ShowroomDetails.SalesPointName','!=','UT Online')
	->get();
    return view('showroom_stock', ['showroom_stock'=>$showroom_stock]);
	}
	
	public function ShowroomStockByDesignRef($district,$designref){
	   $product_style=str_replace('_','/',$designref);
		 $showroom_stock=DB::connection('sqlsrv')
		->table('ShowroomDetails')
		->join('Showroomwiseitemstock', 'ShowroomDetails.SalesPointID', '=', 'Showroomwiseitemstock.salespointid')
		->select('ShowroomDetails.SalesPointName','ShowroomDetails.SPAddress','ShowroomDetails.district','ShowroomDetails.city','ShowroomDetails.OpeningTime','ShowroomDetails.ClosingTime','Showroomwiseitemstock.Barcode','Showroomwiseitemstock.CurrentStock')
		->where('ShowroomDetails.district',$district)
		->where('Showroomwiseitemstock.designref',$product_style)
		->where('ShowroomDetails.SalesPointName','!=','UT Online')
		->get();
		return view('showroom_stock', ['showroom_stock'=>$showroom_stock]);
	}
}
