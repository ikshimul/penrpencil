<?php

namespace App\Http\Controllers\cart;

//use Gloudemans\Shoppingcart\Facades\Cart;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use Cart;
use DB;

class CartController extends Controller {

    public static function GetProductsizelist($pro_id, $color_name) {
        $color_album = DB::table('productsize')
                ->select('productsize_size')
                ->where('product_id', $pro_id)
                ->where('color_name', $color_name)
                ->orderby('productsize_id', 'ASC')
                ->get();
        return $color_album;
    }

    public static function GetProductqty($product_id, $product_size, $color_name) {
        $qty = DB::select(DB::raw("SELECT sum(SizeWiseQty) as SizeWiseQty  from productsize where product_id='$product_id'  AND productsize_size='$product_size' AND color_name='$color_name'"));
        return $qty;
    }

    public function index() {
        $cartinfo = Cart::instance('products')->content();
        // dd($cartinfo);
        return view('cart.index');
    }

    public function cartDestroy() {
        Cart::instance('products')->destroy();
        return view('cart.index');
    }

    public function addProduct(Request $request) {
        // dd($request);
//       exit();
        $product_color = $request->productcolor;
        $product_size = $request->productsize;
        $productqty = $request->productqty;
        $productid = $request->productid;
        $productimage = $request->productimage;
        $get_barcode = DB::table('productsize')->where('product_id', $productid)->where('productsize_size', $product_size)->where('color_name', $product_color)->first();
        $get_productqty = $get_barcode->SizeWiseQty;
        if ($get_productqty > 0) {
            $productqty = $request->productqty;

            $product_barcode = $get_barcode->barcode;
            $product = Product::find($productid);
            $product_name = $product->product_name;
            if ($product->product_pricediscounted < 1) {
                $product_price = $product->product_price;
            } else {
                $product_price = $product->discount_product_price;
            }
            Cart::instance('products')->add(['id' => $product->product_id, 'name' => $product->product_name, 'qty' => $productqty, 'price' => $product_price, 'options' => ['color' => $product_color, 'size' => $product_size, 'barcode' => $product_barcode, 'product_image' => $productimage]]);
            //return view('cart.index');
            // return back();
            $request->session()->flash('cart_product_name', $product_name);
            $request->session()->flash('cart_product_image', $productimage);
            $request->session()->flash('cart_product_price', $product_price);
            $request->session()->flash('cart_product_size', $product_size);
            $request->session()->flash('cart_product_barcode', $product_barcode);
            $request->session()->flash('cart_product_color', $product_color);
            $request->session()->flash('stock_out_message', 1);
            $request->session()->flash('view_bag', 'show_view_bag');
            toastr()->success('Your product has been added to the cart !');
        } else {
            $product_barcode = $get_barcode->barcode;
            $product = Product::find($productid);
            $product_name = $product->product_name;
            if ($product->product_pricediscounted < 1) {
                $product_price = $product->product_price;
            } else {
                $product_price = $product->discount_product_price;
            }
            $request->session()->flash('cart_product_name', $product_name);
            $request->session()->flash('cart_product_image', $productimage);
            $request->session()->flash('cart_product_price', $product_price);
            $request->session()->flash('cart_product_size', $product_size);
            $request->session()->flash('cart_product_barcode', $product_barcode);
            $request->session()->flash('cart_product_color', $product_color);
            $request->session()->flash('stock_out_message', $product_size . ' size has sold out. Please select another size.');
            $request->session()->flash('view_bag', 'show_view_bag');
            toastr()->success('stock_out_message', $product_size . ' size has sold out. Please select another size.');
        }
        return redirect()->back()->with('returned', 'true');
    }

    public function cartProductUpdate(Request $request) {
        $rowId = $request->rowid;
        $color = $request->colorname;
        $size = $request->sizename;
        $qty = $request->quantity;
        if ($qty > 0) {
            $image = $request->image_link;
            Cart::instance('products')->update($rowId, ['qty' => $qty, 'options' => ['color' => $color, 'size' => $size, 'product_image' => $image]]);
        } else {
            Cart::instance('products')->remove($rowId);
        }
        return redirect('/shop-cart')->with('update', 'Item update successfully.');
        //  return redirect('cart.index');
    }

    public function orderpreviewUpdate(Request $request) {
        $rowId = $request->rowid;
        $color = $request->colorname;
        $size = $request->sizename;
        $qty = $request->quantity;
        $image = $request->image_link;
        Cart::instance('products')->update($rowId, ['qty' => $qty, 'options' => ['color' => $color, 'size' => $size, 'product_image' => $image]]);
        return redirect('/orderpriview')->with('update', 'Item update successfully.');
    }

    public function cartProductDelete($rowId) {
        Cart::instance('products')->remove($rowId);
        return redirect('/shop-cart')->with('delete', 'Item removed successfully.');
    }

    public function orderpreviewproductDelete($rowId) {
        Cart::instance('products')->remove($rowId);
        return redirect('/checkout')->with('delete', 'Item removed successfully.');
    }

}
