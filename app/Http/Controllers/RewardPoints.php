<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Registeruser;
use App\Registeruserdetails;

class RewardPoints extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    
    public function PointBalance(){
        return view('user.reward.point');
    }
    
    public function PointsHistory(){
        return view('user.reward.points_history');
    }
    
    public function GetPointBalance(){
        $user_id = Auth::user()->id;
        $registeruser_info = new Registeruser();
        $get_register_id = $registeruser_info->GetLoginUserData($user_id);
		$registeruserdetails = new Registeruserdetails();
		$user_details=$registeruserdetails->GetUserDetailsByUserId($get_register_id->registeruser_id);
		$phone=$user_details->registeruser_phone;
		echo 0;
    }
}
