<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Product;

class CampaignController extends Controller
{
    public function Boishak(){
        $productlist = DB::table('product')
            ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
            ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
            ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
            ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
            ->where('spcollection', 1)
            ->where('product_active_deactive', 0)
            ->orderBy('product.sold','DESC')
            ->orderBy('product.product_order', 'ASC')
            ->orderBy('product.product_id', 'desc')
            ->groupBy('product.product_id')
            ->get();
            $data['product_list'] = $productlist;
        return view('boishak_1425_all',$data);
    }
    
    public function BoishakProduct($title = null, $pro_id = null){
           if ($pro_id == null) {
            $productlist = DB::table('product')
                    ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                    ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                    ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                    ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price','product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                    ->where('product_active_deactive', 0)
                    ->where('isSpecial', 7)
                    ->orderBy('product.sold', 'DESC')
                    ->orderBy('product.product_id', 'desc')
                    ->groupBy('product.product_id')
                    ->paginate(21);
        } else {
            if ($pro_id == 17) {
                $productlist = DB::table('product')
                        ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                        ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                        ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                        ->select('product.product_id', 'product.product_price','product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                        ->whereIn('product.procat_id', [17, 18])
                        ->where('isSpecial', 7)
                        ->where('product_active_deactive', 0)
                        ->orderBy('product.sold', 'DESC')
                        ->groupBy('product.product_id')
                        ->paginate(21);
            } elseif ($pro_id == 9) {
                $productlist = DB::table('product')
                        ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                        ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                        ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                        ->select('product.product_id', 'product.product_price','product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                        ->whereIn('product.procat_id', [5, 9, 10, 11, 12, 13, 20, 16, 7])
                        ->where('isSpecial', 7)
                        ->where('product_active_deactive', 0)
                        ->orderBy('product.sold', 'DESC')
                        // ->orderBy('product.product_id', 'desc')
                        ->groupBy('product.product_id')
                        ->paginate(12);
            }elseif ($pro_id == 16) {
                $productlist = DB::table('product')
                        ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                        ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                        ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                        ->select('product.product_id', 'product.product_price','product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                        ->whereIn('product.subprocat_id', [90])
                        ->where('isSpecial', 7)
                        ->where('product_active_deactive', 0)
                        ->orderBy('product.sold', 'DESC')
                        // ->orderBy('product.product_id', 'desc')
                        ->groupBy('product.product_id')
                        ->paginate(12);
            }elseif($pro_id==5){
                 $productlist = DB::table('product')
                    ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                    ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                    ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                    ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price','product.product_code', 'product.product_name', 'productimg.productimg_img',  'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                    ->whereIn('product.procat_id', [5])
                    ->where('isSpecial',7)
                    ->where('product_active_deactive', 0)
                    ->orderBy('product.sold','DESC')
                   // ->orderBy('product.product_id', 'desc')
                    ->groupBy('product.product_id')
                    ->paginate(18);
            } elseif($pro_id==6){
                $productlist = DB::table('product')
                        ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                        ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                        ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                        ->select('product.product_id', 'product.product_price','product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                        ->where('product.procat_id', 6)
                        ->where('isSpecial', 7)
                        ->where('product_active_deactive', 0)
                        ->orderBy('product.product_order', 'ASC')
                        ->orderBy('product.product_id', 'desc')
                        ->groupBy('product.product_id')
                        ->paginate(18);
            } elseif($pro_id==7){
                $productlist = DB::table('product')
                        ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                        ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                        ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                        ->select('product.product_id', 'product.product_price','product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                        ->where('product.subprocat_id', 40)
                        ->where('isSpecial', 7)
                        ->where('product_active_deactive', 0)
                        ->orderBy('product.product_order', 'ASC')
                        ->orderBy('product.product_id', 'desc')
                        ->groupBy('product.product_id')
                        ->paginate(18);
            }else {
                $productlist = DB::table('product')
                        ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                        ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                        ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                        ->select('product.product_id', 'product.product_price','product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                        ->where('product.procat_id', $pro_id)
                        ->where('isSpecial', 7)
                        ->where('product_active_deactive', 0)
                        ->orderBy('product.product_order', 'ASC')
                        ->orderBy('product.product_id', 'desc')
                        ->groupBy('product.product_id')
                        ->paginate(18);
            }
        }
        $data['title'] = $title;
        $data['procat_id'] = '';
        $data['subcategory'] = '';
        $data['product_list'] = $productlist;
        return view('collection.boishak_1426',$data);
    }
    
    public function IndependenceDay($title = null, $pro_id = null) {
        if ($pro_id == null) {
            $productlist = DB::table('product')
                    ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                    ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                    ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                    ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price','product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                    ->where('product_active_deactive', 0)
                    ->where('isSpecial', 6)
                    ->orderBy('product.sold', 'DESC')
                    ->orderBy('product.product_id', 'desc')
                    ->groupBy('product.product_id')
                    ->paginate(21);
        } else {
            if ($pro_id == 17) {
                $productlist = DB::table('product')
                        ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                        ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                        ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                        ->select('product.product_id', 'product.product_price','product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                        ->whereIn('product.procat_id', [17, 18])
                        ->where('isSpecial', 6)
                        ->where('product_active_deactive', 0)
                        ->orderBy('product.sold', 'DESC')
                        ->groupBy('product.product_id')
                        ->paginate(21);
            } elseif ($pro_id == 9) {
                $productlist = DB::table('product')
                        ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                        ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                        ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                        ->select('product.product_id', 'product.product_price','product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                        ->whereIn('product.procat_id', [5, 9, 10, 11, 12, 13, 20, 7])
                        ->where('isSpecial', 6)
                        ->where('product_active_deactive', 0)
                        ->orderBy('product.sold', 'DESC')
                        // ->orderBy('product.product_id', 'desc')
                        ->groupBy('product.product_id')
                        ->paginate(12);
            }elseif($pro_id==5){
                 $productlist = DB::table('product')
                    ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                    ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                    ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                    ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price','product.product_code', 'product.product_name', 'productimg.productimg_img',  'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                    ->whereIn('product.procat_id', [5])
                    ->where('isSpecial',6)
                    ->where('product_active_deactive', 0)
                    ->orderBy('product.sold','DESC')
                   // ->orderBy('product.product_id', 'desc')
                    ->groupBy('product.product_id')
                    ->paginate(18);
            } else {
                $productlist = DB::table('product')
                        ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                        ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                        ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                        ->select('product.product_id', 'product.product_price','product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                        ->where('product.procat_id', $pro_id)
                        ->where('isSpecial', 6)
                        ->where('product_active_deactive', 0)
                        ->orderBy('product.product_order', 'ASC')
                        ->orderBy('product.product_id', 'desc')
                        ->groupBy('product.product_id')
                        ->paginate(18);
            }
        }
        $data['title'] = $title;
        $data['procat_id'] = '';
        $data['subcategory'] = '';
        $data['product_list'] = $productlist;
        return view('collection.independence-day', $data);
    }
    
    public function EidCollection2019($title = null, $pro_id = null){
        $product=new Product();
        if ($pro_id == null) {
            $productlist=$product->EidCollection();
        }else{
           $productlist = $product->EidCollection2019ByCotegory($pro_id); 
        }
        $data['title'] = $title;
        $data['product_list'] = $productlist;
        return view('collection.eid-collection', $data);
    }
    
    public function SummerCollection($title = null, $pro_id = null){
        $product=new Product();
        if ($pro_id == null) {
            $productlist=$product->SummerCollection();
        }else{
           $productlist = $product->SummerCollection2019ByCotegory($pro_id); 
        }
        $data['title'] = $title;
        $data['product_list'] = $productlist;
        return view('collection.summer-collection', $data);
    }
    
    public function CapsuleCollection($title = null, $pro_id = null){
        $product=new Product();
        if ($pro_id == null) {
            $productlist=$product->CapsuleCollection();
        }else{
           $productlist = $product->CapsuleCollection2019ByCotegory($pro_id); 
        }
        $data['title'] = $title;
        $data['product_list'] = $productlist;
        return view('collection.capsule-collection', $data);
    }
    
    
}
