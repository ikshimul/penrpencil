<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

class bKashController extends Controller
{
    public function GetToken(){
		
			$request_token=$this->bkash_Get_Token();
			$idtoken=$request_token['id_token'];
				
			$_SESSION['token']=$idtoken;
			$strJsonFileContents = file_get_contents("storage/app/config.json");
			$array = json_decode($strJsonFileContents, true);

			$array['token']=$idtoken;

			$newJsonString = json_encode($array);
			file_put_contents('storage/app/config.json',$newJsonString);
			echo $idtoken;
			dd($request_token);
	}
	
	public function bkash_Get_Token(){

	$strJsonFileContents = file_get_contents("storage/app/config.json");
	$array = json_decode($strJsonFileContents, true);
	
	$post_token=array(
        'app_key'=>$array["app_key"],                                              
		'app_secret'=>$array["app_secret"]                  
	);	
    
    $url=curl_init($array["tokenURL"]);
	$proxy = $array["proxy"];
	$posttoken=json_encode($post_token);
	$header=array(
		'Content-Type:application/json',
		'password:'.$array["password"],                                                               
        'username:'.$array["username"]                                                           
    );				
    
    curl_setopt($url,CURLOPT_HTTPHEADER, $header);
	curl_setopt($url,CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($url,CURLOPT_RETURNTRANSFER, true);
	curl_setopt($url,CURLOPT_POSTFIELDS, $posttoken);
	curl_setopt($url,CURLOPT_FOLLOWLOCATION, 1);
	//curl_setopt($url, CURLOPT_PROXY, $proxy);
	$resultdata=curl_exec($url);
	curl_close($url);
	return json_decode($resultdata, true);    
}

 public function CreatePayment(){
	$strJsonFileContents = file_get_contents("storage/app/config.json");
	$array = json_decode($strJsonFileContents, true);
	$amount = $_GET['amount'];
	$invoice = "PLORDER#1000-564"; // must be unique
	$intent = "sale";
	$proxy = $array["proxy"];
		$createpaybody=array('amount'=>$amount, 'currency'=>'BDT', 'merchantInvoiceNumber'=>$invoice,'intent'=>$intent);   
		$url = curl_init($array["createURL"]);
       // dd($createpaybody);
		$createpaybodyx = json_encode($createpaybody);

		$header=array(
			'Content-Type:application/json',
			'authorization:'.$array["token"],
			'x-app-key:'.$array["app_key"]
		);

		curl_setopt($url,CURLOPT_HTTPHEADER, $header);
		curl_setopt($url,CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($url,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($url,CURLOPT_POSTFIELDS, $createpaybodyx);
		curl_setopt($url,CURLOPT_FOLLOWLOCATION, 1);
		//curl_setopt($url, CURLOPT_PROXY, $proxy);
		
		$resultdata = curl_exec($url);
		curl_close($url);
		echo $resultdata;
   }
   
   public function ExecutePayment(){
	   $strJsonFileContents = file_get_contents("storage/app/config.json");
		$array = json_decode($strJsonFileContents, true);
		$paymentID = $_GET['paymentID'];
		$proxy = $array["proxy"];

		$url = curl_init($array["executeURL"].$paymentID);

		$header=array(
			'Content-Type:application/json',
			'authorization:'.$array["token"],
			'x-app-key:'.$array["app_key"]              
		);	
			
		curl_setopt($url,CURLOPT_HTTPHEADER, $header);
		curl_setopt($url,CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($url,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($url,CURLOPT_FOLLOWLOCATION, 1);
		//curl_setopt($url, CURLOPT_PROXY, $proxy);

		$resultdatax=curl_exec($url);
		curl_close($url);
		echo $resultdatax;
	  /*  $strJsonFileContents = file_get_contents("storage/app/config.json");
		$array = json_decode($strJsonFileContents, true);
		$paymentID = $_GET['paymentID'];
		$proxy = $array["proxy"];

		$url = curl_init($array["executeURL"].$paymentID);

		$header=array(
			'Content-Type:application/json',
			'authorization:'.$array["token"],
			'x-app-key:'.$array["app_key"]              
		);	
			
		curl_setopt($url,CURLOPT_HTTPHEADER, $header);
		curl_setopt($url,CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($url,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($url,CURLOPT_FOLLOWLOCATION, 1);
		//curl_setopt($url, CURLOPT_PROXY, $proxy);

		$resultdatax=curl_exec($url);
		curl_close($url);
		print_r($resultdatax);
		//echo $resultdatax; 
		
		$strJsonFileContents = file_get_contents("storage/app/config.json");
		$array = json_decode($strJsonFileContents, true);
		$proxy = $array["proxy"];
		$array["queryURL"] = 'https://checkout.pay.bka.sh/v1.2.0-beta/checkout/payment/query/';

		$url = curl_init($array["queryURL"].$paymentID);

		$header=array(
			'Content-Type:application/json',
			'authorization:'.$array["token"],
			'x-app-key:'.$array["app_key"]              
		);	
			
		curl_setopt($url,CURLOPT_HTTPHEADER, $header);
		curl_setopt($url,CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($url,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($url,CURLOPT_FOLLOWLOCATION, 1);
		//curl_setopt($url, CURLOPT_PROXY, $proxy);

		$resultquery=curl_exec($url);
		
		curl_close($url);
		$query= json_decode($resultquery);
		print_r($resultquery);
		//dd($query);
		
		
		//search transaction 
		$strJsonFileContents = file_get_contents("storage/app/config.json");
		$array = json_decode($strJsonFileContents, true);
		$proxy = $array["proxy"];
		$array["searchURL"] = 'https://checkout.pay.bka.sh/v1.2.0-beta/checkout/payment/search/';

		$url = curl_init($array["searchURL"].$paymentID);

		$header=array(
			'Content-Type:application/json',
			'authorization:'.$array["token"],
			'x-app-key:'.$array["app_key"]              
		);	
			
		curl_setopt($url,CURLOPT_HTTPHEADER, $header);
		curl_setopt($url,CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($url,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($url,CURLOPT_FOLLOWLOCATION, 1);
		//curl_setopt($url, CURLOPT_PROXY, $proxy);

		$resultsearch=curl_exec($url);
		
		curl_close($url);
		$search= json_decode($resultsearch);
		print_r($array);
		print_r($resultsearch);
		*/
   }
   
}
