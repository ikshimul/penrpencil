$(function () {
    $("form[name='edit_procat']").validate({
        rules: {
            txtcategoryname: "required"
        },
        // Specify validation error messages
        messages: {
            txtcategoryname: "Please enter your product category name"
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });
    $("form[name='add_subpro']").validate({
        rules: {
            procat_id: "required",
            txtsubcategoryname: "required",
            filename: "required"
        },
        messages: {
            procat_id: "Please select main category",
            txtsubcategoryname: "Please enter sub category",
            filename: "select image"
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    $("form[name='add_product']").validate({
        rules: {
            ddlprocat: "required",
            ddlprosubcat1: "required",
            txtproductname: "required",
            txtprice: "required",
            ddlfilter: "required"
        },
        messages: {
            ddlprocat: "Please select main category",
            ddlprosubcat1: "Please enter sub category",
            txtproductname: "Please enter product name",
            txtprice: "Please enter product price",
            ddlfilter: "Please enter product price filter"
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
});

